<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmastrukturdata1 extends Model
{
    protected $table = 'mmastrukturdata1';

    protected $fillable = [
    	'id',
    	'konsep_data_tl',
    	'konsep_data_t',
    	'konsep_data_verifikasi',
    	'konsep_data_tes_lisan',
    	'konsep_data_tes_tulis',
    	'konsep_data_wawancara',
    	'konsep_data_pihak_tiga',
    	'konsep_data_studi_kasus',
    	// =====================================
    	'alternatif_tl',
    	'alternatif_t',
    	'alternatif_verifikasi',
    	'alternatif_tes_lisan',
    	'alternatif_tes_tulis',
    	'alternatif_wawancara',
    	'alternatif_pihak_tiga',
    	'alternatif_studi_kasus',
    	// =====================================
    ];
}

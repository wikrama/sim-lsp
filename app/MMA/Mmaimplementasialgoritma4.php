<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasialgoritma4 extends Model
{
    protected $table = 'mmaimplementasialgoritma4';

    protected $fillable = [
    	'id',
    	'konsep_penggunaan_prosedur_tl',
    	'konsep_penggunaan_prosedur_t',
    	'konsep_penggunaan_prosedur_verifikasi',
    	'konsep_penggunaan_prosedur_tes_lisan',
    	'konsep_penggunaan_prosedur_tes_tulis',
    	'konsep_penggunaan_prosedur_wawancara',
    	'konsep_penggunaan_prosedur_pihak_tiga',
    	'konsep_penggunaan_prosedur_studi_kasus',
    	// ====================================
    	'prosedur_dapat_digunakan_tl',
    	'prosedur_dapat_digunakan_t',
    	'prosedur_dapat_digunakan_verifikasi',
    	'prosedur_dapat_digunakan_tes_lisan',
    	'prosedur_dapat_digunakan_tes_tulis',
    	'prosedur_dapat_digunakan_wawancara',
    	'prosedur_dapat_digunakan_pihak_tiga',
    	'prosedur_dapat_digunakan_studi_kasus',
    	// ====================================
    	'fungsi_dapat_digunakan_tl',
    	'fungsi_dapat_digunakan_t',
    	'fungsi_dapat_digunakan_verifikasi',
    	'fungsi_dapat_digunakan_tes_lisan',
    	'fungsi_dapat_digunakan_tes_tulis',
    	'fungsi_dapat_digunakan_wawancara',
    	'fungsi_dapat_digunakan_pihak_tiga',
    	'fungsi_dapat_digunakan_studi_kasus',
    	// ====================================
    	'fungsi_perintah_dml_tl',
    	'fungsi_perintah_dml_t',
    	'fungsi_perintah_dml_verifikasi',
    	'fungsi_perintah_dml_tes_lisan',
    	'fungsi_perintah_dml_tes_tulis',
    	'fungsi_perintah_dml_wawancara',
    	'fungsi_perintah_dml_pihak_tiga',
    	'fungsi_perintah_dml_studi_kasus'
    ];
}

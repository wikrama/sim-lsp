<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmalakukan_debug2 extends Model
{
	protected $table = 'mmalakukan_debug2';

	protected $fillable = [
		'id',
		'kode_kompilasi_tl',
		'kode_kompilasi_t',
		'kode_kompilasi_verifikasi',
		'kode_kompilasi_tes_lisan',
		'kode_kompilasi_tes_tulis',
		'kode_kompilasi_wawancara',
		'kode_kompilasi_pihak_tiga',
		'kode_kompilasi_studi_kasus',
		// ====================================
		'kriteria_lulus_tl',
		'kriteria_lulus_t',
		'kriteria_lulus_verifikasi',
		'kriteria_lulus_tes_lisan',
		'kriteria_lulus_tes_tulis',
		'kriteria_lulus_wawancara',
		'kriteria_lulus_pihak_tiga',
		'kriteria_lulus_studi_kasus',
		// ====================================
		'kriteria_eksekusi_tl',
		'kriteria_eksekusi_t',
		'kriteria_eksekusi_verifikasi',
		'kriteria_eksekusi_tes_lisan',
		'kriteria_eksekusi_tes_tulis',
		'kriteria_eksekusi_wawancara',
		'kriteria_eksekusi_pihak_tiga',
		'kriteria_eksekusi_studi_kasus',
		// ====================================
		'kode_kesalahan_tl',
		'kode_kesalahan_t',
		'kode_kesalahan_verifikasi',
		'kode_kesalahan_tes_lisan',
		'kode_kesalahan_tes_tulis',
		'kode_kesalahan_wawancara',
		'kode_kesalahan_pihak_tiga',
		'kode_kesalahan_studi_kasus',
	];
}

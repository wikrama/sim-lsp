<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmakerjaaman2 extends Model
{
    protected $table = 'mmakerjaaman2';

    protected $fillable = [
    	'id',
    	'mencari_tanggapan_tl',
		'mencari_tanggapan_t',
		'mencari_tanggapan_verifikasi',
		'mencari_tanggapan_tes_lisan',
		'mencari_tanggapan_tes_tulis',
		'mencari_tanggapan_wawancara',
		'mencari_tanggapan_pihak_tiga',
		'mencari_tanggapan_studi_kasus',
		// ===========================================
		'memberikan_tanggapan_tl',
		'memberikan_tanggapan_t',
		'memberikan_tanggapan_verifikasi',
		'memberikan_tanggapan_tes_lisan',
		'memberikan_tanggapan_tes_tulis',
		'memberikan_tanggapan_wawancara',
		'memberikan_tanggapan_pihak_tiga',
		'memberikan_tanggapan_studi_kasus',
		//============================================
		'kontribusi_tl',
		'kontribusi_t',
		'kontribusi_verifikasi',
		'kontribusi_tes_lisan',
		'kontribusi_tes_tulis',
		'kontribusi_wawancara',
		'kontribusi_pihak_tiga',
		'kontribusi_studi_kasus',
		// ===========================================
		'cita_cita_tl',
		'cita_cita_t',
		'cita_cita_verifikasi',
		'cita_cita_tes_lisan',
		'cita_cita_tes_tulis',
		'cita_cita_wawancara',
		'cita_cita_pihak_tiga',
		'cita_cita_studi_kasus'
    ];
}

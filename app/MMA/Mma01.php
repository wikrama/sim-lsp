<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mma01 extends Model
{
    protected $table = 'mma01';

    protected $fillable = [
    	'id',
    	'tanggal',
    	'tuk',
    	'nama_asesor',
    	'tujuan_asesmen',
    	'jalur_asesmen',
    	'benchmark_asesmen',
    	'rpl_arrangements',
    	'metode_alat_asesmen',
    	'pengorganisasian_asesmen',
    	'aturan_paket',
    	'persyaratan_khusus',
    	'mekanisme_jaminan'
    ];
}

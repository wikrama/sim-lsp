<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmapengaturansoftware2 extends Model
{
    protected $table = 'mmapengaturansoftware2';

    protected $fillable = [
    	'id',
    	'fitur_dasar_butuh_tl',
    	'fitur_dasar_butuh_verifikasi',
    	'fitur_dasar_butuh_tes_lisan',
    	'fitur_dasar_butuh_wawancara',
    	'fitur_dasar_butuh_pihak_tiga',
    	'fitur_dasar_butuh_studi_kasus',
    	// =======================================
    	'fitur_dasar_tools_tl',
    	'fitur_dasar_tools_verifikasi',
    	'fitur_dasar_tools_tes_lisan',
    	'fitur_dasar_tools_wawancara',
    	'fitur_dasar_tools_pihak_tiga',
    	'fitur_dasar_tools_studi_kasus'
    ];
}

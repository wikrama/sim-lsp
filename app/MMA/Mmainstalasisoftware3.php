<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmainstalasisoftware3 extends Model
{
    protected $table = 'mmainstalasisoftware3';

    protected $fillable = [
    	'id',
    	'script_sederhana_tl',
    	'script_sederhana_verifikasi',
    	'script_sederhana_tes_lisan',
    	'script_sederhana_wawancara',
    	'script_sederhana_pihak_tiga',
    	'script_sederhana_studi_kasus',
    	// ================================
    	'script_jalan_tl',
    	'script_jalan_verifikasi',
    	'script_jalan_tes_lisan',
    	'script_jalan_wawancara',
    	'script_jalan_pihak_tiga',
    	'script_jalan_studi_kasus'
    ];
}

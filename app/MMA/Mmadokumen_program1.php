<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmadokumen_program1 extends Model
{
    protected $table = 'mmadokumen_program1';

    protected $fillable = [
    	'id',
    	'modul_program_tl',
    	'modul_program_t',
    	'modul_program_verifikasi',
    	'modul_program_tes_lisan',
    	'modul_program_tes_tulis',
    	'modul_program_wawancara',
    	'modul_program_pihak_tiga',
    	'modul_program_studi_kasus',
    	// ====================================
    	'paremeter_tl',
    	'paremeter_t',
    	'paremeter_verifikasi',
    	'paremeter_tes_lisan',
    	'paremeter_tes_tulis',
    	'paremeter_wawancara',
    	'paremeter_pihak_tiga',
    	'paremeter_studi_kasus',
    	// ====================================
    	'mengetahui_defisi_algoritma_tl',
    	'mengetahui_defisi_algoritma_t',
    	'mengetahui_defisi_algoritma_verifikasi',
    	'mengetahui_defisi_algoritma_tes_lisan',
    	'mengetahui_defisi_algoritma_tes_tulis',
    	'mengetahui_defisi_algoritma_wawancara',
    	'mengetahui_defisi_algoritma_pihak_tiga',
    	'mengetahui_defisi_algoritma_studi_kasus',
    	// ===================================
    	'memahami_defisi_algoritma_tl',
    	'memahami_defisi_algoritma_t',
    	'memahami_defisi_algoritma_verifikasi',
    	'memahami_defisi_algoritma_tes_lisan',
    	'memahami_defisi_algoritma_tes_tulis',
    	'memahami_defisi_algoritma_wawancara',
    	'memahami_defisi_algoritma_pihak_tiga',
    	'memahami_defisi_algoritma_studi_kasus',
    	// ==================================
    	'menjelaskan_defisi_algoritma_tl',
    	'menjelaskan_defisi_algoritma_t',
    	'menjelaskan_defisi_algoritma_verifikasi',
    	'menjelaskan_defisi_algoritma_tes_lisan',
    	'menjelaskan_defisi_algoritma_tes_tulis',
    	'menjelaskan_defisi_algoritma_wawancara',
    	'menjelaskan_defisi_algoritma_pihak_tiga',
    	'menjelaskan_defisi_algoritma_studi_kasus',
    	// ====================================
    	'komentar_baris_kode_tl',
    	'komentar_baris_kode_t',
    	'komentar_baris_kode_verifikasi',
    	'komentar_baris_kode_tes_lisan',
    	'komentar_baris_kode_tes_tulis',
    	'komentar_baris_kode_wawancara',
    	'komentar_baris_kode_pihak_tiga',
    	'komentar_baris_kode_studi_kasus'
    ];
}

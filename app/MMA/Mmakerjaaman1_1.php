<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmakerjaaman1_1 extends Model
{
    protected $table = 'mmakerjaaman1_1';

    protected $fillable = [
    	'id',
    	'kerja_tl',
    	'kerja_t',
    	'kerja_verifikasi',
    	'kerja_tes_lisan',
    	'kerja_tes_tulis',
    	'kerja_wawancara',
    	'kerja_pihak_tiga',
    	'kerja_studi_kasus',
    	// ==========================================================

    	'rumah_tl',
    	'rumah_t',
    	'rumah_verifikasi',
    	'rumah_tes_lisan',
    	'rumah_tes_tulis',
    	'rumah_wawancara',
    	'rumah_pihak_tiga',
    	'rumah_studi_kasus',
    	// =========================================================

    	'tanggungjawab_tl',
    	'tanggungjawab_t',
    	'tanggungjawab_verifikasi',
    	'tanggungjawab_tes_lisan',
    	'tanggungjawab_tes_tulis',
    	'tanggungjawab_wawancara',
    	'tanggungjawab_pihak_tiga',
    	'tanggungjawab_studi_kasus',
    	// =========================================================

    	'demontrasi_tanggungjawab_tl',
    	'demontrasi_tanggungjawab_t',
    	'demontrasi_tanggungjawab_verifikasi',
    	'demontrasi_tanggungjawab_tes_lisan',
    	'demontrasi_tanggungjawab_tes_tulis',
    	'demontrasi_tanggungjawab_wawancara',
    	'demontrasi_tanggungjawab_pihak_tiga',
    	'demontrasi_tanggungjawab_studi_kasus',
    	// =========================================================

    	'memakai_perlengkapan_tl',
    	'memakai_perlengkapan_t',
    	'memakai_perlengkapan_verifikasi',
    	'memakai_perlengkapan_tes_lisan',
    	'memakai_perlengkapan_tes_tulis',
    	'memakai_perlengkapan_wawancara',
    	'memakai_perlengkapan_pihak_tiga',
    	'memakai_perlengkapan_studi_kasus',
    	// =========================================================

    	'menyimpan_perlengkapan_tl',
    	'menyimpan_perlengkapan_t',
    	'menyimpan_perlengkapan_verifikasi',
    	'menyimpan_perlengkapan_tes_lisan',
    	'menyimpan_perlengkapan_tes_tulis',
    	'menyimpan_perlengkapan_wawancara',
    	'menyimpan_perlengkapan_pihak_tiga',
    	'menyimpan_perlengkapan_studi_kasus',
    	// =========================================================

    ];
}

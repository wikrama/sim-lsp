<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmapengaturansoftware1 extends Model
{
    protected $table = 'mmapengaturansoftware1';

    protected $fillable = [
    	'id',
    	'target_hasil_tl',
    	'target_hasil_verifikasi',
    	'target_hasil_tes_lisan',
    	'target_hasil_wawancara',
    	'target_hasil_pihak_tiga',
    	'target_hasil_studi_kasus',
    	// ======================================
    	'pemograman_konfigurasi_tl',
    	'pemograman_konfigurasi_verifikasi',
    	'pemograman_konfigurasi_tes_lisan',
    	'pemograman_konfigurasi_wawancara',
    	'pemograman_konfigurasi_pihak_tiga',
    	'pemograman_konfigurasi_studi_kasus'
    ];
}

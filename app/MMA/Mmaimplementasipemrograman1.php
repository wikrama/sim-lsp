<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasipemrograman1 extends Model
{
    protected $table = 'mmaimplementasipemrograman1';

    protected $fillable = [
    	'id',
    	'tipe_data_standar_tl',
    	'tipe_data_standar_t',
    	'tipe_data_standar_verifikasi',
    	'tipe_data_standar_tes_lisan',
    	'tipe_data_standar_tes_tulis',
    	'tipe_data_standar_wawancara',
    	'tipe_data_standar_pihak_tiga',
    	'tipe_data_standar_studi_kasus',
    	// ==========================================
    	'syntax_program_kuasai_tl',
    	'syntax_program_kuasai_t',
    	'syntax_program_kuasai_verifikasi',
    	'syntax_program_kuasai_tes_lisan',
    	'syntax_program_kuasai_tes_tulis',
    	'syntax_program_kuasai_wawancara',
    	'syntax_program_kuasai_pihak_tiga',
    	'syntax_program_kuasai_studi_kasus',
    	// ==========================================
    	'struktur_kontrol_program_tl',
    	'struktur_kontrol_program_t',
    	'struktur_kontrol_program_verifikasi',
    	'struktur_kontrol_program_tes_lisan',
    	'struktur_kontrol_program_tes_tulis',
    	'struktur_kontrol_program_wawancara',
    	'struktur_kontrol_program_pihak_tiga',
    	'struktur_kontrol_program_studi_kasus'
    ];
}

<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmakerjaaman1_2 extends Model
{
    protected $table = 'mmakerjaaman1_2';

    protected $fillable = [

    	'id',
    	'semua_perlengkapan_tl',
    	'semua_perlengkapan_t',
    	'semua_perlengkapan_verifikasi',
    	'semua_perlengkapan_tes_lisan',
    	'semua_perlengkapan_tes_tulis',
    	'semua_perlengkapan_wawancara',
    	'semua_perlengkapan_pihak_tiga',
    	'semua_perlengkapan_studi_kasus',
    	// ==================================================
    	'mengenali_tanda_tl',
    	'mengenali_tanda_t',
    	'mengenali_tanda_verifikasi',
    	'mengenali_tanda_tes_lisan',
    	'mengenali_tanda_tes_tulis',
    	'mengenali_tanda_wawancara',
    	'mengenali_tanda_pihak_tiga',
    	'mengenali_tanda_studi_kasus',
    	// =================================================
    	'mengikuti_tanda_tl',
    	'mengikuti_tanda_t',
    	'mengikuti_tanda_verifikasi',
    	'mengikuti_tanda_tes_lisan',
    	'mengikuti_tanda_tes_tulis',
    	'mengikuti_tanda_wawancara',
    	'mengikuti_tanda_pihak_tiga',
    	'mengikuti_tanda_studi_kasus',
    	// =================================================
    	'pedoman_tl',
    	'pedoman_t',
    	'pedoman_verifikasi',
    	'pedoman_tes_lisan',
    	'pedoman_tes_tulis',
    	'pedoman_wawancara',
    	'pedoman_pihak_tiga',
    	'pedoman_studi_kasus',
    	// =================================================
    	'darurat_akrab_tl',
    	'darurat_akrab_t',
    	'darurat_akrab_verifikasi',
    	'darurat_akrab_tes_lisan',
    	'darurat_akrab_tes_tulis',
    	'darurat_akrab_wawancara',
    	'darurat_akrab_pihak_tiga',
    	'darurat_akrab_studi_kasus',
    	// =================================================
    	'darurat_individu_tl',
    	'darurat_individu_t',
    	'darurat_individu_verifikasi',
    	'darurat_individu_tes_lisan',
    	'darurat_individu_tes_tulis',
    	'darurat_individu_wawancara',
    	'darurat_individu_pihak_tiga',
    	'darurat_individu_studi_kasus'
    ];
}

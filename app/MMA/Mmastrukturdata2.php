<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmastrukturdata2 extends Model
{
    protected $table = 'mmastrukturdata2';

    protected $fillable = [
    	'id',
    	'implementasi_tl',
    	'implementasi_t',
    	'implementasi_verifikasi',
    	'implementasi_tes_lisan',
    	'implementasi_tes_tulis',
    	'implementasi_wawancara',
    	'implementasi_pihak_tiga',
    	'implementasi_studi_kasus',
    	// ====================================
    	'algoritma_tl',
    	'algoritma_t',
    	'algoritma_verifikasi',
    	'algoritma_tes_lisan',
    	'algoritma_tes_tulis',
    	'algoritma_wawancara',
    	'algoritma_pihak_tiga',
    	'algoritma_studi_kasus'
    ];
}

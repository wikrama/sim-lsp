<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmalakukan_debug3 extends Model
{
    protected $table = 'mmalakukan_debug3';

    protected $fillable = [
    	'id',
    	'perbaikan_kompilasi_tl',
    	'perbaikan_kompilasi_t',
    	'perbaikan_kompilasi_verifikasi',
    	'perbaikan_kompilasi_tes_lisan',
    	'perbaikan_kompilasi_tes_tulis',
    	'perbaikan_kompilasi_wawancara',
    	'perbaikan_kompilasi_pihak_tiga',
    	'perbaikan_kompilasi_studi_kasus',
    	// ================================
    	'perbaikan_dilakukan_tl',
    	'perbaikan_dilakukan_t',
    	'perbaikan_dilakukan_verifikasi',
    	'perbaikan_dilakukan_tes_lisan',
    	'perbaikan_dilakukan_tes_tulis',
    	'perbaikan_dilakukan_wawancara',
    	'perbaikan_dilakukan_pihak_tiga',
    	'perbaikan_dilakukan_studi_kasus'
    ];
}

<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasialgoritma3 extends Model
{
    protected $table = 'mmaimplementasialgoritma3';

    protected $fillable = [
    	'id',
    	'algoritma_sorting_menjelaskan_tl',
    	'algoritma_sorting_menjelaskan_t',
    	'algoritma_sorting_menjelaskan_verifikasi',
    	'algoritma_sorting_menjelaskan_tes_lisan',
    	'algoritma_sorting_menjelaskan_tes_tulis',
    	'algoritma_sorting_menjelaskan_wawancara',
    	'algoritma_sorting_menjelaskan_pihak_tiga',
    	'algoritma_sorting_menjelaskan_studi_kasus',
    	// =======================================
    	'algoritma_sorting_membuat_tl',
    	'algoritma_sorting_membuat_t',
    	'algoritma_sorting_membuat_verifikasi',
    	'algoritma_sorting_membuat_tes_lisan',
    	'algoritma_sorting_membuat_tes_tulis',
    	'algoritma_sorting_membuat_wawancara',
    	'algoritma_sorting_membuat_pihak_tiga',
    	'algoritma_sorting_membuat_studi_kasus',
    	// =======================================
    	'algoritma_searching_menjelaskan_tl',
    	'algoritma_searching_menjelaskan_t',
    	'algoritma_searching_menjelaskan_verifikasi',
    	'algoritma_searching_menjelaskan_tes_lisan',
    	'algoritma_searching_menjelaskan_tes_tulis',
    	'algoritma_searching_menjelaskan_wawancara',
    	'algoritma_searching_menjelaskan_pihak_tiga',
    	'algoritma_searching_menjelaskan_studi_kasus',
    	// =======================================
    	'algoritma_searching_membuat_tl',
    	'algoritma_searching_membuat_t',
    	'algoritma_searching_membuat_verifikasi',
    	'algoritma_searching_membuat_tes_lisan',
    	'algoritma_searching_membuat_tes_tulis',
    	'algoritma_searching_membuat_wawancara',
    	'algoritma_searching_membuat_pihak_tiga',
    	'algoritma_searching_membuat_studi_kasus'
    ];
}

<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmatugasrutin3 extends Model
{
    protected $table = 'mmatugasrutin3';

    protected $fillable = [
    	'id',
    	'sasaran_rencana_tl',
    	'sasaran_rencana_t',
    	'sasaran_rencana_verifikasi',
    	'sasaran_rencana_tes_lisan',
    	'sasaran_rencana_tes_tulis',
    	'sasaran_rencana_wawancara',
    	'sasaran_rencana_pihak_tiga',
    	'sasaran_rencana_studi_kasus',
    	// =======================================
    	'rencana_perbaiki_tl',
    	'rencana_perbaiki_t',
    	'rencana_perbaiki_verifikasi',
    	'rencana_perbaiki_tes_lisan',
    	'rencana_perbaiki_tes_tulis',
    	'rencana_perbaiki_wawancara',
    	'rencana_perbaiki_pihak_tiga',
    	'rencana_perbaiki_studi_kasus',
    	// ======================================
    ];
}

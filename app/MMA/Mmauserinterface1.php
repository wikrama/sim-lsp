<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmauserinterface1 extends Model
{
    protected $table = 'mmauserinterface1';

    protected $fillable = [
    	'id',
    	'rancangan_user_tl',
    	'rancangan_user_t',
    	'rancangan_user_verifikasi',
    	'rancangan_user_tes_lisan',
    	'rancangan_user_tes_tulis',
    	'rancangan_user_wawancara',
    	'rancangan_user_pihak_tiga',
    	'rancangan_user_studi_kasus',
    	// ======================================
    	'komponen_user_tl',
    	'komponen_user_t',
    	'komponen_user_verifikasi',
    	'komponen_user_tes_lisan',
    	'komponen_user_tes_tulis',
    	'komponen_user_wawancara',
    	'komponen_user_pihak_tiga',
    	'komponen_user_studi_kasus',
    	// ======================================
    	'urutan_akses_tl',
    	'urutan_akses_t',
    	'urutan_akses_verifikasi',
    	'urutan_akses_tes_lisan',
    	'urutan_akses_tes_tulis',
    	'urutan_akses_wawancara',
    	'urutan_akses_pihak_tiga',
    	'urutan_akses_studi_kasus',
    	// ======================================
    	'mock_up_tl',
    	'mock_up_t',
    	'mock_up_verifikasi',
    	'mock_up_tes_lisan',
    	'mock_up_tes_tulis',
    	'mock_up_wawancara',
    	'mock_up_pihak_tiga',
    	'mock_up_studi_kasus'
    ];
}

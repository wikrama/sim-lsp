<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmadokumen_program2 extends Model
{
    protected $table = 'mmadokumen_program2';

    protected $fillable = [
    	'id',
    	'dokumentasi_buku_tl',
    	'dokumentasi_buku_t',
    	'dokumentasi_buku_verifikasi',
    	'dokumentasi_buku_tes_lisan',
    	'dokumentasi_buku_tes_tulis',
    	'dokumentasi_buku_wawancara',
    	'dokumentasi_buku_pihak_tiga',
    	'dokumentasi_buku_studi_kasus',
    	// ============================================
    	'dokumentasi_terap_tl',
    	'dokumentasi_terap_t',
    	'dokumentasi_terap_verifikasi',
    	'dokumentasi_terap_tes_lisan',
    	'dokumentasi_terap_tes_tulis',
    	'dokumentasi_terap_wawancara',
    	'dokumentasi_terap_pihak_tiga',
    	'dokumentasi_terap_studi_kasus',
    	// ===========================================
    	'kegunaan_mengetauhi_modul_tl',
    	'kegunaan_mengetauhi_modul_t',
    	'kegunaan_mengetauhi_modul_verifikasi',
    	'kegunaan_mengetauhi_modul_tes_lisan',
    	'kegunaan_mengetauhi_modul_tes_tulis',
    	'kegunaan_mengetauhi_modul_wawancara',
    	'kegunaan_mengetauhi_modul_pihak_tiga',
    	'kegunaan_mengetauhi_modul_studi_kasus',
    	// ==========================================
    	'kegunaan_memahami_modul_tl',
    	'kegunaan_memahami_modul_t',
    	'kegunaan_memahami_modul_verifikasi',
    	'kegunaan_memahami_modul_tes_lisan',
    	'kegunaan_memahami_modul_tes_tulis',
    	'kegunaan_memahami_modul_wawancara',
    	'kegunaan_memahami_modul_pihak_tiga',
    	'kegunaan_memahami_modul_studi_kasus',
    	// ==========================================    	
    	'kegunaan_menjelaskan_modul_tl',
    	'kegunaan_menjelaskan_modul_t',
    	'kegunaan_menjelaskan_modul_verifikasi',
    	'kegunaan_menjelaskan_modul_tes_lisan',
    	'kegunaan_menjelaskan_modul_tes_tulis',
    	'kegunaan_menjelaskan_modul_wawancara',
    	'kegunaan_menjelaskan_modul_pihak_tiga',
    	'kegunaan_menjelaskan_modul_studi_kasus',
    	// ==========================================
    	'dokumen_revisi_tl',
    	'dokumen_revisi_t',
    	'dokumen_revisi_verifikasi',
    	'dokumen_revisi_tes_lisan',
    	'dokumen_revisi_tes_tulis',
    	'dokumen_revisi_wawancara',
    	'dokumen_revisi_pihak_tiga',
    	'dokumen_revisi_studi_kasus'
    ];
}

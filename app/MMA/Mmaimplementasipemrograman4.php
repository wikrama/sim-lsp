<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasipemrograman4 extends Model
{
    protected $table = 'mmaimplementasipemrograman4';

    protected $fillable = [
    	'id',
    	'dimensi_array_tl',
    	'dimensi_array_t',
    	'dimensi_array_verifikasi',
    	'dimensi_array_tes_lisan',
    	'dimensi_array_tes_tulis',
    	'dimensi_array_wawancara',
    	'dimensi_array_pihak_tiga',
    	'dimensi_array_studi_kasus',
    	// ====================================
    	'tipe_data_array_tl',
    	'tipe_data_array_t',
    	'tipe_data_array_verifikasi',
    	'tipe_data_array_tes_lisan',
    	'tipe_data_array_tes_tulis',
    	'tipe_data_array_wawancara',
    	'tipe_data_array_pihak_tiga',
    	'tipe_data_array_studi_kasus',
    	// ===================================
    	'panjang_array_tl',
    	'panjang_array_t',
    	'panjang_array_verifikasi',
    	'panjang_array_tes_lisan',
    	'panjang_array_tes_tulis',
    	'panjang_array_wawancara',
    	'panjang_array_pihak_tiga',
    	'panjang_array_studi_kasus',
    	// ===================================
    	'pengurutan_array_tl',
    	'pengurutan_array_t',
    	'pengurutan_array_verifikasi',
    	'pengurutan_array_tes_lisan',
    	'pengurutan_array_tes_tulis',
    	'pengurutan_array_wawancara',
    	'pengurutan_array_pihak_tiga',
    	'pengurutan_array_studi_kasus'
    ];
}

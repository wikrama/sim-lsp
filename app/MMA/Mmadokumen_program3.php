<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmadokumen_program3 extends Model
{
    protected $table = 'mmadokumen_program3';

    protected  $fillable = [
    	'id',
    	'dokumentasi_fungsi_tl',
    	'dokumentasi_fungsi_t',
    	'dokumentasi_fungsi_verifikasi',
    	'dokumentasi_fungsi_tes_lisan',
    	'dokumentasi_fungsi_tes_tulis',
    	'dokumentasi_fungsi_wawancara',
    	'dokumentasi_fungsi_pihak_tiga',
    	'dokumentasi_fungsi_studi_kasus',
    	// ========================================
    	'eksepsi_mengetahui_tl',
    	'eksepsi_mengetahui_t',
    	'eksepsi_mengetahui_verifikasi',
    	'eksepsi_mengetahui_tes_lisan',
    	'eksepsi_mengetahui_tes_tulis',
    	'eksepsi_mengetahui_wawancara',
    	'eksepsi_mengetahui_pihak_tiga',
    	'eksepsi_mengetahui_studi_kasus',
    	// =======================================
    	'eksepsi_memahami_tl',
    	'eksepsi_memahami_t',
    	'eksepsi_memahami_verifikasi',
    	'eksepsi_memahami_tes_lisan',
    	'eksepsi_memahami_tes_tulis',
    	'eksepsi_memahami_wawancara',
    	'eksepsi_memahami_pihak_tiga',
    	'eksepsi_memahami_studi_kasus',
    	// ======================================
    	'eksepsi_menjelaskan_tl',
    	'eksepsi_menjelaskan_t',
    	'eksepsi_menjelaskan_verifikasi',
    	'eksepsi_menjelaskan_tes_lisan',
    	'eksepsi_menjelaskan_tes_tulis',
    	'eksepsi_menjelaskan_wawancara',
    	'eksepsi_menjelaskan_pihak_tiga',
    	'eksepsi_menjelaskan_studi_kasus',
    	// ======================================
    	'dokumen_revisi_kode_tl',
    	'dokumen_revisi_kode_t',
    	'dokumen_revisi_kode_verifikasi',
    	'dokumen_revisi_kode_tes_lisan',
    	'dokumen_revisi_kode_tes_tulis',
    	'dokumen_revisi_kode_wawancara',
    	'dokumen_revisi_kode_pihak_tiga',
    	'dokumen_revisi_kode_studi_kasus'
    ];
}

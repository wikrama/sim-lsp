<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmakerjaaman3 extends Model
{
    protected $table = 'mmakerjaaman3';

    protected $fillable = [
    	'id',
    	'mengerti_pandangan_tl',
    	'mengerti_pandangan_t',
    	'mengerti_pandangan_verifikasi',
    	'mengerti_pandangan_tes_lisan',
    	'mengerti_pandangan_tes_tulis',
    	'mengerti_pandangan_wawancara',
    	'mengerti_pandangan_pihak_tiga',
    	'mengerti_pandangan_studi_kasus',
    	// ============================================
    	'menggambarkan_pandangan_tl',
    	'menggambarkan_pandangan_t',
    	'menggambarkan_pandangan_verifikasi',
    	'menggambarkan_pandangan_tes_lisan',
    	'menggambarkan_pandangan_tes_tulis',
    	'menggambarkan_pandangan_wawancara',
    	'menggambarkan_pandangan_pihak_tiga',
    	'menggambarkan_pandangan_studi_kasus'
    ];
}

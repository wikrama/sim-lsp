<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasipemrograman5 extends Model
{
    protected $table = 'mmaimplementasipemrograman5';

    protected $fillable = [
    	'id',
    	'program_menulis_data_tl',
    	'program_menulis_data_t',
    	'program_menulis_data_verifikasi',
    	'program_menulis_data_tes_lisan',
    	'program_menulis_data_tes_tulis',
    	'program_menulis_data_wawancara',
    	'program_menulis_data_pihak_tiga',
    	'program_menulis_data_studi_kasus',
    	// =======================================
    	'program_membaca_data_tl',
    	'program_membaca_data_t',
    	'program_membaca_data_verifikasi',
    	'program_membaca_data_tes_lisan',
    	'program_membaca_data_tes_tulis',
    	'program_membaca_data_wawancara',
    	'program_membaca_data_pihak_tiga',
    	'program_membaca_data_studi_kasus'
    ];
}

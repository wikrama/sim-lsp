<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaaspekkodeetik1 extends Model
{
    protected $table = 'mmaaspekkodeetik1';

    protected $fillable = [
    	'id',
    	'norma_tl',
    	'norma_verifikasi',
    	'norma_tes_lisan',
    	'norma_tes_tulis',
    	'norma_wawancara',
    	'norma_pihak_tiga',
    	'norma_studi_kasus',
    	// =======================================
    	'spek_legal_tl',
    	'spek_legal_verifikasi',
    	'spek_legal_tes_lisan',
    	'spek_legal_tes_tulis',
    	'spek_legal_wawancara',
    	'spek_legal_pihak_tiga',
    	'spek_legal_studi_kasus'
    ];
}

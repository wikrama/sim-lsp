<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmalakukan_debug1 extends Model
{
    protected $table = 'mmalakukan_debug1';

    protected $fillable = [
    	'id',
    	'kode_program_spesifikasi_tl',
    	'kode_program_spesifikasi_t',
    	'kode_program_spesifikasi_verifikasi',
    	'kode_program_spesifikasi_tes_lisan',
    	'kode_program_spesifikasi_tes_tulis',
    	'kode_program_spesifikasi_wawancara',
    	'kode_program_spesifikasi_pihak_tiga',
    	'kode_program_spesifikasi_studi_kasus',
    	// ========================================
    	'debugging_tools_tl',
    	'debugging_tools_t',
    	'debugging_tools_verifikasi',
    	'debugging_tools_tes_lisan',
    	'debugging_tools_tes_tulis',
    	'debugging_tools_wawancara',
    	'debugging_tools_pihak_tiga',
    	'debugging_tools_studi_kasus'
    ];
}

<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmadokumen_program4 extends Model
{
    protected $table = 'mmadokumen_program4';

    protected $fillable = [
    	'id',
    	'tools_generate_tl',
    	'tools_generate_t',
    	'tools_generate_verifikasi',
    	'tools_generate_tes_lisan',
    	'tools_generate_tes_tulis',
    	'tools_generate_wawancara',
    	'tools_generate_pihak_tiga',
    	'tools_generate_studi_kasus',
    	// ============================================
    	'generate_dokumentasi_tl',
    	'generate_dokumentasi_t',
    	'generate_dokumentasi_verifikasi',
    	'generate_dokumentasi_tes_lisan',
    	'generate_dokumentasi_tes_tulis',
    	'generate_dokumentasi_wawancara',
    	'generate_dokumentasi_pihak_tiga',
    	'generate_dokumentasi_studi_kasus'
    ];
}

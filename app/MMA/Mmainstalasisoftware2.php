<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmainstalasisoftware2 extends Model
{
    protected $table = 'mmainstalasisoftware2';

    protected $fillable = [
    	'id',
    	'tools_terinstall_tl',
    	'tools_terinstall_verifikasi',
    	'tools_terinstall_tes_lisan',
    	'tools_terinstall_wawancara',
    	'tools_terinstall_pihak_tiga',
    	'tools_terinstall_studi_kasus',
    	// =========================================
    	'tools_jalan_tl',
    	'tools_jalan_verifikasi',
    	'tools_jalan_tes_lisan',
    	'tools_jalan_wawancara',
    	'tools_jalan_pihak_tiga',
    	'tools_jalan_studi_kasus'
    ];
}

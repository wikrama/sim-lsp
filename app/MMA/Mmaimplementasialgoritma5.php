<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasialgoritma5 extends Model
{
    protected $table = 'mmaimplementasialgoritma5';

    protected $fillable = [
    	'id',
    	'kompleksitas_waktu_tl',
    	'kompleksitas_waktu_t',
    	'kompleksitas_waktu_verifikasi',
    	'kompleksitas_waktu_tes_lisan',
    	'kompleksitas_waktu_tes_tulis',
    	'kompleksitas_waktu_wawancara',
    	'kompleksitas_waktu_pihak_tiga',
    	'kompleksitas_waktu_studi_kasus',
    	// =================================================
    	'kompleksitas_memory_tl',
    	'kompleksitas_memory_t',
    	'kompleksitas_memory_verifikasi',
    	'kompleksitas_memory_tes_lisan',
    	'kompleksitas_memory_tes_tulis',
    	'kompleksitas_memory_wawancara',
    	'kompleksitas_memory_pihak_tiga',
    	'kompleksitas_memory_studi_kasus'
    ];
}

<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmatugasrutin1 extends Model
{
    protected $table = 'mmatugasrutin1';

    protected $fillable = [
    	'id',
    	'intruksi_prosedur_tl',
    	'intruksi_prosedur_t',
    	'intruksi_prosedur_verifikasi',
    	'intruksi_prosedur_tes_lisan',
    	'intruksi_prosedur_tes_tulis',
    	'intruksi_prosedur_wawancara',
    	'intruksi_prosedur_pihak_tiga',
    	'intruksi_prosedur_studi_kasus',
    	// =============================================
    	'spesifikasi_tl',
    	'spesifikasi_t',
    	'spesifikasi_verifikasi',
    	'spesifikasi_tes_lisan',
    	'spesifikasi_tes_tulis',
    	'spesifikasi_wawancara',
    	'spesifikasi_pihak_tiga',
    	'spesifikasi_studi_kasus',
    	// =============================================
    	'hasil_tugas_tl',
    	'hasil_tugas_t',
    	'hasil_tugas_verifikasi',
    	'hasil_tugas_tes_lisan',
    	'hasil_tugas_tes_tulis',
    	'hasil_tugas_wawancara',
    	'hasil_tugas_pihak_tiga',
    	'hasil_tugas_studi_kasus',
    	// =============================================
    	'syarat_tugas_tl',
    	'syarat_tugas_t',
    	'syarat_tugas_verifikasi',
    	'syarat_tugas_tes_lisan',
    	'syarat_tugas_tes_tulis',
    	'syarat_tugas_wawancara',
    	'syarat_tugas_pihak_tiga',
    	'syarat_tugas_studi_kasus'
    ];
}

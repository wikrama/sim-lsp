<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmainstalasisoftware1 extends Model
{
    protected $table = 'mmainstalasisoftware1';

    protected $fillable = [
    	'id',
    	'platform_tl',
    	'platform_t',
    	'platform_verifikasi',
    	'platform_tes_lisan',
    	'platform_wawancara',
    	'platform_pihak_tiga',
    	'platform_studi_kasus',
    	// ==============================================
    	'tools_bahasa_tl',
    	'tools_bahasa_t',
    	'tools_bahasa_verifikasi',
    	'tools_bahasa_tes_lisan',
    	'tools_bahasa_wawancara',
    	'tools_bahasa_pihak_tiga',
    	'tools_bahasa_studi_kasus'
    ];
}

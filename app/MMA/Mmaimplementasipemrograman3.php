<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasipemrograman3 extends Model
{
    protected $table = 'mmaimplementasipemrograman3';

    protected $fillable = [
    	'id',
    	'program_prosedur_tl',
    	'program_prosedur_t',
    	'program_prosedur_verifikasi',
    	'program_prosedur_tes_lisan',
    	'program_prosedur_tes_tulis',
    	'program_prosedur_wawancara',
    	'program_prosedur_pihak_tiga',
    	'program_prosedur_studi_kasus',
    	// ===========================================
    	'program_fungsi_tl',
    	'program_fungsi_t',
    	'program_fungsi_verifikasi',
    	'program_fungsi_tes_lisan',
    	'program_fungsi_tes_tulis',
    	'program_fungsi_wawancara',
    	'program_fungsi_pihak_tiga',
    	'program_fungsi_studi_kasus',
    	// ===========================================
    	'program_fungsi_prosedur_tl',
    	'program_fungsi_prosedur_t',
    	'program_fungsi_prosedur_verifikasi',
    	'program_fungsi_prosedur_tes_lisan',
    	'program_fungsi_prosedur_tes_tulis',
    	'program_fungsi_prosedur_wawancara',
    	'program_fungsi_prosedur_pihak_tiga',
    	'program_fungsi_prosedur_studi_kasus',
    	// ===========================================
    	'keterangan_fungsi_prosedur_tl',
    	'keterangan_fungsi_prosedur_t',
    	'keterangan_fungsi_prosedur_verifikasi',
    	'keterangan_fungsi_prosedur_tes_lisan',
    	'keterangan_fungsi_prosedur_tes_tulis',
    	'keterangan_fungsi_prosedur_wawancara',
    	'keterangan_fungsi_prosedur_pihak_tiga',
    	'keterangan_fungsi_prosedur_studi_kasus'
    ];
}

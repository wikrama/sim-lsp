<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasialgoritma2 extends Model
{
    protected $table = 'mmaimplementasialgoritma2';

    protected $fillable = [
    	'id',
    	'metode_sesuai_tl',
    	'metode_sesuai_t',
    	'metode_sesuai_verifikasi',
    	'metode_sesuai_tes_lisan',
    	'metode_sesuai_tes_tulis',
    	'metode_sesuai_wawancara',
    	'metode_sesuai_pihak_tiga',
    	'metode_sesuai_studi_kasus',
    	// ======================================
    	'komponen_butuh_tl',
    	'komponen_butuh_t',
    	'komponen_butuh_verifikasi',
    	'komponen_butuh_tes_lisan',
    	'komponen_butuh_tes_tulis',
    	'komponen_butuh_wawancara',
    	'komponen_butuh_pihak_tiga',
    	'komponen_butuh_studi_kasus',
    	// ======================================
    	'relasi_antar_komponen_tl',
    	'relasi_antar_komponen_t',
    	'relasi_antar_komponen_verifikasi',
    	'relasi_antar_komponen_tes_lisan',
    	'relasi_antar_komponen_tes_tulis',
    	'relasi_antar_komponen_wawancara',
    	'relasi_antar_komponen_pihak_tiga',
    	'relasi_antar_komponen_studi_kasus',
    	// ======================================
    	'alur_mulai_tl',
    	'alur_mulai_t',
    	'alur_mulai_verifikasi',
    	'alur_mulai_tes_lisan',
    	'alur_mulai_tes_tulis',
    	'alur_mulai_wawancara',
    	'alur_mulai_pihak_tiga',
    	'alur_mulai_studi_kasus'
    ];
}

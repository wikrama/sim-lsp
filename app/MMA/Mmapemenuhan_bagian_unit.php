<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmapemenuhan_bagian_unit extends Model
{
    protected $table = 'mmapemenuhan_bagian_unit';

    protected $fillable = [
    	'id',
    	'batasan_variable',
    	'panduan_asesmen',
    	'wawancara',
    	'email',
    	'rapat',
    	'video',
    	'fokus'
    ];
}

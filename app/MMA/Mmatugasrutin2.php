<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmatugasrutin2 extends Model
{
    protected $table = 'mmatugasrutin2';

    protected $fillable = [
    	'id',
    	'intruksi_spesifikasi_tl',
    	'intruksi_spesifikasi_t',
    	'intruksi_spesifikasi_verifikasi',
    	'intruksi_spesifikasi_tes_lisan',
    	'intruksi_spesifikasi_tes_tulis',
    	'intruksi_spesifikasi_wawancara',
    	'intruksi_spesifikasi_pihak_tiga',
    	'intruksi_spesifikasi_studi_kasus',
    	// ==========================================
    	'rangkaian_tl',
    	'rangkaian_t',
    	'rangkaian_verifikasi',
    	'rangkaian_tes_lisan',
    	'rangkaian_tes_tulis',
    	'rangkaian_wawancara',
    	'rangkaian_pihak_tiga',
    	'rangkaian_studi_kasus',
    	// ==========================================
    	'langkah_hasil_tl',
    	'langkah_hasil_t',
    	'langkah_hasil_verifikasi',
    	'langkah_hasil_tes_lisan',
    	'langkah_hasil_tes_tulis',
    	'langkah_hasil_wawancara',
    	'langkah_hasil_pihak_tiga',
    	'langkah_hasil_studi_kasus'
    ];
}

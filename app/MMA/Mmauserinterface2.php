<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmauserinterface2 extends Model
{
    protected $table = 'mmauserinterface2';

    protected $fillable = [
    	'id',
    	'menu_program_tl',
    	'menu_program_t',
    	'menu_program_verifikasi',
    	'menu_program_tes_lisan',
    	'menu_program_tes_tulis',
    	'menu_program_wawancara',
    	'menu_program_pihak_tiga',
    	'menu_program_studi_kasus',
    	// ===============================================
    	'penempatan_user_tl',
    	'penempatan_user_t',
    	'penempatan_user_verifikasi',
    	'penempatan_user_tes_lisan',
    	'penempatan_user_tes_tulis',
    	'penempatan_user_wawancara',
    	'penempatan_user_pihak_tiga',
    	'penempatan_user_studi_kasus',
    	// ===============================================
    	'aktif_pasif_tl',
    	'aktif_pasif_t',
    	'aktif_pasif_verifikasi',
    	'aktif_pasif_tes_lisan',
    	'aktif_pasif_tes_tulis',
    	'aktif_pasif_wawancara',
    	'aktif_pasif_pihak_tiga',
    	'aktif_pasif_studi_kasus',
    	// ==============================================
    	'bentuk_style_tl',
    	'bentuk_style_t',
    	'bentuk_style_verifikasi',
    	'bentuk_style_tes_lisan',
    	'bentuk_style_tes_tulis',
    	'bentuk_style_wawancara',
    	'bentuk_style_pihak_tiga',
    	'bentuk_style_studi_kasus',
    	// =============================================
    	'penerapan_simulasi_tl',
    	'penerapan_simulasi_t',
    	'penerapan_simulasi_verifikasi',
    	'penerapan_simulasi_tes_lisan',
    	'penerapan_simulasi_tes_tulis',
    	'penerapan_simulasi_wawancara',
    	'penerapan_simulasi_pihak_tiga',
    	'penerapan_simulasi_studi_kasus'
    ];
}

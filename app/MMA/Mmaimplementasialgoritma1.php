<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasialgoritma1 extends Model
{
    protected $table = 'mmaimplementasialgoritma1';

    protected $fillable = [
    	'id',
    	'tipe_data_jelas_tl',
    	'tipe_data_jelas_t',
    	'tipe_data_jelas_verifikasi',
    	'tipe_data_jelas_tes_lisan',
    	'tipe_data_jelas_tes_tulis',
    	'tipe_data_jelas_wawancara',
    	'tipe_data_jelas_pihak_tiga',
    	'tipe_data_jelas_studi_kasus',
    	// ========================================
    	'variable_jelas_tl',
    	'variable_jelas_t',
    	'variable_jelas_verifikasi',
    	'variable_jelas_tes_lisan',
    	'variable_jelas_tes_tulis',
    	'variable_jelas_wawancara',
    	'variable_jelas_pihak_tiga',
    	'variable_jelas_studi_kasus',
    	// =========================================
    	'konstanta_mengetahui_tl',
    	'konstanta_mengetahui_t',
    	'konstanta_mengetahui_verifikasi',
    	'konstanta_mengetahui_tes_lisan',
    	'konstanta_mengetahui_tes_tulis',
    	'konstanta_mengetahui_wawancara',
    	'konstanta_mengetahui_pihak_tiga',
    	'konstanta_mengetahui_studi_kasus',
    	// ========================================
    	'konstanta_memahami_tl',    	
    	'konstanta_memahami_t',    	
    	'konstanta_memahami_verifikasi',    	
    	'konstanta_memahami_tes_lisan',    	
    	'konstanta_memahami_tes_tulis',    	
    	'konstanta_memahami_wawancara',    	
    	'konstanta_memahami_pihak_tiga',    	
    	'konstanta_memahami_studi_kasus',
    	// ========================================
    	'konstanta_menjelaskan_tl',
    	'konstanta_menjelaskan_t',
    	'konstanta_menjelaskan_verifikasi',
    	'konstanta_menjelaskan_tes_lisan',
    	'konstanta_menjelaskan_tes_tulis',
    	'konstanta_menjelaskan_wawancara',
    	'konstanta_menjelaskan_pihak_tiga',
    	'konstanta_menjelaskan_studi_kasus'

    ];
}

<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaaspekkodeetik2 extends Model
{
    protected $table = 'mmaaspekkodeetik2';

    protected $fillable = [
    	'id',
    	'copyright_piranti_tl',
    	'copyright_piranti_verifikasi',
    	'copyright_piranti_tes_lisan',
    	'copyright_piranti_tes_tulis',
    	'copyright_piranti_wawancara',
    	'copyright_piranti_pihak_tiga',
    	'copyright_piranti_studi_kasus',
    	// ===========================================
    	'tukar_file_tl',
    	'tukar_file_verifikasi',
    	'tukar_file_tes_lisan',
    	'tukar_file_tes_tulis',
    	'tukar_file_wawancara',
    	'tukar_file_pihak_tiga',
    	'tukar_file_studi_kasus',
    	// ===========================================
    	'istilah_shareware_tl',
    	'istilah_shareware_verifikasi',
    	'istilah_shareware_tes_lisan',
    	'istilah_shareware_tes_tulis',
    	'istilah_shareware_wawancara',
    	'istilah_shareware_pihak_tiga',
    	'istilah_shareware_studi_kasus'
    ];
}

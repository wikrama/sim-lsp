<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasipemrograman6 extends Model
{
    protected $table = 'mmaimplementasipemrograman6';

    protected $fillable = [
    	'id',
    	'kesalahan_program_koreksi_tl',
    	'kesalahan_program_koreksi_t',
    	'kesalahan_program_koreksi_verifikasi',
    	'kesalahan_program_koreksi_tes_lisan',
    	'kesalahan_program_koreksi_tes_tulis',
    	'kesalahan_program_koreksi_wawancara',
    	'kesalahan_program_koreksi_pihak_tiga',
    	'kesalahan_program_koreksi_studi_kasus',
		//=============================================== 
    	'kesalahan_syntax_tl',
    	'kesalahan_syntax_t',
    	'kesalahan_syntax_verifikasi',
    	'kesalahan_syntax_tes_lisan',
    	'kesalahan_syntax_tes_tulis',
    	'kesalahan_syntax_wawancara',
    	'kesalahan_syntax_pihak_tiga',
    	'kesalahan_syntax_studi_kasus'
    ];
}

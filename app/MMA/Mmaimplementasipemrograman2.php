<?php

namespace App\MMA;

use Illuminate\Database\Eloquent\Model;

class Mmaimplementasipemrograman2 extends Model
{
    protected $table = 'mmaimplementasipemrograman2';

    protected $fillable = [
    	'id',
    	'program_baca_tl',
    	'program_baca_t',
    	'program_baca_verifikasi',
    	'program_baca_tes_lisan',
    	'program_baca_tes_tulis',
    	'program_baca_wawancara',
    	'program_baca_pihak_tiga',
    	'program_baca_studi_kasus',
    	// ======================================================
    	'struktur_kontrol_percabangan_tl',
    	'struktur_kontrol_percabangan_t',
    	'struktur_kontrol_percabangan_verifikasi',
    	'struktur_kontrol_percabangan_tes_lisan',
    	'struktur_kontrol_percabangan_tes_tulis',
    	'struktur_kontrol_percabangan_wawancara',
    	'struktur_kontrol_percabangan_pihak_tiga',
    	'struktur_kontrol_percabangan_studi_kasus'    
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsessmenMandiri11 extends Model
{
    protected $table = 'assesmen_mandiri_11';

    protected $fillable = [
								'id',
								'nama',
								'tgl',
								'ass_nama',
								'no_reg',
								'tgl_ass'
    						]; 
}

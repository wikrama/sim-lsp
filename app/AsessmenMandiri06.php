<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsessmenMandiri06 extends Model
{
    //
            protected $table = 'assesmen_mandiri_06';

    protected $fillable = [
    						'id',
							'1(1',
							'1(1(alesan',
							'1(1(asesi',
							'1(2',
							'1(2(alesan',
							'1(2(asesi',
							'2(1',
							'2(1(alesan',
							'2(1(asesi',
							'2(2',
							'2(2(alesan',
							'2(2(asesi',
							'3(1',
							'3(1(alesan',
							'3(1(asesi',
							'3(2',
							'3(2(alesan',
							'3(2(asesi'
    						];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsessmenMandiri09 extends Model
{
    //
            protected $table = 'assesmen_mandiri_09';

    protected $fillable = [
								'id',
								'1^1',
								'1^1^alesan',
								'1^1^asesi',
								'1^2',
								'1^2^alesan',
								'1^2^asesi',
								'1^3',
								'1^3^alesan',
								'1^3^asesi',
								'2^1',
								'2^1^alesan',
								'2^1^asesi',
								'2^2',
								'2^2^alesan',
								'2^2^asesi',
								'2^3',
								'2^3^alesan',
								'2^3^asesi',
								'2^4',
								'2^4^alesan',
								'2^4^asesi',
								'3^1',
								'3^1^alesan',
								'3^1^asesi',
								'3^2',
								'3^2^alesan',
								'3^2^asesi',
								'4^1',
								'4^1^alesan',
								'4^1^asesi',
								'4^2',
								'4^2^alesan',
								'4^2^asesi',
								'4^3',
								'4^3^alesan',
								'4^3^asesi',
								'5^1',
								'5^1^alesan',
								'5^1^asesi',
								'5^2',
								'5^2^alesan',
								'5^2^asesi'
    						]; 
}

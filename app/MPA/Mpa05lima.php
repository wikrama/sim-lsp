<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05lima extends Model
{
    protected $table = 'mpa05lima';

    protected $fillable = [
    	'id',
    	'pencapaian_identifikasi_rancangan',
    	'penilaian_identifikasi_rancangan',
    	'pencapaian_identifikasi_komponen',
    	'penilaian_identifikasi_komponen',
    	'pencapaian_buat_simulasi',
    	'penilaian_buat_simulasi',
    	'pencapaian_terapkan_menu',
    	'penilaian_terapkan_menu',
    	'pencapaian_atur_penempatan',
    	'penilaian_atur_penempatan',
    	'pencapaian_sesuaikan_setting',
    	'penilaian_sesuaikan_setting',
    	'pencapaian_bentuk_style',
    	'penilaian_bentuk_style',
    	'pencapaian_penerapan_simulasi',
    	'penilaian_penerapan_simulasi'
    ];
}

<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05enam extends Model
{
    protected $table = 'mpa05enam';

    protected $fillable = [
    	'id',
    	'pencapaian_identifikasi_platform',
    	'penilaian_identifikasi_platform',
    	'pencapaian_install_tools',
    	'penilaian_install_tools',
    	'pencapaian_jalankan_tools',
    	'penilaian_jalankan_tools',
    	'pencapaian_buat_script',
    	'penilaian_buat_script',
    	'pencapaian_Jalankan_script',
    	'penilaian_Jalankan_script'
    ];
}

<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05dua extends Model
{
    protected $table = 'mpa05dua';

    protected $fillable = [
    	'id',
    	'pencapaian_intruksi_prosedur',
    	'penilaian_intruksi_prosedur',
    	'pencapaian_spesifikasi_relavan',
    	'penilaian_spesifikasi_relavan',
    	'pencapaian_hasil_tugas',
    	'penilaian_hasil_tugas',
    	'pencapaian_syarat_tugas',
    	'penilaian_syarat_tugas',
    	'pencapaian_kegiatan_individu',
    	'penilaian_kegiatan_individu',
    	'pencapaian_mencantumkan_kegiatan',
    	'penilaian_mencantumkan_kegiatan',
    	'pencapaian_memeriksa_hasil',
    	'penilaian_memeriksa_hasil',
    	'pencapaian_membandingkan_hasil',
    	'penilaian_membandingkan_hasil',
    	'pencapaian_memperbaiki_rencana',
    	'penilaian_memperbaiki_rencana'
    ];
}

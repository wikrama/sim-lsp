<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05sepuluh extends Model
{
    protected $table = 'mpa05sepuluh';

    protected $fillable = [
    	'id',
    	'pencapaian_penyiapan_kode',
    	'penilaian_penyiapan_kode',
    	'pencapaian_penyiapan_debugging',
    	'penilaian_penyiapan_debugging',
    	'pencapaian_mengkompilasi_kode',
    	'penilaian_mengkompilasi_kode',
    	'pencapaian_menganalisis_kriteria_lulus',
    	'penilaian_menganalisis_kriteria_lulus',
    	'pencapaian_menganalisis_kriteria_eksekusi',
    	'penilaian_menganalisis_kriteria_eksekusi',
    	'pencapaian_mencatat_kode',
    	'penilaian_mencatat_kode',
    	'pencapaian_merumuskan_perbaikan',
    	'penilaian_merumuskan_perbaikan',
    	'pencapaian_melakukan_perbaikan',
    	'penilaian_melakukan_perbaikan',
    	'nama_peserta_dua',
    	'nama_assesor_dua',
    	'tgl_uji_dua',
    	'nama_peserta_tiga',
    	'tgl_uji_tiga'
    ];
}

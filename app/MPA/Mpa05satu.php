<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05satu extends Model
{
    protected $table ='mpa05satu';

    protected $fillable = [
    	'id',
    	'nama_peserta_satu',
    	'nama_asesor_satu',
    	'tgl_uji_satu',
    	'pencapaian_kerja_aman',
    	'penilaian_kerja_aman',
    	'pencapaian_kegiatan_rumah',
    	'penilaian_kegiatan_rumah',
    	'pencapaian_demostrasi_tugas',
    	'penilaian_demostrasi_tugas',
    	'pencapaian_pelindung_diri',
    	'penilaian_pelindung_diri',
    	'pencapaian_keselamatan',
    	'penilaian_keselamatan',
    	'pencapaian_tanda',
    	'penilaian_tanda',
    	'pencapaian_pedoman',
    	'penilaian_pedoman',
    	'pencapaian_demostrasi_darurat',
    	'penilaian_demostrasi_darurat',
    	'pencapaian_laporan_bahaya',
    	'penilaian_laporan_bahaya',
    	'pencapaian_demostrasi_personil',
    	'penilaian_demostrasi_personil',
    	'pencapaian_prosedur_kondisi',
    	'penilaian_prosedur_kondisi'   
    ];
}

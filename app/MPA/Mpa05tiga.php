<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05tiga extends Model
{
    protected $table = 'mpa05tiga';

    protected $fillable = [
    	'id',
    	'pencapaian_identifikasi_norma',
    	'penilaian_identifikasi_norma',
    	'pencapaian_spek_legas',
    	'penilaian_spek_legas',
    	'pencapaian_copyright_lunak',
    	'penilaian_copyright_lunak',
    	'pencapaian_bertukar_file',
    	'penilaian_bertukar_file',
    	'pencapaian_shareware',
    	'penilaian_shareware'
    ];
}

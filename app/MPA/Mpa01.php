<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa01 extends Model
{
    protected $table = 'mpa01';

    protected $fillable = [
    	'id',
    	'memenuhi_standar',
    	'mencerminkan_prinsip',
    	'menggabungkan_prinsip',
    	'memenuhi_aturan',
    	'memberikan_pilihan',
    	'terurut',
    	'mudah',
    	'merefleksikan',
    	'dapat_diperaktekkan',
    	'menggunakan_format',
    	'memperhatikan_bahasa',
    	'memperhatikan_keragaman',
    	'menggunakan_representasi',
    	'menggunakan_media'
    ];
}

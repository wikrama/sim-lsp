<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa03 extends Model
{
    protected $table = 'mpa03';

    protected $fillable = [
        'id',
        'nama_peserta_satu',
        'nama_asesor_satu',
        'tgl_uji_satu',
        'uu_ite',
        'konten_ilegal',
        'copyright',
        'maya_internet',
        'freeware',
        'os_satu',
        'os_dua',
        'ide_satu',
        'ide_dua',
        'smk_bisa',
        'control_panel_xampp',
        'alamat_xampp_satu',
        'alamat_xampp_dua',
        'perintah_sql',
        'nama_peserta_dua',
        'nama_asesor_dua',
        'tgl_uji_dua',
        'asesi_uu_ite',
        'keputusan_uu_ite',
        'asesi_konten_ilegal',
        'keputusan_konten_ilegal',
        'asesi_copyright',
        'keputusan_copyright',
        'asesi_maya_internet',
        'keputusan_maya_internet',
        'asesi_freeware',
        'keputusan_freeware',
        'asesi_os',
        'keputusan_os',
        'asesi_ide',
        'keputusan_ide',
        'asesi_smk_bisa',
        'keputusan_smk_bisa',
        'asesi_control_panel_xampp',
        'keputusan_control_panel_xampp',
        'asesi_alamat_xampp',
        'keputusan_alamat_xampp',
        'asesi_perintah_sql',
        'keputusan_perintah_sql',
        'keputusan',
        'assesor',
        'assesi',
    ];
}

<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa02 extends Model
{
    protected $table = 'mpa02';

    protected $fillable = [
    	'id',
    	'merefleksikan_prinsip',
    	'merefleksikan_aturan',
    	'relavan',
    	'akurat',
    	'mudah',
    	'efektif',
    	'bahasa',
    	'1',
    	'2',
    	'3',
    	'4'
    ];
}

<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05empat extends Model
{
    protected $table = 'mpa05empat';

    protected $fillable = [
    	'id',
    	'pencapaian_konsep_data',
    	'penilaian_konsep_data',
    	'pencapaian_perbandingan_kelebihan',
    	'penilaian_perbandingan_kelebihan',
    	'pencapaian_mengimplementasikan_struktur',
    	'penilaian_mengimplementasikan_struktur',
    	'pencapaian_menyatakan_akses',
    	'penilaian_menyatakan_akses'
    ];
}

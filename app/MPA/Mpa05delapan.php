<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05delapan extends Model
{
    protected $table = 'mpa05delapan';

    protected $fillable = [
    	'id',
    	'pencapaian_tipe_data',
    	'penilaian_tipe_data',
    	'pencapaian_Syntax_program',
    	'penilaian_Syntax_program',
    	'pencapaian_struktur_kontrol_program',
    	'penilaian_struktur_kontrol_program',
    	'pencapaian_program_baca',
    	'penilaian_program_baca',
    	'pencapaian_struktur_kontrol_percabangan',
    	'penilaian_struktur_kontrol_percabangan',
    	'pencapaian_program_prosedur',
    	'penilaian_program_prosedur',
    	'pencapaian_program_fungsi',
    	'penilaian_program_fungsi',
    	'pencapaian_program_prosedur_fungsi',
    	'penilaian_program_prosedur_fungsi',
    	'pencapaian_tipe_data_standar',
    	'penilaian_tipe_data_standar',
    	'pencapaian_dimensi_array',
    	'penilaian_dimensi_array',
    	'pencapaian_tipe_data_array',
    	'penilaian_tipe_data_array',
    	'pencapaian_panjang_array',
    	'penilaian_panjang_array',
    	'pencapaian_pengurutan_array',
    	'penilaian_pengurutan_array',
    	'pencapaian_menulis_data_media',
    	'penilaian_menulis_data_media',
    	'pencapaian_membaca_data_media',
    	'penilaian_membaca_data_media',
    	'pencapaian_kesalahan_koreksi',
    	'penilaian_kesalahan_koreksi',
    	'pencapaian_kesalahan_syntax',
    	'penilaian_kesalahan_syntax'

    ];
}

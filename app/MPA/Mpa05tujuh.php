<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05tujuh extends Model
{
    protected $table = 'mpa05tujuh';

    protected $fillable = [
    	'id',
    	'pencapaian_hasil_konfigurasi',
    	'penilaian_hasil_konfigurasi',
    	'pencapaian_tools_pemograman',
    	'penilaian_tools_pemograman',
    	'pencapaian_fitur_dasar',
    	'penilaian_fitur_dasar',
    	'pencapaian_fitur_dasar_tools',
    	'penilaian_fitur_dasar_tools'
    ];
}

<?php

namespace App\MPA;

use Illuminate\Database\Eloquent\Model;

class Mpa05sembilan extends Model
{
    protected $table = 'mpa05sembilan';

    protected $fillable = [
    	'id',
    	'pencapaian_tipe_data',
    	'penilaian_tipe_data',
    	'pencapaian_variabel',
    	'penilaian_variabel',
    	'pencapaian_konstansta',
    	'penilaian_konstansta',
    	'pencapaian_metode',
    	'penilaian_metode',
    	'pencapaian_komponen',
    	'penilaian_komponen',
    	'pencapaian_relasi',
    	'penilaian_relasi',
    	'pencapaian_alur',
    	'penilaian_alur',
    	'pencapaian_algoritma_sorting',
    	'penilaian_algoritma_sorting',
    	'pencapaian_algoritma_searching',
    	'penilaian_algoritma_searching',
    	'pencapaian_mengidentifikasi_konsep',
    	'penilaian_mengidentifikasi_konsep',
    	'pencapaian_prosedur',
    	'penilaian_prosedur',
    	'pencapaian_fungsi',
    	'penilaian_fungsi',
    	'pencapaian_mengidentifikasi_kompleksitas',
    	'penilaian_mengidentifikasi_kompleksitas',
    ];
}

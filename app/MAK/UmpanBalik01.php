<?php

namespace App\MAK;

use Illuminate\Database\Eloquent\Model;

class UmpanBalik01 extends Model
{
    protected $table = 'umpan_balik_01';

    protected $fillable = [
    						'id',
							'nama_psrt',
							'tim_asesor',
							'tgl',
							'tmpt',
							'1_1',
							'1_1_bl',
							'1_1_btl',
							'1_1_bt',
							'1_2',
							'1_2_bl',
							'1_2_btl',
							'1_2_bt',
							'1_3',
							'1_3_bl',
							'1_3_btl',
							'1_3_bt',
							'1_4',
							'1_4_bl',
							'1_4_btl',
							'1_4_bt',
							'1_5',
							'1_5_bl',
							'1_5_btl',
							'1_5_bt',
							'1_6',
							'1_6_bl',
							'1_6_btl',
							'1_6_bt',
							'1_7',
							'1_7_bl',
							'1_7_btl',
							'1_7_bt',
							'1_8',
							'1_8_bl',
							'1_8_btl',
							'1_8_bt',
							'2_1',
							'2_1_bl',
							'2_1_btl',
							'2_1_bt',
							'3_1',
							'3_1_bl',
							'3_1_btl',
							'3_1_bt',
							'3_2',
							'3_2_bl',
							'3_2_btl',
							'3_2_bt'
    						];
}

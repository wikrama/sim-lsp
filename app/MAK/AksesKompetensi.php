<?php

namespace App\MAK;

use Illuminate\Database\Eloquent\Model;

class AksesKompetensi extends Model
{
    protected $table = 'akses_kompetensi';

    protected $fillable = [
    						'nama_psrt',
							'nama_asesi',
							'waktu',
							'tgl',
							'1_1',
							'1_2',
							'1_3',
							'1_4',
							'2_1',
							'2_2',
							'2_3',
							'2_4',
							'2_5',
							'2_6',
							'3_1',
							'3_2',
							'3_3',
							'3_4',
							'3_5',
							'3_6',
							'4_1',
							'4_2',
							'4_3',
							'4_4',
							'4_5',
							'5_1',
							'5_2',
							'5_3',
							'5_4',
							'5_5',
							'6_1',
							'6_2',
							'6_3'
    					  ];
}

<?php

namespace App\MAK;

use Illuminate\Database\Eloquent\Model;

class PersetujuanAssesmen extends Model
{
    //
            protected $table = 'persetujuan_assesmen';

    protected $fillable = [
    						'id',
							'nama_calon',	
							'nama_ass',	
							'no_skema',	
							'bukti',	
							'tgl',
							'tempat',	
							'tanggal'
    					  ];
}

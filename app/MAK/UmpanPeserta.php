<?php

namespace App\MAK;

use Illuminate\Database\Eloquent\Model;

class UmpanPeserta extends Model
{
    //
    protected $table = 'umpan_peserta';

    protected $fillable = [
    						'id',
    						'pnjls_cukup',
    						'ksmptn_mmpelajari',
    						'ksmptn_diskusi',
    						'menggali_bukti',
    						'jaminan_rahasia',
    						'ksmptn_demontrasi',
    						'pnjlsn_memadai',
    						'umpan_balik',
    						'komunikasi',
    						'tanda_tangan',
    						'catatan'
    						];
}

<?php

namespace App\MAK;

use Illuminate\Database\Eloquent\Model;

class UmpanBalik11 extends Model
{
    protected $table = 'umpan_balik_07';

    protected $fillable = [
    						'id',
						    'umpan_balik',
							'identifikasi',
							'saran_tindak_lanjut',
							'nama_peserta',
							'tgl_peserta',
							'nama_asesor',
							'no_reg',
							'tgl_asesor'
    						];
}

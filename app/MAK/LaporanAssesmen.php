<?php

namespace App\MAK;

use Illuminate\Database\Eloquent\Model;

class LaporanAssesmen extends Model
{
    protected $tablle = 'laporan_assesmens';

    protected $fillable = [

    						'nama_psrt',
							'nama_asesor',
							'tgl',
							'1',
							'2',
							'3',
							'4',
							'5',
							'6',
							'7',
							'8',
							'9',
							'10',
							'11',
							'12',
							'neg_pos',
							'catatan_tolak',
							'saran_perbaikan',
							'kd_file'
						];
}

<?php

namespace App\MAK;

use Illuminate\Database\Eloquent\Model;

class UmpanBalik06 extends Model
{
    protected $table = 'umpan_balik_06';

    protected $fillable = [
    						'id',
							'1*1',
							'1*1*bl',
							'1*1*btl',
							'1*1*bt',
							'1*2',
							'1*2*bl',
							'1*2*btl',
							'1*2*bt',
							'2*1',
							'2*1*bl',
							'2*1*btl',
							'2*1*bt',
							'2*2',
							'2*2*bl',
							'2*2*btl',
							'2*2*bt',
							'3*1',
							'3*1*bl',
							'3*1*btl',
							'3*1*bt',
							'3*2',
							'3*2*bl',
							'3*2*btl',
							'3*2*bt'
    						];
}

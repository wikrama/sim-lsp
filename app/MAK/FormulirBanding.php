<?php

namespace App\MAK;

use Illuminate\Database\Eloquent\Model;

class FormulirBanding extends Model
{
    //
        protected $table = 'formulir_banding';

    protected $fillable = [
    						'id',
							'nama_psrt',	
							'nama_assr',	
							'tgl_ases',	
							'penjelasan_proses',	
							'diskusi_asesor',
							'melibatkan_orng',	
							'no_unit',	
							'judul_unit',	
							'alasan',
							'tanggal'
    					  ];
}

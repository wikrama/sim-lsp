<?php

namespace App\MAK;

use Illuminate\Database\Eloquent\Model;

class UmpanBalik05 extends Model
{
    protected $table = 'umpan_balik_05';

    protected $fillable = [
    						'id',
							'1(1',
							'1(1(bl',
							'1(1(btl',
							'1(1(bt',
							'1(2',
							'1(2(bl',
							'1(2(btl',
							'1(2(bt',
							'1(3',
							'1(3(bl',
							'1(3(btl',
							'1(3(bt',
							'1(4',
							'1(4(bl',
							'1(4(btl',
							'1(4(bt',
							'2(1',
							'2(1(bl',
							'2(1(btl',
							'2(1(bt',
							'2(2',
							'2(2(bl',
							'2(2(btl',
							'2(2(bt',
							'2(3',
							'2(3(bl',
							'2(3(btl',
							'2(3(bt',
							'2(4',
							'2(4(bl',
							'2(4(btl',
							'2(4(bt',
							'2(5',
							'2(5(bl',
							'2(5(btl',
							'2(5(bt'
    						];
}

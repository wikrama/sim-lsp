<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPekerjaan extends Model
{
    protected $table = 'data_pekerjaan';

    protected $fillable = [
    						'id',
							'nama_lembaga',	
							'jabatan',	
							'alamat',	
							'kode_pos',	
							'telp',
							'fax',	
							'email',	
							'tujuan_asesmen',	
							'skema_sertifikasi',
							'cv',
							'rapot',
							'sertifikat_keahlian',
							'kartu_pelajar'
    					  ];
}

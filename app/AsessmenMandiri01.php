<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsessmenMandiri01 extends Model
{
    //
    protected $table = 'assesmen_mandiri_01';

    protected $fillable = [
    						'id',
							'nama_psrt',
							'nama_assesor',
							'tgl',
							'tuk',
							'no_skema',
							'1_1',
							'1_1_alesan',
							'1_1_asesi',
							'1_2',
							'1_2_alesan',
							'1_2_asesi',
							'1_3',
							'1_3_alesan',
							'1_3_asesi',
							'1_4',
							'1_4_alesan',
							'1_4_asesi',
							'1_5',
							'1_5_alesan',
							'1_5_asesi',
							'1_6',
							'1_6_alesan',
							'1_6_asesi',
							'1_7',
							'1_7_alesan',
							'1_7_asesi',
							'1_8',
							'1_8_alesan',
							'1_8_asesi',
							'2_1',
							'2_1_alesan',
							'2_1_asesi',
							'3_1',
							'3_1_alesan',
							'3_1_asesi',
							'3_2',
							'3_2_alesan',
							'3_2_asesi'
    						];
}

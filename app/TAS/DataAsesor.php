<?php

namespace App\TAS;

use Illuminate\Database\Eloquent\Model;

class DataAsesor extends Model
{
    protected $table = 'data_asesor';

    protected $fillable = [
    						'id',
							'nama',
							'no_regis',
							'no_sertifikat',
							'no_blanko',
							'tgl_exp',
							'alamat'
    						];
}

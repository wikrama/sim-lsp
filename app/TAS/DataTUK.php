<?php

namespace App\TAS;

use Illuminate\Database\Eloquent\Model;

class DataTUK extends Model
{
    //
    protected $table = 'data_tuk';

    protected $fillable = [
    						'id',
							'kode',
							'jnis',
							'nama',
							'alamat'
    						];
}

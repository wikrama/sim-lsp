<?php

namespace App\TAS;

use Illuminate\Database\Eloquent\Model;

class DataSkema extends Model
{
    //
    protected $table = 'data_skema';

    protected $fillable = [
    						'id',
							'kode',
							'nama',
							'kategori',
							'bidang',
							'mea',
							'unit'
    						];    
}

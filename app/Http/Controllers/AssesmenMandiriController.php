<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AsessmenMandiri01;
use App\AsessmenMandiri02;
use App\AsessmenMandiri03;
use App\AsessmenMandiri04;
use App\AsessmenMandiri05;
use App\AsessmenMandiri06;
use App\AsessmenMandiri07;
use App\AsessmenMandiri08;
use App\AsessmenMandiri09;
use App\AsessmenMandiri10;
use App\AsessmenMandiri11;

class AssesmenMandiriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view ('APL02.Assesmen_mandiri');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $create=AsessmenMandiri01::create($request->all());
        $create=AsessmenMandiri02::create($request->all());
        $create=AsessmenMandiri03::create($request->all());
        $create=AsessmenMandiri04::create($request->all());
        $create=AsessmenMandiri05::create($request->all());
        $create=AsessmenMandiri06::create($request->all());
        $create=AsessmenMandiri07::create($request->all());
        $create=AsessmenMandiri08::create($request->all());
        $create=AsessmenMandiri09::create($request->all());
        $create=AsessmenMandiri10::create($request->all());
        $create=AsessmenMandiri11::create($request->all());

        alert()->success('Data Berhasil Disimpan', 'SUCCESS')->persistent('Close');
        return redirect ('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

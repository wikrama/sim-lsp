<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataPribadi;
use App\DataPekerjaan;
use App\BuktiLengkap;
use App\AssesmenMandiri01;
use App\AssesmenMandiri02;
use App\AssesmenMandiri03;
use App\AssesmenMandiri04;
use App\AssesmenMandiri05;
use App\AssesmenMandiri06;
use App\AssesmenMandiri07;
use App\AssesmenMandiri08;
use App\AssesmenMandiri09;
use App\AssesmenMandiri10;
use App\AssesmenMandiri11;
//mak
use App\MAK\AksesKompetensi;
use App\MAK\FormulirAssesmen;
use App\MAK\FormulirBanding;
use App\MAK\LaporanAssesmen;
use App\MAK\PersetujuanAssesmen;
use App\MAK\UmpanBalik01;
use App\MAK\UmpanBalik02;
use App\MAK\UmpanBalik03;
use App\MAK\UmpanBalik04;
use App\MAK\UmpanBalik05;
use App\MAK\UmpanBalik06;
use App\MAK\UmpanBalik07;
use App\MAK\UmpanBalik08;
use App\MAK\UmpanBalik09;
use App\MAK\UmpanBalik10;
use App\MAK\UmpanBalik11;
use App\MAK\UmpanPeserta;
//mpa
use App\MPA\Mpa01;
use App\MPA\Mpa02;
use App\MPA\Mpa03;
use App\MPA\Mpa05delapan;
use App\MPA\Mpa05dua;
use App\MPA\Mpa05empat;
use App\MPA\Mpa05enam;
use App\MPA\Mpa05lima;
use App\MPA\Mpa05satu;
use App\MPA\Mpa05sembilan;
use App\MPA\Mpa05sepuluh;
use App\MPA\Mpa05tiga;
use App\MPA\Mpa05tujuh;
//mma
use App\MMA\Mma01;
use App\MMA\Mmaaspekkodeetik1;
use App\MMA\Mmaaspekkodeetik2;
use App\MMA\Mmadokumen_program1;
use App\MMA\Mmadokumen_program2;
use App\MMA\Mmadokumen_program3;
use App\MMA\Mmadokumen_program4;
use App\MMA\Mmaimplementasialgoritma1;
use App\MMA\Mmaimplementasialgoritma2;
use App\MMA\Mmaimplementasialgoritma3;
use App\MMA\Mmaimplementasialgoritma4;
use App\MMA\Mmaimplementasialgoritma5;
use App\MMA\Mmaimplementasipemrograman1;
use App\MMA\Mmaimplementasipemrograman2;
use App\MMA\Mmaimplementasipemrograman3;
use App\MMA\Mmaimplementasipemrograman4;
use App\MMA\Mmaimplementasipemrograman5;
use App\MMA\Mmaimplementasipemrograman6;
use App\MMA\Mmainstalasisoftware1;
use App\MMA\Mmainstalasisoftware2;
use App\MMA\Mmainstalasisoftware3;
use App\MMA\Mmakerjaaman1_1;
use App\MMA\Mmakerjaaman1_2;
use App\MMA\Mmakerjaaman2;
use App\MMA\Mmakerjaaman3;
use App\MMA\Mmalakukan_debug1;
use App\MMA\Mmalakukan_debug2;
use App\MMA\Mmalakukan_debug3;
use App\MMA\Mmapemenuhan_bagian_unit;
use App\MMA\Mmapengaturansoftware1;
use App\MMA\Mmapengaturansoftware2;
use App\MMA\Mmastrukturdata1;
use App\MMA\Mmastrukturdata2;
use App\MMA\Mmatugasrutin1;
use App\MMA\Mmatugasrutin2;
use App\MMA\Mmatugasrutin3;
use App\MMA\Mmauserinterface1;
use App\MMA\Mmauserinterface2;




class DataPesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $get=DataPribadi::get();

        return view ('data-peserta')->with('get',$get);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = DataPribadi::where('id',$id)->delete();
        $data = DataPekerjaan::where('id',$id)->delete();
        $data = BuktiLengkap::where('id',$id)->delete();
        $data = AssesmenMandiri01::where('id',$id)->delete();
        $data = AssesmenMandiri02::where('id',$id)->delete();
        $data = AssesmenMandiri03::where('id',$id)->delete();
        $data = AssesmenMandiri04::where('id',$id)->delete();
        $data = AssesmenMandiri05::where('id',$id)->delete();
        $data = AssesmenMandiri06::where('id',$id)->delete();
        $data = AssesmenMandiri07::where('id',$id)->delete();
        $data = AssesmenMandiri08::where('id',$id)->delete();
        $data = AssesmenMandiri09::where('id',$id)->delete();
        $data = AssesmenMandiri10::where('id',$id)->delete();
        $data = AssesmenMandiri11::where('id',$id)->delete();
        //MAK
        $data = AksesKompetensi::where('id',$id)->delete();
        $data = FormulirAssesmen::where('id',$id)->delete();
        $data = FormulirBanding::where('id',$id)->delete();
        $data = LaporanAssesmen::where('id',$id)->delete();
        $data = PersetujuanAssesmen::where('id',$id)->delete();
        $data = UmpanBalik01::where('id',$id)->delete();
        $data = UmpanBalik02::where('id',$id)->delete();
        $data = UmpanBalik03::where('id',$id)->delete();
        $data = UmpanBalik04::where('id',$id)->delete();
        $data = UmpanBalik05::where('id',$id)->delete();
        $data = UmpanBalik06::where('id',$id)->delete();
        $data = UmpanBalik07::where('id',$id)->delete();
        $data = UmpanBalik08::where('id',$id)->delete();
        $data = UmpanBalik09::where('id',$id)->delete();
        $data = UmpanBalik10::where('id',$id)->delete();
        $data = UmpanBalik11::where('id',$id)->delete();
        $data = UmpanPeserta::where('id',$id)->delete();
        //MPA
        $data = Mpa01::where('id',$id)->delete();
        $data = Mpa02::where('id',$id)->delete();
        $data = Mpa03::where('id',$id)->delete();
        $data = Mpa05delapan::where('id',$id)->delete();
        $data = Mpa05dua::where('id',$id)->delete();
        $data = Mpa05empat::where('id',$id)->delete();
        $data = Mpa05enam::where('id',$id)->delete();
        $data = Mpa05lima::where('id',$id)->delete();
        $data = Mpa05satu::where('id',$id)->delete();
        $data = Mpa05sembilan::where('id',$id)->delete();
        $data = Mpa05sepuluh::where('id',$id)->delete();
        $data = Mpa05tiga::where('id',$id)->delete();
        $data = Mpa05tujuh::where('id',$id)->delete();
        //MMA
        $data = Mma01::where('id',$id)->delete();
        $data = Mmaaspekkodeetik1::where('id',$id)->delete();
        $data = Mmaaspekkodeetik2::where('id',$id)->delete();
        $data = Mmadokumen_program1::where('id',$id)->delete();
        $data = Mmadokumen_program2::where('id',$id)->delete();
        $data = Mmadokumen_program3::where('id',$id)->delete();
        $data = Mmadokumen_program4::where('id',$id)->delete();
        $data = Mmaimplementasialgoritma1::where('id',$id)->delete();
        $data = Mmaimplementasialgoritma2::where('id',$id)->delete();
        $data = Mmaimplementasialgoritma3::where('id',$id)->delete();
        $data = Mmaimplementasialgoritma4::where('id',$id)->delete();
        $data = Mmaimplementasialgoritma5::where('id',$id)->delete();
        $data = Mmaimplementasipemrograman1::where('id',$id)->delete();
        $data = Mmaimplementasipemrograman2::where('id',$id)->delete();
        $data = Mmaimplementasipemrograman3::where('id',$id)->delete();
        $data = Mmaimplementasipemrograman4::where('id',$id)->delete();
        $data = Mmaimplementasipemrograman5::where('id',$id)->delete();
        $data = Mmaimplementasipemrograman6::where('id',$id)->delete();
        $data = Mmainstalasisoftware1::where('id',$id)->delete();
        $data = Mmainstalasisoftware2::where('id',$id)->delete();
        $data = Mmainstalasisoftware3::where('id',$id)->delete();
        $data = Mmakerjaaman1_1::where('id',$id)->delete();
        $data = Mmakerjaaman1_2::where('id',$id)->delete();
        $data = Mmakerjaaman2::where('id',$id)->delete();
        $data = Mmakerjaaman3::where('id',$id)->delete();
        $data = Mmalakukan_debug1::where('id',$id)->delete();
        $data = Mmalakukan_debug2::where('id',$id)->delete();
        $data = Mmalakukan_debug3::where('id',$id)->delete();
        $data = Mmapemenuhan_bagian_unit::where('id',$id)->delete();
        $data = Mmapengaturansoftware1::where('id',$id)->delete();
        $data = Mmapengaturansoftware2::where('id',$id)->delete();
        $data = Mmastrukturdata1::where('id',$id)->delete();
        $data = Mmastrukturdata2::where('id',$id)->delete();
        $data = Mmatugasrutin1::where('id',$id)->delete();
        $data = Mmatugasrutin2::where('id',$id)->delete();
        $data = Mmatugasrutin3::where('id',$id)->delete();
        $data = Mmauserinterface1::where('id',$id)->delete();
        $data = Mmauserinterface2::where('id',$id)->delete();

        alert()->success('Data Berhasil Dihapus', 'SUCCESS')->persistent('Close');
        return redirect ('/data-peserta');
    }
}

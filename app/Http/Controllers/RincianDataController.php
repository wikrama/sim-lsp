<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataPekerjaan;
use App\DataPribadi;

class RincianDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Peserta-Lsp.peserta-lsp');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $create = DataPribadi::create($req->all());

        $store = "N";
        if ($req->hasFile('cv','rapot')) {

            $destinationcv = "cv";
            $cv = $req->file('cv');
            $cv->move($destinationcv, $cv->getClientOriginalName());

            $destinationrapot = "rapot";
            $rapot = $req->file('rapot');
            $rapot->move($destinationrapot, $rapot->getClientOriginalName());

            $destinationsertifikat = "sertifikat";
            $sertifikat_keahlian = $req->file('sertifikat_keahlian');
            $sertifikat_keahlian->move($destinationsertifikat, $sertifikat_keahlian->getClientOriginalName());

            $destinationkartupelajar = "kartupelajar";
            $kartu_pelajar = $req->file('kartu_pelajar');
            $kartu_pelajar->move($destinationkartupelajar, $kartu_pelajar->getClientOriginalName());
            $upload = "Y";
        }

        if ($upload = "Y") {
            $store = new DataPekerjaan;
            $store->nama_lembaga = $req->nama_lembaga;
            $store->jabatan = $req->jabatan;
            $store->alamat = $req->alamat;
            $store->kode_pos = $req->kode_pos;
            $store->telp = $req->telp;
            $store->fax = $req->fax;
            $store->email = $req->email;
            $store->tujuan_asesmen = $req->tujuan_asesmen;
            $store->skema_sertifikasi = $req->skema_sertifikasi;
            $store->cv =$cv->getClientOriginalName();
            $store->rapot =  $rapot->getClientOriginalName();
            $store->sertifikat_keahlian = $sertifikat_keahlian->getClientOriginalName();
            $store->kartu_pelajar =  $kartu_pelajar->getClientOriginalName();
            $store->save();   
        }        

        alert()->success('Data Berhasil Ditambahkan', 'SUCCESS')->persistent('Close');
        return redirect('/kompetensi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

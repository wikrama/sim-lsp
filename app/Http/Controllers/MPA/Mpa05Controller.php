<?php

namespace App\Http\Controllers\MPA;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MPA\Mpa05satu;
use App\MPA\Mpa05dua;
use App\MPA\Mpa05tiga;
use App\MPA\Mpa05empat;
use App\MPA\Mpa05lima;
use App\MPA\Mpa05enam;
use App\MPA\Mpa05tujuh;
use App\MPA\Mpa05delapan;
use App\MPA\Mpa05sembilan;
use App\MPA\Mpa05sepuluh;

class Mpa05Controller extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('MPA.mpa-05');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        unset($request['_token']);
        $satu = Mpa05satu::create($request->all());
        $dua = Mpa05dua::create($request->all());
        $tiga = Mpa05tiga::create($request->all());
        $empat = Mpa05empat::create($request->all());
        $lima = Mpa05lima::create($request->all());
        $enam = Mpa05enam::create($request->all());
        $tujuh = Mpa05tujuh::create($request->all());
        $delapan = Mpa05delapan::create($request->all());
        $sembilan = Mpa05sembilan::create($request->all());
        $sepuluh = Mpa05sepuluh::create($request->all());
        alert()->success('Data Berhasil Disimpan', 'SUCCESS')->persistent('Close');
        return redirect('/mpa-05');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

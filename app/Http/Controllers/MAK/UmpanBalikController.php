<?php

namespace App\Http\Controllers\MAK;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MAK\UmpanBalik01;
use App\MAK\UmpanBalik02;
use App\MAK\UmpanBalik03;
use App\MAK\UmpanBalik04;
use App\MAK\UmpanBalik05;
use App\MAK\UmpanBalik06;
use App\MAK\UmpanBalik07;
// use App\MAK\UmpanBalik08; (DI SKIP)
// use App\MAK\UmpanBalik09; (DI SKIP)
// use App\MAK\UmpanBalik10; (DI SKIP)
use App\MAK\UmpanBalik11; 

class UmpanBalikController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view ('MAK.Umpan_balik');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $create =UmpanBalik01::create($request->all());
        $create =UmpanBalik02::create($request->all());
        $create =UmpanBalik03::create($request->all());
        $create =UmpanBalik04::create($request->all());
        $create =UmpanBalik05::create($request->all());
        $create =UmpanBalik06::create($request->all());
        $create =UmpanBalik07::create($request->all());
        // $create =UmpanBalik08::create($request->all());  (DIS KIP)
        // $create =UmpanBalik09::create($request->all());  (DIS KIP)
        // $create =UmpanBalik10::create($request->all());  (DIS KIP)
           $create =UmpanBalik11::create($request->all());
        alert()->success('Data Berhasil Disimpan', 'SUCCESS')->persistent('Close');
        return redirect ('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

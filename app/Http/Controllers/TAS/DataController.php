<?php

namespace App\Http\Controllers\TAS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TAS\DataTUK;
use App\TAS\DataAsesor;
use App\TAS\DataSkema;

class DataController extends Controller
{

    public function index()
    {
        //
        $get = DataTUK::get();

        return view ('Data_TAS')->with('tuk',$get);
    }


        public function indexAsesor()
    {
        //
        $get = DataAsesor::get();

        return view ('Data_Asesor')->with('asesor',$get);
    }


        public function indexSkema()
    {
        //
        $get = DataSkema::get();

        return view ('Data_Skema')->with('skema',$get);
    }

    public function create()
    {
        //
        return view ('create_tuk');
    }

     public function createAsesor()
    {
        //
        return view ('create_asesor');
    }

    public function createSkema()
    {
        //
        return view ('create_skema');
    }

    public function store(Request $request)
    {
        //
        $create = DataTUK::create($request->all());
        // $data = City::insert([
        // 'name' => $request->nama]);
        alert()->success('Data Berhasil Disimpan', 'SUCCESS')->persistent('Close');        
        return redirect('/data');
    }

        public function storeAsesor(Request $request)
    {
        //
        $create = DataAsesor::create($request->all());
        // $data = City::insert([
        // 'name' => $request->nama]);
        alert()->success('Data Berhasil Disimpan', 'SUCCESS')->persistent('Close');            
        return redirect('/dataAsesor');
    }

        public function storeSkema(Request $request)
    {
        //
        $create = DataSkema::create($request->all());
        // $data = City::insert([
        // 'name' => $request->nama]);
        alert()->success('Data Berhasil Disimpan', 'SUCCESS')->persistent('Close');            
        return redirect('/dataSkema');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
        $data = DataTUK::find($id);

        return view ('edit_tuk')->with('tuk',$data);
    }

    public function editAsesor($id)
    {
        //
        $data = DataAsesor::find($id);

        return view ('edit_asesor')->with('asesor',$data);
    }
    public function editSkema($id)
    {
        //
        $data = DataSkema::find($id);

        return view ('edit_skema')->with('skema',$data);
    }        

    public function updateTUK(Request $request)
    {
        //
        $data = DataTUK::where('id',$request->id)->update
        ([
            'kode' => $request->kode,
            'jnis' =>$request->jnis,
            'nama' => $request->nama,
            'alamat' =>$request->alamat
        ]);
        alert()->success('Data Berhasil Diedit', 'SUCCESS')->persistent('Close');
        return redirect ('/data');
    }

    public function updateAsesor(Request $request)
    {
        //
        $data = DataAsesor::where('id',$request->id)->update
        ([
            'nama' => $request->nama,
            'no_regis' =>$request->no_regis,
            'no_sertifikat' => $request->no_sertifikat,
            'no_blanko' =>$request->no_blanko,
            'tgl_exp' =>$request->tgl_exp,
            'alamat' =>$request->alamat  
        ]);
        alert()->success('Data Berhasil Diedit', 'SUCCESS')->persistent('Close');
        return redirect ('/dataAsesor');
    }

    public function updateSkema(Request $request)
    {
        //
        $data = DataSkema::where('id',$request->id)->update
        ([
            'kode' => $request->kode,
            'nama' =>$request->nama,
            'kategori' => $request->kategori,
            'bidang' => $request->bidang,
            'mea' =>$request->mea,
            'unit' =>$request->unit
        ]);
        alert()->success('Data Berhasil Diedit', 'SUCCESS')->persistent('Close');
        return redirect ('/dataSkema');
    }    




    public function destroy($id)
    {
        //
        $data = DataTUK::where('id',$id)->delete();
        return redirect ('/data');
    }

        public function destroyAsesor($id)
    {
        //
        $data = DataAsesor::where('id',$id)->delete();
        return redirect ('/dataAsesor');
    }

        public function destroySkema($id)
    {
        //
        $data = DataSkema::where('id',$id)->delete();
        return redirect ('/dataSkema');
    }
}

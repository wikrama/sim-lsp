<?php

namespace App\Http\Controllers\MMA;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MMA\Mma01;
use App\MMA\Mmakerjaaman1_1;
use App\MMA\Mmakerjaaman1_2;
use App\MMA\Mmakerjaaman2;
use App\MMA\Mmatugasrutin1;
use App\MMA\Mmatugasrutin2;
use App\MMA\Mmatugasrutin3;
use App\MMA\Mmaaspekkodeetik1;
use App\MMA\Mmaaspekkodeetik2;
use App\MMA\Mmastrukturdata1;
use App\MMA\Mmastrukturdata2;
use App\MMA\Mmauserinterface1;
use App\MMA\Mmauserinterface2;
use App\MMA\Mmainstalasisoftware1;
use App\MMA\Mmainstalasisoftware2;
use App\MMA\Mmainstalasisoftware3;
use App\MMA\Mmapengaturansoftware1;
use App\MMA\Mmapengaturansoftware2;
use App\MMA\Mmaimplementasipemrograman1;
use App\MMA\Mmaimplementasipemrograman2;
use App\MMA\Mmaimplementasipemrograman3;
use App\MMA\Mmaimplementasipemrograman4;
use App\MMA\Mmaimplementasipemrograman5;
use App\MMA\Mmaimplementasipemrograman6;
use App\MMA\Mmaimplementasialgoritma1;
use App\MMA\Mmaimplementasialgoritma2;
use App\MMA\Mmaimplementasialgoritma3;
use App\MMA\Mmaimplementasialgoritma4;
use App\MMA\Mmaimplementasialgoritma5;
use App\MMA\Mmadokumen_program1;
use App\MMA\Mmadokumen_program2;
use App\MMA\Mmadokumen_program3;
use App\MMA\Mmadokumen_program4;
use App\MMA\Mmalakukan_debug1;
use App\MMA\Mmalakukan_debug2;
use App\MMA\Mmalakukan_debug3;
use App\MMA\Mmapemenuhan_bagian_unit;

class MmaController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('MMA.mma');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        unset($request['_token']);
        $insert = Mma01::create($request->all());
        $kerjaaman1_1 = Mmakerjaaman1_1::create($request->all());
        $kerjaaman1_2 = Mmakerjaaman1_2::create($request->all());
        $kerjaaman2 = Mmakerjaaman2::create($request->all());
        $tugasrutin1 = Mmatugasrutin1::create($request->all());
        $tugasrutin2 = Mmatugasrutin2::create($request->all());
        $tugasrutin3 = Mmatugasrutin3::create($request->all());
        $aspekkodeetik1 = Mmaaspekkodeetik1::create($request->all());
        $aspekkodeetik2 = Mmaaspekkodeetik2::create($request->all());
        $strukturdata1 = Mmastrukturdata1::create($request->all());
        $strukturdata2 = Mmastrukturdata2::create($request->all());
        $userinterface1 = Mmauserinterface1::create($request->all());
        $userinterface2 = Mmauserinterface2::create($request->all());
        $instalasisoftware1 = Mmainstalasisoftware1::create($request->all());
        $instalasisoftware2 = Mmainstalasisoftware2::create($request->all());
        $instalasisoftware3 = Mmainstalasisoftware3::create($request->all());
        $pengaturansoftware1 = Mmapengaturansoftware1::create($request->all());
        $pengaturansoftware2 = Mmapengaturansoftware2::create($request->all());
        $implementasipemrograman1 = Mmaimplementasipemrograman1::create($request->all());
        $implementasipemrograman2 = Mmaimplementasipemrograman2::create($request->all());
        $implementasipemrograman3 = Mmaimplementasipemrograman3::create($request->all());
        $implementasipemrograman4 = Mmaimplementasipemrograman4::create($request->all());
        $implementasipemrograman5 = Mmaimplementasipemrograman5::create($request->all());
        $implementasipemrograman6 = Mmaimplementasipemrograman6::create($request->all());
        $implementasialgoritma1 = Mmaimplementasialgoritma1::create($request->all());
        $implementasialgoritma2 = Mmaimplementasialgoritma2::create($request->all());
        $implementasialgoritma3 = Mmaimplementasialgoritma3::create($request->all());
        $implementasialgoritma4 = Mmaimplementasialgoritma4::create($request->all());
        $implementasialgoritma5 = Mmaimplementasialgoritma5::create($request->all());
        $dokumen_program1 = Mmadokumen_program1::create($request->all());
        $dokumen_program2 = Mmadokumen_program2::create($request->all());
        $dokumen_program3 = Mmadokumen_program3::create($request->all());
        $dokumen_program4 = Mmadokumen_program4::create($request->all());
        $lakukan_debug1 = Mmalakukan_debug1::create($request->all());
        $lakukan_debug2 = Mmalakukan_debug2::create($request->all());
        $lakukan_debug3 = Mmalakukan_debug3::create($request->all());
        $pemenuhan_bagian_unit = Mmapemenuhan_bagian_unit::create($request->all());
        
        alert()->success('Data Berhasil Disimpan', 'SUCCESS')->persistent('Close');
        return redirect('/mma');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuktiLengkap extends Model
{
    //
    protected $table = 'bukti__lengkap';

    protected $fillable = [
    						'id',
    						'1',
    						'2',
    						'3',
    						'4',
    						'5',
    						'6',
    						'7',
    						'8',
    						'9',
    						'10',
							'nama_assesi',
							'tgl_assesi',
							'nama_admin',
							'no_reg',
							'tgl_admin'
						];
}

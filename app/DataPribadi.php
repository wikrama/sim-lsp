<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPribadi extends Model
{
    protected $table = 'data_pribadi';

    protected $fillable = [
    						'id',
							'nama',	
							'tempat_lahir',	
							'tanggal_lahir',	
							'jk',	
							'kebangsaan',	
							'alamat_rmh',	
							'kd_pos',
							'pendidikan_terakhir',
							'rumah',	
							'hp',	
							'kantor',	
							'email'
    					  ];
}

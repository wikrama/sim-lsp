<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsessmenMandiri08 extends Model
{
    //
            protected $table = 'assesmen_mandiri_08';

    protected $fillable = [
								 'id',
								'1&1',
								'1&1&alesan',
								'1&1&asesi',
								'1&2',
								'1&2&alesan',
								'1&2&asesi',
								'1&3',
								'1&3&alesan',
								'1&3&asesi',
								'2&1',
								'2&1&alesan',
								'2&1&asesi',
								'2&2',
								'2&2&alesan',
								'2&2%asesi',
								'3&1',
								'3&1&alesan',
								'3&1&sesi',
								'3&2',
								'3&2&alesan',
								'3&2&asesi',
								'3&3',
								'3&3&alesan',
								'3&3&asesi',
								'4&1',
								'4&1&alesan',
								'4&1&asesi',
								'4&2',
								'4&2&alesan',
								'4&2&asesi',
								'4&3',
								'4&3&alesan',
								'4&3&asesi',
								'4&4',
								'4&4&alesan',
								'4&4&asesi',
								'5&1',
								'5&1&alesan',
								'5&1&asesi',
								'5&2',
								'5&2&alesan',
								'5&2&asesi',
								'6&1',
								'6&1&alesan',
								'6&1&asesi',
								'6&2',
								'6&2&alesan',
								'6&2&asesi'
    						]; 



}

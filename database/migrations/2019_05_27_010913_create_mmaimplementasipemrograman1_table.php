<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasipemrograman1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasipemrograman1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipe_data_standar_tl');
            $table->string('tipe_data_standar_t');
            $table->string('tipe_data_standar_verifikasi');
            $table->string('tipe_data_standar_tes_lisan');
            $table->string('tipe_data_standar_tes_tulis');
            $table->string('tipe_data_standar_wawancara');
            $table->string('tipe_data_standar_pihak_tiga');
            $table->string('tipe_data_standar_studi_kasus');
            // ==============================================
            $table->string('syntax_program_kuasai_tl');
            $table->string('syntax_program_kuasai_t');
            $table->string('syntax_program_kuasai_verifikasi');
            $table->string('syntax_program_kuasai_tes_lisan');
            $table->string('syntax_program_kuasai_tes_tulis');
            $table->string('syntax_program_kuasai_wawancara');
            $table->string('syntax_program_kuasai_pihak_tiga');
            $table->string('syntax_program_kuasai_studi_kasus');
            //  =============================================
            $table->string('struktur_kontrol_program_tl'); 
            $table->string('struktur_kontrol_program_t'); 
            $table->string('struktur_kontrol_program_verifikasi'); 
            $table->string('struktur_kontrol_program_tes_lisan'); 
            $table->string('struktur_kontrol_program_tes_tulis'); 
            $table->string('struktur_kontrol_program_wawancara'); 
            $table->string('struktur_kontrol_program_pihak_tiga'); 
            $table->string('struktur_kontrol_program_studi_kasus'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasipemrograman1');
    }
}

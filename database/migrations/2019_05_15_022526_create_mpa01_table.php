<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa01Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa01', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('memenuhi_standar');
            $table->string('mencerminkan_prinsip');
            $table->string('menggabungkan_prinsip');
            $table->string('memenuhi_aturan');
            $table->string('memberikan_pilihan');
            $table->string('terurut');
            $table->string('mudah');
            $table->string('merefleksikan');
            $table->string('dapat_diperaktekkan');
            $table->string('menggunakan_format');
            $table->string('memperhatikan_bahasa');
            $table->string('memperhatikan_keragaman');
            $table->string('menggunakan_representasi');
            $table->string('menggunakan_media');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa01');
    }
}

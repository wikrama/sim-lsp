<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesmenMandiri03Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesmen_mandiri_03', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('1+1');
            $table->string('1+1+alesan');
            $table->string('1+1+asesi');
            $table->string('1+2');
            $table->string('1+2+alesan');
            $table->string('1+2+asesi');
            $table->string('2+1');
            $table->string('2+1+alesan');
            $table->string('2+1+asesi');
            $table->string('2+2');
            $table->string('2+2+alesan');
            $table->string('2+2+asesi');
            $table->string('2+3');
            $table->string('2+3+alesan');
            $table->string('2+3+asesi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesmen_mandiri_03');
    }
}

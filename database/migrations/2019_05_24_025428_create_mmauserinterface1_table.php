<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmauserinterface1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmauserinterface1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rancangan_user_tl');
            $table->string('rancangan_user_t');
            $table->string('rancangan_user_verifikasi');
            $table->string('rancangan_user_tes_lisan');
            $table->string('rancangan_user_tes_tulis');
            $table->string('rancangan_user_wawancara');
            $table->string('rancangan_user_pihak_tiga');
            $table->string('rancangan_user_studi_kasus');
            // ============================================
            $table->string('komponen_user_tl');
            $table->string('komponen_user_t');
            $table->string('komponen_user_verifikasi');
            $table->string('komponen_user_tes_lisan');
            $table->string('komponen_user_tes_tulis');
            $table->string('komponen_user_wawancara');
            $table->string('komponen_user_pihak_tiga');
            $table->string('komponen_user_studi_kasus');
            // ============================================
            $table->string('urutan_akses_tl');
            $table->string('urutan_akses_t');
            $table->string('urutan_akses_verifikasi');
            $table->string('urutan_akses_tes_lisan');
            $table->string('urutan_akses_tes_tulis');
            $table->string('urutan_akses_wawancara');
            $table->string('urutan_akses_pihak_tiga');
            $table->string('urutan_akses_studi_kasus');
            // ===========================================
            $table->string('mock_up_tl');
            $table->string('mock_up_t');
            $table->string('mock_up_verifikasi');
            $table->string('mock_up_tes_lisan');
            $table->string('mock_up_tes_tulis');
            $table->string('mock_up_wawancara');
            $table->string('mock_up_pihak_tiga');
            $table->string('mock_up_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmauserinterface1');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmainstalasisoftware1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmainstalasisoftware1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('platform_tl');
            $table->string('platform_t');
            $table->string('platform_verifikasi');
            $table->string('platform_tes_lisan');
            $table->string('platform_wawancara');
            $table->string('platform_pihak_tiga');
            $table->string('platform_studi_kasus');
            // =====================================
            $table->string('tools_bahasa_tl');
            $table->string('tools_bahasa_t');
            $table->string('tools_bahasa_verifikasi');
            $table->string('tools_bahasa_tes_lisan');
            $table->string('tools_bahasa_wawancara');
            $table->string('tools_bahasa_pihak_tiga');
            $table->string('tools_bahasa_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmainstalasisoftware1');
    }
}

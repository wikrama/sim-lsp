<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmakerjaaman2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmakerjaaman2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mencari_tanggapan_tl');
            $table->string('mencari_tanggapan_t');
            $table->string('mencari_tanggapan_verifikasi');
            $table->string('mencari_tanggapan_tes_lisan');
            $table->string('mencari_tanggapan_tes_tulis');
            $table->string('mencari_tanggapan_wawancara');
            $table->string('mencari_tanggapan_pihak_tiga');
            $table->string('mencari_tanggapan_studi_kasus');
            // ==================================================
            $table->string('memberikan_tanggapan_tl');
            $table->string('memberikan_tanggapan_t');
            $table->string('memberikan_tanggapan_verifikasi');
            $table->string('memberikan_tanggapan_tes_lisan');
            $table->string('memberikan_tanggapan_tes_tulis');
            $table->string('memberikan_tanggapan_wawancara');
            $table->string('memberikan_tanggapan_pihak_tiga');
            $table->string('memberikan_tanggapan_studi_kasus');
            // ==================================================
            $table->string('kontribusi_tl');
            $table->string('kontribusi_t');
            $table->string('kontribusi_verifikasi');
            $table->string('kontribusi_tes_lisan');
            $table->string('kontribusi_tes_tulis');
            $table->string('kontribusi_wawancara');
            $table->string('kontribusi_pihak_tiga');
            $table->string('kontribusi_studi_kasus');
            // ==================================================
            $table->string('cita_cita_tl');
            $table->string('cita_cita_t');
            $table->string('cita_cita_verifikasi');
            $table->string('cita_cita_tes_lisan');
            $table->string('cita_cita_tes_tulis');
            $table->string('cita_cita_wawancara');
            $table->string('cita_cita_pihak_tiga');
            $table->string('cita_cita_studi_kasus');
            // ==================================================

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmakerjaaman2');
    }
}

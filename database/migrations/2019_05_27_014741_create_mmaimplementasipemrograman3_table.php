<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasipemrograman3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasipemrograman3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('program_prosedur_tl');
            $table->string('program_prosedur_t');
            $table->string('program_prosedur_verifikasi');
            $table->string('program_prosedur_tes_lisan');
            $table->string('program_prosedur_tes_tulis');
            $table->string('program_prosedur_wawancara');
            $table->string('program_prosedur_pihak_tiga');
            $table->string('program_prosedur_studi_kasus');
            // ====================================
            $table->string('program_fungsi_tl');
            $table->string('program_fungsi_t');
            $table->string('program_fungsi_verifikasi');
            $table->string('program_fungsi_tes_lisan');
            $table->string('program_fungsi_tes_tulis');
            $table->string('program_fungsi_wawancara');
            $table->string('program_fungsi_pihak_tiga');
            $table->string('program_fungsi_studi_kasus');
            // =====================================
            $table->string('program_fungsi_prosedur_tl');
            $table->string('program_fungsi_prosedur_t');
            $table->string('program_fungsi_prosedur_verifikasi');
            $table->string('program_fungsi_prosedur_tes_lisan');
            $table->string('program_fungsi_prosedur_tes_tulis');
            $table->string('program_fungsi_prosedur_wawancara');
            $table->string('program_fungsi_prosedur_pihak_tiga');
            $table->string('program_fungsi_prosedur_studi_kasus');
            // =====================================
            $table->string('keterangan_fungsi_prosedur_tl');
            $table->string('keterangan_fungsi_prosedur_t');
            $table->string('keterangan_fungsi_prosedur_verifikasi');
            $table->string('keterangan_fungsi_prosedur_tes_lisan');
            $table->string('keterangan_fungsi_prosedur_tes_tulis');
            $table->string('keterangan_fungsi_prosedur_wawancara');
            $table->string('keterangan_fungsi_prosedur_pihak_tiga');
            $table->string('keterangan_fungsi_prosedur_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasipemrograman3');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaaspekkodeetik1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaaspekkodeetik1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('norma_tl');
            $table->string('norma_verifikasi');
            $table->string('norma_tes_lisan');
            $table->string('norma_tes_tulis');
            $table->string('norma_wawancara');
            $table->string('norma_pihak_tiga');
            $table->string('norma_studi_kasus');
            // ======================================
            $table->string('spek_legal_tl');
            $table->string('spek_legal_verifikasi');
            $table->string('spek_legal_tes_lisan');
            $table->string('spek_legal_tes_tulis');
            $table->string('spek_legal_wawancara');
            $table->string('spek_legal_pihak_tiga');
            $table->string('spek_legal_studi_kasus');
            // =====================================
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaaspekkodeetik1');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmatugasrutin2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmatugasrutin2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('intruksi_spesifikasi_tl');
            $table->string('intruksi_spesifikasi_t');
            $table->string('intruksi_spesifikasi_verifikasi');
            $table->string('intruksi_spesifikasi_tes_lisan');
            $table->string('intruksi_spesifikasi_tes_tulis');
            $table->string('intruksi_spesifikasi_wawancara');
            $table->string('intruksi_spesifikasi_pihak_tiga');
            $table->string('intruksi_spesifikasi_studi_kasus');
            // ====================================================
            $table->string('rangkaian_tl');
            $table->string('rangkaian_t');
            $table->string('rangkaian_verifikasi');
            $table->string('rangkaian_tes_lisan');
            $table->string('rangkaian_tes_tulis');
            $table->string('rangkaian_wawancara');
            $table->string('rangkaian_pihak_tiga');
            $table->string('rangkaian_studi_kasus');
            // ==================================================
            $table->string('langkah_hasil_tl');
            $table->string('langkah_hasil_t');
            $table->string('langkah_hasil_verifikasi');
            $table->string('langkah_hasil_tes_lisan');
            $table->string('langkah_hasil_tes_tulis');
            $table->string('langkah_hasil_wawancara');
            $table->string('langkah_hasil_pihak_tiga');
            $table->string('langkah_hasil_studi_kasus');
            // ==================================================
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmatugasrutin2');
    }
}

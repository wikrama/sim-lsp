<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmatugasrutin1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmatugasrutin1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('intruksi_prosedur_tl');
            $table->string('intruksi_prosedur_t');
            $table->string('intruksi_prosedur_verifikasi');
            $table->string('intruksi_prosedur_tes_lisan');
            $table->string('intruksi_prosedur_tes_tulis');
            $table->string('intruksi_prosedur_wawancara');
            $table->string('intruksi_prosedur_pihak_tiga');
            $table->string('intruksi_prosedur_studi_kasus');
            // ==================================================
            $table->string('spesifikasi_tl');
            $table->string('spesifikasi_t');
            $table->string('spesifikasi_verifikasi');
            $table->string('spesifikasi_tes_lisan');
            $table->string('spesifikasi_tes_tulis');
            $table->string('spesifikasi_wawancara');
            $table->string('spesifikasi_pihak_tiga');
            $table->string('spesifikasi_studi_kasus');
            // ==================================================
            $table->string('hasil_tugas_tl');
            $table->string('hasil_tugas_t');
            $table->string('hasil_tugas_verifikasi');
            $table->string('hasil_tugas_tes_lisan');
            $table->string('hasil_tugas_tes_tulis');
            $table->string('hasil_tugas_wawancara');
            $table->string('hasil_tugas_pihak_tiga');
            $table->string('hasil_tugas_studi_kasus');
            // =================================================
            $table->string('syarat_tugas_tl');
            $table->string('syarat_tugas_t');
            $table->string('syarat_tugas_verifikasi');
            $table->string('syarat_tugas_tes_lisan');
            $table->string('syarat_tugas_tes_tulis');
            $table->string('syarat_tugas_wawancara');
            $table->string('syarat_tugas_pihak_tiga');
            $table->string('syarat_tugas_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmatugasrutin1');
    }
}

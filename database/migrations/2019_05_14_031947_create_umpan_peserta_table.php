<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUmpanPesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umpan_peserta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pnjls_cukup');
            $table->string('ksmptn_mmpelajari');
            $table->string('ksmptn_diskusi');
            $table->string('menggali_bukti');
            $table->string('jaminan_rahasia');
            $table->string('ksmptn_demontrasi');
            $table->string('pnjlsn_memadai');
            $table->string('umpan_balik');
            $table->string('komunikasi');
            $table->string('tanda_tangan');
            $table->string('catatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('umpan_peserta');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmauserinterface2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmauserinterface2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('menu_program_tl');
            $table->string('menu_program_t');
            $table->string('menu_program_verifikasi');
            $table->string('menu_program_tes_lisan');
            $table->string('menu_program_tes_tulis');
            $table->string('menu_program_wawancara');
            $table->string('menu_program_pihak_tiga');
            $table->string('menu_program_studi_kasus');
            // ========================================================
            $table->string('penempatan_user_tl');
            $table->string('penempatan_user_t');
            $table->string('penempatan_user_verifikasi');
            $table->string('penempatan_user_tes_lisan');
            $table->string('penempatan_user_tes_tulis');
            $table->string('penempatan_user_wawancara');
            $table->string('penempatan_user_pihak_tiga');
            $table->string('penempatan_user_studi_kasus');
            // =======================================================
            $table->string('aktif_pasif_tl');
            $table->string('aktif_pasif_t');
            $table->string('aktif_pasif_verifikasi');
            $table->string('aktif_pasif_tes_lisan');
            $table->string('aktif_pasif_tes_tulis');
            $table->string('aktif_pasif_wawancara');
            $table->string('aktif_pasif_pihak_tiga');
            $table->string('aktif_pasif_studi_kasus');
            // =======================================================
            $table->string('bentuk_style_tl');
            $table->string('bentuk_style_t');
            $table->string('bentuk_style_verifikasi');
            $table->string('bentuk_style_tes_lisan');
            $table->string('bentuk_style_tes_tulis');
            $table->string('bentuk_style_wawancara');
            $table->string('bentuk_style_pihak_tiga');
            $table->string('bentuk_style_studi_kasus');
            // =======================================================
            $table->string('penerapan_simulasi_tl');
            $table->string('penerapan_simulasi_t');
            $table->string('penerapan_simulasi_verifikasi');
            $table->string('penerapan_simulasi_tes_lisan');
            $table->string('penerapan_simulasi_tes_tulis');
            $table->string('penerapan_simulasi_wawancara');
            $table->string('penerapan_simulasi_pihak_tiga');
            $table->string('penerapan_simulasi_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmauserinterface2');
    }
}

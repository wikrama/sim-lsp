<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesmenMandiri10Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesmen_mandiri_10', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('1!1');
            $table->string('1!1!alesan');
            $table->string('1!1!asesi');
            $table->string('1!2');
            $table->string('1!2!alesan');
            $table->string('1!2!asesi');
            $table->string('2!1');
            $table->string('2!1!alesan');
            $table->string('2!1!asesi');
            $table->string('2!2');
            $table->string('2!2!alesan');
            $table->string('2!2!asesi');
            $table->string('2!3');
            $table->string('2!3!alesan');
            $table->string('2!3!asesi');
            $table->string('2!4');
            $table->string('2!4!alesan');
            $table->string('2!4!asesi');
            $table->string('3!1');
            $table->string('3!1!alesan');
            $table->string('3!1!asesi');
            $table->string('3!2');
            $table->string('3!2!alesan');
            $table->string('3!2!asesi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesmen_mandiri_10');
    }
}

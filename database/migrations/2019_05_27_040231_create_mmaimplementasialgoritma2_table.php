<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasialgoritma2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasialgoritma2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('metode_sesuai_tl');
            $table->string('metode_sesuai_t');
            $table->string('metode_sesuai_verifikasi');
            $table->string('metode_sesuai_tes_lisan');
            $table->string('metode_sesuai_tes_tulis');
            $table->string('metode_sesuai_wawancara');
            $table->string('metode_sesuai_pihak_tiga');
            $table->string('metode_sesuai_studi_kasus');
            // =============================================
            $table->string('komponen_butuh_tl');
            $table->string('komponen_butuh_t');
            $table->string('komponen_butuh_verifikasi');
            $table->string('komponen_butuh_tes_lisan');
            $table->string('komponen_butuh_tes_tulis');
            $table->string('komponen_butuh_wawancara');
            $table->string('komponen_butuh_pihak_tiga');
            $table->string('komponen_butuh_studi_kasus');
            // =============================================
            $table->string('relasi_antar_komponen_tl');
            $table->string('relasi_antar_komponen_t');
            $table->string('relasi_antar_komponen_verifikasi');
            $table->string('relasi_antar_komponen_tes_lisan');
            $table->string('relasi_antar_komponen_tes_tulis');
            $table->string('relasi_antar_komponen_wawancara');
            $table->string('relasi_antar_komponen_pihak_tiga');
            $table->string('relasi_antar_komponen_studi_kasus');
            // =============================================
            $table->string('alur_mulai_tl');
            $table->string('alur_mulai_t');
            $table->string('alur_mulai_verifikasi');
            $table->string('alur_mulai_tes_lisan');
            $table->string('alur_mulai_tes_tulis');
            $table->string('alur_mulai_wawancara');
            $table->string('alur_mulai_pihak_tiga');
            $table->string('alur_mulai_studi_kasus');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasialgoritma2');
    }
}

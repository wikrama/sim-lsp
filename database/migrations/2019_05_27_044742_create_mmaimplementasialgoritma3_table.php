<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasialgoritma3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasialgoritma3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('algoritma_sorting_menjelaskan_tl');
            $table->string('algoritma_sorting_menjelaskan_t');
            $table->string('algoritma_sorting_menjelaskan_verifikasi');
            $table->string('algoritma_sorting_menjelaskan_tes_lisan');
            $table->string('algoritma_sorting_menjelaskan_tes_tulis');
            $table->string('algoritma_sorting_menjelaskan_wawancara');
            $table->string('algoritma_sorting_menjelaskan_pihak_tiga');
            $table->string('algoritma_sorting_menjelaskan_studi_kasus');
            // ==========================================================
            $table->string('algoritma_sorting_membuat_tl');
            $table->string('algoritma_sorting_membuat_t');
            $table->string('algoritma_sorting_membuat_verifikasi');
            $table->string('algoritma_sorting_membuat_tes_lisan');
            $table->string('algoritma_sorting_membuat_tes_tulis');
            $table->string('algoritma_sorting_membuat_wawancara');
            $table->string('algoritma_sorting_membuat_pihak_tiga');
            $table->string('algoritma_sorting_membuat_studi_kasus');
            // =========================================================
            $table->string('algoritma_searching_menjelaskan_tl');
            $table->string('algoritma_searching_menjelaskan_t');
            $table->string('algoritma_searching_menjelaskan_verifikasi');
            $table->string('algoritma_searching_menjelaskan_tes_lisan');
            $table->string('algoritma_searching_menjelaskan_tes_tulis');
            $table->string('algoritma_searching_menjelaskan_wawancara');
            $table->string('algoritma_searching_menjelaskan_pihak_tiga');
            $table->string('algoritma_searching_menjelaskan_studi_kasus');
            // =========================================================
            $table->string('algoritma_searching_membuat_tl');
            $table->string('algoritma_searching_membuat_t');
            $table->string('algoritma_searching_membuat_verifikasi');
            $table->string('algoritma_searching_membuat_tes_lisan');
            $table->string('algoritma_searching_membuat_tes_tulis');
            $table->string('algoritma_searching_membuat_wawancara');
            $table->string('algoritma_searching_membuat_pihak_tiga');
            $table->string('algoritma_searching_membuat_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasialgoritma3');
    }
}

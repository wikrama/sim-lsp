<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05enamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05enam', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pencapaian_identifikasi_platform');
            $table->string('penilaian_identifikasi_platform');
            $table->string('pencapaian_install_tools');
            $table->string('penilaian_install_tools');
            $table->string('pencapaian_jalankan_tools');
            $table->string('penilaian_jalankan_tools');
            $table->string('pencapaian_buat_script');
            $table->string('penilaian_buat_script');
            $table->string('pencapaian_Jalankan_script');
            $table->string('penilaian_Jalankan_script');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05enam');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasialgoritma1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasialgoritma1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipe_data_jelas_tl');
            $table->string('tipe_data_jelas_t');
            $table->string('tipe_data_jelas_verifikasi');
            $table->string('tipe_data_jelas_tes_lisan');
            $table->string('tipe_data_jelas_tes_tulis');
            $table->string('tipe_data_jelas_wawancara');
            $table->string('tipe_data_jelas_pihak_tiga');
            $table->string('tipe_data_jelas_studi_kasus');
            // ================================================
            $table->string('variable_jelas_tl');
            $table->string('variable_jelas_t');
            $table->string('variable_jelas_verifikasi');
            $table->string('variable_jelas_tes_lisan');
            $table->string('variable_jelas_tes_tulis');
            $table->string('variable_jelas_wawancara');
            $table->string('variable_jelas_pihak_tiga');
            $table->string('variable_jelas_studi_kasus');
            // =================================================
            $table->string('konstanta_mengetahui_tl');
            $table->string('konstanta_mengetahui_t');
            $table->string('konstanta_mengetahui_verifikasi');
            $table->string('konstanta_mengetahui_tes_lisan');
            $table->string('konstanta_mengetahui_tes_tulis');
            $table->string('konstanta_mengetahui_wawancara');
            $table->string('konstanta_mengetahui_pihak_tiga');
            $table->string('konstanta_mengetahui_studi_kasus');
            //  ===============================================
            $table->string('konstanta_memahami_tl'); 
            $table->string('konstanta_memahami_t'); 
            $table->string('konstanta_memahami_verifikasi'); 
            $table->string('konstanta_memahami_tes_lisan'); 
            $table->string('konstanta_memahami_tes_tulis'); 
            $table->string('konstanta_memahami_wawancara'); 
            $table->string('konstanta_memahami_pihak_tiga'); 
            $table->string('konstanta_memahami_studi_kasus');
            //  ==============================================
            $table->string('konstanta_menjelaskan_tl'); 
            $table->string('konstanta_menjelaskan_t'); 
            $table->string('konstanta_menjelaskan_verifikasi'); 
            $table->string('konstanta_menjelaskan_tes_lisan'); 
            $table->string('konstanta_menjelaskan_tes_tulis'); 
            $table->string('konstanta_menjelaskan_wawancara'); 
            $table->string('konstanta_menjelaskan_pihak_tiga'); 
            $table->string('konstanta_menjelaskan_studi_kasus'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasialgoritma1');
    }
}

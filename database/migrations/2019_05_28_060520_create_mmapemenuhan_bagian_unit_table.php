<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmapemenuhanBagianUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmapemenuhan_bagian_unit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('batasan_variable');
            $table->string('panduan_asesmen');
            $table->string('wawancara');
            $table->string('email');
            $table->string('rapat');
            $table->string('video');
            $table->string('fokus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmapemenuhan_bagian_unit');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasipemrograman4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasipemrograman4', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dimensi_array_tl');
            $table->string('dimensi_array_t');
            $table->string('dimensi_array_verifikasi');
            $table->string('dimensi_array_tes_lisan');
            $table->string('dimensi_array_tes_tulis');
            $table->string('dimensi_array_wawancara');
            $table->string('dimensi_array_pihak_tiga');
            $table->string('dimensi_array_studi_kasus');
            // =========================================
            $table->string('tipe_data_array_tl');
            $table->string('tipe_data_array_t');
            $table->string('tipe_data_array_verifikasi');
            $table->string('tipe_data_array_tes_lisan');
            $table->string('tipe_data_array_tes_tulis');
            $table->string('tipe_data_array_wawancara');
            $table->string('tipe_data_array_pihak_tiga');
            $table->string('tipe_data_array_studi_kasus');
            // ========================================
            $table->string('panjang_array_tl');
            $table->string('panjang_array_t');
            $table->string('panjang_array_verifikasi');
            $table->string('panjang_array_tes_lisan');
            $table->string('panjang_array_tes_tulis');
            $table->string('panjang_array_wawancara');
            $table->string('panjang_array_pihak_tiga');
            $table->string('panjang_array_studi_kasus');
            // =======================================
            $table->string('pengurutan_array_tl');
            $table->string('pengurutan_array_t');
            $table->string('pengurutan_array_verifikasi');
            $table->string('pengurutan_array_tes_lisan');
            $table->string('pengurutan_array_tes_tulis');
            $table->string('pengurutan_array_wawancara');
            $table->string('pengurutan_array_pihak_tiga');
            $table->string('pengurutan_array_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasipemrograman4');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUmpanBalik01Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umpan_balik_01', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_psrt');
            $table->string('tim_asesor');
            $table->string('tgl');
            $table->string('tmpt');
            $table->string('1_1');
            $table->string('1_1_bl');
            $table->string('1_1_btl');
            $table->string('1_1_bt');
            $table->string('1_2');
            $table->string('1_2_bl');
            $table->string('1_2_btl');
            $table->string('1_2_bt');
            $table->string('1_3');
            $table->string('1_3_bl');
            $table->string('1_3_btl');
            $table->string('1_3_bt');
            $table->string('1_4');
            $table->string('1_4_bl');
            $table->string('1_4_btl');
            $table->string('1_4_bt');
            $table->string('1_5');
            $table->string('1_5_bl');
            $table->string('1_5_btl');
            $table->string('1_5_bt');
            $table->string('1_6');
            $table->string('1_6_bl');
            $table->string('1_6_btl');
            $table->string('1_6_bt');
            $table->string('1_7');
            $table->string('1_7_bl');
            $table->string('1_7_btl');
            $table->string('1_7_bt');
            $table->string('1_8');
            $table->string('1_8_bl');
            $table->string('1_8_btl');
            $table->string('1_8_bt');
            $table->string('2_1');
            $table->string('2_1_bl');
            $table->string('2_1_btl');
            $table->string('2_1_bt');
            $table->string('3_1');
            $table->string('3_1_bl');
            $table->string('3_1_btl');
            $table->string('3_1_bt');
            $table->string('3_2');
            $table->string('3_2_bl');
            $table->string('3_2_btl');
            $table->string('3_2_bt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('umpan_balik_01');
    }
}

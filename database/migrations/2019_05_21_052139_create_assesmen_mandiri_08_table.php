<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesmenMandiri08Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesmen_mandiri_08', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('1&1');
            $table->string('1&1&alesan');
            $table->string('1&1&asesi');
            $table->string('1&2');
            $table->string('1&2&alesan');
            $table->string('1&2&asesi');
            $table->string('1&3');
            $table->string('1&3&alesan');
            $table->string('1&3&asesi');
            $table->string('2&1');
            $table->string('2&1&alesan');
            $table->string('2&1&asesi');
            $table->string('2&2');
            $table->string('2&2&alesan');
            $table->string('2&2%asesi');
            $table->string('3&1');
            $table->string('3&1&alesan');
            $table->string('3&1&sesi');
            $table->string('3&2');
            $table->string('3&2&alesan');
            $table->string('3&2&asesi');
            $table->string('3&3');
            $table->string('3&3&alesan');
            $table->string('3&3&asesi');
            $table->string('4&1');
            $table->string('4&1&alesan');
            $table->string('4&1&asesi');
            $table->string('4&2');
            $table->string('4&2&alesan');
            $table->string('4&2&asesi');
            $table->string('4&3');
            $table->string('4&3&alesan');
            $table->string('4&3&asesi');
            $table->string('4&4');
            $table->string('4&4&alesan');
            $table->string('4&4&asesi');
            $table->string('5&1');
            $table->string('5&1&alesan');
            $table->string('5&1&asesi');
            $table->string('5&2');
            $table->string('5&2&alesan');
            $table->string('5&2&asesi');
            $table->string('6&1');
            $table->string('6&1&alesan');
            $table->string('6&1&asesi');
            $table->string('6&2');
            $table->string('6&2&alesan');
            $table->string('6&2&asesi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesmen_mandiri_08');
    }
}

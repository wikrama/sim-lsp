<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa03Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa03', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_peserta_satu');            
            $table->string('nama_asesor_satu');            
            $table->string('tgl_uji_satu');            
            $table->string('uu_ite');            
            $table->string('konten_ilegal');            
            $table->string('copyright');            
            $table->string('maya_internet');            
            $table->string('freeware');            
            $table->string('os_satu');            
            $table->string('os_dua');            
            $table->string('ide_satu');            
            $table->string('ide_dua');            
            $table->string('smk_bisa');            
            $table->string('control_panel_xampp');            
            $table->string('alamat_xampp_satu');            
            $table->string('alamat_xampp_dua');            
            $table->string('perintah_sql');            
            $table->string('nama_peserta_dua');            
            $table->string('nama_asesor_dua');            
            $table->string('tgl_uji_dua');            
            $table->string('asesi_uu_ite');            
            $table->string('keputusan_uu_ite');            
            $table->string('asesi_konten_ilegal');            
            $table->string('keputusan_konten_ilegal');            
            $table->string('asesi_copyright');            
            $table->string('keputusan_copyright');            
            $table->string('asesi_maya_internet');            
            $table->string('keputusan_maya_internet');            
            $table->string('asesi_freeware');            
            $table->string('keputusan_freeware');            
            $table->string('asesi_os');            
            $table->string('keputusan_os');            
            $table->string('asesi_ide');            
            $table->string('keputusan_ide');            
            $table->string('asesi_smk_bisa');            
            $table->string('keputusan_smk_bisa');            
            $table->string('asesi_control_panel_xampp');            
            $table->string('keputusan_control_panel_xampp');            
            $table->string('asesi_alamat_xampp');            
            $table->string('keputusan_alamat_xampp');            
            $table->string('asesi_perintah_sql');            
            $table->string('keputusan_perintah_sql');            
            $table->string('keputusan');        
            $table->string('assesor');            
            $table->string('assesi');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa03');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasipemrograman5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasipemrograman5', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('program_menulis_data_tl');
            $table->string('program_menulis_data_t');
            $table->string('program_menulis_data_verifikasi');
            $table->string('program_menulis_data_tes_lisan');
            $table->string('program_menulis_data_tes_tulis');
            $table->string('program_menulis_data_wawancara');
            $table->string('program_menulis_data_pihak_tiga');
            $table->string('program_menulis_data_studi_kasus');
            // ========================================
            $table->string('program_membaca_data_tl');
            $table->string('program_membaca_data_t');
            $table->string('program_membaca_data_verifikasi');
            $table->string('program_membaca_data_tes_lisan');
            $table->string('program_membaca_data_tes_tulis');
            $table->string('program_membaca_data_wawancara');
            $table->string('program_membaca_data_pihak_tiga');
            $table->string('program_membaca_data_studi_kasus');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasipemrograman5');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmadokumenProgram4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmadokumen_program4', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tools_generate_tl');
            $table->string('tools_generate_t');
            $table->string('tools_generate_verifikasi');
            $table->string('tools_generate_tes_lisan');
            $table->string('tools_generate_tes_tulis');
            $table->string('tools_generate_wawancara');
            $table->string('tools_generate_pihak_tiga');
            $table->string('tools_generate_studi_kasus');
            // ===================================
            $table->string('generate_dokumentasi_tl');
            $table->string('generate_dokumentasi_t');
            $table->string('generate_dokumentasi_verifikasi');
            $table->string('generate_dokumentasi_tes_lisan');
            $table->string('generate_dokumentasi_tes_tulis');
            $table->string('generate_dokumentasi_wawancara');
            $table->string('generate_dokumentasi_pihak_tiga');
            $table->string('generate_dokumentasi_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmadokumen_program4');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmakerjaaman11Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmakerjaaman1_1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kerja_tl');
            $table->string('kerja_t');
            $table->string('kerja_verifikasi');
            $table->string('kerja_tes_lisan');
            $table->string('kerja_tes_tulis');
            $table->string('kerja_wawancara');
            $table->string('kerja_pihak_tiga');
            $table->string('kerja_studi_kasus');
            // =================================
            $table->string('rumah_tl');
            $table->string('rumah_t');
            $table->string('rumah_verifikasi');
            $table->string('rumah_tes_lisan');
            $table->string('rumah_tes_tulis');
            $table->string('rumah_wawancara');
            $table->string('rumah_pihak_tiga');
            $table->string('rumah_studi_kasus');
            // ===================================
            $table->string('tanggungjawab_tl');
            $table->string('tanggungjawab_t');
            $table->string('tanggungjawab_verifikasi');
            $table->string('tanggungjawab_tes_lisan');
            $table->string('tanggungjawab_tes_tulis');
            $table->string('tanggungjawab_wawancara');
            $table->string('tanggungjawab_pihak_tiga');
            $table->string('tanggungjawab_studi_kasus');
            // ===================================
            $table->string('demontrasi_tanggungjawab_tl');
            $table->string('demontrasi_tanggungjawab_t');
            $table->string('demontrasi_tanggungjawab_verifikasi');
            $table->string('demontrasi_tanggungjawab_tes_lisan');
            $table->string('demontrasi_tanggungjawab_tes_tulis');
            $table->string('demontrasi_tanggungjawab_wawancara');
            $table->string('demontrasi_tanggungjawab_pihak_tiga');
            $table->string('demontrasi_tanggungjawab_studi_kasus');
            // ====================================
            $table->string('memakai_perlengkapan_tl');
            $table->string('memakai_perlengkapan_t');
            $table->string('memakai_perlengkapan_verifikasi');
            $table->string('memakai_perlengkapan_tes_lisan');
            $table->string('memakai_perlengkapan_tes_tulis');
            $table->string('memakai_perlengkapan_wawancara');
            $table->string('memakai_perlengkapan_pihak_tiga');
            $table->string('memakai_perlengkapan_studi_kasus');
            // ===================================
            $table->string('menyimpan_perlengkapan_tl');
            $table->string('menyimpan_perlengkapan_t');
            $table->string('menyimpan_perlengkapan_verifikasi');
            $table->string('menyimpan_perlengkapan_tes_lisan');
            $table->string('menyimpan_perlengkapan_tes_tulis');
            $table->string('menyimpan_perlengkapan_wawancara');
            $table->string('menyimpan_perlengkapan_pihak_tiga');
            $table->string('menyimpan_perlengkapan_studi_kasus');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmakerjaaman1_1');
    }
}

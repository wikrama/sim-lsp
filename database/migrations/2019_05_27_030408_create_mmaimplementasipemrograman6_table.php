<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasipemrograman6Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasipemrograman6', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kesalahan_program_koreksi_tl');
            $table->string('kesalahan_program_koreksi_t');
            $table->string('kesalahan_program_koreksi_verifikasi');
            $table->string('kesalahan_program_koreksi_tes_lisan');
            $table->string('kesalahan_program_koreksi_tes_tulis');
            $table->string('kesalahan_program_koreksi_wawancara');
            $table->string('kesalahan_program_koreksi_pihak_tiga');
            $table->string('kesalahan_program_koreksi_studi_kasus');
            // ====================================
            $table->string('kesalahan_syntax_tl');
            $table->string('kesalahan_syntax_t');
            $table->string('kesalahan_syntax_verifikasi');
            $table->string('kesalahan_syntax_tes_lisan');
            $table->string('kesalahan_syntax_tes_tulis');
            $table->string('kesalahan_syntax_wawancara');
            $table->string('kesalahan_syntax_pihak_tiga');
            $table->string('kesalahan_syntax_studi_kasus');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasipemrograman6');
    }
}

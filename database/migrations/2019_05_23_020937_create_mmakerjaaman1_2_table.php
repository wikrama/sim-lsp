<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmakerjaaman12Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmakerjaaman1_2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('semua_perlengkapan_tl');
            $table->string('semua_perlengkapan_t');
            $table->string('semua_perlengkapan_verifikasi');
            $table->string('semua_perlengkapan_tes_lisan');
            $table->string('semua_perlengkapan_tes_tulis');
            $table->string('semua_perlengkapan_wawancara');
            $table->string('semua_perlengkapan_pihak_tiga');
            $table->string('semua_perlengkapan_studi_kasus');
            // =============================================
            $table->string('mengenali_tanda_tl');
            $table->string('mengenali_tanda_t');
            $table->string('mengenali_tanda_verifikasi');
            $table->string('mengenali_tanda_tes_lisan');
            $table->string('mengenali_tanda_tes_tulis');
            $table->string('mengenali_tanda_wawancara');
            $table->string('mengenali_tanda_pihak_tiga');
            $table->string('mengenali_tanda_studi_kasus');
            // =============================================
            $table->string('mengikuti_tanda_tl');
            $table->string('mengikuti_tanda_t');
            $table->string('mengikuti_tanda_verifikasi');
            $table->string('mengikuti_tanda_tes_lisan');
            $table->string('mengikuti_tanda_tes_tulis');
            $table->string('mengikuti_tanda_wawancara');
            $table->string('mengikuti_tanda_pihak_tiga');
            $table->string('mengikuti_tanda_studi_kasus');
            // ============================================
            $table->string('pedoman_tl');
            $table->string('pedoman_t');
            $table->string('pedoman_verifikasi');
            $table->string('pedoman_tes_lisan');
            $table->string('pedoman_tes_tulis');
            $table->string('pedoman_wawancara');
            $table->string('pedoman_pihak_tiga');
            $table->string('pedoman_studi_kasus');
            // ============================================
            $table->string('darurat_akrab_tl');
            $table->string('darurat_akrab_t');
            $table->string('darurat_akrab_verifikasi');
            $table->string('darurat_akrab_tes_lisan');
            $table->string('darurat_akrab_tes_tulis');
            $table->string('darurat_akrab_wawancara');
            $table->string('darurat_akrab_pihak_tiga');
            $table->string('darurat_akrab_studi_kasus');
            // ===========================================
            $table->string('darurat_individu_tl');
            $table->string('darurat_individu_t');
            $table->string('darurat_individu_verifikasi');
            $table->string('darurat_individu_tes_lisan');
            $table->string('darurat_individu_tes_tulis');
            $table->string('darurat_individu_wawancara');
            $table->string('darurat_individu_pihak_tiga');
            $table->string('darurat_individu_studi_kasus');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmakerjaaman1_2');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmapengaturansoftware1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmapengaturansoftware1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('target_hasil_tl');
            $table->string('target_hasil_verifikasi');
            $table->string('target_hasil_tes_lisan');
            $table->string('target_hasil_wawancara');
            $table->string('target_hasil_pihak_tiga');
            $table->string('target_hasil_studi_kasus');
            // ======================================================
            $table->string('pemograman_konfigurasi_tl');
            $table->string('pemograman_konfigurasi_verifikasi');
            $table->string('pemograman_konfigurasi_tes_lisan');
            $table->string('pemograman_konfigurasi_wawancara');
            $table->string('pemograman_konfigurasi_pihak_tiga');
            $table->string('pemograman_konfigurasi_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmapengaturansoftware1');
    }
}

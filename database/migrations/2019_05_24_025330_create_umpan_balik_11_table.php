<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUmpanBalik11Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umpan_balik_11', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('umpan_balik');
            $table->string('identifikasi');
            $table->string('saran_tindak_lanjut');
            $table->string('nama_peserta');
            $table->string('tgl_peserta');
            $table->string('nama_asesor');
            $table->string('no_reg');
            $table->string('tgl_asesor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('umpan_balik_11');
    }
}

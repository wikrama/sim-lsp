<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulirBandingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulir_banding', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_psrt');
            $table->string('nama_assr');
            $table->string('tgl_ases');
            $table->string('penjelasan_proses');
            $table->string('diskusi_asesor');
            $table->string('melibatkan_orng');
            $table->string('no_unit');
            $table->string('judul_unit');
            $table->string('alasan');
            $table->string('tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulir_banding');
    }
}

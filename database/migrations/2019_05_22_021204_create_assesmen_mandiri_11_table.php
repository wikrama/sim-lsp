<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesmenMandiri11Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesmen_mandiri_11', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('tgl');
            $table->string('ass_nama');
            $table->string('no_reg');
            $table->string('tgl_ass');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesmen_mandiri_11');
    }
}

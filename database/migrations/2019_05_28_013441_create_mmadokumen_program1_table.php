<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmadokumenProgram1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmadokumen_program1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('modul_program_tl');
            $table->string('modul_program_t');
            $table->string('modul_program_verifikasi');
            $table->string('modul_program_tes_lisan');
            $table->string('modul_program_tes_tulis');
            $table->string('modul_program_wawancara');
            $table->string('modul_program_pihak_tiga');
            $table->string('modul_program_studi_kasus');
            // =========================================            
            $table->string('paremeter_tl');
            $table->string('paremeter_t');
            $table->string('paremeter_verifikasi');
            $table->string('paremeter_tes_lisan');
            $table->string('paremeter_tes_tulis');
            $table->string('paremeter_wawancara');
            $table->string('paremeter_pihak_tiga');
            $table->string('paremeter_studi_kasus');
            // =========================================
            $table->string('mengetahui_defisi_algoritma_tl');
            $table->string('mengetahui_defisi_algoritma_t');
            $table->string('mengetahui_defisi_algoritma_verifikasi');
            $table->string('mengetahui_defisi_algoritma_tes_lisan');
            $table->string('mengetahui_defisi_algoritma_tes_tulis');
            $table->string('mengetahui_defisi_algoritma_wawancara');
            $table->string('mengetahui_defisi_algoritma_pihak_tiga');
            $table->string('mengetahui_defisi_algoritma_studi_kasus');
            // =========================================
            $table->string('memahami_defisi_algoritma_tl');
            $table->string('memahami_defisi_algoritma_t');
            $table->string('memahami_defisi_algoritma_verifikasi');
            $table->string('memahami_defisi_algoritma_tes_lisan');
            $table->string('memahami_defisi_algoritma_tes_tulis');
            $table->string('memahami_defisi_algoritma_wawancara');
            $table->string('memahami_defisi_algoritma_pihak_tiga');
            $table->string('memahami_defisi_algoritma_studi_kasus');
            // =========================================
            $table->string('menjelaskan_defisi_algoritma_tl');
            $table->string('menjelaskan_defisi_algoritma_t');
            $table->string('menjelaskan_defisi_algoritma_verifikasi');
            $table->string('menjelaskan_defisi_algoritma_tes_lisan');
            $table->string('menjelaskan_defisi_algoritma_tes_tulis');
            $table->string('menjelaskan_defisi_algoritma_wawancara');
            $table->string('menjelaskan_defisi_algoritma_pihak_tiga');
            $table->string('menjelaskan_defisi_algoritma_studi_kasus');
            // =========================================
            $table->string('komentar_baris_kode_tl');
            $table->string('komentar_baris_kode_t');
            $table->string('komentar_baris_kode_verifikasi');
            $table->string('komentar_baris_kode_tes_lisan');
            $table->string('komentar_baris_kode_tes_tulis');
            $table->string('komentar_baris_kode_wawancara');
            $table->string('komentar_baris_kode_pihak_tiga');
            $table->string('komentar_baris_kode_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmadokumen_program1');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasialgoritma4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasialgoritma4', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('konsep_penggunaan_prosedur_tl');
            $table->string('konsep_penggunaan_prosedur_t');
            $table->string('konsep_penggunaan_prosedur_verifikasi');
            $table->string('konsep_penggunaan_prosedur_tes_lisan');
            $table->string('konsep_penggunaan_prosedur_tes_tulis');
            $table->string('konsep_penggunaan_prosedur_wawancara');
            $table->string('konsep_penggunaan_prosedur_pihak_tiga');
            $table->string('konsep_penggunaan_prosedur_studi_kasus');
            // =====================================
            $table->string('prosedur_dapat_digunakan_tl');
            $table->string('prosedur_dapat_digunakan_t');
            $table->string('prosedur_dapat_digunakan_verifikasi');
            $table->string('prosedur_dapat_digunakan_tes_lisan');
            $table->string('prosedur_dapat_digunakan_tes_tulis');
            $table->string('prosedur_dapat_digunakan_wawancara');
            $table->string('prosedur_dapat_digunakan_pihak_tiga');
            $table->string('prosedur_dapat_digunakan_studi_kasus');
            // =====================================
            $table->string('fungsi_dapat_digunakan_tl');
            $table->string('fungsi_dapat_digunakan_t');
            $table->string('fungsi_dapat_digunakan_verifikasi');
            $table->string('fungsi_dapat_digunakan_tes_lisan');
            $table->string('fungsi_dapat_digunakan_tes_tulis');
            $table->string('fungsi_dapat_digunakan_wawancara');
            $table->string('fungsi_dapat_digunakan_pihak_tiga');
            $table->string('fungsi_dapat_digunakan_studi_kasus');
            // =====================================
            $table->string('fungsi_perintah_dml_tl');
            $table->string('fungsi_perintah_dml_t');
            $table->string('fungsi_perintah_dml_verifikasi');
            $table->string('fungsi_perintah_dml_tes_lisan');
            $table->string('fungsi_perintah_dml_tes_tulis');
            $table->string('fungsi_perintah_dml_wawancara');
            $table->string('fungsi_perintah_dml_pihak_tiga');
            $table->string('fungsi_perintah_dml_studi_kasus');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasialgoritma4');
    }
}

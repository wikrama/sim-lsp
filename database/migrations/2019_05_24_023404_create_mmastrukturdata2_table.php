<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmastrukturdata2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmastrukturdata2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('implementasi_tl');
            $table->string('implementasi_t');
            $table->string('implementasi_verifikasi');
            $table->string('implementasi_tes_lisan');
            $table->string('implementasi_tes_tulis');
            $table->string('implementasi_wawancara');
            $table->string('implementasi_pihak_tiga');
            $table->string('implementasi_studi_kasus');
            // ==============================================
            $table->string('algoritma_tl');
            $table->string('algoritma_t');
            $table->string('algoritma_verifikasi');
            $table->string('algoritma_tes_lisan');
            $table->string('algoritma_tes_tulis');
            $table->string('algoritma_wawancara');
            $table->string('algoritma_pihak_tiga');
            $table->string('algoritma_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmastrukturdata2');
    }
}

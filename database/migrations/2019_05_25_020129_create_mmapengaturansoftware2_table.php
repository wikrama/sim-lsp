<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmapengaturansoftware2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmapengaturansoftware2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fitur_dasar_butuh_tl');
            $table->string('fitur_dasar_butuh_verifikasi');
            $table->string('fitur_dasar_butuh_tes_lisan');
            $table->string('fitur_dasar_butuh_wawancara');
            $table->string('fitur_dasar_butuh_pihak_tiga');
            $table->string('fitur_dasar_butuh_studi_kasus');
            // =================================================
            $table->string('fitur_dasar_tools_tl');
            $table->string('fitur_dasar_tools_verifikasi');
            $table->string('fitur_dasar_tools_tes_lisan');
            $table->string('fitur_dasar_tools_wawancara');
            $table->string('fitur_dasar_tools_pihak_tiga');
            $table->string('fitur_dasar_tools_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmapengaturansoftware2');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmastrukturdata1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmastrukturdata1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('konsep_data_tl');
            $table->string('konsep_data_t');
            $table->string('konsep_data_verifikasi');
            $table->string('konsep_data_tes_lisan');
            $table->string('konsep_data_tes_tulis');
            $table->string('konsep_data_wawancara');
            $table->string('konsep_data_pihak_tiga');
            $table->string('konsep_data_studi_kasus');
            // ===========================================
            $table->string('alternatif_tl');
            $table->string('alternatif_t');
            $table->string('alternatif_verifikasi');
            $table->string('alternatif_tes_lisan');
            $table->string('alternatif_tes_tulis');
            $table->string('alternatif_wawancara');
            $table->string('alternatif_pihak_tiga');
            $table->string('alternatif_studi_kasus');
            // ==========================================
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmastrukturdata1');
    }
}

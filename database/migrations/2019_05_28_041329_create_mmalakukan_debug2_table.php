<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmalakukanDebug2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmalakukan_debug2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_kompilasi_tl');
            $table->string('kode_kompilasi_t');
            $table->string('kode_kompilasi_verifikasi');
            $table->string('kode_kompilasi_tes_lisan');
            $table->string('kode_kompilasi_tes_tulis');
            $table->string('kode_kompilasi_wawancara');
            $table->string('kode_kompilasi_pihak_tiga');
            $table->string('kode_kompilasi_studi_kasus');
            // ===========================================
            $table->string('kriteria_lulus_tl');
            $table->string('kriteria_lulus_t');
            $table->string('kriteria_lulus_verifikasi');
            $table->string('kriteria_lulus_tes_lisan');
            $table->string('kriteria_lulus_tes_tulis');
            $table->string('kriteria_lulus_wawancara');
            $table->string('kriteria_lulus_pihak_tiga');
            $table->string('kriteria_lulus_studi_kasus');
            // ==========================================
            $table->string('kriteria_eksekusi_tl');
            $table->string('kriteria_eksekusi_t');
            $table->string('kriteria_eksekusi_verifikasi');
            $table->string('kriteria_eksekusi_tes_lisan');
            $table->string('kriteria_eksekusi_tes_tulis');
            $table->string('kriteria_eksekusi_wawancara');
            $table->string('kriteria_eksekusi_pihak_tiga');
            $table->string('kriteria_eksekusi_studi_kasus');
            // =========================================            
            $table->string('kode_kesalahan_tl');
            $table->string('kode_kesalahan_t');
            $table->string('kode_kesalahan_verifikasi');
            $table->string('kode_kesalahan_tes_lisan');
            $table->string('kode_kesalahan_tes_tulis');
            $table->string('kode_kesalahan_wawancara');
            $table->string('kode_kesalahan_pihak_tiga');
            $table->string('kode_kesalahan_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmalakukan_debug2');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmatugasrutin3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmatugasrutin3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sasaran_rencana_tl');
            $table->string('sasaran_rencana_t');
            $table->string('sasaran_rencana_verifikasi');
            $table->string('sasaran_rencana_tes_lisan');
            $table->string('sasaran_rencana_tes_tulis');
            $table->string('sasaran_rencana_wawancara');
            $table->string('sasaran_rencana_pihak_tiga');
            $table->string('sasaran_rencana_studi_kasus');
            // ==============================================
            $table->string('rencana_perbaiki_tl');
            $table->string('rencana_perbaiki_t');
            $table->string('rencana_perbaiki_verifikasi');
            $table->string('rencana_perbaiki_tes_lisan');
            $table->string('rencana_perbaiki_tes_tulis');
            $table->string('rencana_perbaiki_wawancara');
            $table->string('rencana_perbaiki_pihak_tiga');
            $table->string('rencana_perbaiki_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmatugasrutin3');
    }
}

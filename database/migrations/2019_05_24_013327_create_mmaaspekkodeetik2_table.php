<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaaspekkodeetik2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaaspekkodeetik2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('copyright_piranti_tl');
            $table->string('copyright_piranti_verifikasi');
            $table->string('copyright_piranti_tes_lisan');
            $table->string('copyright_piranti_tes_tulis');
            $table->string('copyright_piranti_wawancara');
            $table->string('copyright_piranti_pihak_tiga');
            $table->string('copyright_piranti_studi_kasus');
            // ===============================================
            $table->string('tukar_file_tl');
            $table->string('tukar_file_verifikasi');
            $table->string('tukar_file_tes_lisan');
            $table->string('tukar_file_tes_tulis');
            $table->string('tukar_file_wawancara');
            $table->string('tukar_file_pihak_tiga');
            $table->string('tukar_file_studi_kasus');
            // ===============================================
            $table->string('istilah_shareware_tl');
            $table->string('istilah_shareware_verifikasi');
            $table->string('istilah_shareware_tes_lisan');
            $table->string('istilah_shareware_tes_tulis');
            $table->string('istilah_shareware_wawancara');
            $table->string('istilah_shareware_pihak_tiga');
            $table->string('istilah_shareware_studi_kasus');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaaspekkodeetik2');
    }
}

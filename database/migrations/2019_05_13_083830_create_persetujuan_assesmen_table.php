<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersetujuanAssesmenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persetujuan_assesmen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_calon');
            $table->string('nama_ass');
            $table->string('no_skema');
            $table->string('bukti');
            $table->string('tgl');
            $table->string('tempat');
            $table->string('tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persetujuan_assesmen');
    }
}

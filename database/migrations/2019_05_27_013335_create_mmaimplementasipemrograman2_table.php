<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasipemrograman2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasipemrograman2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('program_baca_tl');
            $table->string('program_baca_t');
            $table->string('program_baca_verifikasi');
            $table->string('program_baca_tes_lisan');
            $table->string('program_baca_tes_tulis');
            $table->string('program_baca_wawancara');
            $table->string('program_baca_pihak_tiga');
            $table->string('program_baca_studi_kasus');
            // ================================================
            $table->string('struktur_kontrol_percabangan_tl');
            $table->string('struktur_kontrol_percabangan_t');
            $table->string('struktur_kontrol_percabangan_verifikasi');
            $table->string('struktur_kontrol_percabangan_tes_lisan');
            $table->string('struktur_kontrol_percabangan_tes_tulis');
            $table->string('struktur_kontrol_percabangan_wawancara');
            $table->string('struktur_kontrol_percabangan_pihak_tiga');
            $table->string('struktur_kontrol_percabangan_studi_kasus');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasipemrograman2');
    }
}

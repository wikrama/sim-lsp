<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesmenMandiri09Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesmen_mandiri_09', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('1^1');
            $table->String('1^1^alesan');
            $table->String('1^1^asesi');
            $table->String('1^2');
            $table->String('1^2^alesan');
            $table->String('1^2^asesi');
            $table->String('1^3');
            $table->String('1^3^alesan');
            $table->String('1^3^asesi');
            $table->String('2^1');
            $table->String('2^1^alesan');
            $table->String('2^1^asesi');
            $table->String('2^2');
            $table->String('2^2^alesan');
            $table->String('2^2^asesi');
            $table->String('2^3');
            $table->String('2^3^alesan');
            $table->String('2^3^asesi');
            $table->String('2^4');
            $table->String('2^4^alesan');
            $table->String('2^4^asesi');
            $table->String('3^1');
            $table->String('3^1^alesan');
            $table->String('3^1^asesi');
            $table->String('3^2');
            $table->String('3^2^alesan');
            $table->String('3^2^asesi');
            $table->String('4^1');
            $table->String('4^1^alesan');
            $table->String('4^1^asesi');
            $table->String('4^2');
            $table->String('4^2^alesan');
            $table->String('4^2^asesi');
            $table->String('4^3');
            $table->String('4^3^alesan');
            $table->String('4^3^asesi');
            $table->String('5^1');
            $table->String('5^1^alesan');
            $table->String('5^1^asesi');
            $table->String('5^2');
            $table->String('5^2^alesan');
            $table->String('5^2^asesi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesmen_mandiri_09');
    }
}

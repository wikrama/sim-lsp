<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmaimplementasialgoritma5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmaimplementasialgoritma5', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kompleksitas_waktu_tl');
            $table->string('kompleksitas_waktu_t');
            $table->string('kompleksitas_waktu_verifikasi');
            $table->string('kompleksitas_waktu_tes_lisan');
            $table->string('kompleksitas_waktu_tes_tulis');
            $table->string('kompleksitas_waktu_wawancara');
            $table->string('kompleksitas_waktu_pihak_tiga');
            $table->string('kompleksitas_waktu_studi_kasus');
            // =================================
            $table->string('kompleksitas_memory_tl');
            $table->string('kompleksitas_memory_t');
            $table->string('kompleksitas_memory_verifikasi');
            $table->string('kompleksitas_memory_tes_lisan');
            $table->string('kompleksitas_memory_tes_tulis');
            $table->string('kompleksitas_memory_wawancara');
            $table->string('kompleksitas_memory_pihak_tiga');
            $table->string('kompleksitas_memory_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmaimplementasialgoritma5');
    }
}

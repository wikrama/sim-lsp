<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmalakukanDebug1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmalakukan_debug1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_program_spesifikasi_tl');
            $table->string('kode_program_spesifikasi_t');
            $table->string('kode_program_spesifikasi_verifikasi');
            $table->string('kode_program_spesifikasi_tes_lisan');
            $table->string('kode_program_spesifikasi_tes_tulis');
            $table->string('kode_program_spesifikasi_wawancara');
            $table->string('kode_program_spesifikasi_pihak_tiga');
            $table->string('kode_program_spesifikasi_studi_kasus');
            // =============================================
            $table->string('debugging_tools_tl');
            $table->string('debugging_tools_t');
            $table->string('debugging_tools_verifikasi');
            $table->string('debugging_tools_tes_lisan');
            $table->string('debugging_tools_tes_tulis');
            $table->string('debugging_tools_wawancara');
            $table->string('debugging_tools_pihak_tiga');
            $table->string('debugging_tools_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmalakukan_debug1');
    }
}

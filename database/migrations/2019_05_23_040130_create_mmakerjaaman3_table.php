<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmakerjaaman3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmakerjaaman3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mengerti_pandangan_tl');
            $table->string('mengerti_pandangan_t');
            $table->string('mengerti_pandangan_verifikasi');
            $table->string('mengerti_pandangan_tes_lisan');
            $table->string('mengerti_pandangan_tes_tulis');
            $table->string('mengerti_pandangan_wawancara');
            $table->string('mengerti_pandangan_pihak_tiga');
            $table->string('mengerti_pandangan_studi_kasus');
            // =================================================            
            $table->string('menggambarkan_pandangan_tl');
            $table->string('menggambarkan_pandangan_t');
            $table->string('menggambarkan_pandangan_verifikasi');
            $table->string('menggambarkan_pandangan_tes_lisan');
            $table->string('menggambarkan_pandangan_tes_tulis');
            $table->string('menggambarkan_pandangan_wawancara');
            $table->string('menggambarkan_pandangan_pihak_tiga');
            $table->string('menggambarkan_pandangan_studi_kasus');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmakerjaaman3');
    }
}

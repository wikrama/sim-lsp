<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesmenMandiri01Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesmen_mandiri_01', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_psrt');
            $table->string('nama_assesor');
            $table->string('tgl');
            $table->string('tuk');
            $table->string('no_skema');
            $table->string('1_1');
            $table->string('1_1_alesan');
            $table->string('1_1_asesi');
            $table->string('1_2');
            $table->string('1_2_alesan');
            $table->string('1_2_asesi');
            $table->string('1_3');
            $table->string('1_3_alesan');
            $table->string('1_3_asesi');
            $table->string('1_4');
            $table->string('1_4_alesan');
            $table->string('1_4_asesi');
            $table->string('1_5');
            $table->string('1_5_alesan');
            $table->string('1_5_asesi');
            $table->string('1_6');
            $table->string('1_6_alesan');
            $table->string('1_6_asesi');
            $table->string('1_7');
            $table->string('1_7_alesan');
            $table->string('1_7_asesi');
            $table->string('1_8');
            $table->string('1_8_alesan');
            $table->string('1_8_asesi');
            $table->string('2_1');
            $table->string('2_1_alesan');
            $table->string('2_1_asesi');
            $table->string('3_1');
            $table->string('3_1_alesan');
            $table->string('3_1_asesi');
            $table->string('3_2');
            $table->string('3_2_alesan');
            $table->string('3_2_asesi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesmen_mandiri_01');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUmpanBalik05Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umpan_balik_05', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('1(1');
            $table->string('1(1(bl');
            $table->string('1(1(btl');
            $table->string('1(1(bt');
            $table->string('1(2');
            $table->string('1(2(bl');
            $table->string('1(2(btl');
            $table->string('1(2(bt');
            $table->string('1(3');
            $table->string('1(3(bl');
            $table->string('1(3(btl');
            $table->string('1(3(bt');
            $table->string('1(4');
            $table->string('1(4(bl');
            $table->string('1(4(btl');
            $table->string('1(4(bt');
            $table->string('2(1');
            $table->string('2(1(bl');
            $table->string('2(1(btl');
            $table->string('2(1(bt');
            $table->string('2(2');
            $table->string('2(2(bl');
            $table->string('2(2(btl');
            $table->string('2(2(bt');
            $table->string('2(3');
            $table->string('2(3(bl');
            $table->string('2(3(btl');
            $table->string('2(3(bt');
            $table->string('2(4');
            $table->string('2(4(bl');
            $table->string('2(4(btl');
            $table->string('2(4(bt');
            $table->string('2(5');
            $table->string('2(5(bl');
            $table->string('2(5(btl');
            $table->string('2(5(bt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('umpan_balik_05');
    }
}

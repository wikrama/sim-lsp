<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPekerjaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pekerjaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_lembaga');
            $table->string('jabatan');
            $table->string('alamat');
            $table->string('kode_pos');
            $table->string('telp');
            $table->string('fax');
            $table->string('email');
            $table->string('tujuan_asesmen');
            $table->string('skema_sertifikasi');
            $table->string('cv');
            $table->string('rapot');
            $table->string('sertifikat_keahlian');
            $table->string('kartu_pelajar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pekerjaan');
    }
}

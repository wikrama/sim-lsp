<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmadokumenProgram3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmadokumen_program3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dokumentasi_fungsi_tl');
            $table->string('dokumentasi_fungsi_t');
            $table->string('dokumentasi_fungsi_verifikasi');
            $table->string('dokumentasi_fungsi_tes_lisan');
            $table->string('dokumentasi_fungsi_tes_tulis');
            $table->string('dokumentasi_fungsi_wawancara');
            $table->string('dokumentasi_fungsi_pihak_tiga');
            $table->string('dokumentasi_fungsi_studi_kasus');
            // ======================================
            $table->string('eksepsi_mengetahui_tl');
            $table->string('eksepsi_mengetahui_t');
            $table->string('eksepsi_mengetahui_verifikasi');
            $table->string('eksepsi_mengetahui_tes_lisan');
            $table->string('eksepsi_mengetahui_tes_tulis');
            $table->string('eksepsi_mengetahui_wawancara');
            $table->string('eksepsi_mengetahui_pihak_tiga');
            $table->string('eksepsi_mengetahui_studi_kasus');
            // =====================================
            $table->string('eksepsi_memahami_tl');
            $table->string('eksepsi_memahami_t');
            $table->string('eksepsi_memahami_verifikasi');
            $table->string('eksepsi_memahami_tes_lisan');
            $table->string('eksepsi_memahami_tes_tulis');
            $table->string('eksepsi_memahami_wawancara');
            $table->string('eksepsi_memahami_pihak_tiga');
            $table->string('eksepsi_memahami_studi_kasus');
            // ===================================
            $table->string('eksepsi_menjelaskan_tl');
            $table->string('eksepsi_menjelaskan_t');
            $table->string('eksepsi_menjelaskan_verifikasi');
            $table->string('eksepsi_menjelaskan_tes_lisan');
            $table->string('eksepsi_menjelaskan_tes_tulis');
            $table->string('eksepsi_menjelaskan_wawancara');
            $table->string('eksepsi_menjelaskan_pihak_tiga');
            $table->string('eksepsi_menjelaskan_studi_kasus');
            // ==================================
            $table->string('dokumen_revisi_kode_tl');
            $table->string('dokumen_revisi_kode_t');
            $table->string('dokumen_revisi_kode_verifikasi');
            $table->string('dokumen_revisi_kode_tes_lisan');
            $table->string('dokumen_revisi_kode_tes_tulis');
            $table->string('dokumen_revisi_kode_wawancara');
            $table->string('dokumen_revisi_kode_pihak_tiga');
            $table->string('dokumen_revisi_kode_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmadokumen_program3');
    }
}

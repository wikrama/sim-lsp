<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmainstalasisoftware2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmainstalasisoftware2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tools_terinstall_tl');
            $table->string('tools_terinstall_verifikasi');
            $table->string('tools_terinstall_tes_lisan');
            $table->string('tools_terinstall_wawancara');
            $table->string('tools_terinstall_pihak_tiga');
            $table->string('tools_terinstall_studi_kasus');
            // =================================================
            $table->string('tools_jalan_tl');
            $table->string('tools_jalan_verifikasi');
            $table->string('tools_jalan_tes_lisan');
            $table->string('tools_jalan_wawancara');
            $table->string('tools_jalan_pihak_tiga');
            $table->string('tools_jalan_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmainstalasisoftware2');
    }
}

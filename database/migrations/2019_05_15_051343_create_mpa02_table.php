<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa02Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa02', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('merefleksikan_prinsip');
            $table->string('merefleksikan_aturan');
            $table->string('relavan');
            $table->string('akurat');
            $table->string('mudah');
            $table->string('efektif');
            $table->string('bahasa');
            $table->string('1');
            $table->string('2');
            $table->string('3');
            $table->string('4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa02');
    }
}

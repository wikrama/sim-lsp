<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05empatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05empat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pencapaian_konsep_data');
            $table->string('penilaian_konsep_data');
            $table->string('pencapaian_perbandingan_kelebihan');
            $table->string('penilaian_perbandingan_kelebihan');
            $table->string('pencapaian_mengimplementasikan_struktur');
            $table->string('penilaian_mengimplementasikan_struktur');
            $table->string('pencapaian_menyatakan_akses');
            $table->string('penilaian_menyatakan_akses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05empat');
    }
}

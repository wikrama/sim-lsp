<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmadokumenProgram2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmadokumen_program2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dokumentasi_buku_tl');
            $table->string('dokumentasi_buku_t');
            $table->string('dokumentasi_buku_verifikasi');
            $table->string('dokumentasi_buku_tes_lisan');
            $table->string('dokumentasi_buku_tes_tulis');
            $table->string('dokumentasi_buku_wawancara');
            $table->string('dokumentasi_buku_pihak_tiga');
            $table->string('dokumentasi_buku_studi_kasus');
            // ============================================
            $table->string('dokumentasi_terap_tl');
            $table->string('dokumentasi_terap_t');
            $table->string('dokumentasi_terap_verifikasi');
            $table->string('dokumentasi_terap_tes_lisan');
            $table->string('dokumentasi_terap_tes_tulis');
            $table->string('dokumentasi_terap_wawancara');
            $table->string('dokumentasi_terap_pihak_tiga');
            $table->string('dokumentasi_terap_studi_kasus');
            // ============================================
            $table->string('kegunaan_mengetauhi_modul_tl');
            $table->string('kegunaan_mengetauhi_modul_t');
            $table->string('kegunaan_mengetauhi_modul_verifikasi');
            $table->string('kegunaan_mengetauhi_modul_tes_lisan');
            $table->string('kegunaan_mengetauhi_modul_tes_tulis');
            $table->string('kegunaan_mengetauhi_modul_wawancara');
            $table->string('kegunaan_mengetauhi_modul_pihak_tiga');
            $table->string('kegunaan_mengetauhi_modul_studi_kasus');
            // ============================================
            $table->string('kegunaan_memahami_modul_tl');
            $table->string('kegunaan_memahami_modul_t');
            $table->string('kegunaan_memahami_modul_verifikasi');
            $table->string('kegunaan_memahami_modul_tes_lisan');
            $table->string('kegunaan_memahami_modul_tes_tulis');
            $table->string('kegunaan_memahami_modul_wawancara');
            $table->string('kegunaan_memahami_modul_pihak_tiga');
            $table->string('kegunaan_memahami_modul_studi_kasus');
            // ============================================
            $table->string('kegunaan_menjelaskan_modul_tl');
            $table->string('kegunaan_menjelaskan_modul_t');
            $table->string('kegunaan_menjelaskan_modul_verifikasi');
            $table->string('kegunaan_menjelaskan_modul_tes_lisan');
            $table->string('kegunaan_menjelaskan_modul_tes_tulis');
            $table->string('kegunaan_menjelaskan_modul_wawancara');
            $table->string('kegunaan_menjelaskan_modul_pihak_tiga');
            $table->string('kegunaan_menjelaskan_modul_studi_kasus');
            // ============================================
            $table->string('dokumen_revisi_tl');
            $table->string('dokumen_revisi_t');
            $table->string('dokumen_revisi_verifikasi');
            $table->string('dokumen_revisi_tes_lisan');
            $table->string('dokumen_revisi_tes_tulis');
            $table->string('dokumen_revisi_wawancara');
            $table->string('dokumen_revisi_pihak_tiga');
            $table->string('dokumen_revisi_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmadokumen_program2');
    }
}

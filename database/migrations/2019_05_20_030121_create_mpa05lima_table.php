<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05limaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05lima', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pencapaian_identifikasi_rancangan');
            $table->string('penilaian_identifikasi_rancangan');
            $table->string('pencapaian_identifikasi_komponen');
            $table->string('penilaian_identifikasi_komponen');
            $table->string('pencapaian_buat_simulasi');
            $table->string('penilaian_buat_simulasi');
            $table->string('pencapaian_terapkan_menu');
            $table->string('penilaian_terapkan_menu');
            $table->string('pencapaian_atur_penempatan');
            $table->string('penilaian_atur_penempatan');
            $table->string('pencapaian_sesuaikan_setting');
            $table->string('penilaian_sesuaikan_setting');
            $table->string('pencapaian_bentuk_style');
            $table->string('penilaian_bentuk_style');
            $table->string('pencapaian_penerapan_simulasi');
            $table->string('penilaian_penerapan_simulasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05lima');
    }
}

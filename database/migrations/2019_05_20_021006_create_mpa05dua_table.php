<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05duaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05dua', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pencapaian_intruksi_prosedur');
            $table->string('penilaian_intruksi_prosedur');
            $table->string('pencapaian_spesifikasi_relavan');
            $table->string('penilaian_spesifikasi_relavan');
            $table->string('pencapaian_hasil_tugas');
            $table->string('penilaian_hasil_tugas');
            $table->string('pencapaian_syarat_tugas');
            $table->string('penilaian_syarat_tugas');
            $table->string('pencapaian_kegiatan_individu');
            $table->string('penilaian_kegiatan_individu');
            $table->string('pencapaian_mencantumkan_kegiatan');
            $table->string('penilaian_mencantumkan_kegiatan');
            $table->string('pencapaian_memeriksa_hasil');
            $table->string('penilaian_memeriksa_hasil');
            $table->string('pencapaian_membandingkan_hasil');
            $table->string('penilaian_membandingkan_hasil');
            $table->string('pencapaian_memperbaiki_rencana');
            $table->string('penilaian_memperbaiki_rencana');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05dua');
    }
}

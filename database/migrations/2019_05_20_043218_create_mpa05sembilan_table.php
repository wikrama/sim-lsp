<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05sembilanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05sembilan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pencapaian_tipe_data');
            $table->string('penilaian_tipe_data');
            $table->string('pencapaian_variabel');
            $table->string('penilaian_variabel');
            $table->string('pencapaian_konstansta');
            $table->string('penilaian_konstansta');
            $table->string('pencapaian_metode');
            $table->string('penilaian_metode');
            $table->string('pencapaian_komponen');
            $table->string('penilaian_komponen');
            $table->string('pencapaian_relasi');
            $table->string('penilaian_relasi');
            $table->string('pencapaian_alur');
            $table->string('penilaian_alur');
            $table->string('pencapaian_algoritma_sorting');
            $table->string('penilaian_algoritma_sorting');
            $table->string('pencapaian_algoritma_searching');
            $table->string('penilaian_algoritma_searching');
            $table->string('pencapaian_mengidentifikasi_konsep');
            $table->string('penilaian_mengidentifikasi_konsep');
            $table->string('pencapaian_prosedur');
            $table->string('penilaian_prosedur');
            $table->string('pencapaian_fungsi');
            $table->string('penilaian_fungsi');
            $table->string('pencapaian_mengidentifikasi_kompleksitas');
            $table->string('penilaian_mengidentifikasi_kompleksitas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05sembilan');
    }
}

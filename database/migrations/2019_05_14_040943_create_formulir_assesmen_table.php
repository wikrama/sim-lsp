<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulirAssesmenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulir_assesmen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nma_psrt');
            $table->string('tgl_pencapaian');
            $table->string('nma_ass');
            $table->string('ket');
            $table->string('aspek_neg_pos');
            $table->string('catatan_tolak_ass');
            $table->string('saran_perbaikan');
            $table->string('kd_file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulir_assesmen');
    }
}

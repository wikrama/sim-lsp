<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05satuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05satu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_peserta_satu');
            $table->string('nama_asesor_satu');
            $table->string('tgl_uji_satu');
            $table->string('pencapaian_kerja_aman');
            $table->string('penilaian_kerja_aman');
            $table->string('pencapaian_kegiatan_rumah');
            $table->string('penilaian_kegiatan_rumah');
            $table->string('pencapaian_demostrasi_tugas');
            $table->string('penilaian_demostrasi_tugas');
            $table->string('pencapaian_pelindung_diri');
            $table->string('penilaian_pelindung_diri');
            $table->string('pencapaian_keselamatan');
            $table->string('penilaian_keselamatan');
            $table->string('pencapaian_tanda');
            $table->string('penilaian_tanda');
            $table->string('pencapaian_pedoman');
            $table->string('penilaian_pedoman');
            $table->string('pencapaian_demostrasi_darurat');
            $table->string('penilaian_demostrasi_darurat');
            $table->string('pencapaian_laporan_bahaya');
            $table->string('penilaian_laporan_bahaya');
            $table->string('pencapaian_demostrasi_personil');
            $table->string('penilaian_demostrasi_personil');
            $table->string('pencapaian_prosedur_kondisi');
            $table->string('penilaian_prosedur_kondisi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05satu');
    }
}

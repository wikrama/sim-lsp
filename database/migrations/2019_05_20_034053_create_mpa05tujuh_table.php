<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05tujuhTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05tujuh', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pencapaian_hasil_konfigurasi');
            $table->string('penilaian_hasil_konfigurasi');
            $table->string('pencapaian_tools_pemograman');
            $table->string('penilaian_tools_pemograman');
            $table->string('pencapaian_fitur_dasar');
            $table->string('penilaian_fitur_dasar');
            $table->string('pencapaian_fitur_dasar_tools');
            $table->string('penilaian_fitur_dasar_tools');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05tujuh');
    }
}

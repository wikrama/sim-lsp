<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmalakukanDebug3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmalakukan_debug3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('perbaikan_kompilasi_tl');
            $table->string('perbaikan_kompilasi_t');
            $table->string('perbaikan_kompilasi_verifikasi');
            $table->string('perbaikan_kompilasi_tes_lisan');
            $table->string('perbaikan_kompilasi_tes_tulis');
            $table->string('perbaikan_kompilasi_wawancara');
            $table->string('perbaikan_kompilasi_pihak_tiga');
            $table->string('perbaikan_kompilasi_studi_kasus');
            // ================================================
            $table->string('perbaikan_dilakukan_tl');
            $table->string('perbaikan_dilakukan_t');
            $table->string('perbaikan_dilakukan_verifikasi');
            $table->string('perbaikan_dilakukan_tes_lisan');
            $table->string('perbaikan_dilakukan_tes_tulis');
            $table->string('perbaikan_dilakukan_wawancara');
            $table->string('perbaikan_dilakukan_pihak_tiga');
            $table->string('perbaikan_dilakukan_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmalakukan_debug3');
    }
}

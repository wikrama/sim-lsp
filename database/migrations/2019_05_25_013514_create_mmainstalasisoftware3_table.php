<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmainstalasisoftware3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmainstalasisoftware3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('script_sederhana_tl');
            $table->string('script_sederhana_verifikasi');
            $table->string('script_sederhana_tes_lisan');
            $table->string('script_sederhana_wawancara');
            $table->string('script_sederhana_pihak_tiga');
            $table->string('script_sederhana_studi_kasus');
            // =============================================
            $table->string('script_jalan_tl');
            $table->string('script_jalan_verifikasi');
            $table->string('script_jalan_tes_lisan');
            $table->string('script_jalan_wawancara');
            $table->string('script_jalan_pihak_tiga');
            $table->string('script_jalan_studi_kasus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmainstalasisoftware3');
    }
}

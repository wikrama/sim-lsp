<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05tigaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05tiga', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pencapaian_identifikasi_norma');
            $table->string('penilaian_identifikasi_norma');
            $table->string('pencapaian_spek_legas');
            $table->string('penilaian_spek_legas');
            $table->string('pencapaian_copyright_lunak');
            $table->string('penilaian_copyright_lunak');
            $table->string('pencapaian_bertukar_file');
            $table->string('penilaian_bertukar_file');
            $table->string('pencapaian_shareware');
            $table->string('penilaian_shareware');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05tiga');
    }
}

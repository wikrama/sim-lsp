<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAksesKompetensiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akses_kompetensi', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('nama_psrt');
            $table->string('nama_asesi');
            $table->string('waktu');
            $table->string('tgl');
            $table->string('1_1');
            $table->string('1_2');
            $table->string('1_3');
            $table->string('1_4');
            $table->string('2_1');
            $table->string('2_2');
            $table->string('2_3');
            $table->string('2_4');
            $table->string('2_5');
            $table->string('2_6');
            $table->string('3_1');
            $table->string('3_2');
            $table->string('3_3');
            $table->string('3_4');
            $table->string('3_5');
            $table->string('3_6');
            $table->string('4_1');
            $table->string('4_2');
            $table->string('4_3');
            $table->string('4_4');
            $table->string('4_5');
            $table->string('5_1');
            $table->string('5_2');
            $table->string('5_3');
            $table->string('5_4');
            $table->string('6_1');
            $table->string('6_2');
            $table->string('6_3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akses_kompetensi');
    }
}

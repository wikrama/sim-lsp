<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05delapanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05delapan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pencapaian_tipe_data');
            $table->string('penilaian_tipe_data');
            $table->string('pencapaian_Syntax_program');
            $table->string('penilaian_Syntax_program');
            $table->string('pencapaian_struktur_kontrol_program');
            $table->string('penilaian_struktur_kontrol_program');
            $table->string('pencapaian_program_baca');
            $table->string('penilaian_program_baca');
            $table->string('pencapaian_struktur_kontrol_percabangan');
            $table->string('penilaian_struktur_kontrol_percabangan');
            $table->string('pencapaian_program_prosedur');
            $table->string('penilaian_program_prosedur');
            $table->string('pencapaian_program_fungsi');
            $table->string('penilaian_program_fungsi');
            $table->string('pencapaian_program_prosedur_fungsi');
            $table->string('penilaian_program_prosedur_fungsi');
            $table->string('pencapaian_tipe_data_standar');
            $table->string('penilaian_tipe_data_standar');
            $table->string('pencapaian_dimensi_array');
            $table->string('penilaian_dimensi_array');
            $table->string('pencapaian_tipe_data_array');
            $table->string('penilaian_tipe_data_array');
            $table->string('pencapaian_panjang_array');
            $table->string('penilaian_panjang_array');
            $table->string('pencapaian_pengurutan_array');
            $table->string('penilaian_pengurutan_array');
            $table->string('pencapaian_menulis_data_media');
            $table->string('penilaian_menulis_data_media');
            $table->string('pencapaian_membaca_data_media');
            $table->string('penilaian_membaca_data_media');
            $table->string('pencapaian_kesalahan_koreksi');
            $table->string('penilaian_kesalahan_koreksi');
            $table->string('pencapaian_kesalahan_syntax');
            $table->string('penilaian_kesalahan_syntax');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05delapan');
    }
}

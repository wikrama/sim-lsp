<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpa05sepuluhTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpa05sepuluh', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pencapaian_penyiapan_kode');
            $table->string('penilaian_penyiapan_kode');
            $table->string('pencapaian_penyiapan_debugging');
            $table->string('penilaian_penyiapan_debugging');
            $table->string('pencapaian_mengkompilasi_kode');
            $table->string('penilaian_mengkompilasi_kode');
            $table->string('pencapaian_menganalisis_kriteria_lulus');
            $table->string('penilaian_menganalisis_kriteria_lulus');
            $table->string('pencapaian_menganalisis_kriteria_eksekusi');
            $table->string('penilaian_menganalisis_kriteria_eksekusi');
            $table->string('pencapaian_mencatat_kode');
            $table->string('penilaian_mencatat_kode');
            $table->string('pencapaian_merumuskan_perbaikan');
            $table->string('penilaian_merumuskan_perbaikan');
            $table->string('pencapaian_melakukan_perbaikan');
            $table->string('penilaian_melakukan_perbaikan');
            $table->string('nama_peserta_dua');
            $table->string('nama_assesor_dua');
            $table->string('tgl_uji_dua');
            $table->string('nama_peserta_tiga');
            $table->string('tgl_uji_tiga');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpa05sepuluh');
    }
}

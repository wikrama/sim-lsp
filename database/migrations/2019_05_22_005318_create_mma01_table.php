<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMma01Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mma01', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tanggal');
            $table->string('tuk');
            $table->string('nama_asesor');
            $table->string('tujuan_asesmen');
            $table->string('jalur_asesmen');
            $table->string('benchmark_asesmen');
            $table->string('rpl_arrangements');
            $table->string('metode_alat_asesmen');
            $table->string('pengorganisasian_asesmen');
            $table->string('aturan_paket');
            $table->string('persyaratan_khusus');
            $table->string('mekanisme_jaminan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mma01');
    }
}

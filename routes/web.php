<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/res', 'test@tes');
Route::post('/res/store', 'test@store');

// Step 1 
Route::get('/peserta-lsp', 'RincianDataController@index');
Route::post('/peserta-lsp/store', 'RincianDataController@store');
// Step 2
Route::get('/kompetensi', 'UnitKompetensiController@index');
// Step 3
Route::get('/bukti-lengkap', 'BuktiLengkapController@index');
Route::post('/bukti-lengkap/store', 'BuktiLengkapController@store');

//=======================================================================================
//APL-02
Route::get('/asesmen_mandiri','AssesmenMandiriController@index');
Route::post('/asesmen_mandiri/store','AssesmenMandiriController@store');

Route::get('/registeradmin10203024056', 'Auth\RegisterController@index');

//Route MAK,MPA,MMA,dll

//MMA

Route::get('/mma', 'MMA\MmaController@index');
//simpan
Route::post('/mma/store', 'MMA\MmaController@store');
//=======================================================================================
// MPA

//01
Route::get('/mpa-01', 'MPA\Mpa01Controller@index');
//simpan
Route::post('/mpa-01/store', 'MPA\Mpa01Controller@store');

//02
Route::get('/mpa-02', 'MPA\Mpa02Controller@index');
//simpan
Route::post('/mpa-02/store', 'MPA\Mpa02Controller@store');

//03
Route::get('/mpa-03', 'MPA\Mpa03Controller@index');
//simpan
Route::post('/mpa-03/store', 'MPA\Mpa03Controller@store');

//05
Route::get('/mpa-05', 'MPA\Mpa05Controller@index');
//simpan
Route::post('/mpa-05/store', 'MPA\Mpa05Controller@store');




//============================================================================================
// MAK

// 01
Route::get('/akses_kompetensi','MAK\Akses_kompetensiController@index');
Route::post('/akses_kompetensi/store','MAK\Akses_kompetensiController@store');
//02
Route::get('/formulir_banding','MAK\Formulir_bandingController@index');
Route::post('/formulir_banding/store','MAK\Formulir_bandingController@store');
//03
Route::get('/formulir_persetujuan','MAK\Formulir_persetujuanController@index');
Route::post('/formulir_persetujuan/store','MAK\Formulir_persetujuanController@store');
//04
Route::get('/umpan_balik','MAK\UmpanBalikController@index');
Route::post('/umpan_balik/store','MAK\UmpanBalikController@store');

//05
Route::get('/umpan_peserta','MAK\Umpan_balik_pesertaController@index');
Route::post('/umpan_peserta/store','MAK\Umpan_balik_pesertaController@store');
//06
Route::get('/laporan_asesmen','MAK\Formulir_laporan_asesmenController@index');
Route::post('/laporan_asesmen/store','MAK\Formulir_laporan_asesmenController@store');

//===========================================================================

Route::get('/kkni','KKNI\KKNIController@index');

//=============================================================================

// TUK
Route::get('/data','TAS\DataController@index');
Route::get('/create','TAS\DataController@create');
Route::post('/data/store','TAS\DataController@store');
Route::get('/data/delete/{id}','TAS\DataController@destroy');
Route::get('/edit/{id}','TAS\DataController@edit');
Route::post('/data/updateTUK','TAS\DataController@updateTUK');


// Asesor
Route::get('/dataAsesor','TAS\DataController@indexAsesor');
Route::get('/createAsesor','TAS\DataController@createAsesor');
Route::post('/dataAsesor/store','TAS\DataController@storeAsesor');
Route::get('/dataAsesor/delete/{id}','TAS\DataController@destroyAsesor');
Route::get('/editAsesor/{id}','TAS\DataController@editAsesor');
Route::post('/dataAsesor/updateAsesor','TAS\DataController@updateAsesor');
//=======================================================================================
Route::get('/data-peserta', 'DataPesertaController@index');
Route::get('/data-peserta/delete/{id}', 'DataPesertaController@destroy');

//=======================================================================================
// Skema
Route::get('/dataSkema','TAS\DataController@indexSkema');
Route::get('/createSkema','TAS\DataController@createSkema');
Route::post('/dataSkema/store','TAS\DataController@storeSkema');
Route::get('/dataSkema/delete/{id}','TAS\DataController@destroySkema');
Route::get('/editSkema/{id}','TAS\DataController@editSkema');
Route::post('/dataSkema/updateSkema','TAS\DataController@updateSkema');




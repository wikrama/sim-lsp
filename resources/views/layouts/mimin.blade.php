<!DOCTYPE html>
<html lang="en">
<head>
 
 <meta charset="utf-8">
 <meta name="description" content="Miminium Admin Template v.1">
 <meta name="author" content="Isna Nur Azis">
 <meta name="keyword" content="">
 <meta name="csrf-token" content="{{ csrf_token() }}">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>LSP</title>

 <!-- start: Css -->
 <link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">

 <!-- plugins -->
 <link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
 <link rel="stylesheet" type="text/css" href="asset/css/plugins/simple-line-icons.css"/>
 <link rel="stylesheet" type="text/css" href="asset/css/plugins/mediaelementplayer.css"/>
 <link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
 <link href="asset/css/style.css" rel="stylesheet">
 <!-- end: Css -->

 <link rel="shortcut icon" href="asset/img/lsp-wk.jpg">
 <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
</head>
<style type="text/css">
   .navbar.transparent.navbar-inverse .navbar-inner {
    border-width: 0px;
    -webkit-box-shadow: 0px 0px;
    box-shadow: 0px 0px;
    background-color: rgba(0,0,0,0.0);
    background-image: -webkit-gradient(linear, 50.00% 0.00%, 50.00% 100.00%, color-stop( 0% , rgba(0,0,0,0.00)),color-stop( 100% , rgba(0,0,0,0.00)));
    background-image: -webkit-linear-gradient(270deg,rgba(0,0,0,0.00) 0%,rgba(0,0,0,0.00) 100%);
    background-image: linear-gradient(180deg,rgba(0,0,0,0.00) 0%,rgba(0,0,0,0.00) 100%);
}
</style>

<body id="mimin" class="dashboard topnav">
      <!-- start: Header -->

        <nav class="navbar navbar-default header navbar-fixed-top">
          <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
                <a href="/" class="navbar-brand"> 
                 <img src="asset/img/lsp-wk.jpg" class="img-responsive" style="width: 70px;height: 40px;">
                </a>              
              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span><a href="{{ URL('/login') }}">Login</a></span></li>                 
              </ul>
            </div>
          </div>
        </nav>
      <!-- end: Header -->

      <!-- start: Content -->
        <div id="content">
          @yield('content')
        </div>
      <!-- end: content -->      

<!-- start: Javascript -->
<script src="asset/js/jquery.min.js"></script>
<script src="asset/js/jquery.ui.min.js"></script>
<script src="asset/js/bootstrap.min.js"></script>



<!-- plugins -->
<script src="asset/js/plugins/holder.min.js"></script>
<script src="asset/js/plugins/moment.min.js"></script>
<script src="asset/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="asset/js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')
<script type="text/javascript">
  $(document).ready(function(){

  });
</script>
<!-- end: Javascript -->
</body>
</html>

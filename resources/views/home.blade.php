@extends('layouts.mimin-login')

@section('content')
    <img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
    <div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 680px;">
        <div class="container">
            <div class="text-center" style="margin-top: 250px;">
                <i><h2 style="font-size: 50px; color: white;">Selamat Datang {{ Auth::user()->name }}!</h2></i>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.mimin-admin')

@section('content')

	<img src="../asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;"></img>
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft" style="color: white">Edit Skema</h3>
                        <p class="animated fadeInDown" style="color: white">
                          Home <span class="fa-angle-right fa"></span> Data T.A.S
                        </p>
                    </div>
                  </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Edit Skema</h3></div>
                    <div class="panel-body">
                      <form action="{{URL('/dataSkema/updateSkema')}}" method="post">
                        {{ csrf_field() }}
                      <div class="form-group">
                        <input type="text" name="id" hidden="" value="{{ $skema->id }}">
                  <label class="text blue"><b>Kode :</b></label>
                  <input class="input border form-control" name="kode" value="{{$skema->kode}}" type="text" required="required"><br>
                  <label class="text blue"><b>Nama :</b></label>
                  <input class="input border form-control" name="nama" value="{{$skema->nama}}" type="text" required="required"><br>
                  <label class="text blue"><b>Kategori :</b></label>
                  <input class="input border form-control" name="kategori" value="{{$skema->kategori}}" type="text" required="required"><br>
                  <label class="text blue"><b>Bidang :</b></label>
                  <input class="input border form-control" name="bidang" value="{{$skema->bidang}}" type="text" required="required"><br>
                  <label class="text blue"><b>Mea :</b></label>
                  <input class="input border form-control" name="mea" value="{{$skema->mea}}" type="date" required="required"><br>
                  <label class="text blue"><b>Unit :</b></label>
                  <input class="input border form-control" name="unit" value="{{$skema->unit}}" type="text" required="required">
                  <div class="col-md-7">
                  </br>
                    <button value="Save" class="btn btn-primary">Update</button>
                    <a onclick="return confirm('Do You Want To Back?')" href="{{URL('/dataSkema')}}" class="btn btn-danger">Cancel</a>
                  </div> 
<!--                       <input type="text" name="kode">
                      <input type="text" name="jnis">
                      <input type="text" name="jnis">
                      <input type="text" name="alamat"> -->
                      </form>
                  </div>
                </div>
              </div>  
              </div>
            </div>
@endsection
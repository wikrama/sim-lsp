@extends('layouts.mimin-login')

@section('content')

	<img src="../asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;"></img>
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft" style="color: white">Create TUK</h3>
                        <p class="animated fadeInDown" style="color: white">
                          Home <span class="fa-angle-right fa"></span> Data T.A.S
                        </p>
                    </div>
                  </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Edit TUK</h3></div>
                    <div class="panel-body">
                      <form action="/data/updateTUK" method="post">
                        {{ csrf_field() }}
                      <div class="form-group">
                        <input type="text" name="id" hidden="" value="{{ $tuk->id }}">
                  <label class="text blue"><b>Kode :</b></label>
                  <input class="input border form-control" name="kode" value="{{$tuk->kode}}" type="text" required="required"><br>
                  <label class="text blue"><b>Jenis :</b></label>
                  <input class="input border form-control" name="jnis" value="{{$tuk->jnis}}" type="text" required="required"><br>
                  <label class="text blue"><b>Nama :</b></label>
                  <input class="input border form-control" name="nama" value="{{$tuk->nama}}" type="text" required="required"><br>
                  <label class="text blue"><b>Alamat :</b></label>
                  <input class="input border form-control" name="alamat" value="{{$tuk->alamat}}" type="text" required="required">
                  <div class="col-md-7">
                  </br>
                    <button value="Save" class="btn btn-primary">Update</button>
                    <a onclick="return confirm('Do You Want To Back?')" href="{{URL('/data')}}" class="btn btn-danger">Cancel</a>
                  </div> 
<!--                       <input type="text" name="kode">
                      <input type="text" name="jnis">
                      <input type="text" name="jnis">
                      <input type="text" name="alamat"> -->
                      </form>
                  </div>
                </div>
              </div>  
              </div>
            </div>
@endsection
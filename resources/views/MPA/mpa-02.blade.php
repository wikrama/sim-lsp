@extends('layouts.mimin-login')

@section('content')
	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
  		<div class="container">
  			<div class="col-md-12 top-20 padding-0">
  				<div class="col-md-12">
            <form method="post" action="{{URL('/mpa-02/store')}}">
              {{ csrf_field() }}
    					<div class="panel" style="border-radius: 5px;">
    						<div class="panel-heading">
    							<h4>MENINJAU DAN MENGUJI COBA PERANGKAT ASESMEN</h4>
    						</div>
    						<div class="panel-body Responsive-table">
    							<table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
    								<thead style="background-color: green; color: white;">
    									<tr>
    										<th colspan="2">Meninjau dan menguji coba perangkat  asesmen</th>
    									</tr>
    								</thead>
    								<tbody>
    									<tr>
    										<td class="col-md-6"><strong>INSTRUKSI PENINJAUAN/UJI COBA</strong></td>
    										<td></td>
    									</tr>
    									<tr>
    										<td rowspan="1"><strong>4.1</strong> Periksa konsep perangkat asesmen  berdasarkan kriteria evaluasi dan ubahlah bila perlu</td>
    										<td>Konsep perangkat asesmen :
    											<br><br>
    											<ol>
    												<li>Ceklis Observasi Demonstrasi</li>
    												<li>Tugas Praktek Demonstrasi</li>
    												<li>Daftar Pertanyaan Pilihan Ganda</li>
    											</ol>
    											<br><br><br>
    											<p>Kriteria evaluasi)** :</p> 
                            <div class="form-group">
                              <p>merefleksikan prinsip-prinsip asesmen</p>
                              <input type="radio" name="merefleksikan_prinsip" value="Ya" required> Ya <br>
                              <input type="radio" name="merefleksikan_prinsip" value="Tidak" required> Tidak
                            </div>
                            <div class="form-group">
                              <p>merefleksikan aturan bukti</p>
                              <input type="radio" name="merefleksikan_aturan" value="Ya" required> Ya <br>
                              <input type="radio" name="merefleksikan_aturan" value="Tidak" required> Tidak
                            </div>
                            <div class="form-group">
                              <p>relevan dengan konteks tempat kerja</p>
                              <input type="radio" name="relavan" value="Ya" required> Ya <br>
                              <input type="radio" name="relavan" value="Tidak" required> Tidak
                            </div>
                            <div class="form-group">
                              <p>memiliki isi yang akurat</p>
                              <input type="radio" name="akurat" value="Ya" required> Ya <br>
                              <input type="radio" name="akurat" value="Tidak" required> Tidak
                            </div>
                            <div class="form-group">
                              <p>mudah untuk digunakan</p>
                              <input type="radio" name="mudah" value="Ya" required> Ya <br>
                              <input type="radio" name="mudah" value="Tidak" required> Tidak
                            </div>
                            <div class="form-group">
                              <p>efektif dari segi waktu and biaya bagi peserta sertifikasi dan asesor</p>
                              <input type="radio" name="efektif" value="Ya" required> Ya <br>
                              <input type="radio" name="efektif" value="Tidak" required> Tidak
                            </div>
                            <div class="form-group">
                              <p>menggunakan  bahasa, numerasi dan literasi yang tepat</p>
                              <input type="radio" name="bahasa" value="Ya" required> Ya <br>
                              <input type="radio" name="bahasa" value="Tidak" required> Tidak
                            </div>
    										</td>
    									</tr>
    									<tr>
    										<td rowspan="1"><strong> 4.2</strong> Uji cobalah konsep perangkat asesmen untuk memvalidasi isi dan tingkat kecocokan penggunaannya </td>
    										<td>Uji coba konsep perangkat asesmen yang dilakukan meliputi :
    											<br><br>
    											<ol>
    												<li>Ceklis Observasi Demostrasi</li>
    												<li>Tugas Praktek Demostrasi</li>
    												<li>Daftar Pertanyaan Pilihan Ganda</li>
    											</ol>
    											<br><br>
    											<p>Dan  hasil uji coba  konsep perangkat asesmen (telah memenuhi atau belum memenuhi)* Prinsip-prinsip Asesmen, Aturan Bukti dan Dimensi Kompetensi.</p>
    										</td>
    									</tr>
    									<tr>
    										<td rowspan="1"><strong>4.3</strong> Kumpulkan dan dokumentasi-kan umpan balik dari orang yang relevan  dan terlibat dalam uji coba konsep perangkat asesmen </td>
    										<td>Catatan umpan balik dari (sesama asesor, para manajer, para supervisor, teknisi dan penasehat ahli/ pakar di bidang terkait, termasuk k3,  pakar  bahasa, literasi dan numerasi, koordinator pelatihan dan asesmen, regulator industri, serikat pekerja dan perwakilan pengusaha, para anggota dari asosiasi profesi)* Memerlukan perubahan/ modifikasi ( ya atau tidak)***</td>
    									</tr>
    									<tr>
    										<td rowspan="1"><strong>4.4</strong> Lakukan, perubahan-perubahan terakhir terhadap konsep perangkat asesmen berdasarkan analisis umpan balik, bila perlu.</td>
    										<td>Perangkat asesmen  yang memerlukan perubahan/ modifikasi yaitu:  dari (tes tertulis, tes lisan, ceklis observasi demonstrasi)*** dirubah/dimodifikasi menjadi konsep perangkat asesmen: (tes tertulis, tes lisan, ceklis observasi demonstrasi)*Perangkat asesmen yang dibuat (siap atau tidak) untuk digunakan.</td>
    									</tr>
    									<tr>
    										<td rowspan="1"><strong>4.5</strong> Format dan arsipkan perangkat asesmen yang telah  direvisi dengan benar sesuai kebijakan dan prosedur sistem asesmen serta  persyaratan organisasi/ hukum/etika</td>
    										<td>Perangkat asesemen (tes tertulis, tes lisan, ceklis observasi demonstrasi) *diarsipkan (ya atau tidak)***</td>
    									</tr>
    								</tbody>
    							</table>
    							<p>Catatan :
    								<br>
    								<p> )* jika diperlukan tambahkan nama perangkat yang akan ditinjau dan diuji coba</p>
    								<p> )**  jika memenuhi kriteria evaluasi dan x jika tidak memenuhi kriteria</p>
    								<p>)*** coretlah yang tidak sesuai</p>
    							</p>
    							<br>
    							<div class="panel">
    								<div class="panel-heading">
    									<h5>Uji coba konsep perangkat asesmen melalui peer asesor :</h5>
    								</div>
    								<div class="panel-body">
    									<div class="form-group">
    										<div class="col-md-6">
    											<label>1.</label>
    											<textarea name="1" class="form-control"></textarea>
    										</div>
    										<div class="col-md-6">
    											<label>2.</label>
    											<textarea name="2" class="form-control"></textarea>
    										</div>
    									</div>
    									<div class="form-group">
    										<div class="col-md-6" style="margin-top: 20px;">
    											<label>3.</label>
    											<textarea name="3" class="form-control"></textarea>
    										</div>
    										<div class="col-md-6" style="margin-top: 20px;">
    											<label>4.</label>
    											<textarea name="4" class="form-control"></textarea>
    										</div>
    									</div>
    								</div>
    							</div>
                  <div class="form-group">
                    <button class="btn btn-primary" value="save" style="margin-left: 90%;">Next</button>
                  </div>
    						</div>
    					</div>
            </form>
  				</div>
  			</div>
  		</div>
  	</div>
@endsection
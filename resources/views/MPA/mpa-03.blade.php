@extends('layouts.mimin-login')

@section('content')
	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
  		<div class="container">  			
  			<div class="col-md-12 top-20 padding-0">
  				<div class="col-md-12">
            <form method="post" action="{{URL('/mpa-03/store')}}">
              {{ csrf_field() }}
              <div class="panel" style="border-radius: 5px;">
              <div class="panel-heading">
                <h5>DAFTAR PERTANYAAN TERTULIS – JAWABAN SINGKAT</h5>
              </div>
              <div class="panel-body Responsive-table">
                <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                  <tbody>
                    <tr>
                      <td>Perangkat Asesmen</td>
                      <td>:</td>
                      <td>Daftar Pertanyaan Tertulis – Jawaban Singkat</td>
                    </tr>
                    <tr>
                      <td>Nama peserta sertifikasi</td>
                      <td>:</td>
                      <td>
                        <input type="text" name="nama_peserta_satu" class="form-control" required>
                      </td>
                    </tr>
                    <tr>
                      <td>Nama Asesor</td>
                      <td>:</td>
                      <td>
                        <input type="text" name="nama_asesor_satu" class="form-control" required>
                      </td>
                    </tr>
                    <tr>
                      <td>Kode Unit Kompetensi</td>
                      <td>:</td>
                      <td>
                        <ol>
                          <li>TIK.OP01.002.01</li>
                          <li>J.620100.010.01</li>
                          <li>J.620100.011.01</li>
                          <li>J.620100.012.01</li>
                        </ol>
                      </td>
                    </tr>
                    <tr>
                      <td>Judul Unit Kompetensi</td>
                      <td>:</td>
                      <td>
                        <ol>
                          <li>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</li>
                          <li>Menerapkan perintah eksekusi bahasa pemrograman berbasis teks, grafik, dan multimedia</li>
                          <li>Melakukan instalasi software tools pemrograman</li>
                          <li>Melakukan pengaturan software tools pemrograman</li>
                        </ol>
                      </td>
                    </tr>
                    <tr>
                      <td>Tanggal Uji Kompetensi</td>
                      <td></td>
                      <td>
                        <input type="date" name="tgl_uji_satu" class="form-control" required>
                      </td>
                    </tr>
                    <tr>
                      <td>Waktu</td>
                      <td>:</td>
                      <td>30 Mengidentifikasi</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="panel" style="border-radius: 5px;">
              <div class="panel-heading">
                <h5>Kode Unit: TIK.OP01.002.01</h5>
                <h5>Unit Kompetensi:Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</h5>
              </div>
              <div class="panel-body">
                <table class="table">
                  <thead style="background-color: green; color: white;">
                    <tr>
                      <th rowspan="2" style="padding-bottom: 25px;">KUK</th>
                      <th rowspan="2" style="padding-bottom: 25px;">Pertanyaan</th>
                      <th rowspan="2" style="padding-bottom: 25px;">Jawaban Yang diharapkan</th>
                      <th colspan="3" class="text-center">Keputusan
                        <tr>
                          <th class="text-center">K</th>
                          <th class="text-center">BK</th>
                          <th class="text-center">PL</th>
                        </tr>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.1</td>
                      <td>Sebutkan kepanjangan dari UU ITE?</td>
                      <td>Undang-undang informasi dan transaksi elektronik</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="uu_ite" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="uu_ite" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="uu_ite" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>1.2</td>
                      <td>Sebutkan beberapa contoh konten ilegal?</td>
                      <td>Kesusilaan, perjudian, pencemaran nama baik</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="konten_ilegal" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="konten_ilegal" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="konten_ilegal" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>2.1</td>
                      <td>Apakah fungsi copyright yang selalu tercantum pada aplikasi?</td>
                      <td>Copyright merupakan bentuk perlindungan hak cipta yang diberikan kepada pembuat karya asli</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="copyright" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="copyright" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="copyright" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>2.2</td>
                      <td>Coba sebutkan beberapa bahaya yang mungkin terjadi pada saat terjadi transfer file di dunia maya/internet</td>
                      <td>
                        <li>Konten illegal/virus</li>
                        <li>Pelanggaran hak cipta</li>
                        <li>Pencurian identitas</li>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="maya_internet" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="maya_internet" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="maya_internet" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>2.3</td>
                      <td>Apa yang ketahui tentang freeware?</td>
                      <td>adalah software gratis yang berketentuan mengikuti kehendak penyedia software</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="freeware" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="freeware" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="freeware" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="panel" style="border-radius: 5px;">
              <div class="panel-heading">
                <h5>Kode Unit: J.620100.011.01</h5>
                <h5>Unit Kompetensi: Melakukan instalasi software tools pemrograman</h5>
              </div>
              <div class="panel-body">
                <table class="table">
                  <thead style="background-color: green; color: white;">  
                    <tr>                                        
                  <th rowspan="2" style="padding-bottom: 25px;">KUK</th>
                  <th rowspan="2" style="padding-bottom: 25px;">Pertanyaan</th>
                  <th rowspan="2" style="padding-bottom: 25px;">Jawaban Yang diharapkan</th>
                  <th colspan="3" class="text-center">Keputusan
                    <tr>
                      <th class="text-center">K</th>
                      <th class="text-center">BK</th>
                      <th class="text-center">PL</th>
                    </tr>
                    </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.1</td>
                      <td rowspan="2">Apa sebutkan OS dan bahasa pemrogramam yang anda ketahui!</td>
                      <td>Operating System Windows, Linux, Mac</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="os_satu" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="os_satu" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="os_satu" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>1.2</td>
                      <td>C, C++, PASCAL, JAVA, PHP, VB, ANDROID</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="os_dua" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="os_dua" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="os_dua" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>2.1</td>
                      <td rowspan="2">Sebutkan IDE/Text Editor yang anda ketahui!</td>
                      <td rowspan="2">Netbeans, Sublime, Notepad++</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ide_satu" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ide_satu" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ide_satu" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>                      
                      </td>
                    </tr>
                    <tr>
                      <td>2.2</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ide_dua" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>                      
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ide_dua" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>                      
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ide_dua" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>                        
                      </td>
                    </tr>
                    <tr>
                      <td>3.1</td>
                      <td rowspan="2">Tuliskan source code untuk menampilkan kalimat “SMK BISA” dengan menggunakan bahasa pemrograman PHP!</td>
                      <td rowspan="2"><?php echo "SMK BISA"; ?></td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="smk_bisa" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>                        
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="smk_bisa" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>                      
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="smk_bisa" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>                        
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="panel" style="border-radius: 5px;">
              <div class="panel-heading">
                <h5>Kode Unit: J.620100.012.01</h5>
                <h5>Unit Kompetensi: Melakukan pengaturan software tools pemrograman</h5>
              </div>
              <div class="panel-body">
                <table class="table">
                  <thead style="background-color: green; color: white;">
                    <tr>
                      <th rowspan="2" style="padding-bottom: 25px;">KUK</th>
                      <th rowspan="2" style="padding-bottom: 25px;">Pertanyaan</th>
                      <th rowspan="2" style="padding-bottom: 25px;">Jawaban Yang diharapkan</th>
                      <th colspan="3" class="text-center"> Keputusan
                        <tr>
                          <th>K</th>
                          <th>BK</th>
                          <th>PL</th>
                        </tr>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.1</td>
                      <td>Sebutkan apa saja  yang harus diaktifkan pada Control Panel XAMPP agar web server dan database berjalan!</td>
                      <td>Apache dan Mysql</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="control_panel_xampp" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="control_panel_xampp" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="control_panel_xampp" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>1.2</td>
                      <td rowspan="2">Tuliskan alamat/IP web server lokal dan halaman databse saat kita menggunakan XAMPP!</td>
                      <td rowspan="2">localhost atau 127.0.0.1</td>                     
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="alamat_xampp_satu" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="alamat_xampp_satu" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="alamat_xampp_satu" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>2.1</td>                      
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="alamat_xampp_dua" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="alamat_xampp_dua" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="alamat_xampp_dua" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>2.2</td>
                      <td>Tuliskan perintah SQL untuk menampilkan seluruh data dari tbl_siswa</td>
                      <td>select * from siswa</td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="perintah_sql" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="perintah_sql" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="perintah_sql" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="panel" style="border-radius: 5px;">
              <div class="panel-heading">
                <h5>FR-MPA.02 :  DAFTAR PERTANYAAN TERTULIS – JAWABAN SINGKAT</h5>
              </div>
              <div class="panel-body">
                <table class="table">
                  <tbody>
                    <tr>
                      <td>Perangkat asesmen</td>
                      <td>:</td>
                      <td>Daftar Pertanyaan Tertulis – Jawaban Singkat</td>
                    </tr>
                    <tr>
                      <td>Nama peserta sertifikasi</td>
                      <td>:</td>
                      <td>
                        <input type="text" name="nama_peserta_dua" class="form-control" required>
                      </td>
                    </tr>
                    <tr>
                      <td>Nama asesor</td>
                      <td>:</td>
                      <td>
                        <input type="text" name="nama_asesor_dua" class="form-control" required>
                      </td>
                    </tr>
                    <tr>
                      <td>Kode Unit Kompetensi</td>
                      <td>:</td>
                      <td>
                        <p>5. TIK.OP01.002.01</p>                       
                        <p>6. J.620100.010.01</p>
                        <p>7. J.620100.011.01</p>
                        <p>8. J.620100.012.01</p>
                      </td>
                    </tr>
                    <tr>
                      <td>Judul Unit kompetensi</td>
                      <td>:</td>
                      <td>
                        <p>5. Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</p>
                        <p>6. Menerapkan perintah eksekusi bahasa pemrograman berbasis teks, grafik, dan multimedia</p>
                        <p>7. Melakukan instalasi software tools pemrograman</p>
                        <p>8. Melakukan pengaturan software tools pemrograman</p>
                      </td>
                    </tr>
                    <tr>
                      <td>Tanggal uji kompetensi</td>
                      <td>:</td>
                      <td>
                        <input type="date" name="tgl_uji_dua" class="form-control" required>
                      </td>
                    </tr>
                    <tr>
                      <td>Waktu</td>
                      <td>:</td>
                      <td>30 Menit</td>
                    </tr>
                  </tbody>
                </table>
                <table class="table">
                  <thead style="background-color: green; color: white;">
                    <tr>
                      <th rowspan="2" style="padding-bottom: 25px;">No</th>
                      <th rowspan="2" style="padding-bottom: 25px;">Pertanyaan</th>
                      <th rowspan="2" style="padding-bottom: 25px;">Jawaban Asesi</th>
                      <th colspan="3" class="text-center"> Keputusan
                        <tr>
                          <th>K</th>
                          <th>BK</th>
                          <th>PL</th>
                        </tr>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.</td>
                      <td>Sebutkan kepanjangan dari UU ITE?</td>
                      <td>
                        <textarea name="asesi_uu_ite" class="form-control" required></textarea>          
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_uu_ite" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_uu_ite" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_uu_ite" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>Sebutkan beberapa contoh konten ilegal?</td>
                      <td>                        
                        <textarea name="asesi_konten_ilegal" class="form-control" required></textarea>            
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_konten_ilegal" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_konten_ilegal" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_konten_ilegal" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>Apakah fungsi copyright yang selalu tercantum pada aplikasi?</td>
                      <td>
                        <textarea name="asesi_copyright" class="form-control" required></textarea>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_copyright" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_copyright" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_copyright" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>4.</td>
                      <td>Coba sebutkan beberapa bahaya yang mungkin terjadi pada saat terjadi transfer file di dunia maya/internet</td>
                      <td>
                        <textarea name="asesi_maya_internet" class="form-control" required></textarea>                          
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_maya_internet" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_maya_internet" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_maya_internet" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>5.</td>
                      <td>Apa yang ketahui tentang freeware?</td>
                      <td>
                        <textarea name="asesi_freeware" class="form-control" required></textarea>  
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_freeware" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_freeware" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_freeware" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>6.</td>
                      <td>Apa sebutkan OS dan bahasa pemrogramam yang anda ketahui!</td>
                      <td>
                        <textarea name="asesi_os" class="form-control" required></textarea>  
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_os" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_os" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_os" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>7.</td>
                      <td>Sebutkan IDE/Text Editor yang anda ketahui!</td>
                      <td>
                        <textarea name="asesi_ide" class="form-control" required></textarea>  
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_ide" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_ide" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_ide" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>8.</td>
                      <td>Tuliskan source code untuk menampilkan kalimat “SMK BISA” dengan menggunakan bahasa pemrograman PHP!</td>
                      <td>
                        <textarea name="asesi_smk_bisa" class="form-control" required></textarea>  
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_smk_bisa" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_smk_bisa" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_smk_bisa" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>9.</td>
                      <td>Sebutkan apa saja  yang harus diaktifkan pada Control Panel XAMPP agar web server dan database berjalan!</td>
                      <td>
                        <textarea name="asesi_control_panel_xampp" class="form-control" required></textarea>  
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_control_panel_xampp" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_control_panel_xampp" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_control_panel_xampp" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>10.</td>
                      <td>Tuliskan alamat/IP web server lokal dan halaman database saat kita menggunakan XAMPP!</td>
                      <td>
                        <textarea name="asesi_alamat_xampp" class="form-control" required></textarea>  
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_alamat_xampp" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_alamat_xampp" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>                        
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_alamat_xampp" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td>11.</td>
                      <td>Tuliskan perintah SQL untuk menampilkan seluruh data dari tbl_siswa</td>
                      <td>                      
                        <textarea name="asesi_perintah_sql" class="form-control" required></textarea>            
                      </td>
                      <td>
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_perintah_sql" value="K" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>                        
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_perintah_sql" value="BK" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                      <td>                        
                        <div class="form-animate-radio">
                          <label class="radio">
                            <input id="radio1" type="radio" name="keputusan_perintah_sql" value="PL" required />
                            <span class="outer">
                              <span class="inner"></span>
                            </span>
                            </label>
                          </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="panel" style="border-radius: 5px;">
              <div class="panel-body">                          
                <table class="table">
                  <div class="form-group">
                    <div class="col-md-6 panel" style="padding: 20px; padding-bottom: 0px;">
                      <div class="form-animate-radio">
                        <label class="radio">
                          <input id="radio1" type="radio" name="keputusan" value="Kompeten" required />
                          <span class="outer">
                            <span class="inner"></span>
                          </span>Kompeten
                        </label>
                      </div>
                    </div>                    
                    <div class="col-md-6 panel" style="padding: 20px; padding-bottom: 0px;">
                      <div class="form-animate-radio">
                        <label class="radio">
                          <input id="radio2" type="radio" name="keputusan" value="Belum Kompeten" required>
                          <span class="outer">
                            <span class="inner"></span>
                          </span>Belum Kompeten
                        </label>
                      </div>
                    </div>
                  </div>
                  <thead style="background-color: green; color: white;">
                    <tr>
                      <th class="text-center">Assesor</th>
                      <th class="text-center">Assesi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="col-md-6">
                        <textarea name="assesor" class="form-control" required></textarea>          
                      </td>
                      <td class="col-md-6">
                        <textarea name="assesi" class="form-control" required></textarea>          
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="form-group">
              <button value="save" class="btn btn-primary" style="margin-left: 90%;">Next</button>
            </div>
            </form>  					
  				</div>
  			</div>
  		</div>
  	</div>
@endsection
@extends('layouts.mimin-login')

@section('content')	
	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
  		<div class="container">
  			<div class="col-md-12 top-20 padding-0">
  				<div class="col-md-12">
            <form method="post" action="{{URL('/mpa-05/store')}}">              
              {{ csrf_field() }}
              <div class="panel" style="border-radius: 5px;">
                <div class="panel-heading">
                  <h5>DAFTAR CEK OBSERVASI-DEMONSTRASI/PRAKTEK</h5>
                </div>
                <div class="panel-body Responsive-table">
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>
                      <tr>
                        <td>Perangkat Asesmen</td>
                        <td>:</td>
                        <td>Daftar Cek Observasi – Demonstrasi</td>
                      </tr>
                      <tr>
                        <td>Nama peserta sertifikasi</td>
                        <td>:</td>
                        <td>
                          <input type="text" name="nama_peserta_satu" class="form-control">          
                        </td>
                      </tr>
                      <tr>
                        <td>Nama asesor</td>
                        <td>:</td>
                        <td>
                          <input type="text" name="nama_asesor_satu" class="form-control">          
                        </td>
                      </tr>
                      <tr>
                        <td>Kode Unit kompetensi</td>
                        <td>:</td>
                        <td>
                          <ol>
                            <li>LOG.OO01.002.01</li>
                            <li>LOG.OO01.004.01</li>
                            <li>TIK.OP01.002.01</li>
                            <li>J.620100.004.02</li>
                            <li>J.620100.005.02</li>
                            <li>J.620100.011.01</li>
                            <li>J.620100.012.01</li>
                            <li>J.620100.017.02</li>
                            <li>J.620100.022.02</li>
                            <li>J.620100.025.02</li>
                          </ol>
                        </td>
                      </tr>
                      <tr>
                        <td>Judul Unit kompetensi</td>
                        <td>:</td>
                        <td>
                          <ol>
                            <li>Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</li>
                            <li>Merencanakan tugas rutin</li>
                            <li>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</li>
                            <li>Menggunakan struktur data</li>
                            <li>Mengimplementasikan user interface</li>
                            <li>Melakukan instalasi software tools pemrograman</li>
                            <li>Melakukan pengaturan software tools pemrograman</li>
                            <li>Mengimplementasikan pemrograman terstruktur</li>
                            <li>Mengimplementasikan algoritma pemrograman</li>
                            <li>Melakukan debugging</li>
                          </ol>
                        </td>
                      </tr>
                      <tr>
                        <td>Tanggal uji Kompetensi</td>
                        <td>:</td>
                        <td>
                          <input type="date" name="tgl_uji_satu" class="form-control">          
                        </td>
                      </tr>
                      <tr>
                        <td>Waktu</td>
                        <td>:</td>
                        <td>180 menit</td>
                      </tr>
                    </tbody>
                  </table>
                  <p><i><strong>Setiap tugas / instruksi harus terkait dengan elemen/kuk</strong></i></p>
                </div>
              </div>
              <div class="panel" style="border-radius: 5px;">
                <div class="panel-heading">
                  <h5>1. Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Melaksanakan kerja dilaksanakan dengan aman sehubungan dengan kebijakan dan prosedur perusahaan serta persyaratan perundang-undangan</td>
                        <td>Ketepatan dalam melaksanakan kerja dengan aman sehubungan dengan kebijakan dan prosedur perusahaan serta persyaratan perundang-undangan</td>
                        <td>                        
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kerja_aman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>          
                        </td>
                        <td>                        
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kerja_aman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>          
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kerja_aman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kerja_aman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.2</td>
                        <td>Melakukan kegiatan rumah tangga perusahaan sesuai dengan prosedur perusahaan</td>
                        <td>Ketepatan dalam melakukan kegiatan rumah tangga perusahaan sesuai dengan prosedur perusahaan</td>
                        <td>                        
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kegiatan_rumah" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>          
                        </td>
                        <td>                        
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kegiatan_rumah" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>          
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kegiatan_rumah" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kegiatan_rumah" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.3</td>
                        <td>Mengerti dan mendemonstrasikan tanggung jawab dan tugas-tugas karyawan dalam kegiatan sehari-hari</td>
                        <td>Kemampuan dalam mengerti dan demonstrasikan tanggung jawab dan tugas-tugas karyawan dalam kegiatan sehari-hari</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_demostrasi_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_demostrasi_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_demostrasi_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_demostrasi_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.4</td>
                        <td>Memakai dan menyimpan perlengkapan pelindung diri sesuai dengan prosedur perusahaan</td>
                        <td>Kemampuan dalam memakai dan menyimpan perlengkapan pelindung diri sesuai dengan prosedur perusahaan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_pelindung_diri" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_pelindung_diri" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_pelindung_diri" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_pelindung_diri" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.5</td>
                        <td>Menggunakan semua perlengkapan dan alat-alat keselamatan sesuai dengan persyaratan perundang-undangan dan prosedur perusahaan</td>
                        <td>Ketepatan dalam menggunakan  semua perlengkapan dan alat-alat keselamatan sesuai dengan persyaratan perundang-undangan dan prosedur perusahaan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_keselamatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_keselamatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_keselamatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_keselamatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.6</td>
                        <td>Mengenali dan mengikuti Tanda-tanda/simbol sesuai instruksi</td>
                        <td>Ketepatan dalam mengenali dan mengikuti Tanda-tanda/simbol sesuai instruksi</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tanda" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tanda" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tanda" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tanda" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.7</td>
                        <td>Melaksanakan semua pedoman penanganan sesuai dengan persyaratan, prosedur perusahaan dan pedoman Komisi Kesehatan dan Keselamatan Kerja Nasional yang sah</td>
                        <td>Ketepatan dalam melaksanakan semua pedoman penanganan sesuai dengan persyaratan, prosedur perusahaan dan pedoman Komisi Kesehatan dan Keselamatan Kerja Nasional yang sah</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_pedoman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_pedoman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_pedoman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_pedoman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.8</td>
                        <td>Mengenali dan mendemonstrasikan perlengkapan darurat dengan tepat</td>
                        <td>Kemampuan dalam mengenali dan mendemonstrasikan perlengkapan darurat dengan tepat</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_demostrasi_darurat" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_demostrasi_darurat" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_demostrasi_darurat" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_demostrasi_darurat" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Mengenali dan melaporkan bahaya-bahaya di tempat kerja selama waktu kerja kepada orang yang tepat sesuai dengan prosedur pengoperasian standar</td>
                        <td>Kemampuan dalam megenali dan melaporkan bahaya-bahaya di tempat kerja selama waktu kerja kepada orang yang tepat sesuai dengan prosedur pengoperasian standar</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_laporan_bahaya" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_laporan_bahaya" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_laporan_bahaya" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_laporan_bahaya" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.1</td>
                        <td>Mendemonstrasikan cara-cara menghubungi personil yang tepat dan layanan darurat jika terjadi kecelakaan</td>
                        <td>Kemampuan dalam mendemonstrasikan cara-cara menghubungi personil yang tepat dan layanan darurat jika terjadi kecelakaan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_demostrasi_personil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_demostrasi_personil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_demostrasi_personil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_demostrasi_personil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.2</td>
                        <td>Mengerti dan melaksanakan prosedur kondisi darurat dan evakuasi (pengungsian) bila diperlukan</td>
                        <td>Kemampuan dalam memahami dan melaksanakan bila diperlukan prosedur kondisi darurat dan evakuasi (pengungsian)</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_prosedur_kondisi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_prosedur_kondisi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_prosedur_kondisi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_prosedur_kondisi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>2. Merencanakan tugas rutin</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Memperoleh, mengerti, dan menjelaskan bila perlu instruksi-instruksi tentang prosedur</td>
                        <td>Ketepatan dalam memperoleh, memahami dan menjelaskan instruksi-instruksi tentang prosedur</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_intruksi_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_intruksi_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_intruksi_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_intruksi_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.2</td>
                        <td>Mendapatkan, memahami dan menjelaskan bila perlu spesifikasi yang relevan terhadap hasil-hasil tugas </td>
                        <td>Ketepatan dalam mendapatkan, memahami, dan menjelaskan spesifikasi yang relevan terhadap hasil-hasil tugas</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_spesifikasi_relavan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_spesifikasi_relavan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_spesifikasi_relavan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_spesifikasi_relavan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.3</td>
                        <td>Mengenali hasil-hasil tugas </td>
                        <td>Ketepatan dalam mengenali hasil-hasil tugas</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_hasil_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_hasil_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_hasil_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_hasil_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.4</td>
                        <td>Mengenali syarat-syarat tugas seperti waktu penyelesaian dan ukuran kualitas</td>
                        <td>Ketepatan dalam mengenali syarat-syarat tugas seperti waktu penyelesaian dan ukuran kualitas</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_syarat_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_syarat_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_syarat_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_syarat_tugas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Memahami dan menjelaskan bila perlu langkah-langkah atau kegiatan-kegiatan individu yang diperlukan untuk melaksanakan tugas, berdasarkan instruksi-instruksi dan spesifikasi-spesifikasi yang ada</td>
                        <td>Ketepatan dalam memahami dan menjelaskan langkah-langkah atau kegiatan-kegiatan individu yang diperlukan untuk melaksanakan tugas, berdasarkan instruksi-instruksi dan spesifikasi-spesifikasi yang ada</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kegiatan_individu" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kegiatan_individu" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kegiatan_individu" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kegiatan_individu" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.2</td>
                        <td>Mencantumkan rangkaian kegiatan yang perlu diselesaikan dalam rencana</td>
                        <td>Ketepatan dalam mencantumkan rangkaian kegiatan yang perlu diselesaikan dalam rencana</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mencantumkan_kegiatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mencantumkan_kegiatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mencantumkan_kegiatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mencantumkan_kegiatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.3</td>
                        <td>Memeriksa langkah-langkah dan hasil yang direncanakan untuk menjamin kesesuaian dengan instruksi-instruksi dan spesifikasi-spesifikasi yang relevan</td>
                        <td>Ketepatan dalam memeriksa langkah-langkah dan hasil yang direncanakan untuk menjamin kesesuaian dengan instruksi-instruksi dan spesifikasi-spesifikasi yang relevan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_memeriksa_hasil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_memeriksa_hasil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_memeriksa_hasil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_memeriksa_hasil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.1</td>
                        <td>Mengenali dan membandingkan hasil-hasil dengan sasaran-sasaran (yang direncanakan) instruksi-instruksi tugas, spesifikasi-spesifikasi dan syarat-syarat tugas</td>
                        <td>Ketepatan dalam mengenali dan membandingkan hasil-hasil dengan sasaran-sasaran (yang direncanakan) instruksi-instruksi tugas, spesifikasi-spesifikasi dan syarat-syarat tugas</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_membandingkan_hasil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_membandingkan_hasil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_membandingkan_hasil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_membandingkan_hasil" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.2</td>
                        <td>Jika perlu, Memperbaiki rencana untuk memenuhi sasaran-sasaran dan syarat-syarat tugas yang lebih baik</td>
                        <td>Ketepatan dalam memperbaiki rencana untuk memenuhi sasaran-sasaran dan syarat-syarat tugas yang lebih baik</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_memperbaiki_rencana" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_memperbaiki_rencana" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_memperbaiki_rencana" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_memperbaiki_rencana" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>3. Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">                    
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Identifikasi norma yang berlaku di dunia TIK </td>
                        <td>Ketepatan dalam mematuhi aturan dan norma yang berlaku di dunia TIK</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_identifikasi_norma" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_identifikasi_norma" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_identifikasi_norma" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_identifikasi_norma" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.2</td>
                        <td>Memahami spek legas atas dokumen elektronik hasil karya orang lain</td>
                        <td>Ketepatan dalam memahami spek legas atas dokumen elektronik hasil karya orang lain</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_spek_legas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_spek_legas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_spek_legas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_spek_legas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Memahami copyright piranti lunak dan isu hukum yang berkaitan dengan menggandakan dan membagi file</td>
                        <td>Ketepatan dalam memahami copyright piranti lunak dan isu hukum yang berkaitan dengan menggandakan dan membagi file</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_copyright_lunak" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_copyright_lunak" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_copyright_lunak" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_copyright_lunak" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.2</td>
                        <td>Memahami akibat yang dapat terjadi jika bertukar file pada suatu jaringan internet</td>
                        <td>Ketepatan dalam memahami akibat yang dapat terjadi jika bertukar file pada suatu jaringan internet</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_bertukar_file" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_bertukar_file" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_bertukar_file" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_bertukar_file" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.3</td>
                        <td>Memahami dan mengenali istilah-istilah shareware, freeware dan user license</td>
                        <td>Ketepatan dalam memahami dan mengenali istilah-istilah shareware, freeware dan user license</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_shareware" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_shareware" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_shareware" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_shareware" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>4. Menggunakan struktur data</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Mengidentifikasi konsep data dan struktur data sesuai dengan konteks permasalahan</td>
                        <td>Ketepatan dalam mengidentifikasi konsep data dan struktur data sesuai dengan konteks permasalahan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_konsep_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_konsep_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_konsep_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_konsep_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.2</td>
                        <td>Membandingkan kelebihan dan kekurangan alternatif struktur data untuk konteks permasalahan yang diselesaikan</td>
                        <td>Kemampuan dalam membandingkan kelebihan dan kekurangan alternatif struktur data untuk konteks permasalahan yang diselesaikan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_perbandingan_kelebihan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_perbandingan_kelebihan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_perbandingan_kelebihan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_perbandingan_kelebihan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Mengimplementasikan struktur data sesuai dengan bahasa pemrograman yang akan dipergunakan</td>
                        <td>Ketepatan dalam mengimplementasikan struktur data sesuai dengan bahasa pemrograman yang akan dipergunakan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mengimplementasikan_struktur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mengimplementasikan_struktur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mengimplementasikan_struktur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mengimplementasikan_struktur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.2</td>
                        <td>Menyatakan akses terhadap data dalam algoritma yang efisiensi sesuai bahasa pemrograman yang akan di pakai</td>
                        <td>Ketepatan dalam menyatakan akses terhadap data dalam algoritma yang efisiensi sesuai bahasa pemrograman yang akan dipakai</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_menyatakan_akses" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_menyatakan_akses" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_menyatakan_akses" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_menyatakan_akses" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>5. Mengimplementasikan user interface</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Identifikasi rancangan user interface sesuai kebutuhan</td>
                        <td>Mengidentifikasi rancangan user interface sesuai kebutuhan dengan baik</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_identifikasi_rancangan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_identifikasi_rancangan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_identifikasi_rancangan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_identifikasi_rancangan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.2</td>
                        <td>Identifikasi komponen user interface dialog sesuai konteks rancangan proses</td>
                        <td>Mengidentifikasi atau memilih komponen user interface yang digunakan untuk dialog sesuai konteks rancangan proses</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_identifikasi_komponen" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_identifikasi_komponen" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_identifikasi_komponen" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_identifikasi_komponen" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.4</td>
                        <td>Buat simulasi (mock-up) dari aplikasi yang akan dikembangkan </td>
                        <td>Membuat simulasi (mock-up) dari aplikasi yang akan dikembangkan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_buat_simulasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_buat_simulasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_buat_simulasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_buat_simulasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Terapkan menu program sesuai dengan rancangan program </td>
                        <td>Menerapkan menu program sesuai dengan rancangan program</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_terapkan_menu" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_terapkan_menu" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_terapkan_menu" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_terapkan_menu" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.2</td>
                        <td>Atur penempatan user interface dialog secara sekuensial</td>
                        <td>Mengatur penempatan user interface dialog secara sekuensial</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_atur_penempatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_atur_penempatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_atur_penempatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_atur_penempatan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.3</td>
                        <td>Sesuaikan setting aktif-pasif komponen user interface dialog dengan urutan alur proses</td>
                        <td>Menyesuaikan setting aktif-pasif komponen user interface dialog dengan urutan alur proses</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_sesuaikan_setting" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_sesuaikan_setting" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_sesuaikan_setting" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_sesuaikan_setting" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.4</td>
                        <td>Bentuk style dari komponen user interface ditentukan</td>
                        <td>Membentuk style dari komponen user interface ditentukan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_bentuk_style" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_bentuk_style" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_bentuk_style" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_bentuk_style" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.5</td>
                        <td>Jadikan penerapan simulasi suatu proses yang sesungguhnya</td>
                        <td>Menerapkan simulasi dari suatu proses dengan benar</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_penerapan_simulasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_penerapan_simulasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_penerapan_simulasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_penerapan_simulasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>6. Melakukan instalasi software tools pemrograman</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Identifikasi platform (lingkungan) yang akan digunakan untuk menjalankan tools pemrograman sesuai dengan kebutuhan</td>
                        <td>Mengidentifikasi platform (lingkungan) yang akan digunakan untuk menjalankan tools pemrograman sesuai dengan kebutuhan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_identifikasi_platform" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_identifikasi_platform" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_identifikasi_platform" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_identifikasi_platform" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Install tools pemrogaman sesuai dengan prosedur</td>
                        <td>Menginstall tools/perangkat bahasa pemrogaman sesuai dengan prosedur/ SOP yang berlaku</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_install_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_install_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_install_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_install_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.2</td>
                        <td>Jalankan tools pemrograman bisa di lingkungan pengembangan yang telah ditetapkan</td>
                        <td>Menjalankan tools pemrograman yang telah di install sesuai dengan platform yang telah ditetapkan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_jalankan_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_jalankan_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_jalankan_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_jalankan_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.1</td>
                        <td>Buat Script (source code) sederhana sesuai tools pemrogaman yang di-install</td>
                        <td>Membuat Script (source code) sederhana sesuai dengan tools bahasa pemrogaman yang di-install</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_buat_script" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_buat_script" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_buat_script" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_buat_script" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.2</td>
                        <td>Jalankan script dapat  dengan benar dan menghasilkan keluaran sesuai scenario yang diharapkan</td>
                        <td>Menjalankan script dapat  dengan benar dan menghasilkan keluaran sesuai scenario yang diharapkan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_Jalankan_script" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_Jalankan_script" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_Jalankan_script" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_Jalankan_script" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>7. Melakukan pengaturan software tools pemrograman</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">                    
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Tentukan hasil dari konfigurasi</td>
                        <td>Menentukan target hasil dari konfigurasi</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_hasil_konfigurasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_hasil_konfigurasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_hasil_konfigurasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_hasil_konfigurasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.2</td>
                        <td>Periksa tools pemrograman setelah dikonfigurasikan, tetap bisa digunakan sebagaimana mestinya</td>
                        <td>Memeriksa tools pemrograman setelah dikonfigurasikan, tetap bisa digunakan sebagaimana mestinya</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tools_pemograman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tools_pemograman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tools_pemograman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tools_pemograman" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Kuasai fitur-fitur dasar untuk pembuatan program</td>
                        <td>Menguasai fitur-fitur dasar untuk pembuatan program</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_fitur_dasar" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_fitur_dasar" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_fitur_dasar" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_fitur_dasar" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.2</td>
                        <td>Kuasai fitur-fitur dasar tools untuk pembuatan program</td>
                        <td>Menguasai fitur-fitur dasar tools untuk pembuatan program</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_fitur_dasar_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_fitur_dasar_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_fitur_dasar_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_fitur_dasar_tools" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>8. Mengimplementasikan pemrograman terstruktur</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Tipe data yang sesuai standar ditentukan</td>
                        <td>membuat source code dengan tipe data sesuai kebutuhan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tipe_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tipe_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tipe_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
      .                 <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tipe_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.2</td>
                        <td>Syntax program yang dikuasai digunakan sesuai standar</td>
                        <td>membuat source code dengan sintax yang benar</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_Syntax_program" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_Syntax_program" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_Syntax_program" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_Syntax_program" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.3</td>
                        <td>Struktur kontrol program yang dikuasai digunakan sesuai standar.</td>
                        <td>membuat coding program menggunakan struktur kontrol</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_struktur_kontrol_program" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_struktur_kontrol_program" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_struktur_kontrol_program" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_struktur_kontrol_program" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Program baca tulis untuk memasukkan data dari keyboard dan menampilkan ke layar monitor termasuk variasinya sesuai standar masukan/keluaran telah dibuat  membuat aplikasi sederhana dengan input user dan output hasil pengolahan data</td>
                        <td>membuat aplikasi sederhana dengan input user dan output hasil pengolahan data</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_program_baca" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_program_baca" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_program_baca" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_program_baca" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.2</td>
                        <td>Struktur kontrol percabangan dan pengulangan dalam membuat program telah digunakan</td>
                        <td>membuat code program menggunakan struktur kontrol dan pengulangan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_struktur_kontrol_percabangan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_struktur_kontrol_percabangan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_struktur_kontrol_percabangan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_struktur_kontrol_percabangan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.1</td>
                        <td>Program dengan menggunakan prosedur dibuat sesuai aturan penulisan program</td>
                        <td>membuat code program menggunakan procedure</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_program_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_program_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_program_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_program_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.2</td>
                        <td>Program dengan menggunakan fungsi dibuat sesuai aturan penulisan program</td>
                        <td>membuat code program menggunakan function</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_program_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_program_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_program_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_program_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.3</td>
                        <td>Program dengan menggunakan prosedur dan fungsi secara bersamaan dibuat sesuai aturan penulisan program</td>
                        <td>membuat code program menggunakan function dan procedure dalam sebuah proses</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_program_prosedur_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_program_prosedur_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_program_prosedur_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_program_prosedur_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.1</td>
                        <td>Tipe data yang sesuai standar ditentukan</td>
                        <td>membuat source code dengan tipe data sesuai kebutuhan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tipe_data_standar" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tipe_data_standar" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tipe_data_standar" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tipe_data_standar" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>4.1</td>
                        <td>Dimensi array telah ditentukan</td>
                        <td>membuat code program menggunakan minimal array 1 dimensi</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_dimensi_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_dimensi_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_dimensi_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_dimensi_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>4.2</td>
                        <td>Tipe data array telah ditentukan</td>
                        <td>membuat code program menggunakan salah satu tipe data</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tipe_data_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tipe_data_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tipe_data_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tipe_data_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>4.3</td>
                        <td>Panjang array telah ditentukan</td>
                        <td>membuat array dengan panjang data sesuai kebutuhan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_panjang_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_panjang_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_panjang_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_panjang_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>4.4</td>
                        <td>Pengurutan array telah digunakan</td>
                        <td>membuat pengurutan data menggunakan array</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_pengurutan_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_pengurutan_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_pengurutan_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_pengurutan_array" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>5.1</td>
                        <td>Program untuk menulis data dalam media penyimpan telah dibuat</td>
                        <td>menyimpan data hasil aplikasi kedalam file txt</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_menulis_data_media" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_menulis_data_media" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_menulis_data_media" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_menulis_data_media" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>5.2</td>
                        <td>Program untuk membaca data dari media penyimpan telah dibuat</td>
                        <td>membuka/mengambil data dari file txt</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_membaca_data_media" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_membaca_data_media" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_membaca_data_media" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_membaca_data_media" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>6.1</td>
                        <td>Kesalahan program telah dikoreksi</td>
                        <td>membuat penanganan error pada source code program</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kesalahan_koreksi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kesalahan_koreksi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kesalahan_koreksi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kesalahan_koreksi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>6.2</td>
                        <td>Kesalahan syntax dalam program telah dibebaskan</td>
                        <td>membuat penanganan error untuk kesalahan penulisan syntax</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kesalahan_syntax" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_kesalahan_syntax" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kesalahan_syntax" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_kesalahan_syntax" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>9.  Mengimplementasikan algoritma pemrograman</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">                    
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Menjelaskan tipe data sesuai kaidah pemrograman</td>
                        <td>Ketepatan dalam menjelaskan tipe data sesuai kaidah pemrograman</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tipe_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_tipe_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tipe_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_tipe_data" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Menjelaskan variabel sesuai kaidah pemrograman</td>
                        <td>Ketepatan dalam menjelaskan variabel sesuai kaidah pemrograman</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_variabel" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_variabel" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_variabel" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_variabel" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.3</td>
                        <td>menjelaskan konstanta sesuai kaidah pemrograman</td>
                        <td>Ketepatan dalam menjelaskan konstanta sesuai kaidah pemrograman </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_konstansta" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_konstansta" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_konstansta" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_konstansta" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Menentukan metode yang sesuai</td>
                        <td>Ketepatan dalam menentukan metode yang sesuai</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_metode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_metode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_metode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_metode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.2</td>
                        <td>Menentukan komponen yang dibutuhkan</td>
                        <td>Ketepatan dalam menentukan komponen yang dibutuhkan </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_komponen" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_komponen" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_komponen" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_komponen" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.3</td>
                        <td>Menetapkan relasi antar komponen</td>
                        <td>Kemampuan dalam menetapkan relasi antar komponen</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_relasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_relasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_relasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_relasi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.4</td>
                        <td>Menetapkan alur mulai dan selesai </td>
                        <td>Kemampuan dalam menetapkan alur mulai dan selesai</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_alur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_alur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_alur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_alur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.1</td>
                        <td>Membuat Algoritma untuk sorting</td>
                        <td>Katepatan dalam membuat Algoritma untuk sorting</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_algoritma_sorting" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_algoritma_sorting" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_algoritma_sorting" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_algoritma_sorting" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.2</td>
                        <td>Membuat Algoritma untuk searching</td>
                        <td>Ketepatan dalam membuat Algoritma untuk searching</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_algoritma_searching" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_algoritma_searching" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_algoritma_searching" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_algoritma_searching" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>4.1</td>
                        <td>Mengidentifikasi konsep penggunaan kembali prosedur dan fungsi </td>
                        <td>Ketepatan dalam mengidentifikasi konsep penggunaan kembali prosedur dan fungsi</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mengidentifikasi_konsep" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mengidentifikasi_konsep" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mengidentifikasi_konsep" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mengidentifikasi_konsep" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>4.2</td>
                        <td>Menggunakan prosedur </td>
                        <td>Ketepatan dalam menggunakan prosedure</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_prosedur" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>5.1</td>
                        <td>Menggunakan Fungsi</td>
                        <td>Ketepatan dalam menggunakan fungsi</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_fungsi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>5.2</td>
                        <td>Mengidentifikasi kompleksitas penggunaan memory algoritma</td>
                        <td>Ketepatan dalam mengidentifikasi kompleksitas penggunaan memory algoritma</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mengidentifikasi_kompleksitas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mengidentifikasi_kompleksitas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mengidentifikasi_kompleksitas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mengidentifikasi_kompleksitas" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>10. Melakukan debugging</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <thead style="background-color: green; color: white;">
                      <tr>
                        <th rowspan="2" style="padding-bottom: 25px;">No.KUK</th>
                        <th rowspan="2" style="padding-bottom: 25px;">Daftar tugas/instruksi </th>
                        <th rowspan="2" style="padding-bottom: 25px;">Poin yang dicek/ diobservasi</th>
                        <th colspan="2" class="text-center">Pencapaian
                          <th colspan="2" class="text-center"> Penilaian</th>
                          <tr>
                            <th class="text-center">Ya</th>
                            <th class="text-center">tidak</th>
                            <th class="text-center">K</th>
                            <th class="text-center">BK</th>
                          </tr>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1.1</td>
                        <td>Menyiapkan kode program sesuai spesifikasi</td>
                        <td>Ketepatan dalam menyiapkan kode program sesuai spesifikasi</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_penyiapan_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_penyiapan_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_penyiapan_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_penyiapan_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>1.2</td>
                        <td>Mempersiapkan debugging tools untuk melihat proses suatu modul </td>
                        <td>Kemampuan dalam mempersiapkan debugging tools untuk melihat proses suatu modul</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_penyiapan_debugging" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_penyiapan_debugging" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_penyiapan_debugging" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_penyiapan_debugging" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.1</td>
                        <td>Mengkompilasi kode program sesuai bahasa pemrograman yang digunakan</td>
                        <td>Kemampuan dalam mengkompilasi kode program sesuai bahasa pemrograman yang digunakan</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mengkompilasi_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mengkompilasi_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mengkompilasi_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mengkompilasi_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.2</td>
                        <td>Menganalisis kriteria lulus build</td>
                        <td>Kemampuan dalam menganalisis kriteria lulus build</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_menganalisis_kriteria_lulus" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_menganalisis_kriteria_lulus" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_menganalisis_kriteria_lulus" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_menganalisis_kriteria_lulus" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.3</td>
                        <td>Menganalisis kriteria eksekusi aplikasi </td>
                        <td>Ketepatan dalam menganalisis kriteria eksekusi aplikasi</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_menganalisis_kriteria_eksekusi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_menganalisis_kriteria_eksekusi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_menganalisis_kriteria_eksekusi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_menganalisis_kriteria_eksekusi" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2.4</td>
                        <td>Mencatat kode kesalahan </td>
                        <td>Ketepatan dalam mencatat kode kesalahan </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mencatat_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_mencatat_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mencatat_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_mencatat_kode" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.1</td>
                        <td>Merumuskan perbaikan terhadap kesalahan kompilasi maupun build </td>
                        <td>Ketepatan dalam merumuskan perbaikan terhadap kesalahan kompilasi maupun build</td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_merumuskan_perbaikan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_merumuskan_perbaikan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_merumuskan_perbaikan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_merumuskan_perbaikan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3.2</td>
                        <td>Melakukan Perbaikan </td>
                        <td>Ketepatan dalam melakukan perbaikan </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_melakukan_perbaikan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="pencapaian_melakukan_perbaikan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_melakukan_perbaikan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="form-animate-radio">
                            <label class="radio">
                              <input id="radio1" type="radio" name="penilaian_melakukan_perbaikan" value="K" required />
                              <span class="outer">
                                <span class="inner"></span>
                              </span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>TUGAS PRAKTEK-DEMONSTRASI/PRAKTEK</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td>Perangkat asesmen</td>
                        <td>:</td>
                        <td>Tugas  Praktek /Demonstrasi (Paket 1)</td>
                      </tr>
                      <tr>
                        <td>Nama peserta sertifikasi</td>
                        <td>:</td>
                        <td>
                          <input type="text" name="nama_peserta_dua" class="form-control">
                        </td>
                      </tr>
                      <tr>
                        <td>Nama asesor</td>
                        <td>:</td>
                        <td>
                          <input type="text" name="nama_assesor_dua" class="form-control">
                        </td>
                      </tr>
                      <tr>
                        <td>Kode unit kompetensi</td>
                        <td>:</td>
                        <td>
                          <ol>
                            <li>LOG.OO01.002.01</li>
                            <li>LOG.OO01.004.01</li>
                            <li>TIK.OP01.002.01</li>
                            <li>J.620100.004.02</li>
                            <li>J.620100.005.02</li>
                            <li>J.620100.011.01</li>
                            <li>J.620100.012.01</li>
                            <li>J.620100.017.02</li>
                            <li>J.620100.022.02</li>
                            <li>J.620100.025.02</li>
                          </ol>
                        </td>
                      </tr>
                      <tr>
                        <td>Judul unit kompetensi</td>
                        <td>:</td>
                        <td>
                          <ol>
                            <li>Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</li>
                            <li>Merencanakan tugas rutin</li>
                            <li>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</li>
                            <li>Menggunakan struktur data</li>
                            <li>Mengimplementasikan user interface</li>
                            <li>Melakukan instalasi software tools pemrograman</li>
                            <li>Melakukan pengaturan software tools pemrograman</li>
                            <li>Mengimplementasikan pemrograman terstruktur</li>
                            <li>Mengimplementasikan algoritma pemrograman</li>
                            <li>Melakukan debugging</li>
                          </ol>
                        </td>
                      </tr>
                      <tr>
                        <td>Tanggal uji kompetensi</td>
                        <td>:</td>
                        <td>
                          <input type="date" name="tgl_uji_dua" class="form-control">
                        </td>
                      </tr>
                      <tr>
                        <td>Waktu</td>
                        <td>:</td>
                        <td>180 menit</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="form-group">                  
                    <h4>A. PETUNJUK UMUM</h4>
                    <ol>
                      <li>Periksalah dengan teliti dokumen soal ujian praktik.</li>
                      <li>Baca dan pahami maksud soal agar tidak terjadi kesalahan pekerjaan.</li>
                      <li>Bekerjalah dengan memperhatikan jadwal dengan alur pengerjaan</li>
                      <li>Peralatan utama dan bahan telah disediakan sesuai dengan kebutuhan.</li>
                      <li>Dalam bekerja selalu memperhatikan keselamatan kerja</li>
                    </ol>
                  </div>
                  <div class="form-group">
                    <h4>B. SOAL/TUGAS</h4>
                    <ol>
                      <li>Judul Tugas : Aplikasi Inventaris Sarana dan Prasarana di SMK</li>
                      <li>Langkah Kerja
                        <p>a. Lakukan Instalasi dan Pengaturan Software Tools Pemrograman</p>
                        <p>b. Interpretasikan Spesifikasi Program yang diberikan
                          <ul>
                            <p>i. Aplikasi berbasis sistem client-server</p>
                            <p>ii.  Aplikasi dapat dibuat berbasis web, atau perpaduan web & perangkat bergerak</p>
                            <p>iii. Aplikasi menggunakan pembagian privilege dengan tingkatan (administrator, operator, peminjam)</p>
                          </ul>                        
                        </p>
                        <p>c. Buatlah Desain User Interface / wireframe (Menu, Form Modifikasi Data, dan Pelaporan)
                          <ul>
                            <p>i. Fitur minimal dan pembagian privilege dalam aplikasi
                              <ul>                              
                                <table>
                                  <tbody>
                                    <tr>
                                      <td>Fitur</td>
                                      <td>Administrator</td>
                                      <td>operator</td>
                                      <td>Peminjam</td>
                                    </tr>
                                    <tr>
                                      <td>Login</td>
                                      <td>X</td>
                                      <td>X</td>
                                      <td>X</td>
                                    </tr>
                                    <tr>
                                      <td>Logout</td>
                                      <td>X</td>
                                      <td>X</td>
                                      <td>X</td>
                                    </tr>
                                    <tr>
                                      <td>Inventarisir</td>
                                      <td>X</td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>Peminjaman</td>
                                      <td>X</td>
                                      <td>X</td>
                                      <td>X</td>
                                    </tr>
                                    <tr>
                                      <td>Pengembalian</td>
                                      <td>X</td>
                                      <td>X</td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td>Generate laporan</td>
                                      <td>X</td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </ul>
                            </p>
                            <p>ii.  Desain dibuat dengan memperhatikan estetika dan user-friendliness</p>
                            <p>iii. Form pelaporan dibuat sekomunikatif mungkin</p>
                            <p>iv.  Terapkan pemrograman berbasis obyek (object oriented programming)</p>
                          </ul>
                        </p>
                        <p>d. Hubungkan Aplikasi dengan Basis Data
                          <ul>
                            <p>i. Pilih dan instal aplikasi server basis data yang diinginkan</p>
                            <p>ii.  Buat basis data dengan PDM sebagai berikut:
                              <ul>                              
                              <img src="asset/img/pdm.png" style="height: 400px;">
                              <p>Keterangan :
                                <ul>
                                  <p>Desain basis data diatas adalah basis data dasar,</p>
                                  <p>peserta ujian dapat menambah tabel / field sesuai kebutuhan</p>
                                  <p>berdasarkan kreativitas agar Aplikasi yang dibuat dapat berfungsi secara maksimal.</p>
                                </ul>
                              </p>
                              </ul>
                            </p>
                            <p>iii. Terapkan stored procedure</p>
                            <p>iv.  Hubungkan aplikasi dengan basis data</p>
                          </ul>
                        </p>
                        <p>e. Lakukan Debugging
                          <ul>
                            <p>i. Lakukan ujicoba real-time</p>
                            <p>ii.  Pastikan langkah-langkah dalam pendeteksian kesalahan dilakukan dengan benar</p>
                            <p>iii. Pastikan seluruh fitur berfungsi dengan baik</p>
                          </ul>
                        </p>
                        <p>f. Buatlah Dokumentasi dan Manual Singkat Penggunaan Aplikasi
                          <ul>
                            <p>i. Buatlah dokumentasi fitur-fitur dalam aplikasi (user manual)</p>
                            <p>ii.  Buatlah dokumentasi pengembangan aplikasi</p>
                          </ul>
                        </p>
                        <br>
                        <p>Catatan :efisiensi baris program, kreativitas, atau inovasi akan dinilai lebih oleh penguji</p>
                        <p style="margin-left: 150px;">
                          <strong>“SELAMAT & SUKSES”</strong>
                        </p>
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
              <div class="panel">
                <div class="panel-heading">
                  <h5>TUGAS PRAKTEK-DEMONSTRASI/PRAKTEK</h5>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td>Perangkat Asesmen</td>
                        <td>:</td>
                        <td>Tugas  Praktek /Demonstrasi (Paket 1)</td>
                      </tr>
                      <tr>
                        <td>Nama peserta sertifikasi</td>
                        <td>:</td>
                        <td>
                          <input type="text" name="nama_peserta_tiga" class="form-control">
                        </td>
                      </tr>
                      <tr>
                        <td>Kode unit kompetensi</td>
                        <td>:</td>
                        <td>
                          <ol>
                            <li>LOG.OO01.002.01</li>
                            <li>LOG.OO01.004.01</li>
                            <li>TIK.OP01.002.01</li>
                            <li>J.620100.004.02</li>
                            <li>J.620100.005.02</li>
                            <li>J.620100.011.01</li>
                            <li>J.620100.012.01</li>
                            <li>J.620100.017.02</li>
                            <li>J.620100.022.02</li>
                            <li>J.620100.025.02</li>
                          </ol>
                        </td>
                      </tr>
                      <tr>
                        <td>Judul unit kompetensi</td>
                        <td>:</td>
                        <td>                        
                          <ol>
                            <li>Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</li>
                            <li>Merencanakan tugas rutin</li>
                            <li>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</li>
                            <li>Menggunakan struktur data</li>
                            <li>Mengimplementasikan user interface</li>
                            <li>Melakukan instalasi software tools pemrograman</li>
                            <li>Melakukan pengaturan software tools pemrograman</li>
                            <li>Mengimplementasikan pemrograman terstruktur</li>
                            <li>Mengimplementasikan algoritma pemrograman</li>
                            <li>Melakukan debugging</li>
                          </ol>
                        </td>
                      </tr>
                      <tr>
                        <td>Tanggal uji kompetensi</td>
                        <td>:</td>
                        <td>
                          <input type="date" name="tgl_uji_tiga" class="form-control">
                        </td>
                      </tr>
                      <tr>
                        <td>Waktu</td>
                        <td>:</td>
                        <td>180 menit</td>
                      </tr>
                    </tbody>
                  </table>
                  <h4>A. PETUNJUK UMUM</h4>
                  <ol>
                    <li>Periksalah dengan teliti dokumen soal ujian praktik.
                    <li>Baca dan pahami maksud soal agar tidak terjadi kesalahan pekerjaan.</li>
                    <li>Bekerjalah dengan memperhatikan jadwal dengan alur pengerjaan.</li>
                    <li>Peralatan utama dan bahan telah disediakan sesuai dengan kebutuhan.</li>
                    <li>Dalam bekerja selalu memperhatikan keselamatan kerja.</li>
                  </ol>
                  <h4>B. SOAL/TUGAS</h4>
                  <ol>
                    <li>Judul Tugas : Aplikasi Pembayaran Listrik Pasca Bayar Melalui Payment Point Online Bank (PPOB)</li>
                    <li>Langkah Kerja:
                        <ul>
                          <p>a. Lakukan Instalasi dan Pengaturan Software Tools Pemrograman</p>
                          <p>b. Interpretasikan Spesifikasi Program yang diberikan
                            <ul>
                              <p>i.  Aplikasi berbasis sistem client-server</p>
                              <p>ii. Aplikasi dapat dibuat berbasis web, atau perpaduan web & perangkat bergerak</p>
                              <p>iii.  Aplikasi menggunakan pembagian privilege dengan tingkatan (administrator, pelanggan, dan bank)</p>
                            </ul>
                          </p>
                          <p>c. Buatlah Desain User Interface / wireframe (Menu, Form Modifikasi Data, dan Pelaporan)
                            <ul>
                              <p>i. Fitur minimal dan pembagian privilege dalam aplikasi
                                <ul>
                                  <table>
                                    <tbody>
                                      <tr>
                                        <td>Fitur</td>
                                        <td>Administrator</td>
                                        <td>operator</td>
                                        <td>peminjam</td>
                                      </tr>
                                      <tr>
                                        <td>Login</td>
                                        <td>X</td>
                                        <td>X</td>
                                        <td>X</td>
                                      </tr>
                                      <tr>
                                        <td>lOGOUT</td>
                                        <td>X</td>
                                        <td>X</td>
                                        <td>X</td>
                                      </tr>
                                      <tr>
                                        <td>Registrasi</td>
                                        <td>X</td>
                                        <td>X</td>
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td>Pengelolaan Data Pelanggan</td>
                                        <td>X</td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td>Verifikasi dan validasi</td>
                                        <td>X</td>
                                        <td></td>
                                        <td>X</td>
                                      </tr>
                                      <tr>
                                        <td>Pembayaran</td>
                                        <td></td>
                                        <td>X</td>
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td>Generate laporan</td>
                                        <td>X</td>
                                        <td></td>
                                        <td>X</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </ul>
                              </p>
                              <p>ii.  Desain dibuat dengan memperhatikan estetika dan user-friendliness</p>
                              <p>iii. Form pelaporan dibuat sekomunikatif mungkin</p>
                              <p>iv.  Terapkan pemrograman berbasis obyek (object oriented programming)</p>
                            </ul>
                          </p>
                          <p>d. Hubungkan Aplikasi dengan Basis Data
                            <ul>
                              <p>i. Pilih dan instal aplikasi server basis data yang diinginkan</p>
                              <p>ii.  Buat basis data dengan PDM sebagai berikut:
                                <ul>
                                  <img src="asset/img/pdm02.png" style="height: 300px;">
                                  <p>Keterangan :
                                    <ul>
                                      <p>Desain basis data diatas adalah basis data dasar,</p>
                                      <p>peserta ujian dapat menambah tabel / field sesuai kebutuhan</p>
                                      <p>berdasarkan kreativitas agar Aplikasi yang dibuat dapat berfungsi secara maksimal.</p>
                                    </ul>
                                  </p>
                                </ul>
                              </p>
                              <p>iii. Terapkan stored procedure</p>
                              <p>iv.  Hubungkan aplikasi dengan basis data</p>
                            </ul>
                          </p>
                          <p>e. Lakukan Debugging
                            <ul>
                              <p>i. Lakukan ujicoba real-time</p>
                              <p>ii.  Pastikan langkah-langkah dalam pendeteksian kesalahan dilakukan dengan benar</p>
                              <p>iii. Pastikan seluruh fitur berfungsi dengan baik</p>
                            </ul>
                          </p>
                          <p>f. Buatlah Dokumentasi dan Manual Singkat Penggunaan Aplikasi
                            <ul>
                              <p>i. Buatlah dokumentasi fitur-fitur dalam aplikasi (user manual)</p>
                              <p>ii.  Buatlah dokumentasi pengembangan aplikasi</p>
                            </ul>
                          </p>
                          <br>
                          <p>Catatan  :   efisiensi baris program, kreativitas, atau inovasi akan dinilai lebih oleh penguji</p>
                          <p style="margin-left: 180px;">
                            <strong>"SELAMAT&SUKSES"</strong>
                          </p>
                        </ul>
                    </li>
                  </ol>
                  <button value="save" class="btn btn-primary" style="margin-left: 90%;">Simpan</button>
                </div>
              </div>
            </form>  					
  				</div>
  			</div>
  		</div>
  	</div>
@endsection
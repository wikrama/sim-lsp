@extends('layouts.mimin-login')

@section('content')
   <img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
  		<div>
  			<div class="col-md-12 top-20 padding-0">
  				<div class="col-md-12">
  					<div class="panel" style="border-radius: 5px;">
  						<div class="panel-heading">
  							<h4>Mengembangkan Perangkat Asesmen</h4>
  						</div>
  						<div class="panel-body Responsive-table">
  							<table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
  								<thead style="background-color: green; color: white;">
  									<tr>
  										<th colspan="4">1. Menentukan fokus perangkat asesmen</th>
  									</tr>
  								</thead>
  								<tbody>
  									<tr>
  										<td rowspan="4" class="text-center">1.1</td>
  										<tr>
  											<td>a. Identifikasi/klarifikasi kelompok target peserta sertifikasi</td>
  											<td>:</td>
  											<td>Individual</td>
  										</tr>
  										<tr>
  											<td>b. Identifikasi/klarifikasi tujuan asesmen</td>
  											<td>:</td>
  											<td>Sertifikasi</td>
  										</tr>
  										<tr>
  											<td>c. Identifikasi/klarifikasi konteks asesmen</td>
  											<td>:</td>
  											<td>TUK (sewaktu/ tempat kerja)* dengan karakteristik (produk/ sistem/ tempat kerja)*</td>
  										</tr>
  									</tr>
  									<tr>
  										<td rowspan="2" class="text-center">1.2</td>
  										<tr>
  											<td>Akses dan interpretasi acuan pembanding asesmen</td>
  											<td>:</td>
  											<td>Standar kompetensi:
  												<br>
  												<strong>Kode unit:</strong>
  												<br>
  												<ol>
  													<li>LOG.OO01.002.01</li>
  													<li>LOG.OO01.004.01</li>
  													<li>TIK.OP01.002.01</li>
  													<li>J.620100.004.02</li>
  													<li>J.620100.005.02</li>
  													<li>J.620100.011.01</li>
  													<li>J.620100.012.01</li>
  													<li>J.620100.017.01</li>
  													<li>J.620100.017.01</li>
  													<li>J.620100.025.02</li>
  												</ol>
  												<br>
  												<strong>Judul Unit:</strong>
  												<br>
  												<ol>
  													<li>Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</li>
  													<li>Merencanakan tugas rutin</li>
  													<li>Mengidentifikasi aspek kode etik dan HAKI di bidang TIK</li>
  													<li>Menggunakan struktur data</li>
  													<li>Mengimplementasikan user interface</li>
  													<li>Melakukan Instalasi software tools pemrograman</li>
  													<li>Melakukan pengaturan software tools pemrograman</li>
  													<li>Mengimplementasikan pemrograman terstruktur</li>
  													<li>Mengimplementasikan algoritma pemrograman</li>
  													<li>Melakukan debuging</li>
  												</ol>
  												<br>
  												<p>Standar Produk :</p>
  												<p>Standar Sistem :</p>
  												<p>Regulasi Teknis :</p>
  												<p>SOP :</p>
  											</td>
  										</tr>
  									</tr>
  									<tr>
  										<td rowspan="4" class="text-center">1.3</td>
  										<tr>
  											<td>a.Interpretasi semua komponen standard-standar kompetensi</td>
  											<td>:</td>
  											<td>Kode unit, deskripsi unit, elemen, KUK, batasan variable, panduan penilaian, kompetensi kunci</td>
  										</tr>
  										<tr>
  											<td>b.Kontektualisasi standar-standar  kompetensi</td>
  											<td>:</td>
  											<td>Tidak ada penyesuaian terhadap batasan variable dan panduan penilaian</td>
  										</tr>
  										<tr>
  											<td>c. Persyaratan organisasi/hukum/etika</td>
  											<td>:</td>
  											<td>Panduan Mutu LSP</td>
  										</tr>
  									</tr>
  									<tr>
  										<td rowspan="4" class="text-center">1.4</td>
  										<tr>
  											<td>Identifikasi dokumentasi terkait</td>
  											<td>:</td>
  											<td>Sumber daya fisik sesuai dengan form MMA</td>
  										</tr>
  									</tr>
  								</tbody>
  							</table>
  							<table class="table">
  								<thead style="background-color: green; color: white;">
  									<tr>
  										<th colspan="4">2. Menentukan Kebutuhan Perangkat Asesmen</th>
  									</tr>
  								</thead>
  								<tbody>
  									<tr>
  										<td rowspan="2" class="text-center">2.1</td>
  										<tr>
  											<td>Pilih metode-metode asesmen  dengan mempertimbangkan konteks asesmen dan prinsip-prinsip asesmen.</td>
  											<td>:</td>
  											<td>(observasi, lisan, tertulis)*</td>
  										</tr>
  										<tr>
  											<td rowspan="2" class="text-center">2.2</td>
  											<tr>
  												<td>Tentukan metode-metode  asesmen yang dinominasikan</td>
  												<td>:</td>
  												<td>(observasi,tulis)*</td>
  											</tr>
  										</tr>
  										<tr>
  											<td rowspan="3" class="text-center">2.3</td>
  											<tr>
  												<td>Pertimbangkan instrumen untuk setiap metode asesmen</td>
  												<td>:</td>
  												<td>(ceklis observasi, tugas praktek demonstrasi, daftar pertanyaan lisan)*</td>
  											</tr>
  											<tr>
  												<td>Ciptakan pilihan (opsi) aktifitas asesmen menggunakan ketrampilan berfikir kritis</td>
  												<td>:</td>
  												<td>Peserta sertifikasi normal dan tidak diperlukan penyesuaian metode dan perangkat  asesmen sebagai pilihan/ opsi</td>
  											</tr>
  										</tr>
  									</tr>
  								</tbody>
  							</table>
                <form method="post" action="{{URL('/mpa-01/store')}}">
                {{ csrf_field() }}                  
                <table class="table">
                  <thead style="background-color: green; color: white;">
                    <tr>
                      <th colspan="4">3.Merancang dan mengembangkan perangkat asesmen</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td rowspan="2" class="text-center">3.1</td>
                      <tr>
                        <td class="col-md-6">Kembangkan instrumen-instrumen  spesifik/ sesuai  dengan bukti yang akan dikumpulkan,  berdasarkan rancangan aktifitas asesmen yang</td>
                        <td>:</td>
                        <td>
                          <div class="form-group">
                            <p>memenuhi standar-standar kompetensi</p>
                            <input type="radio" name="memenuhi_standar" value="Ya" required> Ya <br>
                            <input type="radio" name="memenuhi_standar" value="Tidak" required> Tidak
                          </div>
                          <div class="form-group">
                            <p>mencerminkan prinsip-prinsip asesmen</p>
                            <input type="radio" name="mencerminkan_prinsip" value="Ya" required> Ya <br>
                            <input type="radio" name="mencerminkan_prinsip" value="Tidak" required> Tidak
                          </div>
                          <div class="form-group">
                            <p>menggabungkan prinsip-prinsip akses dan keadilan</p>
                            <input type="radio" name="menggabungkan_prinsip" value="Ya" required> Ya <br>
                            <input type="radio" name="menggabungkan_prinsip" value="Tidak" required> Tidak
                          </div>
                          <div class="form-group">
                            <p>memenuhi aturan bukti</p>
                            <input type="radio" name="memenuhi_aturan" value="Ya" required> Ya <br>
                            <input type="radio" name="memenuhi_aturan" value="Tidak" required> Tidak
                          </div> 
                          <div class="form-group">
                            <p>memberikan pilihan, bila perlu</p>
                            <input type="radio" name="memberikan_pilihan" value="Ya" required> Ya <br>
                            <input type="radio" name="memberikan_pilihan" value="Tidak" required> Tidak
                          </div> 
                          <div class="form-group">
                            <p>terurut  untuk mencerminkan pengembangan kompetensi dalam jalur pembelajaran dan asesmen</p>
                            <input type="radio" name="terurut" value="Ya" required> Ya <br>
                            <input type="radio" name="terurut" value="Tidak" required> Tidak
                          </div>
                          <div class="form-group">
                            <p>mudah di gunakan oleh pengguna</p>
                            <input type="radio" name="mudah" value="Ya" required> Ya <br>
                            <input type="radio" name="mudah" value="Tidak" required> Tidak
                          </div> 
                          <div class="form-group">
                            <p>merefleksikan lingkungan asesmen</p>
                            <input type="radio" name="merefleksikan" value="Ya" required> Ya <br>
                            <input type="radio" name="merefleksikan" value="Tidak" required> Tidak
                          </div> 
                          <div class="form-group">
                            <p>dapat dipraktekkan</p>
                            <input type="radio" name="dapat_diperaktekkan" value="Ya" required> Ya <br>
                            <input type="radio" name="dapat_diperaktekkan" value="Tidak" required> Tidak
                          </div>
                        </td>
                      </tr>
                    </tr>
                    <tr>
                      <td rowspan="2" class="text-center">3.2</td>
                      <tr>
                        <td>Kembangkan instrumen-instrumen asesmen spesifik/sesuai  dengan bukti yang akan dikumpulkan,  berdasarkan rancangan aktifitas asesmen yang</td>
                        <td>:</td>
                        <td>                          
                          <div class="form-group">
                            <p>Menggunakan format yang sesuai</p>
                            <input type="radio" name="menggunakan_format" value="Ya" required> Ya <br>
                            <input type="radio" name="menggunakan_format" value="Tidak" required> Tidak
                          </div>                          
                          <div class="form-group">
                            <p>memperhatikan bahasa dan kemampuan baca tulis  dan numerasi peserta sertifikasi</p>
                            <input type="radio" name="memperhatikan_bahasa" value="Ya" required> Ya <br>
                            <input type="radio" name="memperhatikan_bahasa" value="Tidak" required> Tidak
                          </div>                          
                          <div class="form-group">
                            <p>memperhatikan keragaman peserta sertifikasi</p>
                            <input type="radio" name="memperhatikan_keragaman" value="Ya" required> Ya <br>
                            <input type="radio" name="memperhatikan_keragaman" value="Tidak" required> Tidak
                          </div>                          
                          <div class="form-group">
                            <p>menggunakan representasi visual dan suara</p>
                            <input type="radio" name="menggunakan_representasi" value="Ya" required> Ya <br>
                            <input type="radio" name="menggunakan_representasi" value="Tidak" required> Tidak
                          </div>                          
                          <div class="form-group">
                            <p>menggunakan media</p>
                            <input type="radio" name="menggunakan_media" value="Ya" required> Ya <br>
                            <input type="radio" name="menggunakan_media" value="Tidak" required> Tidak
                          </div>     
                        </td>
                      </tr>
                    </tr>
                    <tr>
                      <td rowspan="2" class="text-center">3.3</td>
                      <tr>
                        <td>Tetapkan dan dokumentasikan prosedur-prosedur  spesifik  dan jelas yang memandu asesor dan/atau peserta sertifikasi  dalam pengadministrasian dan penggunaan instrumen-instrumen  </td>
                        <td>:</td>
                        <td>Perangkat asesmen yang digunakan didokumentasikan sesuai SOP yang berlaku di LSP </td>
                      </tr>
                    </tr>
                    <tr>
                      <td rowspan="2" class="text-center">3.4</td>
                      <tr>
                        <td>pertimbangkan dan kemukaan kebijakan  dan prosedur sistem asesmen yang relevan</td>
                        <td>:</td>
                        <td>Prosedur menyimpan dokumen dan melacak hasil uji kompetensi/ rekaman</td>
                      </tr>
                    </tr>
                  </tbody>
                </table>                
                <button class="btn btn-primary" value="save" style="margin-left: 90%;">
                  Next
                </button>
                </form>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
@endsection
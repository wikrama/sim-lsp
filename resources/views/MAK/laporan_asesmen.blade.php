@extends('layouts.mimin-login')

@section('content')
  <img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  <div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
        <div>
        	<div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-heading">
                            <h4>Formulir Laporan Assesmen</h4>
                        </div>
                        <div class="panel-body">
                        	<div class="responsive-table">
                            <form action="{{URL('/laporan_asesmen/store')}}" method="post">
                              {{ csrf_field() }}
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      	<tr>                  		
                      		<td><strong>Nama Peserta</strong></td>
                          <td><strong><input class="form-control border-bottom" type="text" name="nama_psrt" required=""></strong></td>
                          <td><strong>Nama Asesor</strong></td>
                          <td><strong><input class="form-control border-bottom" type="text" name="nama_asesor" required=""></strong></td>
                      	</tr>
                        <tr>                      
                          <td><strong>Tanggal Pencapaian Kompetensi</strong></td>
                          <td><strong><input required="" class="form-control border-bottom" type="Date" name="tgl"></strong></td>
                          <td><strong>Tanda Tangan Asesor dan Tanggal</strong></td>
                          <td><strong><br><br><br> .......................................................</strong></td>
                        </tr>
                        <tr bgcolor="green" style="color: white">
                          <td>Unit Kompetensi</td>
                          <td align="center">K</td>
                          <td align="center">BK</td>
                          <td>Keterangan</td>
                        </tr>
                        <tr>
                          <td>1.  Melakukan komunikasi kerja timbal balik</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>

                        <tr>
                          <td>2.  Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>

                        <tr>
                          <td>3.  Merencanakan tugas rutin</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>

                        <tr>
                          <td>4.  Melakukan pekerjaan yang membutuhkan kerjasama tim</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>

                        <tr>
                          <td>5.  Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>

                        <tr>
                          <td>6.  Menggunakan Struktur Data</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="6" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="6" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>

                        <tr>
                          <td>7.  Mengimplementasikan User Interface</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="7" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="7" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>

                        <tr>
                          <td>8.  Mengimplementasikan Rancangan Entitas dan Keterkaitan Antar Entitas</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="8" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="8" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea>t</td>
                        </tr>

                        <tr>
                          <td>9.  Menggunakan Spesifikasi Program</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="9" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="9" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>

                        <tr>
                          <td>10. Melakukan Pengaturan Software Tools Pemrograman</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="10" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="10" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>

                        <tr>
                          <td>11. Mengimplementasikan Pemrograman Terstruktur</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="11" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="11" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>
                        
                        <tr>
                          <td>12. Menggunakan SQL</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="12" value="K"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="12" value="BK"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td><textarea></textarea></td>
                        </tr>
                        </table>
                          </div>
                        </div>
                    </div>
                </div>
            </div>

          <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                          <div class="responsive-table">
                      <table CELLPADDING="1" CELLSPACING="1" id="datatables-example"  width="100%">
                        <thead bgcolor="green" style="color: white">
                        <tr>                      
                          <th><strong>Aspek Negatif dan Positif Dalam asesemen</strong></th>
                          <th><strong>Pencatatan Penolakan Hasil Asesmen</strong></th>
                          <th><strong>Saran Perbaikan : <br>(Asesor/Personil Terkait)</strong></th>
                        </tr>
                        </thead>
                        <tr>
                          <td><textarea required="" name="neg_pos" class="form-control border-bottom"></textarea></td>
                          <td><textarea required="" name="catatan_tolak" class="form-control border-bottom"></textarea></td>
                          <td><textarea required="" name="saran_perbaikan" class="form-control border-bottom"></textarea></td>
                        </tr>
                        <tr>
                          <td colspan="3"><strong>Kode File : (Diisi oleh LSP) </strong><br><input class="form-control border-bottom" type="text" name="kd_file"></td>
                        </tr>
                        </table>
                      </div>
                        </div>
                    </div>
                        <div align="right">  
                            <input type="submit" name="Save" class="btn btn-primary" value="SAVE">
                        </div>
                            </form>
                </div>
            </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

	</div>
</div>
@endsection
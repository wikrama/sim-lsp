@extends('layouts.mimin-login')

@section('content')

	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;width: 100%">
		<div>
			<div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        	<div class="responsive-table">
                            <form action="{{URL ('/akses_kompetensi/store') }}" method="post">
                              {{ csrf_field() }}
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      	<tr>                  		
                      		<td><strong>Nama Peserta</strong></td>
                      		<td><input type="text" class="form-control border-bottom" name="nama_psrt" value="{{ Auth::user()->name }}" readonly=""></td>
                      	</tr>
                      	<tr>
	                  		<td><strong>Nama Asesi</strong></td>
                      		<td><input type="text" class="form-control border-bottom" name="nama_asesi"></td>
                      	</tr>
                      	<tr>
	                  		<td rowspan="10"><strong>Unit Kompetensi</strong></td>
                      		<td>LOG.OO01.002.01 - Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja
                      		</td>
                      	</tr>
                      	<tr>
                      		<td>LOG.OO01.004.01 - Merencanakan tugas rutin</td>
                      	</tr>
                      	<tr>
                      		<td>TIK.OP01.002.01 - Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</td>
                      	</tr>
                      	<tr>
                      		<td>J.620100.004.02 - Menggunakan struktur data</td>
                      	</tr>
                      	<tr>
                      		<td>J.620100.005.02 - Mengimplementasikan user interface</td>
                      	</tr>
                      	<tr>
                      		<td>J.620100.011.01 - Melakukan instalasi software tools pemrograman</td>
                      	</tr>
                      	<tr>
                      		<td>J.620100.012.01 - Melakukan pengaturan software tools pemrograman</td>
                      	</tr>
                      	<tr>
                      		<td>J.620100.017.02 - Mengimplementasikan pemrograman terstruktur</td>
                      	</tr>
                      	<tr>
                      		<td>J.620100.022.02 - Mengimplementasikan algoritma pemrograman</td>
                      	</tr>
                      	<tr>
                      		<td>J.620100.025.02 - Melakukan debugging</td>
                      	</tr>
                      	<tr>
                      		<td><strong>Waktu</strong></td>
                      		<td><input type="time" class="form-control border-bottom" name="waktu"></td>
                      	</tr>
                      	<tr>
                      		<td><strong>Tanggal uji kompetensi</strong></td>
                      		<td><input type="date" class="form-control border-bottom" name="tgl"></td>
                      	</tr>
                        </table>
                      </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        	<div class="responsive-table">
                        		<table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th class="text-center">No.</th>
                          <th class="text-center">KEGIATAN</th>
                          <th class="text-center">REKAMAN</th>
                          <th>YA</th>
                          <th>TDK</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="5"><h5><strong>PRA ASESMEN</strong></h5></td>
                        </tr>

                        <!-- 1 -->
                        <tr>
                          <td><legend>1.</legend></td>
                          <td><legend>MENYIAPKAN DAN MEMELIHARA LINGKUNGAN ASESMEN</legend></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>1.1</td>
                          <td>Menginterpretasi dan konfirmasi Asesmen mandiri dan Rencana asesmen (MMA) dengan peserta asesmen.</td>
                          <td>
                          	FR. MMA - APL 01 - APL 02
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1_1" value="YA"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1_1" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>1.2</td>
                          <td>Mengakses  dan interpretasi acuan pembanding asesmen yang relevan dan perangkat  asesmen  yang dinominasikan guna mengukuhkan bukti dan cara pengumpulan bukti tersebut.</td>
                          <td>
                          	FR. MMA - SKKNI 
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1_2" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1_2" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>1.3</td>
                          <td>Menjelaskan, membahas, dan klarifikasi rincian mengenai rencana asesmen  dan proses asesmen kepada asesi,  termasuk kesempatan untuk melakukan penyesuaian yang beralasan, asesmen ulang dan banding.</td>
                          <td>
                          	FR MMA Form Banding (MAK 02)
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1_3" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1_3" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>1.4</td>
                          <td>Jika relevan, merundingkan dan mensepakati usulan perubahan terhadap proses asesmen dengan asesi.</td>
                          <td>
                          	 APL 01 - APL 02 - FR. MMA
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1_4" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="1_4" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>

                        <!-- 2 -->
                        <tr>
                          <td><legend>2.</legend></td>
                          <td><legend>MENGUMPULKAN BUKTI YANG BERKUALITAS</legend></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>2.1</td>
                          <td>Mengikuti Rencana asesmen (MMA) sebagai panduan dalam melaksanakan asesmen, guna penentuan kompetensi,  (ES)</td>
                          <td>
                          	 FR MMA
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_1" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_1" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>2.2</td>
                          <td>Menerapkan Prinsip-prinsip asesmen dan aturan-aturan bukti dalam pengumpulan bukti yang berkualitas dari HASIL ASESMEN MANDIRI (APL 02). (ES)</td>
                          <td>
                          	 APL 02
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_2" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_2" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>2.3</td>
                          <td>Menentukan bersama asesi kesempatan pengumpulan bukti pada saat bekerja atau dalam aktifitas kerja yang disimulasikan berdasarkan APL 02 yang telah dikumpulkan bukti-buktinya.</td>
                          <td>
                          	 APL 02
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_3" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_3" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>2.4</td>
                          <td>Mengidentifikasi kesempatan untuk aktifitas asesmen terpadu dan jika memungkinkan memodifikasi perangkat asesmen.</td>
                          <td>
                          	 APL 02
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_4" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_4" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>2.5</td>
                          <td>Membahas kebijakan yang relevan dan memastikan bahwa peserta mengerti implikasinya.</td>
                          <td>
                          	 APL 02
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_5" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_5" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          >
                        </tr>
                        <tr>
                          <td>2.6</td>
                          <td>Merekam semua kesepakatan di atas lembar persetujuan (kolom Rekaman dan Rekomendasi) termasuk persetujuan tanggal, tempat, waktu serta durasi asesmen lanjut)</td>
                          <td>
                          	 MAK 03
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_6" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="2_6" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>


                        <!-- 3 -->
                        <tr>
                          <td><legend>3.</legend></td>
                          <td><legend>MENDUKUNG PESERTA</legend></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>3.1</td>
                          <td>Membimbing Asesi dalam pengumpulan bukti guna pencapaian pengakuan kompetensi terkini.</td>
                          <td>
                          	 MPA -01
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_1" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_1" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>3.2</td>
                          <td>Menggunakan keterampilan komunikasi interpersonal pada saat menggunakan perangkat asesmen, termasuk mengomunikasikan penyesuaian yang memungkinkan. (ES)</td>
                          <td>
                          	 MPA -02
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_2" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_2" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>3.3</td>
                          <td>Bila diperlukan, membuat keputusan-keputusan bersama asesi mengenai penyesuaian yang beralasan berdasarkan kebutuhan dan karakteristik asesi.</td>
                          <td>
                          	 MPA -03
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_3" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_3" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>3.4</td>
                          <td>Membuat penyesuaian yang beralasan untuk mempertahankan integritas standar kompetensi yang relevan dan memungkinkan prinsip-prinsip asesmen dan aturan bukti dapat diterapkan berimbang.</td>
                          <td>
                          	 MPA
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_4" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_4" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>3.5</td>
                          <td>Mengakses dukungan spesialis sesuai rencana asesmen bila ada</td>
                          <td>
                          	 MPA
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_5" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_5" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>3.6</td>
                          <td>Segera menanggulangi, Risiko kesehatan dan keselamatan kerja apa pun terhadap orang atau peralatan.</td>
                          <td>
                          	 MPA
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_6" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="3_6" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>

                        <!-- 4 -->
                        <tr>
                          <td><legend>4.</legend></td>
                          <td><legend>MEMBUAT KEPUTUSAN ASESMEN</legend></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>4.1</td>
                          <td>Meminta arahan dari orang yang relevan, bila ada keterbatasan perolehan dan evaluasi bukti yang berkualitas</td>
                          <td>
                          	 MAK 04
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_1" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_1" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>4.2</td>
                          <td>Memeriksa dan evaluasi bukti yang telah terkumpul untuk memastikan bahwa bukti tersebut dapat merefleksikan bukti yang diperlukan dalam memperlihatkan kompetensi dan mencakup seluruh bagian komponen standar kompetensi yang dijadikan acuan pembanding asesmen dan dimensi kompetensi, serta memperhatikan dokumentasi terkait lainnya, dan memenuhi aturan bukti.</td>
                          <td>
                          	 MAK 04
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_2" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_2" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>4.3</td>
                          <td>Menggunakan Pertimbangan berdasarkan prinsip asesmen dan aturan bukti untuk memutuskan pencapaian kompetensi yang telah didemonstrasikan asesi berdasarkan bukti yang dikumpulkan. (ES)</td>
                          <td>
                          	 MAK 04
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_3" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_3" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>4.4</td>
                          <td>Menggunakan kebijakan dan prosedur sistem asesmen yang relevan dan pertimbangan-pertimbangan organisasi/hukum/etika dalam membuat keputusan asesmen, (ES)</td>
                          <td>
                          	 MAK 04
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_4" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_4" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>4.5</td>
                          <td>Menggunakan kebijakan dan prosedur sistem asesmen yang relevan dan pertimbangan-pertimbangan organisasi/hukum/etika dalam membuat keputusan asesmen, (ES)</td>
                          <td>
                          	 MAK 04  MAK 05
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_5" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="4_5" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>

                        <!-- 5 -->
                        <tr>
                          <td><legend>5.</legend></td>
                          <td><legend>MEREKAM DAN MELAPORKAN KEPUTUSAN ASESMEN</legend></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>5.1</td>
                          <td>Mencatat segera hasil asesmen secara akurat sesuai dengan kebijakan dan prosedur sistem asesmen serta persyaratan organisasi/hukum/etika.</td>
                          <td>
                          	  MAK 06
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5_1" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5_1" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>5.2</td>
                          <td>Melengkapi dan memproses laporan asesmen sesuai dengan kebijakan dan prosedur sistem asesmen serta persyaratan organisasi/hukum/etika.</td>
                          <td>
                          	 MAK 06
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5_2" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5_2" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>5.3</td>
                          <td>Bila diperlukan, Menyerahkan rekomendasi tindak lanjut kepada orang yang relevan. </td>
                          <td>
                          	  MAK 06
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5_3" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5_3" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>5.4</td>
                          <td>Bila diperlukan, Memberi tahu pihakpihak terkait lainnya diberitahu tentang keputusan asesmen dengan memperhatikan ketentuan kerahasiaan.</td>
                          <td>
                          	  MAK 06
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5_4" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="5_4" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>


                        <!-- 6 -->
                        <tr>
                          <td><legend>6.</legend></td>
                          <td><legend>MEREKAM DAN MELAPORKAN KEPUTUSAN ASESMEN</legend></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>6.1</td>
                          <td>Meninjau Proses asesmen berdasarkan kriteria yang ada melalui konsultasi dengan orang yang relevan guna perbaikan dan perubahan pelaksanaan asesmen di masa datang.</td>
                          <td>
                          	  MAK 07
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="6_1" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="6_1" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>6.2</td>
                          <td>Merekam dan mendokumentasikan Tinjauan sesuai dengan kebijakan dan prosedur sistem asesmen yang relevan serta persyaratan organisasi/hukum/etika.</td>
                          <td>
                          	  MAK 07
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="6_2" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="6_2" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                        <tr>
                          <td>6.3</td>
                          <td>Menggunakan keterampilan kematangan berfikir (refleksi) secara mandiri untuk meninjau dan mengevaluasi praktek asesmen. (ES)</td>
                          <td>
                          	  MAK 07
						 </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="6_3" value="Ya"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                                <label class="radio">
                                  <input id="radio1" type="radio" name="6_3" value="Tidak"/>
                                <span class="outer">
                                   <span class="inner"></span>
                                </span>
                              </label>
                            </div>
                          </td>
                          
                        </tr>
                      </tbody>
                        </table>
                        	</div>
                        </div>
                    </div>
                    <div class="text-right">
                        <input type="submit" name="Save" class="btn btn-primary" value="SAVE">
                    </div>
                      </form>
                </div>
            </div>
		</div>
	</div>

@endsection
@extends('layouts.mimin-login')

@section('content')
  <img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  <div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
        <div>
        	<div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-heading">
                            <h4>Formulir Persetujuan Assesmen Dan Kerahasiaan</h4>
                        </div>
                        <div class="panel-body">
                        	<div class="responsive-table">
                            <form action="{{ URL('/formulir_persetujuan/store')}}" method="post">
                              {{ csrf_field() }}
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      	<tr>                  		
                      		<td colspan="3"><strong>Persetujuan Asesmen ini untuk menjamin bahwa Peserta telah diberi arahan secara rinci tentang proses asesmen</strong></td>
                      	</tr>
                      	<tr>
	                  		<td>Nama Calon Peserta</td>
                        <td colspan="3"><input type="text" name="nama_calon"></td>
                      	</tr>
                      	<tr>
                      		<td>Nama Asesor</td>
                          <td colspan="3"><input type="text" name="nama_ass"></td>
                      	</tr>
                        <tr>
                          <td>Skema Sertifikasi</td>
                          <td colspan="3"> Kualifikasi KKNI Level II Pada Kompetensi Keahlian 
 Rekayasa Perangkat Lunak Klaster Pemrograman Dasar
                          </td>
                        </tr>
                        <tr>
                          <td>No. Skema Sertifikasi</td>
                          <td colspan="3"><input type="text" name="no_skema"></td>
                        </tr>
                        <tr>
                          <td>Bukti yang akan dikumpulkan :</td>
                          <td colspan="3"><input type="text" name="bukti"></td>
<!--                           <td colspan="3"><input type="checkbox" name="bukti">--------<input type="checkbox" name="bukti">--------<input type="checkbox" name="bukti"></td> -->
                        </tr>
                        <tr>
                          <td rowspan="2">Pelaksanaan asesmen akan dilaksanakan pada:</td>
                        </tr>
                        <td>Hari/ Tanggal : <input type="date" name="tgl"><br><br><br><br></td>
                        <td align="top">Tempat : <input type="text" name="tempat"><br><br><br><br></td>
                        <tr>
                          <td colspan="3">Peserta Sertifikasi: <br> <br>
Saya setuju mengikuti asesmen dengan pemahaman bahwa informasi yang dikumpulkan hanya digunakan untuk pengembangan profesional dan hanya dapat diakses oleh orang tertentu saja.
                        </tr>
                        <tr>
                          <td colspan="3">Asesor: <br> <br>
Menyatakan tidak akan membuka hasil pekerjaan yang saya peroleh karena penugasan saya sebagai asesor dalam pekerjaan Asesmen kepada siapapun atau organisasi apapun selain kepada pihak yang berwenang sehubungan dengan kewajiban saya sebagai Asesor yang ditugaskan oleh LSP.
</td>
                        </tr>
                        <tr>
                          <td colspan="3"> Tanda tangan Peserta   : …………………………………………………   <br>  <br>
                              Tanda tangan Peserta  : …………………………………………………     <br> <br>
                              Tanggal : <input type="date" name="tanggal">
                          </td>
                        </tr>
                        </table>
                        <div class="text-right">
                        <input type="submit" name="save" class="btn btn-primary" value="Save">
                        </div>
                      </form>
                      </div>
                        </div>
                    </div>
                </div>
            </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

	</div>
</div>
@endsection
@extends('layouts.mimin-login')

@section('content')
  <img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  <div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
        <div>
        	<div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                    	<div class="panel-heading">
                            <h4>Umpan Balik</h4>
                        </div>
                        <form action="{{URL('/umpan_balik/store')}}" method="post">
                            {{ csrf_field() }}
                        <div class="panel-body">
                        	<div class="responsive-table">
                        		<table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                        				<tr>
                        					<td>
                        						Nama Peserta : <input class="form-control border-bottom" type="text" required="" name="nama_psrt">
                        					</td>
                        					<td>
                        						Tanggal/Waktu : <input class="form-control border-bottom" required="" type="datetime-local" name="tgl">
                        					</td>
                        				</tr>
                        				<tr>
                        					<td>
                        						Tim Asesor : <input class="form-control border-bottom" required="" type="text" name="tim_asesor">
                        					</td>
                        					<td>
                        						Tempat : <input class="form-control border-bottom" required="" type="text" name="tmpt">
                        					</td>
                        				</tr>
                        		</table>
                                <br><br>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>LOG.OO01.002.01</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Menerapkan Prinsip-Prinsip Keselamatan Dan Kesehatan Kerja Di Lingkungan Kerja  </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Mengikuti praktekpraktek kerja yang aman </td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Kerja dilaksanakan dengan aman sehubungan dengan<br> kebijakan dan prosedur perusahaan serta persyaratan <br>perundang-undangan.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1" value="K"></td>
                                                <td><input type="radio" name="1_1" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Kegiatan rumah tangga perusahaan dilakukan sesuai <br> dengan prosedur perusahaan</td>
                                                <td><textarea name="1_2_bl"></textarea></td>
                                                <td><textarea name="1_2_btl"></textarea></td>
                                                <td><textarea name="1_2_bt"></textarea></td>
                                                <td><input type="radio" name="1_2" value="K"></td>
                                                <td><input type="radio" name="1_2" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.3</td>
                                                <td>Tanggung jawab dan tugas-tugas karyawan dimengerti dan <br> didemostrasikan dalam <br> kegiatan sehari-hari.  </td>
                                                <td><textarea name="1_3_bl"></textarea></td>
                                                <td><textarea name="1_3_btl"></textarea></td>
                                                <td><textarea name="1_3_bt"></textarea></td>
                                                <td><input type="radio" name="1_3" value="K"></td>
                                                <td><input type="radio" name="1_3" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.4</td>
                                                <td>Perlengkapan pelindung diri dipakai dan disimpan <br> sesuai dengan prosedur perusahaan.  </td>
                                                <td><textarea name="1_4_bl"></textarea></td>
                                                <td><textarea name="1_4_btl"></textarea></td>
                                                <td><textarea name="1_4_bt"></textarea></td>
                                                <td><input type="radio" name="1_4" value="K"></td>
                                                <td><input type="radio" name="1_4" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.5</td>
                                                <td>Semua perlengkapan dan alat-alat keselamatan digunakan <br> sesuai dengan persyaratan perundang-undangan dan <br> prosedur perusahaan. </td>
                                                <td><textarea name="1_5_bl"></textarea></td>
                                                <td><textarea name="1_5_btl"></textarea></td>
                                                <td><textarea name="1_5_bt"></textarea></td>
                                                <td><input type="radio" name="1_5" value="K"></td>
                                                <td><input type="radio" name="1_5" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.6</td>
                                                <td>Tanda-tanda/simbol dikenali dan diikuti sesuai <br> instruksi. </td>
                                                <td><textarea name="1_6_bl"></textarea></td>
                                                <td><textarea name="1_6_btl"></textarea></td>
                                                <td><textarea name="1_6_bt"></textarea></td>
                                                <td><input type="radio" name="1_6" value="K"></td>
                                                <td><input type="radio" name="1_6" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.7</td>
                                                <td>Semua pedoman penanganan dilaksanakan sesuai dengan <br> persyaratan, prosedur perusahaan dan pedoman Komisi <br>Kesehatan dan Keselamatan Kerja Nasional yang sah.</td>
                                                <td><textarea name="1_7_bl"></textarea></td>
                                                <td><textarea name="1_7_btl"></textarea></td>
                                                <td><textarea name="1_7_bt"></textarea></td>
                                                <td><input type="radio" name="1_7" value="K"></td>
                                                <td><input type="radio" name="1_7" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.8</td>
                                                <td>Perlengkapan darurat dikenali dan didemonstrasikan <br> dengan tepat.  </td>
                                                <td><textarea name="1_8_bl"></textarea></td>
                                                <td><textarea name="1_8_btl"></textarea></td>
                                                <td><textarea name="1_8_bt"></textarea></td>
                                                <td><input type="radio" name="1_8" value="K"></td>
                                                <td><input type="radio" name="1_8" value="BK"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2.Melaporkan bahayabahaya di tempat kerja  </td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Bahaya-bahaya di tempat kerja selama waktu kerja <br>dikenali dan dilaporkan kepada orang yang tepat sesuai <br>dengan prosedur pengoperasian standar. </td>
                                                <td><textarea name="2_1_bl"></textarea></td>
                                                <td><textarea name="2_1_btl"></textarea></td>
                                                <td><textarea name="2_1_bt"></textarea></td>
                                                <td><input type="radio" name="2_1" value="K"></td>
                                                <td><input type="radio" name="2_1" value="BK"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">3.Mengikuti prosedurprosedur darurat</td>
                                            </tr>
                                            <tr>
                                                <td>3.1</td>
                                                <td>Cara-cara menghubungi personil yang tepat dan layanan <br>darurat jika terjadi kecelakaan <br> didemonstrasikan.  </td>
                                                <td><textarea name="3_1_bl"></textarea></td>
                                                <td><textarea name="3_1_btl"></textarea></td>
                                                <td><textarea name="3_1_bt"></textarea></td>
                                                <td><input type="radio" name="3_1" value="K"></td>
                                                <td><input type="radio" name="3_1" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>3.2</td>
                                                <td>Bila diperlukan prosedur kondisi darurat dan evakuasi (pengungsian) <br> dimengerti dan dilaksanakan.  </td>
                                                <td><textarea name="3_2_bl"></textarea></td>
                                                <td><textarea name="3_2_btl"></textarea></td>
                                                <td><textarea name="3_2_bt"></textarea></td>
                                                <td><input type="radio" name="3_2" value="K"></td>
                                                <td><input type="radio" name="3_2" value="BK"></td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                        	</div>
                        </div>
                    </div> 
                    <!-- 2 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>LOG.OO01.004.01 </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Merencanakan Tugas Rutin </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Mengenali persyaratan tugas  </td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Instruksi-instruksi tentang prosedur diperoleh, <br> dimengerti dan bila perlu dijelaskan</td>
                                                <td><textarea name="1-1-bl"></textarea></td>
                                                <td><textarea name="1-1-btl"></textarea></td>
                                                <td><textarea name="1-1-bt"></textarea></td>
                                                <td><input type="radio" name="1-1" value="K"></td>
                                                <td><input type="radio" name="1-1" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Spesifikasi yang relevan terhadap hasil-hasil tugas <br>diperoleh, dimengerti dan bila perlu dijelaskan</td>
                                                <td><textarea name="1-2-bl"></textarea></td>
                                                <td><textarea name="1-2-btl"></textarea></td>
                                                <td><textarea name="1-2-bt"></textarea></td>
                                                <td><input type="radio" name="1-2" value="K"></td>
                                                <td><input type="radio" name="1-2" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.3</td>
                                                <td>Hasil-hasil tugas dikenali. </td>
                                                <td><textarea name="1-3-bl"></textarea></td>
                                                <td><textarea name="1-3-btl"></textarea></td>
                                                <td><textarea name="1-3-bt"></textarea></td>
                                                <td><input type="radio" name="1-3" value="K"></td>
                                                <td><input type="radio" name="1-3" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.4</td>
                                                <td>Syarat-syarat tugas seperti waktu penyelesaian dan <br> ukuran kualitas dikenali.  </td>
                                                <td><textarea name="1-4-bl"></textarea></td>
                                                <td><textarea name="1-4-btl"></textarea></td>
                                                <td><textarea name="1-4-bt"></textarea></td>
                                                <td><input type="radio" name="1-4" value="K"></td>
                                                <td><input type="radio" name="1-4" value="BK"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2.Merencanakan langkahlangkah yang dibutuhkan untuk menyelesaikan tugas    </td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Berdasarkan instruksi-instruksi dan <br>spesifikasi-spesifikasi yang ada, langkah-langkah atau <br>kegiatan-kegiatan individu yang diperlukan untuk <br>melaksanakan tugas dimengerti dan bila perlu dijelaskan.  </td>
                                                <td><textarea name="2-1-bl"></textarea></td>
                                                <td><textarea name="2-1-btl"></textarea></td>
                                                <td><textarea name="2-1-bt"></textarea></td>
                                                <td><input type="radio" name="2-1" value="K"></td>
                                                <td><input type="radio" name="2-1" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.2</td>
                                                <td>Rangkaian kegiatan yang perlu diselesaikan tercantum <br> dalam rencana. </td>
                                                <td><textarea name="2-2-bl"></textarea></td>
                                                <td><textarea name="2-2-btl"></textarea></td>
                                                <td><textarea name="2-2-bt"></textarea></td>
                                                <td><input type="radio" name="2-2" value="K"></td>
                                                <td><input type="radio" name="2-2" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.3</td>
                                                <td>Langkah-langkah dan hasil yang direncanakan diperiksa <br> untuk menjamin kesesuaian dengan instruksi-instruksi <br>dan spesifikasi-spesifikasi yang relevan.    </td>
                                                <td><textarea name="2-3-bl"></textarea></td>
                                                <td><textarea name="2-3-btl"></textarea></td>
                                                <td><textarea name="2-3-btl"></textarea></td>
                                                <td><input type="radio" name="2-3" value="K"></td>
                                                <td><input type="radio" name="2-3" value="BK"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">3.Mengulas rencana </td>
                                            </tr>
                                            <tr>
                                                <td>3.1</td>
                                                <td>Hasil-hasil dikenali dan dibandingkan dengan <br>sasaransasaran (yang direncanakan) instruksi-instruksi <br>tugas, spesifikasi-spesifikasi dan syarat-syarat tugas.    </td>
                                                <td><textarea name="3-1-bl"></textarea></td>
                                                <td><textarea name="3-1-btl"></textarea></td>
                                                <td><textarea name="3-1-bt"></textarea></td>
                                                <td><input type="radio" name="3-1" value="K"></td>
                                                <td><input type="radio" name="3-1" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>3.2</td>
                                                <td>Jika perlu, rencana diperbaiki untuk memenuhi <br> sasaransasaran dan syarat-syarat tugas yang lebih baik. </td>
                                                <td><textarea name="3-2-bl"></textarea></td>
                                                <td><textarea name="3-2-btl"></textarea></td>
                                                <td><textarea name="3-2-bt"></textarea></td>
                                                <td><input type="radio" name="3-2"></td>
                                                <td><input type="radio" name="3-2"></td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div> <br>

                    <!-- 3 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>TIK.OP01.002.01 </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Mengidentifikasi Aspek Kode Etik Dan HKI Di Bidang TIK  </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Mengidentifikasi kode etik yang berlaku di dunia TIK.  </td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Norma yang berlaku di dunia TIK dapat diidentifikasi.</td>
                                                <td><textarea name="1=1=bl"></textarea></td>
                                                <td><textarea name="1=1=btl"></textarea></td>
                                                <td><textarea name="1=1=bt"></textarea></td>
                                                <td><input type="radio" name="1=1" value="K"></td>
                                                <td><input type="radio" name="1=1" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Aspek legal atas dokumen elektronik hasil karya orang <br> lain dapat dipahami.</td>
                                                <td><textarea name="1=2=bl"></textarea></td>
                                                <td><textarea name="1=2=btl"></textarea></td>
                                                <td><textarea name="1=2=bt"></textarea></td>
                                                <td><input type="radio" name="1=2" value="K"></td>
                                                <td><input type="radio" name="1=2" value="BK"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2.Mengidentifikasi hal-hal yang berkaitan dengan HKI di dunia TIK.</td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Copyright piranti lunak dan isu hukum berkaitan dengan <br> menggandakan dan membagi file dapat dipahami.  </td>
                                                <td><textarea name="2=1=bl"></textarea></td>
                                                <td><textarea name="2=1=btl"></textarea></td>
                                                <td><textarea name="2=1=bt"></textarea></td>
                                                <td><input type="radio" name="2=1" value="K"></td>
                                                <td><input type="radio" name="2=1" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.2</td>
                                                <td>Akibat yang dapat terjadi jika bertukar file pada suatu <br> jaringan internet dapat dipahami. </td>
                                                <td><textarea name="2=2=bl"></textarea></td>
                                                <td><textarea name="2=2=btl"></textarea></td>
                                                <td><textarea name="2=2=bt"></textarea></td>
                                                <td><input type="radio" name="2=2" value="K"></td>
                                                <td><input type="radio" name="2=2" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.3</td>
                                                <td>Istilah-istilah shareware, freeware dan user license <br> dapat dikenal dan dipahami. </td>
                                                <td><textarea name="2=3=bl"></textarea></td>
                                                <td><textarea name="2=3=btl"></textarea></td>
                                                <td><textarea name="2=3=bt"></textarea></td>
                                                <td><input type="radio" name="2=3" value="K"></td>
                                                <td><input type="radio" name="2=3" value="BK"></td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div> <br>

                    <!-- 4 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>J.620100.004.01 </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Menggunakan Struktur Data   </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Mengidentifikasi konsep data dan struktur data</td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Konsep data dan struktur data diidentifikasi sesuai <br> dengan konteks permasalahan.  </td>
                                                <td><textarea name="1)1)bl"></textarea></td>
                                                <td><textarea name="1)1)btl"></textarea></td>
                                                <td><textarea name="1)1)bt"></textarea></td>
                                                <td><input type="radio" name="1)1" value="K"></td>
                                                <td><input type="radio" name="1)1" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Alternatif struktur data dibandingkan kelebihan dan <br> kekurangannya untuk konteks permasalahan yang diselesaikan. </td>
                                                <td><textarea name="1)2)bl"></textarea></td>
                                                <td><textarea name="1)2)btl"></textarea></td>
                                                <td><textarea name="1)2)bt"></textarea></td>
                                                <td><input type="radio" name="1)2" value="K"></td>
                                                <td><input type="radio" name="1)2" value="BK"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2.Menerapkan struktur data dan akses terhadap struktur data tersebut </td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Struktur data diimplementasikan sesuai dengan bahasa <br> pemrograman yang akan dipergunakan. </td>
                                                <td><textarea name="2)1)bl"></textarea></td>
                                                <td><textarea name="2)1)btl"></textarea></td>
                                                <td><textarea name="2)1)bt"></textarea></td>
                                                <td><input type="radio" name="2)1" value="K"></td>
                                                <td><input type="radio" name="2)1" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.2</td>
                                                <td>Akses terhadap data dinyatakan dalam algoritma yang <br> efisiensi sesuai bahasa pemrograman yang akan dipakai. </td>
                                                <td><textarea name="2)2)bl"></textarea></td>
                                                <td><textarea name="2)2)btl"></textarea></td>
                                                <td><textarea name="2)2)bt"></textarea></td>
                                                <td><input type="radio" name="2)2" value="K"></td>
                                                <td><input type="radio" name="2)2" value="BK"></td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div> <br>
                

                    <!-- 5 -->
                     <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>J.620100.005.01 </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Mengimplementasikan User Interface</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Mengidentifikasi rancangan user interface</td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Rancangan user interface diidentifikasi sesuai <br>kebutuhan. </td>
                                                <td><textarea name="1(1(bl"></textarea></td>
                                                <td><textarea name="1(1(btl"></textarea></td>
                                                <td><textarea name="1(1(bt"></textarea></td>
                                                <td><input type="radio" name="1(1" value="K"></td>
                                                <td><input type="radio" name="1(1" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Komponen user interface dialog diidentifikasi sesuai <br> konteks rancangan proses</td>
                                                <td><textarea name="1(2(bl"></textarea></td>
                                                <td><textarea name="1(2(btl"></textarea></td>
                                                <td><textarea name="1(2(bt"></textarea></td>
                                                <td><input type="radio" name="1(2" value="K"></td>
                                                <td><input type="radio" name="1(2" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.3</td>
                                                <td>Urutan dari akses komponen user interface dialog <br> dijelaskan. </td>
                                                <td><textarea name="1(3(bl"></textarea></td>
                                                <td><textarea name="1(3(btl"></textarea></td>
                                                <td><textarea name="1(3(bt"></textarea></td>
                                                <td><input type="radio" name="1(3" value="K"></td>
                                                <td><input type="radio" name="1(3" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.4</td>
                                                <td>Simulasi (mock-up) dari aplikasi yang akan dikembangkan dibuat</td>
                                                <td><textarea name="1(4(bl"></textarea></td>
                                                <td><textarea name="1(4(btl"></textarea></td>
                                                <td><textarea name="1(4(bt"></textarea></td>
                                                <td><input type="radio" name="1(4" value="K"></td>
                                                <td><input type="radio" name="1(4" value="BK"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2. Melakukan implementasi rancangan user interface</td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Menu program sesuai dengan rancangan program <br> diterapkan. </td>
                                                <td><textarea name="2(1(bl"></textarea></td>
                                                <td><textarea name="2(1(btl"></textarea></td>
                                                <td><textarea name="2(1(bt"></textarea></td>
                                                <td><input type="radio" name="2(1"></td>
                                                <td><input type="radio" name="2(1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.2</td>
                                                <td>Penempatan user interface dialog diatur secara <br> sekuensial. </td>
                                                <td><textarea name="2(2(bl"></textarea></td>
                                                <td><textarea name="2(2(btl"></textarea></td>
                                                <td><textarea name="2(2(bt"></textarea></td>
                                                <td><input type="radio" name="2(2"></td>
                                                <td><input type="radio" name="2(2"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.3</td>
                                                <td>Setting aktif-pasif komponen user interface dialog <br> disesuaikan dengan urutan alur proses. </td>
                                                <td><textarea name="2(3(bl"></textarea></td>
                                                <td><textarea name="2(3(btl"></textarea></td>
                                                <td><textarea name="2(3(bt"></textarea></td>
                                                <td><input type="radio" name="2(3"></td>
                                                <td><input type="radio" name="2(3"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.4</td>
                                                <td>Bentuk style dari komponen user interface ditentukan. </td>
                                                <td><textarea name="2(4(bl"></textarea></td>
                                                <td><textarea name="2(4(btl"></textarea></td>
                                                <td><textarea name="2(4(bt"></textarea></td>
                                                <td><input type="radio" name="2(4"></td>
                                                <td><input type="radio" name="2(4"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.5</td>
                                                <td>Penerapan simulasi dijadikan suatu proses yang <br>sesungguhnya.</td>
                                                <td><textarea name="2(5(bl"></textarea></td>
                                                <td><textarea name="2(5(btl"></textarea></td>
                                                <td><textarea name="2(5(bt"></textarea></td>
                                                <td><input type="radio" name="2(5"></td>
                                                <td><input type="radio" name="2(5"></td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div><br>

                    <!-- 6 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>J.620100.011.01 </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Melakukan Instalasi Software Tools Pemrograman  </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Memilih tools pemrograman yang sesuai dengan kebutuhan </td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Platform (lingkungan) yang akan digunakan untuk <br> menjalankan tools pemrograman diidentifikasi sesuai dengan <br> kebutuhan. </td>
                                                <td><textarea name="1*1*bl"></textarea></td>
                                                <td><textarea name="1*1*btl"></textarea></td>
                                                <td><textarea name="1*1*bt"></textarea></td>
                                                <td><input type="radio" name="1*1" value="K"></td>
                                                <td><input type="radio" name="1*1" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Tools bahasa pemrogram dipilih sesuai dengan kebutuhaan <br> dan lingkungan pengembangan. </td>
                                                <td><textarea name="1*2*bl"></textarea></td>
                                                <td><textarea name="1*2*btl"></textarea></td>
                                                <td><textarea name="1*2*bt"></textarea></td>
                                                <td><input type="radio" name="1*2" value="K"></td>
                                                <td><input type="radio" name="1*2" value="BK"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2.Instalasi tool pemrograman </td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Tools pemrogaman ter-install sesuai dengan prosedur.  </td>
                                                <td><textarea name="2*1*bl"></textarea></td>
                                                <td><textarea name="2*1*btl"></textarea></td>
                                                <td><textarea name="2*1*bt"></textarea></td>
                                                <td><input type="radio" name="2*1" value="K"></td>
                                                <td><input type="radio" name="2*1" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.2</td>
                                                <td>Tools pemrograman bisa dijalankan di lingkungan pengembangan yang telah ditetapkan.  </td>
                                                <td><textarea name="2*2*bl"></textarea></td>
                                                <td><textarea name="2*2*btl"></textarea></td>
                                                <td><textarea name="2*2*bt"></textarea></td>
                                                <td><input type="radio" name="2*2" value="K"></td>
                                                <td><input type="radio" name="2*2" value="BK"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">3. Menerapkan hasil pemodelan kedalam eksekusi script sederhana </td>
                                            </tr>
                                            <tr>
                                                <td>3.1</td>
                                                <td>Script (source code) sederhana dibuat sesuai tools <br> pemrogaman yang di-install</td>
                                                <td><textarea name="3*1*bl"></textarea></td>
                                                <td><textarea name="3*1*btl"></textarea></td>
                                                <td><textarea name="3*1*bt"></textarea></td>
                                                <td><input type="radio" name="3*1" value="K"></td>
                                                <td><input type="radio" name="3*1" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>3.2</td>
                                                <td>Script dapat dijalankan dengan benar dan menghasilkan <br> keluaran sesuai skenario yang diharapkan </td>
                                                <td><textarea name="3*2*bl"></textarea></td>
                                                <td><textarea name="3*2*btl"></textarea></td>
                                                <td><textarea name="3*2*bt"></textarea></td>
                                                <td><input type="radio" name="3*2" value="K"></td>
                                                <td><input type="radio" name="3*2" value="BK"></td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
 
                                        <!-- 7 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>J.620100.012.01  </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Melakukan Pengaturan Software Tools Pemrograman </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Melakukan konfigurasi tools untuk pemrograman </td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Target hasil dari konfigurasi ditentukan. </td>
                                                <td><textarea name="1&1&bl"></textarea></td>
                                                <td><textarea name="1&1&btl"></textarea></td>
                                                <td><textarea name="1&1&bt"></textarea></td>
                                                <td><input type="radio" name="1&1" value="K"></td>
                                                <td><input type="radio" name="1&1" value="BK"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Tools pemrograman setelah dikonfigurasikan, tetap bisa <br> digunakan sebagaimana mestinya. . </td>
                                                <td><textarea name="1&2&bl"></textarea></td>
                                                <td><textarea name="1&2&btl"></textarea></td>
                                                <td><textarea name="1&2&bt"></textarea></td>
                                                <td><input type="radio" name="1&2" value="K"></td>
                                                <td><input type="radio" name="1&2" value="BK"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2.Menggunakan tools sesuai kebutuhan pembuatan program </td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Fitur-fitur dasar yang dibutuhkan untuk mendukung <br> pembuatan program diidentifikasikan. </td>
                                                <td><textarea name="2&1&bl"></textarea></td>
                                                <td><textarea name="2&1&btl"></textarea></td>
                                                <td><textarea name="2&1&bt"></textarea></td>
                                                <td><input type="radio" name="2&1" value="K"></td>
                                                <td><input type="radio" name="2&1" value="BK"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.2</td>
                                                <td>Fitur-fitur dasar tools untuk pembuatan program dikuasai.  </td>
                                                <td><textarea name="2&2&bl"></textarea></td>
                                                <td><textarea name="2&2&btl"></textarea></td>
                                                <td><textarea name="2&2&bt"></textarea></td>
                                                <td><input type="radio" name="2&2" value="K"></td>
                                                <td><input type="radio" name="2&2" value="BK"></td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>

                     <!-- 8 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>J.620100.017.02 </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Mengimplementasikan Pemrograman Terstruktur</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Menggunakan tipe data dan control program</td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Tipe data yang sesuai standar ditentukan.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Syntax program yang dikuasai digunakan sesuai standar. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>
                                            </tr>
                                            <tr>
                                                <td>1.3</td>
                                                <td>Struktur kontrol program yang dikuasai digunakan sesuai <br> standar. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2.Membuat program sederhana </td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Program baca tulis untuk memasukkan data dari keyboard <br>dan menampilkan ke layar monitor termasuk variasinya <br>sesuai standar masukan/keluaran telah dibuat.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.2</td>
                                                <td>Struktur kontrol percabangan dan pengulangan dalam membuat program telah digunakan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">3.Membuat program menggunakan prosedur dan fungsi </td>
                                            </tr>
                                            <tr>
                                                <td>3.1</td>
                                                <td>Program dengan menggunakan prosedur dibuat sesuai aturan penulisan program</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>3.2</td>
                                                <td>Program dengan menggunakan fungsi dibuat sesuai aturan penulisan program. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>3.3</td>
                                                <td>Program dengan menggunakan prosedur dan fungsi secara <br> bersamaan dibuat sesuai aturan penulisan program.  </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>3.4</td>
                                                <td>Keterangan untuk setiap prosedur dan fungsi telah diberikan.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>

                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">4.Membuat program menggunakan array</td>
                                            </tr>
                                            <tr>
                                                <td>4.1</td>
                                                <td>Dimensi array telah ditentukan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>4.2</td>
                                                <td>Tipe data array telah ditentukan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>4.3</td>
                                                <td>Panjang array telah ditentukan.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>4.4</td>
                                                <td>Pengurutan array telah digunakan</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">5. Membuat program untuk akses file</td>
                                            </tr>
                                            <tr>
                                                <td>5.1</td>
                                                <td>Program untuk menulis data dalam media penyimpan telah dibuat.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>5.2</td>
                                                <td>Program untuk membaca data dari media penyimpan telah dibuat.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">6. Mengkompilasi Program </td>
                                            </tr>
                                            <tr>
                                                <td>6.1</td>
                                                <td>Kesalahan program telah dikoreksi.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>6.2</td>
                                                <td>Kesalahan syntax dalam program telah dibebaskan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>

                     <!-- 9 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>J.620100.022.02</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Mengimplementasikan Algoritma Pemrograman</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Menjelaskan varian dan invarian</td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Tipe data telah dijelaskan sesuai kaidah pemrograman. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Variabel telah dijelaskan sesuai kaidah pemrograman. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>
                                            </tr>
                                            <tr>
                                                <td>1.3</td>
                                                <td>Konstanta telah dijelaskan sesuai kaidah pemrograman.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2. Membuat alur logika pemrograman </td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Metode yang sesuai ditentukan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.2</td>
                                                <td>Komponen yang dibutuhkan ditentukan.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.3</td>
                                                <td>Relasi antar komponen ditetapkan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.4</td>
                                                <td>Alur mulai dan selesai ditetapkan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">3. Menerapkan teknik dasar algoritma umum</td>
                                            </tr>
                                            <tr>
                                                <td>3.1</td>
                                                <td>Algoritma untuk sorting dibuat. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>3.2</td>
                                                <td>Algoritma untuk searching dibuat. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">4. Menggunakan prosedur dan fungsi </td>
                                            </tr>
                                            <tr>
                                                <td>4.1</td>
                                                <td>Konsep penggunaan kembali prosedur dan fungsi dapat diidentifikasi. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>4.2</td>
                                                <td>Prosedur dapat digunakan.  </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>4.3</td>
                                                <td>Fungsi dapat digunakan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">5. Mengidentifikasikan kompleksitas algoritma</td>
                                            </tr>
                                            <tr>
                                                <td>5.1</td>
                                                <td>Kompleksitas waktu algoritma diidentifikasi.</td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td> 
                                            <tr>
                                                <td>5.2</td>
                                                <td> Kompleksitas penggunaan memory algoritma diidentifikasi. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                               
                                            </tr>                                              
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>

                     <!-- 10 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr >
                                        <td><strong>Kode Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>J.620100.025.02</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Judul Unit Kompetensi </strong></td>
                                        <td>:</td>
                                        <td>Melakukan Debugging</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Elemen </strong></td>
                                        <td>:</td>
                                        <td>1.Mempersiapkan kode program </td>
                                    </tr>
                                </table>
                                    <table class="table" align="center">
                                        <thead style="background-color: green; color: white;">
                                            <tr>
                                                <th rowspan="3"></th>
                                                <th rowspan="3" colspan="1" style="padding-bottom: 35px;padding-left: 130px">Kriteria Unjuk Kerja</th>
                                                <th colspan="3" class="text-center">Jenis Bukti
                                                    <th colspan="2">Keputusan</th>
                                                    <tr>
                                                        <th>Bukti<br>Langsung</th>
                                                        <th>Bukti Tidak<br>Langsung</th>
                                                        <th>Bukti<br>Tambahan</th>
                                                        <th style="padding-bottom: 20px">K</th>
                                                        <th style="padding-bottom: 20px">BK</th>
                                                    </tr>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.1</td>
                                                <td>Kode program sesuai spesifikasi disiapkan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>
                                            </tr>
                                            <tr>
                                                <td>1.2</td>
                                                <td>Debugging tools untuk melihat proses suatu modul dipersiapkan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">2. Melakukan debugging</td>
                                            </tr>
                                            <tr>
                                                <td>2.1</td>
                                                <td>Kode program dikompilasi sesuai bahasa pemrograman yang digunakan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.2</td>
                                                <td>Kriteria lulus build dianalisis. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.3</td>
                                                <td>Kriteria eksekusi aplikasi dianalisis. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>2.4</td>
                                                <td>Kode kesalahan dicatat. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr style="background-color: green; color: white;">
                                                <td colspan="2"><strong>Elemen</strong></td>
                                                <td align="center">:</td>
                                                <td colspan="4">3. Memperbaiki program</td>
                                            </tr>
                                            <tr>
                                                <td>3.1</td>
                                                <td>Perbaikan terhadap kesalahan kompilasi maupun build dirumuskan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                            <tr>
                                                <td>3.2</td>
                                                <td>Perbaikan dilakukan. </td>
                                                <td><textarea name="1_1_bl"></textarea></td>
                                                <td><textarea name="1_1_btl"></textarea></td>
                                                <td><textarea name="1_1_bt"></textarea></td>
                                                <td><input type="radio" name="1_1"></td>
                                                <td><input type="radio" name="1_1"></td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>

                     <!-- 11 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                    <tr>
                                        <td>Umpan balik terhadap pencapaian unjuk kerja :<br> <br><textarea name="umpan_balik" required="" class="form-control"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td>Identifikasi kesenjangan pencapaian unjuk kerja :<br><br> <textarea required="" name="identifikasi" class="form-control"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td>Saran tindak lanjut hasil asesmen  : <br><br> <textarea name="saran_tindak_lanjut" required="" class="form-control"></textarea></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- 12 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                <tr>
                                    <td rowspan="7"><strong><div style="margin-bottom: 600px">
                                    Rekomendasi Asesor :</div></strong>                   
                                    </td>
                                    <td colspan="2"><strong>Peserta:</strong>
                                <tr>
                                    <td>Nama</td>
                                    <td class="col-md-4"><input required="" type="text" class="form-control" name="nama_peserta"></td>
                                </tr>
                                    </td>
                            <td>Tanda tangan/Tanggal</td>
                            <td><div align="center">Tanda Tangan :</div><br><br><br><br>_________________________________________________ <br><br><input  type="date" class="form-control border-bottom" required="" name="tgl_peserta"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><strong>Asesor:</strong></td>
                        </tr>
                        <tr>
                            <td>Nama :</td>
                            <td><input required="" type="text" class="form-control" name="nama_asesor"></td>
                        </tr>
                        <tr>
                            <td>No Reg :</td>
                            <td><input required="" type="text" class="form-control" name="no_reg"></td>
                        </tr>
                        <tr>
                            <td>Tanda tangan/Tanggal</td>
                            <td><div align="center">Tanda Tangan :</div><br><br><br><br>_________________________________________________ <br><br><input  type="date" required="" class="form-control border-bottom" name="tgl_asesor"></td>
                        </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div align="right">
                    <input type="submit" class="btn btn-primary btn-round" name="Save" value="save">
                    <br>
                    </div>
                    </form>
                </div>

            </div>
        </div>
@endsection
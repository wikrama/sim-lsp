@extends('layouts.mimin-login')

@section('content')
  <img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  <div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
        <div>
        	<div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-heading">
                            <h4>Umpan Balik Peserta</h4>
                        </div>
                        <div class="panel-body">
                        	<div class="responsive-table">
                            <form action="{{ URL('/umpan_peserta/store')}}" method="post">
                              {{ csrf_field() }}
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                        <thead style="background-color: green; color: white;">
                    <tr>
                      <th rowspan="3" style="padding-bottom: 25px;">KOMPONEN</th>
                      <th colspan="2" class="text-center">Hasil
                        <tr>
                          <th class="text-center">Ya</th>
                          <th class="text-center">Tdk</th>
                        </tr>
                      </th>
                    </tr>
                  </thead>
                        <tbody>
                        <tr>
                          <td>• Saya mendapatkan penjelasan yang cukup memadai mengenai proses asesmen/uji kompetensi</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="pnjls_cukup" value="Y " />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="pnjls_cukup" value="N " />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td>• Saya diberikan kesempatan untuk mempelajari standar kompetensi yang akan diujikan dan menilai diri sendiri terhadap pencapaiannya</td>
                         <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ksmptn_mmpelajari" value="Y " />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ksmptn_mmpelajari" value="N " />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td>• Asesor memberikan kesempatan untuk mendiskusikan/ menegosiasikan metoda, instrumen dan sumber asesmen serta jadwal asesmen </td>
                          <td>
                          <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ksmptn_diskusi" value="Y" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ksmptn_diskusi" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td>• Asesor berusaha menggali seluruh bukti pendukung yang sesuai dengan latar belakang pelatihan dan pengalaman yang saya miliki</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="menggali_bukti" value="Y " />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="menggali_bukti" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td>• Saya mendapatkan jaminan kerahasiaan hasil asesmen serta penjelasan penanganan dokumen asesmen</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="jaminan_rahasia" value="Y " />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="jaminan_rahasia" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td>• Saya sepenuhnya diberikan kesempatan untuk mendemonstrasikan kompetensi yang saya miliki selama asesmen</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ksmptn_demontrasi" value="Y " />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="ksmptn_demontrasi" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td>• Saya mendapatkan penjelasan yang memadai mengenai keputusan asesmen</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="pnjlsn_memadai" value="Y" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="pnjlsn_memadai" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td>• Asesor memberikan umpan balik yang mendukung setelah asesmen serta tindak lanjutnya</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="umpan_balik" value="Y" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="umpan_balik" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td>• Asesor menggunakan keterampilan komunikasi yang efektif selama asesmen</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="komunikasi" value="Y" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="komunikasi" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td>• Asesor bersama saya menandatangani semua dokumen hasil asesmen </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="tanda_tangan" value="Y" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="tanda_tangan" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="4">Catatan/komentar lainnya (apabila ada) : <textarea class="form-control border-bottom" name="catatan"></textarea></td>
                        </tr>
                        </tbody>
                        </table>
                        <div class="text-right">
                          <input type="submit" class="btn btn-primary" name="SAVE" value="SAVE">
                        </div>
                        </form>
                      </div>
                        </div>
                    </div>
                </div>
            </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

	</div>
</div>
@endsection
@extends('layouts.mimin-login')

@section('content')
  <img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  <div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
        <div>
        	<div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-heading">
                            <h4>Formulir Data Banding</h4>
                        </div>
                        <div class="panel-body">
                        	<div class="responsive-table">
                        		<form action="{{ URL('/formulir_banding/store')}}" method="post">
                        			{{ csrf_field() }}
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      	<tr>                  		
                      		<td colspan="3"><strong>Nama Peserta : <input class="form-control border-bottom" type="text" name="nama_psrt" value="{{ Auth::user()->name }}" readonly="" required=""></strong></td>
                      	</tr>

                      	<tr>
	                  		<td colspan="3"><strong>Nama Asesi : <input class="form-control border-bottom" type="text" name="nama_assr" required=""></strong></td>
                      	</tr>

                      	<tr>
                      		<td colspan="3"><strong>Tanggal Assesmen : <input class="form-control border-bottom" required="" type="date" name="tgl_ases"></strong></td>
                      	</tr>

                      	<tr style="background-color: green; color: white;">
                      		<td>Jawablah dengan <strong>Ya</strong> atau <strong>Tidak</strong> pertanyaan-pertanyaan berikut ini :</td>
                      		<td align="center">YA</td>
                      		<td align="center">TIDAK</td>
                      	</tr>

                      	<tr>
                      		<td>Apakah Proses Banding telah dijelaskan kepada Anda?</td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="penjelasan_proses" value="Y " />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="penjelasan_proses" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                              <!-- <input type="radio" value="Y"></td> -->
                      	</tr>

                      	<tr>
                      		<td>Apakah Anda telah mendiskusikan Banding dengan Asesor?</td>
                      		<td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="diskusi_asesor" value="Y" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>                         
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="diskusi_asesor" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                      	</tr>

                      	<tr>
                      		<td>Apakah Anda mau melibatkan “orang lain” membantu Anda dalam Proses Banding?</td>
                      		<td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="melibatkan_orng" value="Y" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                          <td>
                            <div class="form-animate-radio" align="center">
                          <label class="radio">
                            <input id="radio1" type="radio" name="melibatkan_orng" value="N" />
                            <span class="outer">
                              <span class="inner"></span></span>
                            </label>
                          </div>
                          </td>
                      	</tr>

                      	<tr>
                      		<td colspan="3"></td>
                      	</tr>
                      	<tr>
                      		<td colspan="3">Banding ini diajukan atas Keputusan Asesmen yang dibuat terhadap Unit Kompetensi berikut :</td>
                      	</tr>
                      	<tr>
                      		<td colspan="3">No. Unit Kompetensi    : <input class="form-control border-bottom" required="" type="text" name="no_unit"></td>
                      	</tr>

                      	<tr>
                      		<td colspan="3">Judul Unit Kompetensi : <input class="form-control border-bottom" required="" type="text" name="judul_unit"></td>
                      	</tr>

                      	<tr>
                      		<td colspan="3">Banding ini diajukan atas alasan sebagai berikut :<br><textarea class="form-control border-bottom" name="alasan"></textarea></td>
                      	</tr>

                      	<tr>
                      		<td colspan="3">Anda mempunyai hak mengajukan banding jika Anda mendapatkan hasil yang <strong>Tidak Sah</strong> dan/atau <strong>Proses Tidak Sah</strong> atau <strong>Tidak Adil.</strong> </td>
                      	</tr>

                      	<tr>
                      		<td colspan="3">Tanda tangan Asesi : <br> <br><br>......................................<br><br>  Tanggal :<input class="form-control border-bottom" type="date" name="tanggal" required=""><br><br><br></td>
                      	</tr>
                        
                        </table><br>
                        <div class="text-right">
                        <button class="btn btn-primary">Save</button>
                        </div>
                        </form>
                      </div>
                        </div>
                    </div>
                </div>
            </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

	</div>
</div>
@endsection
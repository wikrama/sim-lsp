@extends('layouts.mimin-login')

@section('content')

  <img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  <div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
            <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-heading" align="center">
                            <h4><strong>SKEMA SERTIFIKASI KKNI LEVEL II PADA <br>KOMPETENSI KEAHLIAN <br> REKAYASA PERANGKAT LUNAK <br>LSP SMK WIKRAMA BOGOR
</strong></h4>
                        </div>
                        <div align="center" class="panel-body">
                        	<h4> <br>Skema Sertifikasi KKNI Level II pada Kompetensi <br>Keahlian Rekayasa perangkat Lunak merupakan skema <br> sertifikasi kualifikasi yang dikembangkan oleh komite skema <br> sertifikasi LSP SMK Wikrama Bogor. Skema mengacu pada <br> SKKNI Nomor KEP.240/MEN/X/2004 Tentang Penetapan <br>Standar Kompetensi Kerja Nasionai Indonesia Sektor Logam <br>Mesin, SKKNI Nomor KEP.94/MEN/IV/2005 Tentang Penetapan <br>Standar Kompetensi Kerja Nasional Indonesia Sektor <br>Teknologi Informasi dan Komunikasi Sub Sektor Operator <br>Komputer, dan SKKNI Nomor 282 Tahun 2016 Tentang Penetapan <br>Standar Kompetensi Kerja Nasional Indonesia Kategori <br>Informasi dan Komunikasi Golongan Pokok Aktivitas <br>Pemrograman, Konsultasi Komputer dan Kegiatan Yang <br>Berhubungan Dengan Itu (YBDI) Bidang Software Development <br>Subbidang Pemrograman. Skema sertifikasi ini digunakan <br>untuk memastikan kompetensi lulusan Sekolah Menengah <br>Kejuruan dan sebagai acuan bagi LSP dan asesor kompetensi <br>dalam pelaksanan sertifikasi Kompetensi Keahlian <br> Rekayasa Perangkat Lunak.</h4>
                    </div>
                </div>  
            </div> 

            <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-heading">
                          <img src="asset/img/lsp2.png" style="width: 200px; height: 90px;">
                          	<img src="asset/img/lsp-wk.jpg" align="right" style="width: 200px; height: 90px;">
                        </div>
                        <div class="panel-body">
                        	<h4 align="center"><strong>SKEMA SERTIFIKASI KKNI LEVEL II <br>PADA KOMPETENSI KEAHLIAN <br>REKAYASA PERANGKAT LUNAK <br>LSP SMK WIKRAMA BOGOR <br><br>
							Skema sertifikasi ini telah diverifikasi oleh:<br></strong></h4>
							<br>
                        	<H4>
                        	<div class="form-group col-md-4">
								<label>1.	Muslih, M.Kom			: </label>
								<input class="form-control" type="text" name="">
                        	</div>
                        	<div class="form-group col-md-4">
                        		<label>2.	Juliana Mansyur, S.Kom 	: </label>
                        		<input class="form-control" type="text" name="">	
                        	</div>
                        	<div class="form-group col-md-4">
							<label>3.	Acep Rahmat, S.Kom	 	: </label>
							<input class="form-control" type="text" name="">	
                        	</div>
							</H4>
							<h3><strong><br>1.	LATAR BELAKANG</strong></h3><br>
							<h5>
								<ul>
								<li>Pemberlakuan era persaingan bebas dalam regional Asia Tenggara yang dikenal dengan sebutan Masyarakat Ekononni ASEAN (MEA) sudah diberlakukan.</li>
								</ul> Perhimpunan masyarakat bangsa Asia Tenggara dalam organisasi Association of South East Asian Nation (ASEAN) sepakat untuk memperkuat kawasan dengan membuka akses perekonomian lewat pasar bebas yang dimulai sejak tahun 2016 ini. Beberapa sektor sudah disepakati terbuka untuk menuju integrasi ekonomi Visi ASEAN 2020. Masyarakat Ekonomi ASEAN tidak hanya membuka arus perdagangan barang atau jasa, tetapi juga untuk tenaga Rekayasa Perangkat Lunak dan lainnya. Oleh karena itu, MEA secara langsung akan menuntut kualitas tenaga kerja di Indonesia. <br> <br> <br> <br>
								<ul><li>Undang-undang Republik Indonesia No. 20 tahun 2003 Tentang Sistem Pendidikan Nasional Bab XVI pasal 61 ayat 3 menyatakan bahwa sertifikat kompetensi</li></ul> diberikan oleh penyelenggara pendidikan dan pelatihan kepada peserta didik dan warga masyarakat sebagai pengakuan terhadap kompetensi untuk melakukan pekerjaan tertentu setelah lulus uji kompetensi yang diselenggarakan oleh satuan pendidikan yang terakreditasi atau lembaga sertifikasi. <br> <br> <br> <br>
								<ul><li>Tuntutan kebutuhan industri di bidang Rekayasa Perangkat Lunak menghendaki tenaga kerja yang memiliki kompetensi yang terstandarisasi dan profesional.</li></ul> Tenaga kerja yang memiliki kompetensi yang baik bersumber dari proses pendidikan yang baik, maka untuk membangun, memelihara, dan memastikan kompetensi bagi peserta didik program keahlian Rekayasa Perangkat Lunak perlu diselenggarakannya sertifikasi kompetensi oleh LSP SMK yang sesuai dengan Keputusan Direktur Jenderal Pendidikan Dasar dan Menengah Kementerian Pendidikan dan Kebudayaan Nomor: 4678/D/KEP/MK/2016 tentang Spektrum Keahlian Pendidikan Menengah Kejuruan.<br><br><br><br><ul><li>Dengan skema sertifikasi yang mengacu langsung pada SKKNI ini diharapkan dapat memberi manfaat langsung para pemangku kepentingan.</li></ul>
								<h5><strong>1.1	Bagi Industri</strong></h5>
								<ul>
								  <li>1.1.1	Membantu industri meyakinkan kepada kliennya bahwa jasanya telah dibuat oleh tenaga-tenaga yang kompeten.</li>
								  <li>1.1.2	Membantu industri dalam rekruitmen dan mengembangkan tenaga berbasis kompetensi guna meningkatkan efisensi pengembangan SDM khususnya dan efisiensi nasional pada umumnya</li>
								  <li>1.1.3	Membantu industri dalam sistem pengembangan karir dan remunerasi tenaga berbasis kompetensi dan meningkatkan produktivitas.</li>
								</ul>  
							</h5>
							<h5><strong>1.2	Bagi Tenaga Kerja</strong></h5>
								<ul>
								  <li>1.2.1	Membantu tenaga profesi meyakinkan kepada organisasi/industri/kliennya bahwa dirinya kompeten dalam bekerja atau menghasilkan jasa dan meningkatkan percaya diri tenaga profesi</li>
								  <li>1.2.2	Membantu tenaga profesi dalam merencanakan karirnya dan mengukur tingkat pencapaian kompetensi dalam proses belajar di lembaga formal maupun secara mandiri.</li>
								  <li>1.2.3	Membantu tenaga profesi dalam memenuhi persyaratan regulasi.</li>
								  <li>1.2.4	Membantu pengakuan kompetensi lintas sektor dan lintas negara</li>
								  <li>1.2.5	Membantu tenaga profesi dalam promosi profesinya dipasar tenaga kerja</li>
								</ul>
							<h5><strong>1.3	Bagi Lembaga Pendidlkan dan juga Pelatihan.</strong></h5>
								<ul>
								  <li>1.3.1	Membantu memastikan link and match antara kompetensi lulusan dengan tuntutan kompetensi dunia industri.</li>
								  <li>1.3.2	Membantu memastikan tercapainya efisiensi dalam pengembangan program diklat.</li>
								  <li>1.3.3	Membantu memastikan pencapain hasil diklat yang tinggi</li>
								  <li>1.3.4	Membantu Lembaga diklat dalam sistem asesmen yang dapat memastikan dan
								  memelihara kompetensi peserta diklat.</li>
								</ul>
							<h5><strong>2.	RUANG LINGKUP SKEMA SERTIFIKASI</strong></h5>
								<ul>
								  <li>2.1	Ruang Lingkup: Rekayasa Perangkat Lunak</li>
								  <li>2.2	Lingkup penggunaan sertifikat: pada perusahaan, instansi, lembaga, atau organisasi yang memiliki divisi atau berkaitan dengan teknologi informasi dan komunikasi.</li>
								</ul>
							<h5><strong>3.	TUJUAN SERTIFIKASI</strong></h5>
								<ul>
								  <li>3.1	Memastikan kompetensi kerja KKNI Level II Rekayasa Perangkat Lunak</li>
								  <li>3.2	Sebagai acuan dalam melaksanakan asesmen oleh LSP SMK Wikrama Bogor dan asesor kompetensi.</li>
								</ul> 
							<h5><strong>4.	ACUAN NORMATIF</strong></h5>
								<ul>
								  <li>Acuan-acuan yang digunakan dalam menyusun skema sertifikasi ini meliputi:</li>
								  <li>4.1	Undang-undang Republik Indonesia Nomor 13 Tahun 2003 Tentang Ketenagakerjaan. </li>
								  <li>4.2	Undang-undang Republik Indonesia Nomor 20 Tahun 2003 Tentang Sistem Pendidikan Nasional.</li>
								  <li>4.3	Undang-undang Republik Indonesia Nomor 3 Tahun 2014 Tentang Perindustrian.</li>
								  <li>4.4	Peraturan Pemerintah Republik Indonesia Nomor 23 Tahun 2004 Tentang Badan Nasional Sertifikasi Profesi.</li>
								  <li>4.5	Peraturan Pemerintah Republik Indonesia Nomor 31 Tahun 2006 Tentang Sistem Pelatihan Kerja Nasional.</li>
								  <li>4.6	Instruksi Presiden nomor 9 Tahun 2016 Tentang Revitalisasi Sekolah Menengah Kejuruan Dalam Rangka Peningkatan Kualitas dan Daya Saing Sumber Daya Manusia Indonesia.</li>
								  <li>4.7	Peraturan Menteri Ketenagakerjaan Republik Indonesia Nomor 2 Tahun 2016 Tentang Sistem Standardisasi Kompetensi Kerja Nasional.</li>
								  <li>4.8	Peraturan Menteri Ketenagakerjaan Republik Republik Indonesia No. 3 Tahun 2016 Tentang Tatacara Penetapan Standar Kompetensi Kerja Nasional Indonesia.</li>
								  <li>4.9	Keputusan Menteri Tenaga Kerja dan Transmigrasi Republik Indonesia Nomor: KEP.240/MEN/X/2004 Tentang Penetapan Standar Kompetensi Kerja Nasional Indonesia Sektor Logam dan Mesin.</li>
								  <li>4.10	Keputusan Menteri Tenaga Kerja dan Transmigrasi Republik Indonesia Nomor KEP.94/MEN/IV/2005 Tentang Penetapan Standar Kompetensi Kerja Nasional Indonesia Sektor Teknologi Informasi dan Komunikasi Sub Sektor Operator Komputer.</li>
								  <li>4.11	Keputusan Menteri Ketenagakerjaan Republik indonesia Nomor 282 Tahun 2016 Tentang Penetapan Standar Kompetensi Kerja Nasional Indonesia Kategori Informasi dan Komunikasi Golongan Pokok Aktivitas Pemrograman.</li>
								  <li>4.12	Surat Keputusan Direktur Jenderal Pendidikan Dasar dan	Menengah No.130/D/KEP /KR/2017 Tentang Struktur Kurikuium Pendidikan Menengah Kejuruan.</li>
								  <li>4.13	Peraturan Badan Nasional Sertifikasi Profesi Nomor: 1/BNSP/IIl/2014 Tentang Penilaian Persyaratan Umum Lembaga Sertifikasi Profesi.</li>
								  <li>4.14	Peraturan Badan Nasional Sertifikasi Profesi Nomor: 2/BNSP/VIII/2017 tentang Pedoman Pengembangan dan Pemeliharaan Skema Sertifikasi Profesi</li>
								  <li>4.15	Peraturan Badan Nasional Sertifikasi Profesi Nomor: 1/BNSP/II/2017 tentang Pedoman Pelaksanaan Sertifikasi Profesi bagi Lulusan SMK</li>
								  <li>4.16	Peraturan Badan Nasional Sertifiasi Profesi Nomor: 1/BNSP/II/2017 Tentang Pedoman Pelaksanaan Sertifikasi Kompetensi Bagi Lulusan Sekolah Menengah Kejuruan.</li>
								</ul> 
							<h5><strong>5.	KEMASAN / PAKET KOMPETENSI</strong></h5>
								<ul>
								  <li><strong>5.1	Deskripsi</strong><ul> <li>Jenis kemasan ini adalah kemasan KKNI yang merupakan kualifikasi kompetensi teknis lulusan SMK. Kualifikasi ini merefleksikan peran individu dalam melaksanakan satu tugas spesifik, dengan menggunakan alat, dan informasi, dan prosedur kerja yang lazim dilakukan, serta menunjukkan kinerja dengan mutu yang terukur, di bawah pengawasan langsung atasannya. Memiliki pengetahuan operasional dasar dan pengetahuan faktual bidang kerja yang spesifik, sehingga mampu memilih penyelesaian yang tersedia terhadap masalah yang lazim timbul. Bertanggung jawab pada pekerjaan sendiri dan dapat diberi tanggung jawab membimbing orang lain.</li></ul></li>
								  <br>
								  <li><strong>5.2	Sikap Kerja</strong><ul>
								  	<li>5.2.1	Bertaqwa kepada Tuhan Yang Maha Esa.</li>
								  	<li>5.2.2	Memiliki moral, etika dan kepribadian yang baik di dalam menyelesaikan tugasnya.</li>
								  	<li>5.2.3	Berperan sebagai warga negara yang bangga dan cinta tanah air serta mendukung perdamaian dunia.</li>
								  	<li>5.2.4	Mampu bekerja sama dan memiliki kepekaan sosial dan kepedulian yang tinggi terhadap masyarakat dan lingkungannya.</li>
								  	<li>5.2.5	Menghargai keanekaragaman budaya, pandangan, kepercayaan, dan agama serta pendapat/temuan original orang lain.</li>
								  	<li>5.2.6	Menjunjung tinggi penegakan hukum serta memilik semangat untuk mendahulukan kepentingan bangsa serta masyarakat luas.</li></ul>
								</li> <br>
								  <li><strong>5.3	Peran Kerja</strong>
								  	<ul>
								  		<li>KKNI ini merupakan jalur untuk bekerja pada kompetensi keahlian Rekayasa Perangkat Lunak, dalam melaksanakan pekerjaan, bertanggungjawab pada pekerjaan sendiri dan dapat diberi tanggung jawab membimbing orang lain.</li>
								  	</ul>
								  </li><br>

								  <li><strong>5.4	Kemungkinan Jabatan</strong>
								  	<ul>
								  		<li>Kemungkinan jabatan yang dapat diemban oleh pemegang sertifikat ini adalah -</li>
								  	</ul>
								  </li><br>

								  <li><strong>5.5	Aturan Pengemasan</strong>
								  	<ul>
								  		<li>Didalam pemaketan yang ditetapkan untuk level II Kompetensi Keahlian Rekayasa Perangkat Lunak adalah sebagai berikut:</li>
								  		<li>5.5.1	Jenis Kemasan	: KKNl</li>
								  		<li>5.5.2	Nama Skema	: KKNl Level	II pada Kompetensi Keahlian Rekayasa Perangkat Lunak</li>
								  		<li>5.5.3	Aturan Pengemasan: 
								  			<ul>
								  				<li>Untuk mendapatkan KKNl Level II pada Kompetensi Keahlian Rekayasa Perangkat Lunak, kompetensi yang harus dicapai dengan total 23 (dua puluh tiga) unit kompetensi yang terdiri dari:</li>
								  				<li>a.	6 (enam) Unit Kompetensi Inti</li>
								  				<li>b.	17 (tujuh belas) Unit Kompetensi pilihan</li>
								  			</ul>
								  		</li>
								  	</ul>
								  </li>
								  <li><strong>5.6	Rincian Unit Kompetensi</strong>
								  	<ul>
								  		<li>Rincian Unit Kompetensi atau Uraian Tugas</li>
								  		<li>
								  			<table>
								  					<tr>
								  						<td><strong>NO</strong></td>
								  						<td><strong>Kode Unit</strong></td>
								  						<td><strong>Judul Unit</strong></td>
								  					</tr>
								  						<tr>
								  							<td></td>
								  							<td colspan="2"><strong>KOMPETENSI UMUM DAN INTI</strong></td>
								  						</tr>
								  						<tr>
								  							<td>1</td>
								  							<td>LOG.OO01.001.01</td>
								  							<td>Melakukan komunikasi kerja timbal balik</td>
								  						</tr>
								  						<tr>
								  							<td>2</td>
								  							<td>LOG.OO01.002.01</td>
								  							<td>Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</td>
								  						</tr>
								  						<tr>
								  							<td>3</td>
								  							<td>LOG.OO01.004.01</td>
								  							<td>Merencanakan tugas rutin</td>
								  						</tr>
								  						<tr>
								  							<td>4</td>
								  							<td>LOG.OO02.001.01</td>
								  							<td>Menerapkan sistem mutu</td>
								  						</tr>
								  						<tr>
								  							<td>5</td>
								  							<td>LOG.OO02.003.01</td>
								  							<td>Melakukan pekerjaan yang membutuhkan kerjasama tim</td>
								  						</tr>
								  						<tr>
								  							<td>6</td>
								  							<td>TIK.OP01.002.01</td>
								  							<td>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</td>
								  						</tr>
								  						<tr>
								  							<td></td>
								  							<td colspan="2"><strong>KOMPETENSI PILIHAN / FUNGSIONAL</strong></td>
								  						</tr>
								  						<tr>
								  							<td>7</td>
								  							<td>J.620100.004.02</td>
								  							<td>Menggunakan struktur data</td>
								  						</tr>
								  						<tr>
								  							<td>8</td>
								  							<td>J.620100.005.02</td>
								  							<td>Mengimplementasikan user interface</td>
								  						</tr>
								  						<tr>
								  							<td>9</td>
								  							<td>J.620100.007.01</td>
								  							<td>Mengimplementasikan rancangan entitas dan keterkaitan antar entitas</td>
								  						</tr>
								  						<tr>
								  							<td>10</td>
								  							<td>J.620100.009.01</td>
								  							<td>Menggunakan spesifikasi program</td>
								  						</tr>
								  						<tr>
								  							<td>11</td>
								  							<td>J.620100.010.01</td>
								  							<td>Menerapkan perintah eksekusi bahasa pemrograman berbasis teks, grafik, dan multimedia</td>
								  						</tr>
								  						<tr>
								  							<td>12</td>
								  							<td>J.620100.011.01</td>
								  							<td>Melakukan instalasi software tools pemrograman</td>
								  						</tr>
								  						<tr>
								  							<td>13</td>
								  							<td>J.620100.012.01</td>
								  							<td>Melakukan pengaturan software tools pemrograman</td>
								  						</tr>
								  						<tr>
								  							<td>14</td>
								  							<td>J.620100.017.02</td>
								  							<td>Mengimplementasikan pemrograman terstruktur</td>
								  						</tr>
								  						<tr>
								  							<td>15</td>
								  							<td>J.620100.018.02</td>
								  							<td>Mengimplementasikan pemrograman berorientasi objek</td>
								  						</tr>
								  						<tr>
								  							<td>16</td>
								  							<td>J.620100.020.02</td>
								  							<td>Menggunakan SQL</td>
								  						</tr>
								  						<tr>
								  							<td>17</td>
								  							<td>J.620100.022.02</td>
								  							<td>Mengimplementasikan algoritma pemrograman</td>
								  						</tr>
								  						<tr>
								  							<td>18</td>
								  							<td>J.620100.023.02</td>
								  							<td>Membuat dokumen kode program</td>
								  						</tr>
								  						<tr>
								  							<td>19</td>
								  							<td>J.620100.025.02</td>
								  							<td>Melakukan debugging</td>
								  						</tr>
								  						<tr>
								  							<td>20</td>
								  							<td>J.620100.030.02</td>
								  							<td>Menerapkan pemrograman multimedia</td>
								  						</tr>
								  						<tr>
								  							<td>21</td>
								  							<td>J.620100.033.02</td>
								  							<td>Melaksanakan pengujian unit program</td>
								  						</tr>
								  						<tr>
								  							<td>22</td>
								  							<td>J.620100.042.01</td>
								  							<td>Melaksanakan konfigurasi perangkat lunak sesuai environment development, staging, production</td>
								  						</tr>
								  						<tr>
								  							<td>23</td>
								  							<td>J.620100.046.01</td>
								  							<td>Melakukan logging aplikasi</td>
								  						</tr>
								  			</table>
								  		</li>
								  	</ul>
								  </li><br>

								  <li><strong>5.7	Pencapaian Kompetensi</strong>
								  	<ul>
								  		<li>Skema KKNI Level II pada kompetensi keahlian Rekayasa Perangkat Lunak dapat dicapai melalui pendekatan klaster dan harus dicapai dalam 3 (tiga) tahun. Klaster yang digunakan adalah sebagai berikut:</li> <br>
								  		<li><strong>5.7.1	Pemrograman Dasar</strong></li><br>
								  		<li>
								  			<table>
								  					<tr>
								  						<td><strong>NO</strong></td>
								  						<td><strong>Kode Unit</strong></td>
								  						<td><strong>Judul Unit</strong></td>
								  					</tr>
								  						<tr>
								  							<td></td>
								  							<td colspan="2"><strong>KOMPETENSI UMUM DAN INTI</strong></td>
								  						</tr>
								  						<tr>
								  							<td>1</td>
								  							<td>LOG.OO01.002.01</td>
								  							<td>Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</td>
								  						</tr>
								  						<tr>
								  							<td>2</td>
								  							<td>LOG.OO01.004.01</td>
								  							<td>Merencanakan tugas rutin</td>
								  						</tr>
								  						<tr>
								  							<td>3</td>
								  							<td>TIK.OP01.002.01</td>
								  							<td>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</td>
								  						</tr>
								  						<tr>
								  							<td></td>
								  							<td colspan="2"><strong>KOMPETENSI PILIHAN / FUNGSIONAL</strong></td>
								  						</tr>
								  						<tr>
								  							<td>4</td>
								  							<td>J.620100.004.02</td>
								  							<td>Menggunakan struktur data</td>
								  						</tr>
								  						<tr>
								  							<td>5</td>
								  							<td>J.620100.005.02</td>
								  							<td>Mengimplementasikan user interface</td>
								  						</tr>
								  						<tr>
								  							<td>6</td>
								  							<td>J.620100.011.01</td>
								  							<td>Melakukan instalasi software tools pemrograman</td>
								  						</tr>
								  						<tr>
								  							<td>7</td>
								  							<td>J.620100.012.01</td>
								  							<td>Melakukan pengaturan software tools pemrograman</td>
								  						</tr>
								  						<tr>
								  							<td>8</td>
								  							<td>J.620100.017.02</td>
								  							<td>Mengimplementasikan pemrograman terstruktur</td>
								  						</tr>
								  						<tr>
								  							<td>9</td>
								  							<td>J.620100.022.02</td>
								  							<td>Mengimplementasikan algoritma pemrograman</td>
								  						</tr>
								  						<tr>
								  							<td>10</td>
								  							<td>J.620100.025.02</td>
								  							<td>Melakukan debugging</td>
								  						</tr>
								  			</table>
								  		</li> <br>
								  		<li><strong>5.7.2	Pemrograman Web</strong></li><br>
								  		<li>
								  			<table>
								  					<tr>
								  						<td><strong>NO</strong></td>
								  						<td><strong>Kode Unit</strong></td>
								  						<td><strong>Judul Unit</strong></td>
								  					</tr>
								  						<tr>
								  							<td></td>
								  							<td colspan="2"><strong>KOMPETENSI UMUM DAN INTI</strong></td>
								  						</tr>
								  						<tr>
								  							<td>1</td>
								  							<td>LOG.OO01.001.01</td>
								  							<td>Melakukan komunikasi kerja timbal balik</td>
								  						</tr>
								  						<tr>
								  							<td>2</td>
								  							<td>LOG.OO01.002.01</td>
								  							<td>Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</td>
								  						</tr>
								  						<tr>
								  							<td>3</td>
								  							<td>LOG.OO01.004.01</td>
								  							<td>Merencanakan tugas rutin</td>
								  						</tr>
								  						<tr>
								  							<td>4</td>
								  							<td>LOG.OO02.003.01</td>
								  							<td>Melakukan pekerjaan yang membutuhkan kerjasama tim</td>
								  						</tr>
								  						<tr>
								  							<td>5</td>
								  							<td>TIK.OP01.002.01</td>
								  							<td>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</td>
								  						</tr>
								  						<tr>
								  							<td></td>
								  							<td colspan="2"><strong>KOMPETENSI PILIHAN / FUNGSIONAL</strong></td>
								  						</tr>
								  						<tr>
								  							<td>6</td>
								  							<td>J.620100.004.02</td>
								  							<td>Menggunakan struktur data</td>
								  						</tr>
								  						<tr>
								  							<td>7</td>
								  							<td>J.620100.005.02</td>
								  							<td>Mengimplementasikan user interface</td>
								  						</tr>
								  						<tr>
								  							<td>8</td>
								  							<td>J.620100.007.01</td>
								  							<td>Mengimplementasikan rancangan entitas dan keterkaitan antar entitas</td>
								  						</tr>
								  						<tr>
								  							<td>9</td>
								  							<td>J.620100.009.01</td>
								  							<td>Menggunakan spesifikasi program</td>
								  						</tr>
								  						<tr>
								  							<td>10</td>
								  							<td>JJ.620100.012.01</td>
								  							<td>Melakukan pengaturan software tools pemrograman</td>
								  						</tr>
								  						<tr>
								  							<td>11</td>
								  							<td>J.620100.017.02</td>
								  							<td>Mengimplementasikan pemrograman terstruktur</td>
								  						</tr>
								  						<tr>
								  							<td>12</td>
								  							<td>J.620100.020.02</td>
								  							<td>Menggunakan SQL</td>
								  						</tr>
								  			</table>
								  		</li> <br>
								  		<li><strong>5.7.3	Pemrograman Berorientasi Objek</strong></li>
								  		<li>
								  			<table>
								  					<tr>
								  						<td><strong>NO</strong></td>
								  						<td><strong>Kode Unit</strong></td>
								  						<td><strong>Judul Unit</strong></td>
								  					</tr>
								  						<tr>
								  							<td></td>
								  							<td colspan="2"><strong>KOMPETENSI UMUM DAN INTI</strong></td>
								  						</tr>
								  						<tr>
								  							<td>1</td>
								  							<td>LOG.OO01.002.01</td>
								  							<td>Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</td>
								  						</tr>
								  						<tr>
								  							<td>2</td>
								  							<td>LOG.OO02.001.01</td>
								  							<td>Menerapkan sistem mutu</td>
								  						</tr>
								  						<tr>
								  							<td>3</td>
								  							<td>TIK.OP01.002.01</td>
								  							<td>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</td>
								  						</tr>
								  						<tr>
								  							<td></td>
								  							<td colspan="2"><strong>KOMPETENSI PILIHAN / FUNGSIONAL</strong></td>
								  						</tr>
								  						<tr>
								  							<td>4</td>
								  							<td>J.620100.005.02</td>
								  							<td>Mengimplementasikan user interface</td>
								  						</tr>
								  						<tr>
								  							<td>5</td>
								  							<td>J.620100.010.01</td>
								  							<td>Menerapkan perintah eksekusi bahasa
															pemrograman berbasis teks, grafik, dan
															multimedia
															</td>
								  						</tr>
								  						<tr>
								  							<td>6</td>
								  							<td>J.620100.012.01</td>
								  							<td>Melakukan pengaturan software tools pemrograman</td>
								  						</tr>
								  						<tr>
								  							<td>7</td>
								  							<td>J.620100.018.02</td>
								  							<td>Mengimplementasikan pemrograman berorientasi objek</td>
								  						</tr>
								  						<tr>
								  							<td>8</td>
								  							<td>J.620100.020.02</td>
								  							<td>Menggunakan SQL</td>
								  						</tr>
								  						<tr>
								  							<td>9</td>
								  							<td>J.620100.023.02</td>
								  							<td>Membuat dokumen kode program</td>
								  						</tr>
								  						<tr>
								  							<td>10</td>
								  							<td>J.620100.030.02</td>
								  							<td>Menerapkan pemrograman multimedia</td>
								  						</tr>
								  						<tr>
								  							<td>11</td>
								  							<td>J.620100.033.02</td>
								  							<td>Melaksanakan pengujian unit program</td>
								  						</tr>
								  						<tr>
								  							<td>12</td>
								  							<td>J.620100.042.01</td>
								  							<td>Melaksanakan konfigurasi perangkat lunak sesuai environment development, staging, production</td>
								  						</tr>
								  						<tr>
								  							<td>13</td>
								  							<td>J.620100.046.01</td>
								  							<td>Melakukan logging aplikasi</td>
								  						</tr>
								  			</table>
								  			
								  		</li>

								  	</ul>
								  </li><br>

								  <li><strong>6.	PERSYARATAN DASAR PEMOHON SERTIFIKASL</strong>
								  	<ul>
								  		<li>6.1	Peserta didik pada SMK bidang keahlian Rekayasa Perangkat Lunak yang telah menyelesaikan seluruh mata pelajaran</li>
								  		<li>6.2	Telah memiliki sertifikat atau surat keterangan telah melaksanakan Praktek Kerja Industri</li>
								  		<li>6.3	Memiliki nilai rapot kompeten pada kompetensi terkait</li>
								  	</ul>
								  </li><br>

								  <li><strong>7.	HAK PEMOHON SERTIFIKASl DAN KEWAJIBAN PEMEGANG SERTIFIKAT</strong> <ul>
								  	<li><strong>7.1	Hak Pemohon</strong></li></ul>
								  	<ul>
								  		<li>7.1.1	Memperoleh penjelasan tentang gambaran proses sertifikasi sesuai dengan skema sertifikasi</li>
								  		<li>7.1.2	Mendapatkan hak bertanya berkaitan dengan kompetensi</li>
								  		<li>7.1.3	Memperoleh pemberitahuan tentang kesempatan untuk menyatakan, dengan alasan, permintaan untuk disediakan kebutuhan khusus sepanjang integritas asesmen tidak dilanggar, serta mempertimbangkan aturan yang bersifat nasional.</li>
								  		<li>7.1.4	Memperoleh jaminan kerahasiaan terhadap proses sertifikasi</li>
								  		<li>7.1.5	Memperoleh hak banding terhadap keputusan sertifikasi</li>
								  		<li>7.1.6	Memperoleh sertifikat kompetensi jika dinyatakan kompeten</li>
								  		<li>7.1.7	Menggunakan sertifikat yang diperoleh untuk promosi diri sebagai tenaga pada bidang Rekayasa Perangkat Lunak</li>
								  	</ul> <br>
								  	<ul>
								  	<li><strong>7.2	Kewajiban Pemegang Sertifikat</strong></li></ul>
								  	<ul>
								  		<li>7.2.1	Melaksanakan keprofesian di kompetensi keahlian Rekayasa Perangkat Lunak</li>
								  		<li>7.2.2	Menjaga dan mentaati kode etik profesi secara sungguh-sungguh dan konsekuen</li>
								  		<li>7.2.3	Menjamin bahwa sertifikat kompetensi tidak disalahgunakan</li>
								  		<li>7.2.4	Menjamin terpeliharanya kompetensi yang sesuai pada sertifikat kompetensi</li>
								  		<li>7.2.5	Menjamin bahwa seluruh pernyataan dan informasi yang diberikan adalah terbaru, benar dan dapat dipertanggungjawabkan</li>
								  		<li>7.2.6	Membayar biaya sertifikasi</li>
								  	</ul>
								  </li><br>

								  <li><strong>8.	BIAYA SERTIFIKASI</strong>
								  	<ul>
								  		<li>8.1	Biaya sertifikasi dapat bersumber dari pemerintah, partisipasi masyarakat atau sumber dana lainnya.</li>
								  		<li>8.2	Biaya uji terdiri dari biaya pendaftaran peserta, penerbitan sertifikat, honor asesor, penggandaan materi, biaya akomodasi dan transport asesor yang diperhitungkan sesuai kondisi dan rencana pelaksanaan asesmen.</li>
								  	</ul>
								  </li> <br>

								  <li><strong>9.	PROSES SERTIFIKASI</strong>
								  	<ul>
								  		<li><strong>9.1	Proses Pendaftaran</strong></li>
								  	</ul>
								  	<ul>
								  			<ul>
								  		<li>9.1.1	Pemohon memahami proses Asesmen Skema Sertifikasi Rekayasa Perangkat Lunak ini yang mencakup persyaratan dan ruang lingkup sertifikasi, penjeiasan proses penilaian, hak pemohon, biaya sertifikasi dan kewajiban pemegang sertifikat</li>
								  		<li>9.1.2	Pemohon mengisi formulir Permohonan Sertifikasi (APL 01) yang dilengkapi dengan bukti;
								  				<ul>
								  				<li>a.	Fotokopi Kartu Pelajar</li>
								  				<li>b.	Fotokopi Raport</li>
								  				<li>c.	Pas foto warna terbaru 3 x 4 sebanyak 2 lembar</li>
								  				<li>d.	Foto copy sertifikat atau surat keterangan telah mengikuti Praktik Kerja Industri</li>
								  				</ul>
								  		</li>
								  		<li>9.1.3	Peserta mengisi formulir Asesmen Mandiri (APL 02) dan dilengkapi dengan bukti-bukti pendukung,</li>
								  		<li>9.1.4	Peserta menyatakan setuju untuk memenuhi persyaratan sertifikasi dan memberikan setiap informasi yang diperlukan untuk penilaian.</li>
								  		<li>9.1.5	LSP SMK Wikrama Bogor menelaah berkas pendaftaran untuk konfirmasi bahwa peserta sertifikasi memenuhi persyaratan yang ditetapkan dalam skema sertifikasi.</li>
								  			</ul>
								  	</ul>
								  </li>
								  <br>
								  <li><strong>9.2	Proses Asesmen</strong></li>
								  <ul>
								  	<li>9.2.1	Asesmen skema sertifikasi KKNI Level II Kompetensi Keahlian Rekayasa Perangkat Lunak direncanakan dan disusun dengan cara yang menjamin bahwa verifikasi persyaratan skema sertifikasi telah dilakukan secara obyektif dan sistematis dengan bukti terdokumentasi untuk memastikan kompetensi.</li><br>
								  	<li>9.2.2	Pelaksanan Asesmen untuk skema sertifikasi KKNI Level II Kompetensi Keahlian Rekayasa Perangkat Lunak dapat dilakukan sekaligus atau dengan cara asesmen per klaster.</li>
								  	<ul>
								  		<li>a.	Pelaksanaan Asesment Per Klaster</li>
								  		<li><table>
								  			<tr>
								  				<td>No</td>
								  				<td align="center">Daftar Klaster</td>
								  				<td>Waktu Pelaksanaan</td>
								  			</tr>
								  			<tr>
								  				<td>1</td>
								  				<td>Pemrograman Dasar</td>
								  				<td>Semester 2</td>
								  			</tr>
								  			<tr>
								  				<td>2</td>
								  				<td>Pemrograman Web</td>
								  				<td>Semester 4</td>
								  			</tr>
								  			<tr>
								  				<td>3</td>
								  				<td>Pemrograman Berorientasi Objek</td>
								  				<td>Semester 6</td>
								  			</tr>
								  		</table>
								  	</li> <br>
								  	<li>b.	Pelaksanaan Asesment Skema</li>
								  	<li>
								  		<table>
								  			<tr>
								  				<td>No.</td>
								  				<td align="center">Daftar Klaster</td>
								  				<td>Waktu Pelaksanaan</td>
								  			</tr>
								  			<tr>
								  				<td>1</td>
								  				<td>KKNI Level II Kompetensi Keahlian Rekayasa Perangkat Lunak</td>
								  				<td>Semester 6</td>
								  			</tr>
								  		</table>
								  	</li> <br>
								  	</ul>
								  	<li>9.2.3	LSP SMK Wikrama Bogor menugaskan Asesor Kompetensi untuk melaksanakan Asesmen.</li>
								  	<li>9.2.4	Asesor melakukan verifikasi persyaratan skema menggunakan perangkat asesmen dan mengkonfirmasikan bukti yang akan dikumpulkan dan bagaimana bukti tersebut akan dikumpulkan.</li><br>
								  	<li>9.2.5	Asesor menjelaskan, membahas dan mensepakati rincian rencana asesmen dan proses asesmen dengan Peserta Sertifikasi.	</li>
								  	<li>9.2.6	Asesor melakukan pengkajian dan evaluasi kecukupan bukti dari dokumen
									pendukung yang disampaikan pada lampiran dokumen Asesmen Mandiri APL-02, untuk memastikan bahwa bukti tersebut mencerminkan bukti yang diperlukan.
									</li>
								  	<li>9.2.7	Hasil proses asesmen yang telah memenuhi aturan bukti VATM direkomendasikan Kompeten dan yang belum memenuhi aturan bukti VATM direkomendasikan untuk mengikuti proses lanjut uji kompetensi.</li>
								  </ul> <br>
								  <li><strong>9.3	Proses Uji Kompetensi</strong></li>
								  	<ul>
								  		<li>9.3.1	Uji kompetensi skema sertifikasi KKNI Level II Kompetensi Keahlian Rekayasa
										Perangkat Lunak dirancang untuk menilai kompetensi yang dapat dilakukan dengan menggunakan metoda praktik, tertulis, lisan yang andai dan objektif serta konsisten. Rancangan persyaratan uji kompetensi menjamin setiap hasil uji dapat dibandingkan satu sama lain, baik dalam hal muatan dan tingkat kesulitan, termasuk keputusan yang sah untuk kelulusan atau ketidaklulusan.
										</li><br>
								  		<li>9.3.2	Uji kompetensi dilaksanakan di Tempat Uji Kompetensi (TUK) yang ditetapkan
										melalui verifikasi LSP SMK Wikrama Bogor
										</li>
								  		<li>9.3.3	Peralatan teknis yang digunakan dalam proses pengujian skema sertifikasi KKNI level II Kompetensi Keahlian Rekayasa Perangkat Lunak diverifikasi dan dikalibrasi,</li>
								  		<li>9.3.4	Proses Uji kompetensi dapat dilakukan sekaligus atau dengan cara pengujian per klaster. Hasil uji kompetensi per klaster dicatatkan pada lembar Skill Pasport.</li>
								  		<li>9.3.5	Bukti yang dikumpulkan melalui uji kompetensi dievaluasi untuk memastikan bahwa bukti tersebut mencerminkan bukti yang diperlukan untuk memperlihatkan kompetensi telah memenuhi aturan bukti VATM</li>
								  		<li>9.3.6	Hasil	proses uji kompetensi yang	telah	memenuhi aturan bukti VATM direkomendasikan "Kompeten" dan yang belum memenuhi aturan bukti VATM direkomendasikan "Belum Kompeten".</li>
								  		<li>9.3.7	Asesor melaporkan dan menyampaikan rekomendasi hasil uji kompetensi kepada LSP SMK Wikrama Bogor</li>
								  	</ul><br>
								  <li><strong>9.4	Keputusan Sertifikasi</strong></li>
								  <ul>
								  	<li>9.4.1	LSP SMK Wikrama Bogor menjamin bahwa informasi yang dikumpulkan selama proses uji kompetensi mencukupi untuk:
								  		<ul>
								  			<li>a.	Mengambil keputusan sertifikasi;</li>
								  			<li>b.	Melakukan penelusuran apabila terjadi banding.</li>
								  		</ul>
								  	</li>
								  	<li>9.4.2	Keputusan sertifikasi terhadap peserta hanya dilakukan oleh LSP SMK Wikrama Bogor berdasarkan rekomendasi dan informasi yang dikumpulkan oleh asesor melalui proses uji kompetensi. Personil pelaksanaan uji kompetensi tidak ikut serta dalam membuat keputusan sertifikasi.</li>
								  	<li>9.4.3	Personil LSP SMK Wikrama Bogor yang membuat keputusan sertifikasi harus memiliki pengetahuan yang cukup dan pengalaman dalam proses sertifikasi untuk menentukan apakah persyaratan sertifikasi telah dipenuhi.</li>
								  	<li>9.4.4	LSP SMK Wikrama Bogor melakukan rapat teknis untuk memverifikasi berkas sertifikasi dan menetapkan status kompetensi yang dibuat dalam berita acara, untuk proses penerbitan sertifikat kompetensi.</li>
								  	<li>9.4.5	LSP SMK Wikrama Bogor menerbitkan sertifikat kompetensi kepada semua yang telah berhak menerima sertifikat dalam bentuk surat dan/atau kartu, yang ditandatangani dan disahkan oleh personil yang ditunjuk LSP SMK Wikrama Bogor dengan masa berlaku sertifikat 3(tiga) tahun.</li>
								  	<li>9.4.6	Sertifikat KKNI level II diserahkan setelah seluruh persyaratan sertifikasi dipenuhi.</li>
								  </ul> <br>
								  <li><strong>9.5	Pembekuan dan Pencabutan Sertifikat</strong></li>
								  <ul>
								  	<li>9.5.1	Pembekuan dan pencabutan sertifikat dilakukan jika seorang pemegang sertifikat:
								  		<ul>
								  			<li>a.	Melanggar ketentuan pemegang sertifikat</li>
								  			<li>b.	Melanggar ketentuan disiplin peserta didik</li>
								  			<li>c.	Menyalahgunakan kewenangan yang telah diberikan</li>
								  			<li>d.	Mencemarkan nama baik LSP SMK Wikrama Bogor</li>
								  		</ul></li>
								  	<li>9.5.2	LSP SMK Wikrama Bogor akan melakukan pencabutan sertifikat apabila tidak mengindahkan peringatan yang telah diberikan dalam penyalahgunaan sertifikat.</li>
								  </ul> <br>

								  <li><strong>9.6	Pemeliharaan Sertifikat</strong></li>
								  <ul>
								  	<li>LSP SMK Wikrama Bogor tidak melakukan Pemeliharaan terhadap Sertifikat Kompetensi</li>
								  </ul> <br>

								  <li><strong>9.7	Proses Sertifikasi Ulang</strong></li>
								  <ul>
								  	<li>LSP SMK Wikrama Bogor tidak melakukan Proses Sertifikasi Ulang</li>
								  </ul> <br>

								  <li><strong>9.8	Penggunaan Sertifikat</strong></li>
								  <ul>
								  	<li>Pemegang sertifikat KKNI Level II Kompetensi Keahlian Rekayasa Perangkat Lunak harus menandatangani persetujuan untuk:</li>
								  	<li>9.8.1	Memenuhi ketentuan skema sertifikasi yang relevan</li>
								  	<li>9.8.2	Menyatakan bahwa sertifikatnya hanya berlaku untuk ruang lingkup sertifikasi yang diberikan</li>
								  	<li>9.8.3	Tidak menyalahgunakan sertifikat yang dapat merugikan LSP SMK Wikrama Bogor dan tidak memberikan persyaratan yang berkaitan dengan sertifikasi yang menurut LSP SMK Wikrama Bogor dianggap dapat menyesatkan atau tidak sah</li>
								  	<li>9.8.4	Menghentikan penggunaan semua pernyataan yang berhubungan dengan sertifikasi yang memuat acuan LSP SMK Wikrama Bogor setelah dibekukan atau dicabut sertifikatnya serta mengembalikan sertifikat kepada LSP SMK Wikrama Bogor.</li>
								  </ul> <br>
								  <li><strong>9.9	Banding</strong></li>
								  <ul>
								  	<li>9.9.1	LSP SMK Wikrama Bogor menetapkan prosedur untuk menerima, melakukan kajian, dan membuat keputusan terhadap banding.</li>
								  	<li>9.9.2	LSP SMK Wikrama Bogor menetapkan prosedur yang menjamin bahwa semua banding ditangani secara konstruktif, tidak berpihak dan tepat waktu.</li>
								  	<li>9.9.3	Penjelasan mengenai proses penanganan banding dapat diketahui publik tanpa diminta.</li>
								  	<li>9.9.4	LSP SMK Wikrama Bogor memberitahukan secara resmi kepada pemohon banding pada akhir proses penanganan banding.</li>
								  </ul>



								</ul>     
                    </div>
                </div>  
            </div>         
        

  </div>


@endsection
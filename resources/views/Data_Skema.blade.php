@extends('layouts.mimin-login')

@section('content')

	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;"></img>
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft" style="color: white">Data Asesor,TUK Dan Skema</h3>
                        <p class="animated fadeInDown" style="color: white">
                          Home <span class="fa-angle-right fa"></span> Data T.A.S
                        </p>
                    </div>
                  </div>
              </div>

          <div class="col-md-9 top-20" align="center" style="padding-left: 300px">
              <div class="panel">
              	<div class="panel-heading"><h3>Data T.A.S</h3></div>
              		<div class="panel-body">
              			<a href="{{URL('/data')}}" class="btn btn-primary btn-round">TUK</a>
              			<span class=" icon-arrow-right"></span>

              			<a href="{{URL('/dataAsesor')}}" class="btn btn-primary btn-round">Asesor</a>
              			<span class=" icon-arrow-right"></span>

              			<a href="{{URL('/dataSkema')}}" class="btn btn-primary btn-round">Skema</a>
              		</div>
              </div>
          </div>

              <div class="col-md-12">
                <div class="col-md-12">
                	<div align="right">
          				<a href="{{URL('/createSkema')}}" class="btn btn-primary btn-round">Create Skema</a>
                	</div>
                	<br>
                  <div class="panel">
                    <div class="panel-heading"><h3>Data Skema</h3></div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode</th>
                          <th>Nama</th>
                          <th>Kategori</th>
                          <th>Bidang</th>
                          <th>Mea</th>
                          <th>Unit</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      	@foreach($skema as $key=>$p)
                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$p->kode}}</td>
                          <td>{{$p->nama}}</td>
                          <td>{{$p->kategori}}</td>
                          <td>{{$p->bidang}}</td>
                          <td>{{$p->mea}}</td>
                          <td>{{$p->unit}}</td>
                          <td>
                            <div class="btn-group">
                              <a onclick="return confirm('Do You Want To Edit {{$p->nama}}??')" href="{{URL('/editSkema/'.$p->id)}}" class="btn btn-warning">Edit</a>
                              <a onclick="return confirm('Do You Want To Delete Skema {{$p->nama}}??')" href="{{URL('/dataSkema/delete/'.$p->id)}}" class="btn btn-danger"> Delete</a>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                      @endforeach
                        </table>
                      </div>
                  </div>
                </div>
              </div>  
              </div>
@endsection
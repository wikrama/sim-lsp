@extends('layouts.mimin')

@section('content')
  <img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  <div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
        <div class="container">
        <!-- Data Pribadi -->
        <form action="{{ URL('/peserta-lsp/store')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-heading">
                            <h4>Data Pribadi</h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-6">
                                   <label>Nama Lengkap</label>
                                  <input type="text" name="nama" class="form-control" required> 
                                </div> 
                                <div class="col-md-3">
                                   <label>Tempat/Tanggal Lahir</label>
                                  <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat" required> 
                                </div>
                                <div class="col-md-3" style="margin-top: 22px;">
                                  <input type="date" name="tanggal_lahir" class="form-control" required> 
                                </div>                 
                            </div> 
                            <div class="form-group">                  
                              <div class="col-md-6" style="margin-top: 20px;">
                                    <label>Jenis Kelamin</label>
                                    <select name="jk" class="form-control">
                                      <option value="L">Laki-Laki</option>
                                      <option value="P">Perempuan</option>
                                    </select>
                              </div>
                              <div class="col-md-6" style="margin-top: 20px;">
                                    <label>Kebangsaan</label>
                                    <input type="text" name="kebangsaan" class="form-control" required>
                              </div>
                            </div>    
                            <div class="form-group">                  
                              <div class="col-md-6" style="margin-top: 20px;">
                                    <label>Alamat Rumah</label>
                                    <input type="text" name="alamat_rmh" class="form-control" required>
                              </div>
                              <div class="col-md-3" style="margin-top: 20px;">
                                    <h4>No Telepon/Email</h4>                                    
                                    <label>Rumah</label>
                                    <input type="text" name="rumah" class="form-control" required>
                              </div>                
                              <div class="col-md-3" style="margin-top: 58px;">
                                    <label>Kode Pos</label>
                                    <input type="number" name="kd_pos" class="form-control" required>
                              </div>                                  
                            </div>
                            <div class="form-group">
                              <div class="col-md-6" style="margin-top: 10px;">
                                  <label>Pendidikan Terakhir</label>
                                  <input type="text" name="pendidikan_terakhir" class="form-control" required>
                              </div>
                              <div class="col-md-3" style="margin-top: 10px;">
                                  <label>HP</label>
                                  <input type="number" name="hp" class="form-control" required>
                              </div>
                              <div class="col-md-3" style="margin-top: 10px;">
                                  <label>Kantor</label>
                                  <input type="number" name="kantor" class="form-control" required>
                              </div>
                              <div class="col-md-6" style="margin-top: 10px;">
                                  <label>Email</label>
                                  <input type="email" name="email" class="form-control" required>
                              </div>
                            </div>  
                        </div>
                    </div>                                                                           
<!--             <div class="text-right">
              <button class="btn btn-round btn-primary">Save/Next</button>
            </div> -->
                </div>  
            </div>         

            <!-- Data Pekerjaan Sekarang -->
            <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-heading">
                            <h4>Data Pekerjaan Sekarang</h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>Nama Lembaga/Perusahaan</label>
                                    <input type="text" required="" name="nama_lembaga" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Jabatan</label>
                                    <input type="text" required="" name="jabatan" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6" style="margin-top: 20px;">
                                    <label>Alamat</label>
                                    <input type="text" required="" name="alamat" class="form-control">
                                </div>
                                <div class="col-md-3" style="margin-top: 20px;">
                                    <h4>No.Telp/Fax/Email</h4>
                                    <label>Telp</label>
                                    <input type="number" required="" name="telp" class="form-control">
                                </div>
                                <div class="col-md-3" style="margin-top: 58px;">
                                    <label>Kode Pos</label>
                                    <input type="text" required="" name="kode_pos" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3" style="margin-left: 540px; margin-top: 10px;">
                                    <label>Fax</label>
                                    <input type="text" required="" name="fax" class="form-control">
                                </div>
                                <div class="col-md-3" style="margin-top: 10px;">
                                    <label>Email</label>
                                    <input type="email" required="" name="email" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Data Permohonan setifikasi -->
            <div class="col-md-12 top-20 padding-0">
              <div class="col-md-12">
                <div class="panel" style="border-radius: 5px;">
                  <div class="panel-heading">
                    <h4>Data Permohonan Sertifikasi</h4>
                  </div>
                  <div class="panel-body">
                    <h4>Tujuan Asesmen</h4>
                      <div class="col-md-2 panel" style="padding-bottom: 2px;  padding-left: 5px; margin-left: 10px;">
                        <div class="form-animate-radio" style="padding-top: 10px;">
                          <label class="radio">
                          <input id="radio1" type="radio" value="RPL" name="tujuan_asesmen" style="margin-top: 10px;">
                          <span class="outer">
                            <span class="inner"></span>
                          </span>RPL
                          </label>
                        </div>
                      </div>
                      <div class="col-md-2 panel" style="padding-bottom: 2px; padding-left: 5px; margin-left: 10px;">
                        <div class="form-animate-radio" style="padding-top: 10px;">
                          <label class="radio">
                          <input id="radio1" type="radio" value="Pembelajaran" name="tujuan_asesmen" style="margin-top: 10px;">
                          <span class="outer">
                            <span class="inner"></span></span>Pembelajaran
                          </label>
                        </div>
                      </div>
                      <div class="col-md-2 panel" style="padding-bottom: 2px; padding-left: 5px; margin-left: 10px;">
                        <div class="form-animate-radio" style="padding-top: 10px;">
                          <label class="radio">
                          <input id="radio1" type="radio" value="RCC" name="tujuan_asesmen" style="margin-top: 10px;">
                          <span class="outer">
                            <span class="inner"></span>
                          </span>RCC
                          </label>
                        </div>
                      </div>
                      <div class="col-md-2 panel" style="padding-bottom: 2px; padding-left: 5px; margin-left: 10px;">
                        <div class="form-animate-radio" style="padding-top: 10px;">
                          <label class="radio">
                          <input id="radio1" type="radio" value="Sertifikasi" name="tujuan_asesmen" style="margin-top: 10px;">
                          <span class="outer">
                            <span class="inner"></span>
                          </span>Sertifikasi
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6">
                          <h4>Skema Sertifikasi</h4>
                          <select required="" name="skema_sertifikasi" class="form-control">
                            <option value="Klaster">Klaster</option>
                            <option value="Okupasi">Okupasi</option>
                            <option value="KKNI">KKNI</option>
                          </select>
                        </div><br><br><br><br><br>
                        <p>* Upload Dokumen (CV, Raport, Sertifikat Keahlian, Kartu Pelajar)</p>
                        <div class="links">                          
                          <div class="col-sm-3">
                            <label>(Cv)
                              <input type="file" name="cv">
                            </label><br>
                            <label>(Rapot)
                              <input type="file" name="rapot">
                            </label>                          
                          </div>                          
                          <div class="col-sm-3">
                            <label>(Sertifikat Keahlian)
                              <input type="file" name="sertifikat_keahlian">  
                            </label><br>
                            <label>(Kartu Pelajar)
                              <input type="file" name="kartu_pelajar">
                            </label>                                                    
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Button -->
            <div class="form-group text-right" style="margin-right: 20px;">              
              <a href="{{URL('/')}}" class="btn btn-round btn-danger">Cancel</a>
              <button class="btn btn-round btn-primary" value="save">Simpan</button>
            </div>
        </div>
      </form>
    </div>
@endsection

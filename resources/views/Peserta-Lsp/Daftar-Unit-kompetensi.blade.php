@extends('layouts.mimin')

@section('content')
	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
<div class="container">
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
          <div class="panel" style="border-radius: 5px;">
            <div class="panel-heading">
                <h4 style="text-align: center"><strong>Data Unit Kompetensi</strong></h4>
            </div>
            <div class="panel-body">
              <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th class="text-center">No.</th>
                          <th class="text-center">Kode Unit</th>
                          <th class="text-center">Judul Unit</th>
                          <th>Jenis Standar (Standar Khusus/Standar Internasional/SKKNI)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="4"><h5><strong>Kompetensi Umum Dan Inti</strong></h5></td>
                        </tr>
                        <tr>
                          <td>1.</td>
                          <td>LOG.OO01.002.01</td>
                          <td>
                              Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja
                          </td>
                          <td>SKKNI Logam</td>
                        </tr>
                        <tr>
                          <td>2.</td>
                          <td>LOG.OO01.004.01</td>
                          <td>
                              Merencanakan tugas rutin
                          </td>
                          <td>SKKNI Logam</td>
                        </tr>
                        <tr>
                          <td>3.</td>
                          <td>TIK.OP01.002.01</td>
                          <td>
                              Mengidentifikasi aspek kode etik dan HAKI
                              dibidang TIK
                          </td>
                          <td>SKKNI Operator Komputer</td>
                        </tr>
                        <tr>
                          <td colspan="4"><h5><strong>Kompetensi Pilihan / Fungsional</strong></h5></td>
                        </tr>
                        <tr>
                          <td>4.</td>
                          <td>J.620100.004.02</td>
                          <td>
                              Menggunakan struktur data
                          </td>
                          <td>SKKNI Software Development
                              Sub Bidang Pemrograman
                          </td>
                        </tr>
                        <tr>
                          <td>5.</td>
                          <td>J.620100.005.02</td>
                          <td>
                              Mengimplementasikan user interface
                          </td>
                          <td>SKKNI Software Development
                              Sub Bidang Pemrograman
                          </td>
                        </tr>
                        <tr>
                          <td>6.</td>
                          <td>J.620100.011.01</td>
                          <td>
                              Melakukan instalasi software tools
                              pemrograman
                          </td>
                          <td>SKKNI Software Development
                              Sub Bidang Pemrograman
                          </td>
                        </tr>
                        <tr>
                          <td>7.</td>
                          <td>J.620100.012.01</td>
                          <td>Melakukan pengaturan software tools
                            pemrograman
                          </td>
                          <td>SKKNI Software Development
                              Sub Bidang Pemrograman
                          </td>
                        </tr>
                        <tr>
                          <td>8.</td>
                          <td>J.620100.017.02</td>
                          <td>
                            Mengimplementasikan pemrograman terstruktur
                          </td>
                          <td>SKKNI Software Development
                              Sub Bidang Pemrograman
                          </td>
                        </tr>
                        <tr>
                          <td>9.</td>
                          <td>J.620100.022.01</td>
                          <td>
                            Mengimplementasikan algoritma pemrograman
                          </td>
                          <td>SKKNI Software Development
                              Sub Bidang Pemrograman
                          </td>
                        </tr>
                        <tr>
                          <td>10.</td>
                          <td>J.620100.025.02</td>
                          <td>Melakukan debugging
                          </td>
                          <td>SKKNI Software Development
                              Sub Bidang Pemrograman
                          </td>
                        </tr>
                      </tbody>
                        </table> <!-- 
                        <div class="col-md-11">
                        <a href="/peserta-lsp" class="btn btn-round btn-danger">Back
                        </a>
                        </div>
                        <a href="" class="btn btn-round btn-primary">Next
                        </a> -->
                      </div>                                             
          </div>
        </div>
          <div class="text-right">

            <a href="{{URL('bukti-lengkap')}}" onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-round btn-primary">Next
            </a>
<!--             <button onclick="sweet()">Sweet Alert</button> -->
          </div>
      </div>  
      </div>
    </div>
  </div>  
</div>
@endsection
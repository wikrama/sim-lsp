@extends('layouts.mimin')

@section('content')

	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
		<div >
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
         <form action="{{URL('/bukti-lengkap/store')}}" method="post">
            {{ csrf_field ()}}
          <div class="panel" style="border-radius: 5px;">
            <div class="panel-heading">
                <h4 style="text-align: center"><strong>Bukti Kelengkapan Pemohon</strong></h4>
            </div>
            <div class="panel-body">
                <div class="responsive-table">
                    <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead style="background-color: green; color: white;">
                        <tr>
                          <th class="text-center" style="padding-bottom: 15px">No</th>
                          <th class="text-center" style="padding-bottom: 15px">Unit/Elemen Kompetensi</th>
                          <th class="text-center">
                          	Bukti (paling relevan) : Rincian Pendidikan/Pelatihan,Pengalaman Kerja,Pengalaman Hidup
                      	 </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>
                          		Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja
                          </td>
                          <td><input type="text" class="form-control" name="1" required=""></td>
                        </tr>
                        <tr>
                        	<td>2</td>
                        	<td>Merencanakan tugas rutin</td>
                        	<td><input type="text" class="form-control" name="2" required=""></td>
                        </tr>
                        <tr>
                        	<td>3</td>
                        	<td>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</td>
                        	<td><input type="text" class="form-control" name="3" required=""></td>
                        </tr>
                        <tr>
                        	<td>4</td>
                        	<td>Menggunakan struktur data</td>
                        	<td><input type="text" class="form-control" name="4" required=""></td>
                        </tr>
                        <tr>
                        	<td>5</td>
                        	<td>Mengimplementasikan user interface</td>
                        	<td><input type="text" class="form-control" name="5" required=""></td>
                        </tr>
                        <tr>
                        	<td>6</td>
                        	<td>Melakukan instalasi software tools pemrograman</td>
                        	<td><input type="text" class="form-control" name="6" required=""></td>
                        </tr>
                        <tr>
                        	<td>7</td>
                        	<td>Melakukan pengaturan software toolspemrograman</td>
                        	<td><input type="text" class="form-control" name="7" required=""></td>
                        </tr>
                        <tr>
                        	<td>8</td>
                        	<td>Mengimplementasikan pemrogramanterstruktur</td>
                        	<td><input type="text" class="form-control" name="8" required=""></td>
                        </tr>
                        <tr>
                        	<td>9</td>
                        	<td>Mengimplementasikan algoritmapemrograman</td>
                        	<td><input type="text" class="form-control" name="9" required=""></td>
                        </tr>
                        <tr>
                        	<td>10</td>
                        	<td>Melakukan debugging</td>
                        	<td><input type="text" class="form-control" name="10" required=""></td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                    <table align="center" width="100%">
                    	<tr>
                    		<td rowspan="4"><strong><div style="margin-bottom: 90px">
		                    		Rekomendasi :</div>• Pemohon diterima sebagai peserta sertifikasi <br> <br>
									• Pemohon belum diterima sebagai peserta sertifikasi</strong>					
							</td>
                    		<td colspan="2"><strong>Asesi:</strong>
                    			<tr>
                    				<td>Nama</td>
                    				<td class="col-md-4"><input required="" type="text" class="form-control" name="nama_assesi"></td>
                    			</tr>
                    		</td>
                    		<td>Tanda tangan/Tanggal</td>
                    		<td><div align="center">Tanda Tangan :</div><br><br><br><br>_________________________________________________ <br><br><input required="" type="date" class="form-control border-bottom" name="tgl_assesi"></td>
                    	</tr>
                    </table>
                    <table align="center" width="100%">
                    	<tr>
                    		<td rowspan="5"><strong>
		                    		<div style="margin-bottom: 90px">Rekomendasi </div>• Pemohon diterima sebagai peserta sertifikasi <br><br><br>
									• Pemohon belum diterima sebagai peserta sertifikasi</strong>					
							</td>
                    		<td colspan="2"><strong>Admin LSP : </strong>
                    			<tr>
                    				<td>Nama</td>
                    				<td class="col-md-4"><input required="" type="text" class="form-control" name="nama_admin"></td>
                    			</tr>
                    			<tr>
                    				<td>No Reg</td>
                    				<td><input required="" type="text" class="form-control" name="no_reg"></td>
                    			</tr>
                    		</td>
                    		<td>Tanda tangan/Tanggal</td>
                    		<td><div align="center">Tanda Tangan :</div><br><br><br><br>_________________________________________________ <br><br><input required="" type="date" class="form-control border-bottom" name="tgl_admin"></td>
                    	</tr>
                    </table>
          		</div>
        	</div>
      	  </div>  
            <div align="right">
                <button class="btn btn-primary btn-round">SAVE</button>
            </div>
         </form>                                      
      	</div>
    </div>
  </div>  
</div>

@endsection
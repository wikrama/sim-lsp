@extends('layouts.mimin-login')

@section('content')
	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
  	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">

        <div class="col-md-12 top-20 padding-0">
          <div class="col-md-12">
            <form method="post" action="{{URL('/mma/store')}}">                
              {{ csrf_field() }}          
              <div class="panel" style="border-radius: 5px;">
                <div class="panel-heading">
                  <h4>MERENCANAKAN DAN MENGORGANISASIKAN ASESMEN</h4>
                </div>
                <div class="panel-body">
                  <div class="responsive-table">                    
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>
                      <tr>
                        <td>Judul Skema Sertifikasi</td>
                        <td>Kualifikasi KKNI Level II Pada Kompetensi Keahlian Rekayasa Perangkat Lunak</td>
                        <td>Tanggal</td>
                        <td colspan="7">
                          <input type="date" name="tanggal" class="form-control" required>
                        </td>
                      </tr>
                      <tr>
                        <td>No. Skema</td>
                        <td>Skema-179-RPL-1</td>
                        <td>TUK</td>
                        <td colspan="7">
                          <label>Pilih yang tidak sesuai</label>
                          <select class="form-control" name="tuk" required>
                            <option value="sewaktu">Sewaktu</option>
                            <option value="tempat kerja">Tempat Kerja</option>
                            <option value="mandiri">Mandiri</option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td>LSP</td>
                        <td>SMK Wikrama Bogor</td>
                        <td></td>
                        <td colspan="7"></td>
                      </tr>
                      <tr>
                        <td>Tim/Nama Asesor</td>
                        <td>
                          <input type="text" name="nama_asesor" class="form-control" required>
                        </td>
                        <td></td>
                        <td colspan="7"></td>
                      </tr>
                      <tr>
                        <td colspan="7" style="background-color: grey; color: white;">1.  Menentukan pendekatan asesmen</td>
                      </tr>
                      <tr>
                        <td rowspan="3" class="text-center">1.1</td>
                        <td>Nama Peserta</td>
                        <td>:</td>
                        <td colspan="7">Kelas XII Kompetensi Keahlian Rekayasa Perangkat Lunak</td>
                      </tr>
                      <tr>
                        <td>Tujuan asesmen</td>
                        <td>:</td>
                        <td class="col-md-2">
                        <p>
                          <input type="radio" name="tujuan_asesmen" value="sertifikasi" required> sertifikasi
                        </p>
                        </td>
                        <td class="col-md-1">
                          <p>
                            <input type="radio" name="tujuan_asesmen" value="rcc" required> RCC
                          </p>
                        </td>
                        <td class="col-md-1">
                          <p>
                            <input type="radio" name="tujuan_asesmen" value="rpl" required> RPL
                          </p>
                        </td>
                        <td class="col-md-3">
                          <p>
                            <input type="radio" name="tujuan_asesmen" value="pencapaian" required>
                            Pencapaian Proses pembelajaran
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td>Konteks asesmen</td>
                        <td>:</td>
                        <td colspan="7">TUK sewaktu /tempat kerja* dengan karakteristik   produk/sistem/tempat kerja*</td>
                      </tr>
                      <tr>
                        <td class="text-center">1.2</td>
                        <td>Pendekatan/Jalur asesmen</td>
                        <td>:</td>
                        <td>
                          <p>
                            <input type="radio" name="jalur_asesmen" value="Mengikuti" required> Mengikuti proses kerja ditempat kerja
                          </p>
                        </td>
                        <td colspan="2">
                          <p>
                            <input type="radio" name="jalur_asesmen" value="sumatif dan formatif" required> Proses pembelajaran (Sumatif dan formatif)
                          </p>
                        </td>
                        <td>
                          <p>
                            <input type="radio" name="jalur_asesmen" value="hasil" required> Hasil akhir proses pelatihan.
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td class="text-center">1.3</td>
                        <td>Strategi asesmen</td>
                        <td>:</td>
                        <td colspan="7">
                          <p><strong>Mengikuti*:</strong>
                            <ol>
                              <div class="form-group">
                                <li>Benchmark asesmen (unit kompetensi)</li>
                                <input type="radio" name="benchmark_asesmen" value="Ya" required> Ya
                                <input type="radio" name="benchmark_asesmen" value="Tidak" required style="margin-left: 20px;"> tidak
                              </div>
                              <div class="form-group">
                                <li>RPL arrangements</li>
                                <input type="radio" name="rpl_arrangements" value="Ya" required> Ya
                                <input type="radio" name="rpl_arrangements" value="Tidak" required style="margin-left: 20px;"> tidak
                              </div>
                              <div class="form-group">
                                <li>Metode dan alat asesmen</li>
                                <input type="radio" name="metode_alat_asesmen" value="Ya" required> Ya
                                <input type="radio" name="metode_alat_asesmen" value="Tidak" required style="margin-left: 20px;"> tidak
                              </div>
                              <div class="form-group">
                                <li>Pengorganisasian asesmen</li>
                                <input type="radio" name="pengorganisasian_asesmen" value="Ya" required> Ya
                                <input type="radio" name="pengorganisasian_asesmen" value="Tidak" required style="margin-left: 20px;"> tidak
                              </div>
                              <div class="form-group">
                                <li>Aturan paket kualifikasi</li>
                                <input type="radio" name="aturan_paket" value="Ya" required> Ya
                                <input type="radio" name="aturan_paket" value="Tidak" required style="margin-left: 20px;"> Tidak
                              </div>
                              <div class="form-group">
                                <li>Persyaratan khusus</li>
                                <input type="radio" name="persyaratan_khusus" value="Ya" required> Ya
                                <input type="radio" name="persyaratan_khusus" value="Tidak" required style="margin-left: 20px;"> tidak
                              </div>
                              <div class="form-group">
                                <li>Mekanisme jaminan mutu </li>
                                <input type="radio" name="mekanisme_jaminan" value="Ya" required> Ya
                                <input type="radio" name="mekanisme_jaminan" value="Tidak" required style="margin-left: 20px;"> tidak
                              </div>
                            </ol>
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td class="text-center">1.4</td>
                        <td>Acuan pembanding/ benchmark</td>
                        <td>:</td>
                        <td colspan="7">
                          <ul>
                            <li>SKKNI/Standar Produk/Standar Sistem/Regulasi Teknis/SOP : </li>
                            <li><strong>SKKNI 2004-240 - Logam Mesin</strong></li>
                            <li><strong>SKKNI 2005-094 - Operator Komputer</strong></li>
                            <li><strong>SKKNI 2016-282 - Programmer Komputer (Software Development)</strong></li>
                          </ul>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>
                      <tr>
                        <td>Kode Unit </td>
                        <td>:</td>
                        <td colspan="11"><strong>LOG.OO01.002.01</strong></td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td>:</td>
                        <td colspan="11"><strong>Menerapkan Prinsip-Prinsip Keselamatan Dan Kesehatan Kerja Di Lingkungan Kerja</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN  : 1. </strong>Mengikuti praktek-praktek kerja yang aman</td>
                      </tr>
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                        <tr>                        
                          <td colspan="10" class="text-center">PERANGKAT ASESMEN</td>
                        </tr>
                        <tr>
                          <td colspan="10" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                        </tr>
                        <tr>
                          <td colspan="10" class="text-center">METODE</td>
                        </tr>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td>Studi Kasus</td>
                      </tr>
                      <tr>
                        <td>1.1 Kerja dilaksanakan dengan aman sehubungan dengan kebijakan dan prosedur perusahaan serta persyaratan perundang-undangan.</td>
                        <td><strong>1.1.1 Hasil asesi</strong><br>
                        melaksanakan kerja dengan aman sehubungan dengan kebijakan dan prosedur perusahaan serta persyaratan perundang-undangan</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="kerja_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kerja_tl" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="kerja_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kerja_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td class="col-md-1">
                          <input type="radio" name="kerja_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="kerja_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="kerja_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kerja_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="kerja_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="kerja_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kerja_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kerja_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kerja_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kerja_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="kerja_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kerja_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.2 Kegiatan rumah tangga perusahaan dilakukan sesuai dengan prosedur perusahaan.</td>
                        <td><strong>1.2.1 Hasil asesi</strong><br>melakukan kegiatan rumah tangga perusahaan sesuai dengan prosedur perusahaan
                        </td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="rumah_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="rumah_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rumah_t" value="Ya" required> Ya <br>
                          <input type="radio" name="rumah_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="rumah_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="rumah_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rumah_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="rumah_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rumah_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="rumah_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rumah_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="rumah_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rumah_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="rumah_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rumah_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="rumah_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td rowspan="3">1.3 Tanggung jawab dan tugas-tugas karyawan dimengerti dan didemostrasikan dalam kegiatan sehari-hari</td>
                      </tr>
                      <tr>
                        <td>1.3.1 Hasil asesi mengerti tanggungjawan dan tugas-tugas karyawan dalam kegiatan sehari-hari</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="tanggungjawab_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="tanggungjawab_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tanggungjawab_t" value="Ya" required> Ya <br>
                          <input type="radio" name="tanggungjawab_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="tanggungjawab_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="tanggungjawab_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tanggungjawab_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="tanggungjawab_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tanggungjawab_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="tanggungjawab_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tanggungjawab_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="tanggungjawab_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tanggungjawab_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="tanggungjawab_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tanggungjawab_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="tanggungjawab_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.3.2 Hasil asesi mendemonstrasika tanggungjawab dan tugas-tugas karyawan dalam kegiatan sehari-hari</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="demontrasi_tanggungjawab_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="demontrasi_tanggungjawab_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="demontrasi_tanggungjawab_t" value="Ya" required> Ya <br>
                          <input type="radio" name="demontrasi_tanggungjawab_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="demontrasi_tanggungjawab_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="demontrasi_tanggungjawab_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="demontrasi_tanggungjawab_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="demontrasi_tanggungjawab_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="demontrasi_tanggungjawab_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="demontrasi_tanggungjawab_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="demontrasi_tanggungjawab_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="demontrasi_tanggungjawab_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="demontrasi_tanggungjawab_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="demontrasi_tanggungjawab_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="demontrasi_tanggungjawab_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="demontrasi_tanggungjawab_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td rowspan="3">1.4 Perlengkapan pelindung diri dipakai dan disimpan sesuai dengan prosedur perusahaan.</td>
                      </tr>
                      <tr>
                        <td>1.4.1 Hasil asesi memakai perlengkapan pelindung diri sesuai dengan prosedur perusahaan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="memakai_perlengkapan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="memakai_perlengkapan_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memakai_perlengkapan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="memakai_perlengkapan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="memakai_perlengkapan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="memakai_perlengkapan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memakai_perlengkapan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="memakai_perlengkapan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memakai_perlengkapan_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="memakai_perlengkapan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memakai_perlengkapan_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="memakai_perlengkapan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memakai_perlengkapan_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="memakai_perlengkapan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memakai_perlengkapan_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="memakai_perlengkapan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.4.2 Hasil asesi menyimpan perlengkapan pelindung diri sesuai dengan prosedur perusahaan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="menyimpan_perlengkapan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="menyimpan_perlengkapan_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menyimpan_perlengkapan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="menyimpan_perlengkapan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="menyimpan_perlengkapan_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="menyimpan_perlengkapan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menyimpan_perlengkapan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="menyimpan_perlengkapan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menyimpan_perlengkapan_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="menyimpan_perlengkapan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menyimpan_perlengkapan_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="menyimpan_perlengkapan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menyimpan_perlengkapan_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="menyimpan_perlengkapan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menyimpan_perlengkapan_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="menyimpan_perlengkapan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.5 Semua perlengkapan dan alat-alat keselamatan digunakan sesuai dengan persyaratan perundang-undangan danprosedur perusahaan.</td>
                        <td>1.5.1 Hasil asesi menggunakan semua perlengkapan dan alat-alat keselamatan sesuai dengan persyaratan perundang-undangan dan prosedur perusahaan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="semua_perlengkapan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="semua_perlengkapan_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="semua_perlengkapan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="semua_perlengkapan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="semua_perlengkapan_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="semua_perlengkapan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="semua_perlengkapan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="semua_perlengkapan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="semua_perlengkapan_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="semua_perlengkapan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="semua_perlengkapan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="semua_perlengkapan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="semua_perlengkapan_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="semua_perlengkapan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="semua_perlengkapan_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="semua_perlengkapan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td rowspan="3">1.6 Tanda-tanda/simbol dikenali dan diikuti sesuai instruksi.</td>
                      </tr>
                      <tr>
                        <td>1.6.1 Hasil asesi mengenali tanda-tanda / simbol sesuai instruksi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="mengenali_tanda_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="mengenali_tanda_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengenali_tanda_t" value="Ya" required> Ya <br>
                          <input type="radio" name="mengenali_tanda_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="mengenali_tanda_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="mengenali_tanda_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengenali_tanda_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="mengenali_tanda_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengenali_tanda_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="mengenali_tanda_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengenali_tanda_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="mengenali_tanda_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengenali_tanda_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="mengenali_tanda_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengenali_tanda_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="mengenali_tanda_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.6.2 Hasil asesi mengikuti tanda-tanda / simbol sesuai instruksi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="mengikuti_tanda_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="mengikuti_tanda_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengikuti_tanda_t" value="Ya" required> Ya <br>
                          <input type="radio" name="mengikuti_tanda_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="mengikuti_tanda_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="mengikuti_tanda_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengikuti_tanda_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="mengikuti_tanda_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengikuti_tanda_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="mengikuti_tanda_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengikuti_tanda_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="mengikuti_tanda_wawancara" value="Tidak"required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengikuti_tanda_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="mengikuti_tanda_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengikuti_tanda_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="mengikuti_tanda_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.7 Semua pedoman penanganan dilaksanakan sesuai dengan persyaratan, prosedur perusahaan dan pedoman Komisi Kesehatan dan Keselamatan Kerja Nasional yang sah.</td>
                        <td>1.7.1 Hasil asesi melakukan laporan lisan dan tulisan bila perlu</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="pedoman_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="pedoman_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pedoman_t" value="Ya" required> Ya <br>
                          <input type="radio" name="pedoman_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="pedoman_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="pedoman_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pedoman_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="pedoman_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pedoman_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="pedoman_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pedoman_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="pedoman_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pedoman_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="pedoman_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pedoman_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="pedoman_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td rowspan="3">1.8 Perlengkapan darurat dikenali dan didemonstrasikan dengan tepat.</td>
                      </tr>
                      <tr>
                        <td>1.8.1 Hasil asesi mendemonstrasikan komunikasi baik dalam situasi akrab maupun tidak akrab</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="darurat_akrab_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="darurat_akrab_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_akrab_t" value="Ya" required> Ya <br>
                          <input type="radio" name="darurat_akrab_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="darurat_akrab_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="darurat_akrab_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_akrab_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="darurat_akrab_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_akrab_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="darurat_akrab_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_akrab_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="darurat_akrab_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_akrab_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="darurat_akrab_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_akrab_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="darurat_akrab_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.8.2 Hasil asesi mendemontrasikan komunikasi baik untuk individu dan kelompok yang akrab maupun tidak akrab</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="darurat_individu_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="darurat_individu_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_individu_t" value="Ya" required> Ya <br>
                          <input type="radio" name="darurat_individu_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="darurat_individu_verifikasi" value="Ya" required>Ya <br>
                          <input type="radio" name="darurat_individu_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_individu_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="darurat_individu_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_individu_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="darurat_individu_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_individu_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="darurat_individu_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_individu_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="darurat_individu_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="darurat_individu_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="darurat_individu_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN :2.</strong> Berpartisipasi dalam diskusi kelompok untuk mencapai hasil-hasil kerja yang tepat</td>
                      </tr>
                      <tr>
                        <td rowspan="7"><strong>KRITERIA UNJUK KERJA</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="10" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="10" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="10" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verivikasi Pihak ketiga</td>
                        <td>Studi Kasus</td>
                      </tr>
                      <tr>
                        <td rowspan="3">2.1 Tanggapan-tanggapan dicari dan diberikan untuk orang-orang dalam kelompok</td>
                      </tr>
                      <tr>
                        <td><strong>2.1.1 Hasil asesi</strong> mencari tanggapan-tanggapan dan diberikan untuk orang-orang dalam kelompok</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="mencari_tanggapan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="mencari_tanggapan_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mencari_tanggapan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="mencari_tanggapan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="mencari_tanggapan_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="mencari_tanggapan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mencari_tanggapan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="mencari_tanggapan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mencari_tanggapan_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="mencari_tanggapan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mencari_tanggapan_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="mencari_tanggapan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mencari_tanggapan_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="mencari_tanggapan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mencari_tanggapan_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="mencari_tanggapan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                    
                      <tr>
                        <td><strong>2.1.2 Hasil asesi</strong> memberikan tanggapan-tanggapan untuk orang-orang dalam kelompok</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="memberikan_tanggapan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="memberikan_tanggapan_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memberikan_tanggapan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="memberikan_tanggapan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="memberikan_tanggapan_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="memberikan_tanggapan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memberikan_tanggapan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="memberikan_tanggapan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memberikan_tanggapan_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="memberikan_tanggapan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memberikan_tanggapan_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="memberikan_tanggapan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memberikan_tanggapan_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="memberikan_tanggapan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memberikan_tanggapan_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="memberikan_tanggapan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>2.2 Kontribusi yang membangun dibuat berkenaan dengan proses produksi terkait</td>
                        <td><strong>2.2.1 Hasil asesi</strong> membuat kontribusi yang membangun berkenaan dengan proses produksi terkait</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kontribusi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kontribusi_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kontribusi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kontribusi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kontribusi_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="kontribusi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kontribusi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kontribusi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kontribusi_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="kontribusi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kontribusi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kontribusi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kontribusi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kontribusi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kontribusi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kontribusi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>2.3 Cita-cita dan tujuan dikomunikasikan</td>
                        <td><strong>2.3.1 Hasil asesi</strong> mengkomunikasikan cita-cita dan tujuan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="cita_cita_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="cita_cita_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="cita_cita_t" value="Ya" required> Ya <br>
                          <input type="radio" name="cita_cita_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="cita_cita_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="cita_cita_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="cita_cita_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="cita_cita_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="cita_cita_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="cita_cita_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="cita_cita_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="cita_cita_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="cita_cita_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="cita_cita_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="cita_cita_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="cita_cita_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 3.</strong> Mewakili pandangan kelompok terhadap orang lain</td>
                      </tr>
                      <tr>
                        <td rowspan="7"><strong>KRITERIA UNJUK KERJA</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="10" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="10" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="10" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstras</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td>Studi Kasus</td>
                      </tr>
                      <tr>
                        <td rowspan="3">3.1 Pandangan, pendapat orang lain dimengerti dan digambarkan dengan akurat</td>
                      </tr>
                      <tr>
                        <td><strong>3.1.1 Hasil asesi</strong> mengerti pandangan pendapat orang</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="mengerti_pandangan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="mengerti_pandangan_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengerti_pandangan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="mengerti_pandangan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="mengerti_pandangan_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="mengerti_pandangan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengerti_pandangan_tes_lisan" value="Ya"> Ya <br>
                          <input type="radio" name="mengerti_pandangan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengerti_pandangan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="mengerti_pandangan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengerti_pandangan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="mengerti_pandangan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengerti_pandangan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="mengerti_pandangan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengerti_pandangan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="mengerti_pandangan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td><strong>3.1.2 Hasil asesi</strong> menggambarkan pandangan pendapat orang lain dengan akurat</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="menggambarkan_pandangan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="menggambarkan_pandangan_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menggambarkan_pandangan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="menggambarkan_pandangan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="menggambarkan_pandangan_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="menggambarkan_pandangan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menggambarkan_pandangan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="menggambarkan_pandangan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menggambarkan_pandangan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="menggambarkan_pandangan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menggambarkan_pandangan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="menggambarkan_pandangan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menggambarkan_pandangan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="menggambarkan_pandangan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menggambarkan_pandangan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="menggambarkan_pandangan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: LOG.OO01.004.01</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Merencanakan Tugas Rutin   </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN : 1.</strong> Mengenali persyaratan Tugas</td>
                      </tr>
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6    "><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>
                      <tr>
                        <td>1.1 Instruksi-instruksi  tentang prosedur diperoleh, dimengerti dan bila perlu dijelaskan.</td>
                        <td>1.1.1 Hasil asesi memperoleh, mengerti dan bila perlu menjelaskan instruksi-instruksi tentang prosedur</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="intruksi_prosedur_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_prosedur_tl" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="intruksi_prosedur_t" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_prosedur_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="intruksi_prosedur_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="intruksi_prosedur_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="intruksi_prosedur_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_prosedur_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="intruksi_prosedur_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="intruksi_prosedur_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="intruksi_prosedur_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="intruksi_prosedur_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="intruksi_prosedur_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="intruksi_prosedur_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2" class="col-md-1">
                          <input type="radio" name="intruksi_prosedur_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="intruksi_prosedur_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.2 Spesifikasi yang relevan terhadap hasil-hasil tugas diperoleh, dimengerti dan bila perlu dijelaskan.</td>
                        <td>1.2.1 Hasil asesi memperoleh, mengerti dan bila perlu menjelaskan spesifikasi yang relevan terhadap tugas-tugas</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="spesifikasi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="spesifikasi_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="spesifikasi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="spesifikasi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="spesifikasi_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="spesifikasi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="spesifikasi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="spesifikasi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="spesifikasi_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="spesifikasi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="spesifikasi_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="spesifikasi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="spesifikasi_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="spesifikasi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="spesifikasi_studi_kasus" value="Ya" required>Ya <br>
                          <input type="radio" name="spesifikasi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.3 Hasil-hasil tugas dikenali</td>
                        <td>1.3.1 Hasil asesi mengenali hasil-hasi tugas</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="hasil_tugas_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="hasil_tugas_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="hasil_tugas_t" value="Ya" required> Ya <br>
                          <input type="radio" name="hasil_tugas_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="hasil_tugas_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="hasil_tugas_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="hasil_tugas_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="hasil_tugas_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="hasil_tugas_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="hasil_tugas_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="hasil_tugas_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="hasil_tugas_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="hasil_tugas_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="hasil_tugas_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="hasil_tugas_studi_kasus" value="Ya"required> Ya<br>
                          <input type="radio" name="hasil_tugas_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.4 Syarat-syarat tugas seperti waktu penyelesaian dan ukuran kualitas dikenali.</td>
                        <td>1.4.1 Hasil asesi mengenali syarat-syarat tugas seperti waktu penyelesaian dan ukuran kualitas</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="syarat_tugas_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="syarat_tugas_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="syarat_tugas_t" value="Ya" required> Ya <br>
                          <input type="radio" name="syarat_tugas_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="syarat_tugas_verifikasi" value="Ya" required> Ya<br>
                          <input type="radio" name="syarat_tugas_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="syarat_tugas_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="syarat_tugas_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="syarat_tugas_tes_tulis" value="Ya" required> Ya<br>
                          <input type="radio" name="syarat_tugas_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="syarat_tugas_wawancara" value="Ya" required> Ya<br>
                          <input type="radio" name="syarat_tugas_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="syarat_tugas_pihak_tiga" value="Ya" required> Ya<br>
                          <input type="radio" name="syarat_tugas_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="syarat_tugas_studi_kasus" value="Ya" required> Ya<br>
                          <input type="radio" name="syarat_tugas_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                      
                      <tr>
                        <td colspan="13"><strong>ELEMEN : 2.</strong> Merencanakan langkahlangkah yang dibutuhkan untuk menyelesaikan tugas</td>
                      </tr>
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                    
                      <tr>
                        <td>2.1 Berdasarkan instruksi-instruksi dan spesifikasi-spesifika yang ada, langkah-langkah atau kegiatan-kegiatan individu yang diperlukan untuk melaksanakan tugas dimengerti dan bila perlu dijelaskan.</td>
                        <td>2.1.1 Hasil asesi mengerti dan bila perlu menjelaskan langkah-langkah atau kegiatan kegiatan individu yang diperlukan untuk melaksanakan tugas berdasarkan instruksi-instruksi dan spesifikasi-spesifikasi yang ada</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="intruksi_spesifikasi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_spesifikasi_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="intruksi_spesifikasi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_spesifikasi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="intruksi_spesifikasi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_spesifikasi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="intruksi_spesifikasi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_spesifikasi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="intruksi_spesifikasi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_spesifikasi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="intruksi_spesifikasi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_spesifikasi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="intruksi_spesifikasi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_spesifikasi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="intruksi_spesifikasi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="intruksi_spesifikasi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                    
                      <tr>
                        <td>2.2 Rangkaian kegiatan yang perlu diselesaikan tercantum dalam rencana.</td>
                        <td>2.2.1 Hasil asesi menyelesaikan rangkaian kegiatan yang perlu yang tercantum dalam rencana</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="rangkaian_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="rangkaian_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rangkaian_t" value="Ya" required> Ya <br>
                          <input type="radio" name="rangkaian_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="rangkaian_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="rangkaian_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rangkaian_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="rangkaian_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rangkaian_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="rangkaian_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rangkaian_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="rangkaian_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rangkaian_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="rangkaian_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="rangkaian_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="rangkaian_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                    
                      <tr>
                        <td>2.3 Langkah-langkah dan hasil yang direncanakan diperiksa untuk menjamin kesesuaian dengan instruksi-instruksi dan spesifikasi-spesifikasi yang relevan.</td>
                        <td>2.2.1 Hasil asesi memeriksa langkah -langkah dan hasil yang direncanakan untuk menjamin kesesuaian dengan instruksi-instruksi dan spesifikasi-spesifikasi yang relevan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="langkah_hasil_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="langkah_hasil_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="langkah_hasil_t" value="Ya" required> Ya <br>
                          <input type="radio" name="langkah_hasil_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="langkah_hasil_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="langkah_hasil_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="langkah_hasil_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="langkah_hasil_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="langkah_hasil_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="langkah_hasil_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="langkah_hasil_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="langkah_hasil_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="langkah_hasil_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="langkah_hasil_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="langkah_hasil_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="langkah_hasil_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                    
                      <tr>
                        <td colspan="13"><strong>ELEMEN : 3.</strong> Mengulas rencana</td>
                      </tr>                        
                      <tr>
                        <td>3.1 Hasil-hasil dikenali dan dibandingkan dengan sasaransasaran (yang direncanakan)  instruksi-instruksi  tugas, spesifikasi-spesifikasi dan syarat-syarat tugas.</td>
                        <td>3.1.1 Hasil asesi mengenali dan membandingkan hasil-hasil dengan sasaran-sasaran (yang direncanakan)  instruksi-instruksi  tugas, spesifikasi-spesifikasi dan syarat-syarat tugas</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="sasaran_rencana_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="sasaran_rencana_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="sasaran_rencana_t" value="Ya" required> Ya <br>
                          <input type="radio" name="sasaran_rencana_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="sasaran_rencana_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="sasaran_rencana_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="sasaran_rencana_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="sasaran_rencana_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="sasaran_rencana_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="sasaran_rencana_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="sasaran_rencana_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="sasaran_rencana_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="sasaran_rencana_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="sasaran_rencana_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="sasaran_rencana_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="sasaran_rencana_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                        
                      <tr>
                        <td>3.2 Jika perlu, rencana diperbaiki untuk memenuhi sasaran-sasaran dan syarat-syarat tugas yang lebih baik.</td>
                        <td>3.2.1 Hasil asesi jika perlu memperbaiki rencana untuk memenuhi sasaran-sasaran dan syarat-syarat tugas yang lebih baik</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="rencana_perbaiki_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="rencana_perbaiki_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rencana_perbaiki_t" value="Ya" required> Ya <br>
                          <input type="radio" name="rencana_perbaiki_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="rencana_perbaiki_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="rencana_perbaiki_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rencana_perbaiki_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="rencana_perbaiki_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rencana_perbaiki_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="rencana_perbaiki_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rencana_perbaiki_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="rencana_perbaiki_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rencana_perbaiki_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="rencana_perbaiki_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="rencana_perbaiki_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="rencana_perbaiki_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>                    
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: TIK.OP01.002.01</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Mengidentifikasi Aspek Kode Etik Dan HAKI Di Bidang TIK</td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN : 1.</strong> Mengidentifikasi kode etik yang berlaku di dunia TIK.</td>
                      </tr>
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6    "><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                        
                      <tr>
                        <td>1.1 Norma yang berlaku di dunia TIK dapat diidentifikasi.</td>
                        <td>1.1.1 Hasil asesi Mengidentifikasi norma yang belaku di dunia TIK</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="norma_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="norma_tl" value="Tidak" required> tidak
                        </td>
                        <td>V</td>
                        <td>CLO TPD</td>
                        <td class="col-md-1">
                          <input type="radio" name="norma_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="norma_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="norma_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="norma_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="norma_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="norma_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="norma_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="norma_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="norma_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="norma_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2" class="col-md-1">
                          <input type="radio" name="norma_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="norma_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                       
                      <tr>
                        <td>1.2 Spek legal atas dokumen elektronik hasil karya orang lain dapat dipahami.</td>
                        <td>1.2.1 Hasil asesi Memahami spek legas atas dokumen elektronik hasil karya orang lain</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="spek_legal_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="spek_legal_tl" value="Tidak" required> tidak
                        </td>
                        <td>V</td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="spek_legal_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="spek_legal_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="spek_legal_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="spek_legal_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="spek_legal_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="spek_legal_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="spek_legal_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="spek_legal_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="spek_legal_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="spek_legal_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="spek_legal_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="spek_legal_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 2.</strong> Mengidentifikasi hal-hal yang berkaitan dengan HKI di dunia TIK.</td>
                      </tr>                    
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>2.1 Copyright piranti lunak dan isu hukum berkaitan dengan menggandakan dan membagi file dapat dipahami.</td>
                        <td>2.1.1 Hasil asesi Memahami copyright piranti lunak dan isu hukum yang berkaitan dengan menggandakan dan membagi file</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="copyright_piranti_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="copyright_piranti_tl" value="Tidak" required> tidak
                        </td>
                        <td>V</td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="copyright_piranti_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="copyright_piranti_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="copyright_piranti_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="copyright_piranti_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="copyright_piranti_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="copyright_piranti_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="copyright_piranti_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="copyright_piranti_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="copyright_piranti_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="copyright_piranti_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="copyright_piranti_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="copyright_piranti_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>2.2 Akibat yang dapat terjadi jika bertukar file pada suatu jaringan internet dapat dipahami.</td>
                        <td>2.2.1 Hasil asesi Memahami akibat yang dapat terjadi jika bertukar file pada suatu jaringan internet</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="tukar_file_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="tukar_file_tl" value="Tidak" required> tidak
                        </td>
                        <td>V</td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="tukar_file_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="tukar_file_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tukar_file_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="tukar_file_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tukar_file_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="tukar_file_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tukar_file_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="tukar_file_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tukar_file_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="tukar_file_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="tukar_file_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="tukar_file_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                            
                      <tr>
                        <td>2.3 Istilah-istilah shareware, freeware dan user license dapat dikenal dan dipahami.</td>
                        <td>2.3.1 Hasil asesi Memahami dan mengenali istilah-istilah shareware, freeware dan user license</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="istilah_shareware_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="istilah_shareware_tl" value="Tidak" required> tidak
                        </td>
                        <td>V</td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="istilah_shareware_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="istilah_shareware_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="istilah_shareware_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="istilah_shareware_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="istilah_shareware_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="istilah_shareware_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="istilah_shareware_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="istilah_shareware_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="istilah_shareware_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="istilah_shareware_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="istilah_shareware_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="istilah_shareware_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>   
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>                    
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: J.620100.004.01</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Menggunakan Struktur Data</td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN : 1.</strong>Mengidentifikasi konsep data dan struktur data</td>
                      </tr>
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                        
                      <tr>
                        <td>1.1 Konsep data dan struktur data diidentifikasi sesuai dengan konteks permasalahan</td>
                        <td>1.1.1 Hasil asesi mengidentifikasi konsep data dan struktur data sesuai dengan konteks permasalahan</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="konsep_data_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_data_tl" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="konsep_data_t" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_data_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td class="col-md-1">
                          <input type="radio" name="konsep_data_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_data_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="konsep_data_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_data_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="konsep_data_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_data_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konsep_data_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_data_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konsep_data_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_data_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2" class="col-md-1">
                          <input type="radio" name="konsep_data_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_data_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                       
                      <tr>
                        <td>1.2 Alternatif struktur data dibandingkan kelebihan dan kekurangannya untuk konteks permasalahan yang diselesaikan</td>
                        <td>1.2.1 Hasil asesi membandingkan kelebihan dan kekurangan alternatif struktur data untuk konteks permasalahan yang diselesaikan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="alternatif_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="alternatif_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="alternatif_t" value="Ya" required> Ya <br>
                          <input type="radio" name="alternatif_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="alternatif_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="alternatif_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="alternatif_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="alternatif_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="alternatif_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="alternatif_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="alternatif_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="alternatif_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="alternatif_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="alternatif_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="alternatif_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="alternatif_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 2.</strong> Menerapkan struktur data dan akses terhadap struktur data tersebut</td>
                      </tr>                    
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>2.1 Struktur data diimplementasikan sesuai dengan bahasa pemrograman yang akan dipergunakan.</td>
                        <td>2.1.1 Hasil asesi mengimplementasikan struktur data sesuai dengan bahasa pemrograman yang dipergunakan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="implementasi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="implementasi_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="implementasi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="implementasi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="implementasi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="implementasi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="implementasi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="implementasi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="implementasi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="implementasi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="implementasi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="implementasi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="implementasi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="implementasi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="implementasi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="implementasi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>2.2 Akses terhadap data dinyatakan dalam algoritma yang efisiensi sesuai bahasa pemrograman yang akan dipakai</td>
                        <td>2.2.1 Hasil asesi menyatakan dalam bentuk algoritma akses terhadap data yang efisiensi sesuai bahasa pemrograman yang akan dipakai.</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="algoritma_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_t" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="algoritma_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="algoritma_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>                    
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: J.620100.005.01</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Mengimplementasikan User Interface</td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN : 1.</strong> Mengidentifikasi rancangan user interface</td>
                      </tr>
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6    "><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                        
                      <tr>
                        <td>1.1 Rancangan user interface diidentifikasi sesuai kebutuhan.</td>
                        <td>1.1.1 Hasil asesi mengidentifikasi rancangan user interface sesuai kebutuhan</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="rancangan_user_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="rancangan_user_tl" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="rancangan_user_t" value="Ya" required> Ya <br>
                          <input type="radio" name="rancangan_user_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="rancangan_user_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="rancangan_user_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="rancangan_user_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="rancangan_user_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="rancangan_user_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="rancangan_user_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rancangan_user_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="rancangan_user_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="rancangan_user_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="rancangan_user_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2" class="col-md-1">
                          <input type="radio" name="rancangan_user_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="rancangan_user_studi_kasus" value="Tidak" required> Tidak
                        </td>
                      </tr>                       
                      <tr>
                        <td>1.2 Komponen user interface dialog diidentifikasi sesuai konteks rancangan proses</td>
                        <td>1.2.1 Hasil asesi mengidentifikasi komponen user interface dialog sesuai konteks rancangan proses</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="komponen_user_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_user_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komponen_user_t" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_user_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="komponen_user_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_user_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komponen_user_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_user_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komponen_user_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_user_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komponen_user_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_user_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komponen_user_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_user_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="komponen_user_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_user_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                      
                      <tr>
                        <td>1.3 Urutan dari akses komponen user interface dialog dijelaskan</td>
                        <td>1.3.1 Hasil asesi menjelaskan urutan dari akses komponen user interface dialog</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="urutan_akses_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="urutan_akses_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="urutan_akses_t" value="Ya" required> Ya <br>
                          <input type="radio" name="urutan_akses_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="urutan_akses_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="urutan_akses_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="urutan_akses_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="urutan_akses_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="urutan_akses_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="urutan_akses_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="urutan_akses_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="urutan_akses_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="urutan_akses_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="urutan_akses_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="urutan_akses_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="urutan_akses_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                      
                      <tr>
                        <td>1.4 Simulasi (mock-up) dari aplikasi yang akan dikembangkan dibuat.</td>
                        <td>1.4.1 Hasil asesi membuat simulasi ( mock-up) dari aplikasi yang akan dikembangkan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="mock_up_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="mock_up_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mock_up_t" value="Ya" required> Ya <br>
                          <input type="radio" name="mock_up_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="mock_up_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="mock_up_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mock_up_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="mock_up_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mock_up_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="mock_up_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mock_up_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="mock_up_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mock_up_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="mock_up_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="mock_up_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="mock_up_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 2.</strong> Melakukan implementasi rancangan user interface</td>
                      </tr>                    
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>2.1 Menu program sesuai dengan rancangan program diterapkan.</td>
                        <td>2.1.1 Hasil asesi menerapkan menu program sesuai dengan rancangan program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="menu_program_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="menu_program_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menu_program_t" value="Ya" required> Ya <br>
                          <input type="radio" name="menu_program_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="menu_program_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="menu_program_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menu_program_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="menu_program_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menu_program_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="menu_program_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menu_program_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="menu_program_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menu_program_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="menu_program_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="menu_program_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="menu_program_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>2.2 Penempatan user interface dialog diatur secara sekuensial.</td>
                        <td>2.2.1 Hasil asesi mengatur penempatan user interface dialog secara sekuensial</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="penempatan_user_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="penempatan_user_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penempatan_user_t" value="Ya" required> Ya <br>
                          <input type="radio" name="penempatan_user_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="penempatan_user_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="penempatan_user_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penempatan_user_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="penempatan_user_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penempatan_user_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="penempatan_user_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penempatan_user_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="penempatan_user_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penempatan_user_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="penempatan_user_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="penempatan_user_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="penempatan_user_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>2.3 Setting aktif-pasif komponen user interface dialog disesuaikan dengan urutan alur proses.</td>
                        <td>2.3.1 Hasil asesi menyesuaikan seting aktif-pasif komponen user interface dengan urutan alur proses</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="aktif_pasif_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="aktif_pasif_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="aktif_pasif_t" value="Ya" required> Ya <br>
                          <input type="radio" name="aktif_pasif_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="aktif_pasif_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="aktif_pasif_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="aktif_pasif_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="aktif_pasif_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="aktif_pasif_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="aktif_pasif_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="aktif_pasif_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="aktif_pasif_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="aktif_pasif_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="aktif_pasif_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="aktif_pasif_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="aktif_pasif_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                          
                      <tr>
                        <td>2.4 Bentuk style dari komponen user interface ditentukan.</td>
                        <td>2.4.1 Hasil asesi menentukan bentuk style dari komponen user interface</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="bentuk_style_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="bentuk_style_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="bentuk_style_t" value="Ya" required> Ya <br>
                          <input type="radio" name="bentuk_style_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="bentuk_style_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="bentuk_style_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="bentuk_style_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="bentuk_style_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="bentuk_style_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="bentuk_style_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="bentuk_style_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="bentuk_style_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="bentuk_style_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="bentuk_style_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="bentuk_style_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="bentuk_style_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                         
                      <tr>
                        <td>2.5 Penerapan simulasi dijadikan suatu proses yang sesungguhnya.</td>
                        <td>2.5.1 Hasil asesi menjadikan penerapan simulasi suatu proses yang sesungguhnya.</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="penerapan_simulasi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="penerapan_simulasi_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penerapan_simulasi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="penerapan_simulasi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="penerapan_simulasi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="penerapan_simulasi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penerapan_simulasi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="penerapan_simulasi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penerapan_simulasi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="penerapan_simulasi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penerapan_simulasi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="penerapan_simulasi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="penerapan_simulasi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="penerapan_simulasi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="penerapan_simulasi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="penerapan_simulasi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>                    
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: J.620100.011.01</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Melakukan Instalasi Software Tools Pemrograman</td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN : 1.</strong> Mengidentifikasi mekanisme running atau eksekusi sourcecode</td>
                      </tr>
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                        
                      <tr>
                        <td>1.1 Platform (lingkungan) yang akan digunakan untuk menjalankan tools pemrograman diidentifikasi sesuai dengan kebutuhan.</td>
                        <td>1.1.1 Hasil asesi mengidentifikasi platform (lingkungan) yang akan digunakan untuk menjalankan tools program sesuai dengan kebutuhan</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="platform_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="platform_tl" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="platform_t" value="Ya" required> Ya <br>
                          <input type="radio" name="platform_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="platform_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="platform_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="platform_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="platform_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td class="col-md-1">
                          <input type="radio" name="platform_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="platform_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="platform_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="platform_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2" class="col-md-1">
                          <input type="radio" name="platform_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="platform_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                       
                      <tr>
                        <td>1.2 Tools bahasa pemrogram dipilih sesuai dengan kebutuhaan dan lingkungan pengembangan.</td>
                        <td>1.2.1 Hasil asesi memilih tools bahasa pemrograman sesuai dengan kebutuhan dan lingkungan pengembangan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="tools_bahasa_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_bahasa_tl" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_bahasa_t" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_bahasa_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="tools_bahasa_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_bahasa_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_bahasa_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_bahasa_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td>
                          <input type="radio" name="tools_bahasa_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_bahasa_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_bahasa_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_bahasa_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="tools_bahasa_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_bahasa_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 2.</strong> . Instalasi tool pemrograman</td>
                      </tr>                    
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>2.1 Tools pemrogaman ter-install sesuai dengan prosedur.</td>
                        <td>2.1.1 Hasil asesi menginstal tools pemrograman sesuai dengan prosedur</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="tools_terinstall_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_terinstall_tl" value="Tidak" required> tidak
                        </td>
                        <td>V
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="tools_terinstall_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_terinstall_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_terinstall_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_terinstall_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td>
                          <input type="radio" name="tools_terinstall_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_terinstall_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_terinstall_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_terinstall_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="tools_terinstall_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_terinstall_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>2.2 Tools pemrograman bisa dijalankan di lingkungan pengembangan yang telah ditetapkan.</td>
                        <td>2.2.1 Hasil asesi menjalankan tools pemrograman dilingkungan pengembangan yang telah ditetapkan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="tools_jalan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_jalan_tl" value="Tidak" required> tidak
                        </td>
                        <td>V
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="tools_jalan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_jalan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_jalan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_jalan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td>
                          <input type="radio" name="tools_jalan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_jalan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_jalan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_jalan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="tools_jalan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_jalan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 3.</strong> Menerapkan hasil pemodelan kedalam eksekusi script sederhana</td>
                      </tr>                           
                      <tr>
                        <td>3.1 Script (source code) sederhana dibuat sesuai tools pemrogaman yang di-install</td>
                        <td>2.1.1 Hasil asesi membuat script (source code) sederhana sesuai tools pemrograman yang di instal</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="script_sederhana_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="script_sederhana_tl" value="Tidak" required> tidak
                        </td>
                        <td>V
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="script_sederhana_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="script_sederhana_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="script_sederhana_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="script_sederhana_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td>
                          <input type="radio" name="script_sederhana_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="script_sederhana_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="script_sederhana_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="script_sederhana_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="script_sederhana_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="script_sederhana_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>3.2 Script dapat dijalankan dengan benar dan menghasilkan keluaran sesuai scenario yang diharapkan</td>
                        <td>2.2.1 Hasil asesi menjalankan script dengan benar dan menghasilkan keluaran sesuai scenario yang diharapkan.</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="script_jalan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="script_jalan_tl" value="Tidak" required> tidak
                        </td>
                        <td>V
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="script_jalan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="script_jalan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="script_jalan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="script_jalan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td>
                          <input type="radio" name="script_jalan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="script_jalan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="script_jalan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="script_jalan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="script_jalan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="script_jalan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: J.620100.012.01</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Melakukan Pengaturan Software Tools Pemrograman</td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 1.</strong> Melakukan konfigurasi tools untuk pemrograman</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>1.1 Target hasil dari konfigurasi ditentukan.</td>
                        <td>1.1.1 Hasil asesi menentukan target hasil dari konfigurasi</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="target_hasil_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="target_hasil_tl" value="Tidak" required> tidak
                        </td>
                        <td>V
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="target_hasil_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="target_hasil_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="target_hasil_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="target_hasil_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td>
                          <input type="radio" name="target_hasil_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="target_hasil_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="target_hasil_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="target_hasil_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2" class="col-md-1">
                          <input type="radio" name="target_hasil_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="target_hasil_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>1.2 Tools pemrograman setelah dikonfigurasikan, tetap bisa digunakan sebagaimana mestinya.</td>
                        <td>1.2.1 Hasil asesi dapat menggunakan tools pemrograman setelah dikonfigurasikan sebagaimana mestinya</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="pemograman_konfigurasi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="pemograman_konfigurasi_tl" value="Tidak" required> tidak
                        </td>
                        <td>V
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="pemograman_konfigurasi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="pemograman_konfigurasi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pemograman_konfigurasi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="pemograman_konfigurasi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td>
                          <input type="radio" name="pemograman_konfigurasi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="pemograman_konfigurasi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pemograman_konfigurasi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="pemograman_konfigurasi_pihak_tiga" value="Tidak" required>tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="pemograman_konfigurasi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="pemograman_konfigurasi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr> 
                      <tr>
                        <td colspan="13"><strong>ELEMEN 2.</strong> Menggunakan tools sesuai kebutuhan pembuatan program</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>2.1 Fitur-fitur dasar yang dibutuhkan untuk mendukung pembuatan program diidentifikasikan</td>
                        <td>2.1.1 Hasil asesi mengidentifikasi fitur-fitur dasar yang dibutuhkan untuk mendukung pembuatan program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="fitur_dasar_butuh_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_butuh_tl" value="Tidak" required> tidak
                        </td>
                        <td>V
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="fitur_dasar_butuh_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_butuh_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fitur_dasar_butuh_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_butuh_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td>
                          <input type="radio" name="fitur_dasar_butuh_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_butuh_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fitur_dasar_butuh_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_butuh_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="fitur_dasar_butuh_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_butuh_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                            
                      <tr>
                        <td>2.2 Fitur-fitur dasar tools untuk pembuatan program dikuasai</td>
                        <td>2.2.1 Hasil asesi menguasai fitur-fitur dasar tools untuk pembuatan program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="fitur_dasar_tools_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_tools_tl" value="Tidak" required> tidak
                        </td>
                        <td>V
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="fitur_dasar_tools_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_tools_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fitur_dasar_tools_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_tools_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>DPT KJT</td>
                        <td>
                          <input type="radio" name="fitur_dasar_tools_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_tools_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fitur_dasar_tools_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_tools_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="fitur_dasar_tools_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="fitur_dasar_tools_studi_kasus" value="Tidak" required> tidak
                        </td> 
                      </tr>    
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: J.620100.017.01</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Mengimplementasikan Pemrograman Terstruktur</td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 1.</strong> Menggunakan tipe data dan kontrol program</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>1.1 Tipe data yang sesuai standar ditentukan</td>
                        <td>1.1.1 Hasil asesi menentukan tipe data yang sesuai standar</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="tipe_data_standar_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_standar_tl" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">                          
                          <input type="radio" name="tipe_data_standar_t" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_standar_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="tipe_data_standar_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_standar_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="tipe_data_standar_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_standar_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="tipe_data_standar_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_standar_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tipe_data_standar_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_standar_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tipe_data_standar_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_standar_pihak_tiga" value="Tidak" required> Tidak
                        </td>
                        <td colspan="2" class="col-md-1">
                          <input type="radio" name="tipe_data_standar_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_standar_studi_kasus" value="Tidak" required> Tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>1.2 Syntax program yang dikuasai digunakan sesuai standar.</td>
                        <td>1.2.1 Hasil asesi menggunakan syntax yang dikuasi sesuai standar</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="syntax_program_kuasai_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="syntax_program_kuasai_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="syntax_program_kuasai_t" value="Ya" required> Ya <br>
                          <input type="radio" name="syntax_program_kuasai_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="syntax_program_kuasai_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="syntax_program_kuasai_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="syntax_program_kuasai_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="syntax_program_kuasai_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="syntax_program_kuasai_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="syntax_program_kuasai_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="syntax_program_kuasai_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="syntax_program_kuasai_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="syntax_program_kuasai_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="syntax_program_kuasai_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="syntax_program_kuasai_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="syntax_program_kuasai_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>1.3 Struktur kontrol program yang dikuasai digunakan sesuai standar.</td>
                        <td>1.3.1 Hasil asesi menggunakan struktur kontrol program yang dikuasasi sesuai standar</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="struktur_kontrol_program_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_program_tl" value="Tidak" required> Tidak
                        </td>
                        <td>                          
                          <input type="radio" name="struktur_kontrol_program_t" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_program_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="struktur_kontrol_program_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_program_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="struktur_kontrol_program_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_program_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="struktur_kontrol_program_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_program_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="struktur_kontrol_program_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_program_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="struktur_kontrol_program_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_program_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="struktur_kontrol_program_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_program_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 2.</strong> Membuat program sederhana</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>2.1 Program baca tulis untuk memasukkan data dari keyboard dan menampilkan ke layar monitor termasuk variasinya sesuai standar masukan/keluaran telah dibuat.</td>
                        <td>2.1.1 Hasil asesi Membuat program baca tulis data dari keyboard dan menampilkan ke layar monitor</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="program_baca_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="program_baca_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="program_baca_t" value="Ya" required> Ya <br>
                          <input type="radio" name="program_baca_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="program_baca_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="program_baca_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_baca_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="program_baca_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_baca_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="program_baca_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_baca_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="program_baca_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_baca_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="program_baca_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="program_baca_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="program_baca_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                            
                      <tr>
                        <td>2.2 Struktur kontrol percabangan dan pengulangan dalam membuat program telah digunakan.</td>
                        <td>2.1.2. Hasil asesi Menggunakan struktur percabangan dan pengualangan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="struktur_kontrol_percabangan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_percabangan_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="struktur_kontrol_percabangan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_percabangan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="struktur_kontrol_percabangan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_percabangan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="struktur_kontrol_percabangan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_percabangan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="struktur_kontrol_percabangan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_percabangan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="struktur_kontrol_percabangan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_percabangan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="struktur_kontrol_percabangan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_percabangan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="struktur_kontrol_percabangan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="struktur_kontrol_percabangan_studi_kasus" value="Tidak" required> Tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 3.</strong> Membuat program menggunakan prosedur dan fungsi</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>3.1 Program dengan menggunakan prosedur dibuat sesuai aturan penulisan program.</td>
                        <td>3.1.1 Hasil asesi Membuat program dengan menggunakan prosedur sesuai aturan penulisan program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="program_prosedur_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="program_prosedur_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="program_prosedur_t" value="Ya" required> Ya <br>
                          <input type="radio" name="program_prosedur_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="program_prosedur_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="program_prosedur_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_prosedur_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="program_prosedur_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_prosedur_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="program_prosedur_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_prosedur_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="program_prosedur_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_prosedur_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="program_prosedur_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="program_prosedur_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="program_prosedur_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                            
                      <tr>
                        <td>3.2 Program dengan menggunakan fungsi dibuat sesuai aturan penulisan program.</td>
                        <td>3.2.1 Hasil asesi Membuat program dengan menggunakan sesuai aturan penulisan program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="program_fungsi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="program_fungsi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="program_fungsi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_fungsi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_fungsi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_fungsi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_fungsi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="program_fungsi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                            
                      <tr>
                        <td>3.3 Program dengan menggunakan prosedur dan fungsi secara bersamaan dibuat sesuai aturan penulisan program.</td>
                        <td>3.3.1 Hasil asesi Membuat program dengan menggunakan prosedur dan fungsi secara bersamaan sesuai aturan penulisan program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="program_fungsi_prosedur_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_prosedur_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="program_fungsi_prosedur_t" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_prosedur_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="program_fungsi_prosedur_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_prosedur_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_fungsi_prosedur_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_prosedur_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_fungsi_prosedur_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_prosedur_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_fungsi_prosedur_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_prosedur_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_fungsi_prosedur_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_prosedur_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="program_fungsi_prosedur_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="program_fungsi_prosedur_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                              
                      <tr>
                        <td>3.4 Keterangan untuk setiap prosedur dan fungsi telah diberikan.</td>
                        <td>3.4.1 Hasil asesi Memberikan keterangan untuk setiap prosedur dan fungsi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="keterangan_fungsi_prosedur_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="keterangan_fungsi_prosedur_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="keterangan_fungsi_prosedur_t" value="Ya" required> Ya <br>
                          <input type="radio" name="keterangan_fungsi_prosedur_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="keterangan_fungsi_prosedur_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="keterangan_fungsi_prosedur_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="keterangan_fungsi_prosedur_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="keterangan_fungsi_prosedur_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="keterangan_fungsi_prosedur_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="keterangan_fungsi_prosedur_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="keterangan_fungsi_prosedur_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="keterangan_fungsi_prosedur_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="keterangan_fungsi_prosedur_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="keterangan_fungsi_prosedur_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="keterangan_fungsi_prosedur_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="keterangan_fungsi_prosedur_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 4.</strong> Membuat program menggunakan array</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>4.1 Dimensi array telah ditentukan.</td>
                        <td>4.1.1 Hasil asesi Menentukan dimensi array</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="dimensi_array_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="dimensi_array_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="dimensi_array_t" value="Ya" required> Ya <br>
                          <input type="radio" name="dimensi_array_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="dimensi_array_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="dimensi_array_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dimensi_array_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="dimensi_array_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dimensi_array_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="dimensi_array_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dimensi_array_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="dimensi_array_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dimensi_array_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="dimensi_array_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="dimensi_array_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="dimensi_array_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                            
                      <tr>
                        <td>4.2 Tipe data array telah ditentukan.</td>
                        <td>4.2.1 Hasil asesi Menentukan tipe data array</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="tipe_data_array_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_array_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="tipe_data_array_t" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_array_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="tipe_data_array_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_array_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tipe_data_array_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_array_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tipe_data_array_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_array_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tipe_data_array_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_array_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tipe_data_array_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_array_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="tipe_data_array_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_array_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                             
                      <tr>
                        <td>4.3 Panjang array telah ditentukan.</td>
                        <td>4.3.1 Hasil asesi Menentukan panjang array</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="panjang_array_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="panjang_array_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="panjang_array_t" value="Ya" required> Ya <br>
                          <input type="radio" name="panjang_array_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="panjang_array_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="panjang_array_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="panjang_array_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="panjang_array_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="panjang_array_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="panjang_array_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="panjang_array_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="panjang_array_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="panjang_array_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="panjang_array_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="panjang_array_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="panjang_array_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                              
                      <tr>
                        <td>4.4 Pengurutan array telah digunakan</td>
                        <td>4.4.1 Hasil asesi Menggunakan pengurutan array</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="pengurutan_array_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="pengurutan_array_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="pengurutan_array_t" value="Ya" required> Ya <br>
                          <input type="radio" name="pengurutan_array_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="pengurutan_array_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="pengurutan_array_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pengurutan_array_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="pengurutan_array_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pengurutan_array_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="pengurutan_array_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pengurutan_array_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="pengurutan_array_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="pengurutan_array_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="pengurutan_array_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="pengurutan_array_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="pengurutan_array_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 5.</strong> Membuat program untuk akses file</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>5.1 Program untuk menulis data dalam media penyimpan telah dibuat.</td>
                        <td>5.1.1 Hasil asesi Membuat program untuk menulis data dalam media penyimpanan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="program_menulis_data_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="program_menulis_data_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="program_menulis_data_t" value="Ya" required> Ya <br>
                          <input type="radio" name="program_menulis_data_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="program_menulis_data_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="program_menulis_data_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_menulis_data_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="program_menulis_data_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_menulis_data_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="program_menulis_data_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_menulis_data_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="program_menulis_data_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_menulis_data_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="program_menulis_data_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="program_menulis_data_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="program_menulis_data_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                            
                      <tr>
                        <td>5.2 Program untuk membaca data dari media penyimpan telah dibuat.</td>
                        <td>5.2.1 Hasil asesi Membuat program untuk mebaca data dari media penyimpanan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="program_membaca_data_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="program_membaca_data_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="program_membaca_data_t" value="Ya" required> Ya <br>
                          <input type="radio" name="program_membaca_data_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="program_membaca_data_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="program_membaca_data_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_membaca_data_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="program_membaca_data_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_membaca_data_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="program_membaca_data_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_membaca_data_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="program_membaca_data_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="program_membaca_data_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="program_membaca_data_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="program_membaca_data_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="program_membaca_data_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 6.</strong> Mengkompilasi Program</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>6.1 Kesalahan program telah dikoreksi.</td>
                        <td>6.1.1 Hasil asesi Mengoreksi kesalahan program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kesalahan_program_koreksi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_program_koreksi_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kesalahan_program_koreksi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_program_koreksi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kesalahan_program_koreksi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_program_koreksi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kesalahan_program_koreksi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_program_koreksi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kesalahan_program_koreksi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_program_koreksi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kesalahan_program_koreksi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_program_koreksi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kesalahan_program_koreksi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_program_koreksi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kesalahan_program_koreksi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_program_koreksi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                            
                      <tr>
                        <td>6.2 Kesalahan syntax dalam program telah dibebaskan.</td>
                        <td>6.1.2 Hasil Asesi Membebaskan kesalahan syntax</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kesalahan_syntax_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_syntax_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kesalahan_syntax_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_syntax_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kesalahan_syntax_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_syntax_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kesalahan_syntax_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_syntax_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kesalahan_syntax_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_syntax_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kesalahan_syntax_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_syntax_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kesalahan_syntax_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_syntax_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kesalahan_syntax_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kesalahan_syntax_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>          
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: J.620100.022.02</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Mengimplementasikan Algoritma Pemrograman</td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 1.</strong> Menjelaskan varian dan invarian</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>1.1 Tipe data telah dijelaskan sesuai kaidah pemrograman</td>
                        <td>1.1.1 Hasil asesi menjelaskan tipe data sesuai kaidah pemrograman</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="tipe_data_jelas_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_jelas_tl" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">                          
                          <input type="radio" name="tipe_data_jelas_t" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_jelas_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="tipe_data_jelas_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_jelas_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="tipe_data_jelas_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_jelas_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="tipe_data_jelas_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_jelas_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tipe_data_jelas_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_jelas_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tipe_data_jelas_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_jelas_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2" class="col-md-1">
                          <input type="radio" name="tipe_data_jelas_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="tipe_data_jelas_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>1.2 Variabel telah dijelaskan sesuai kaidah pemrograman</td>
                        <td>1.2.1 Hasil asesi menjelaskan variabel sesuai kaidah pemrograman</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="variable_jelas_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="variable_jelas_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="variable_jelas_t" value="Ya" required> Ya <br>
                          <input type="radio" name="variable_jelas_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="variable_jelas_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="variable_jelas_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="variable_jelas_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="variable_jelas_tes_lisan" value="Tidak" required> Tidak
                        </td>
                        <td>
                          <input type="radio" name="variable_jelas_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="variable_jelas_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="variable_jelas_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="variable_jelas_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="variable_jelas_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="variable_jelas_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="variable_jelas_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="variable_jelas_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td rowspan="4">1.3 Konstanta telah dijelaskan sesuai kaidah pemrograman</td>
                      </tr>
                      <tr>
                        <td>1.3.1 Hasil asesi mengetahui definisi konstanta sesuai kaidah pemrograman</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="konstanta_mengetahui_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_mengetahui_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="konstanta_mengetahui_t" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_mengetahui_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="konstanta_mengetahui_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_mengetahui_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_mengetahui_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_mengetahui_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_mengetahui_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_mengetahui_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_mengetahui_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_mengetahui_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_mengetahui_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_mengetahui_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="konstanta_mengetahui_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_mengetahui_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.3.2 Hasil asesi memahami definisi konstanta sesuai kaidah pemrograman </td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="konstanta_memahami_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_memahami_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="konstanta_memahami_t" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_memahami_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="konstanta_memahami_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_memahami_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_memahami_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_memahami_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_memahami_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_memahami_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_memahami_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_memahami_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_memahami_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_memahami_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="konstanta_memahami_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_memahami_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.3.2 Hasil asesi menjelaskan konstanta sesuai kaidah pemrograman</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="konstanta_menjelaskan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_menjelaskan_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="konstanta_menjelaskan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_menjelaskan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="konstanta_menjelaskan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_menjelaskan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_menjelaskan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_menjelaskan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_menjelaskan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_menjelaskan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_menjelaskan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_menjelaskan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konstanta_menjelaskan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_menjelaskan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="konstanta_menjelaskan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="konstanta_menjelaskan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 2.</strong> Membuat alur logika pemrograman</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>                           
                      <tr>
                        <td>2.1 Metode yang sesuai ditentukan</td>
                        <td>2.1.1 Hasil asesi menentukan metode yang sesuai</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="metode_sesuai_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="metode_sesuai_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="metode_sesuai_t" value="Ya" required> Ya <br>
                          <input type="radio" name="metode_sesuai_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="metode_sesuai_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="metode_sesuai_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="metode_sesuai_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="metode_sesuai_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="metode_sesuai_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="metode_sesuai_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="metode_sesuai_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="metode_sesuai_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="metode_sesuai_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="metode_sesuai_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="metode_sesuai_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="metode_sesuai_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>                           
                      <tr>
                        <td>2.2 Komponen yang dibutuhkan ditentukan</td>
                        <td>2.2.1 Hasil asesi menentukan komponen yang dibutuhkan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="komponen_butuh_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_butuh_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="komponen_butuh_t" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_butuh_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="komponen_butuh_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_butuh_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komponen_butuh_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_butuh_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komponen_butuh_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_butuh_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komponen_butuh_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_butuh_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komponen_butuh_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_butuh_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="komponen_butuh_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="komponen_butuh_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                        <td>2.3 Relasi antar komponen ditetapkan</td>
                        <td>2.3.1 Hasil asesi menetapkan relasi antar komponen</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="relasi_antar_komponen_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="relasi_antar_komponen_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="relasi_antar_komponen_t" value="Ya" required> Ya <br>
                          <input type="radio" name="relasi_antar_komponen_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="relasi_antar_komponen_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="relasi_antar_komponen_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="relasi_antar_komponen_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="relasi_antar_komponen_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="relasi_antar_komponen_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="relasi_antar_komponen_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="relasi_antar_komponen_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="relasi_antar_komponen_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="relasi_antar_komponen_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="relasi_antar_komponen_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="relasi_antar_komponen_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="relasi_antar_komponen_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      </tr>
                        <td>2.4 Alur mulai dan selesai ditetapkan</td>
                        <td>2.4.1 Hasil asesi menetapkan alur mulai dan selesai</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="alur_mulai_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="alur_mulai_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="alur_mulai_t" value="Ya" required> Ya <br>
                          <input type="radio" name="alur_mulai_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="alur_mulai_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="alur_mulai_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="alur_mulai_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="alur_mulai_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="alur_mulai_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="alur_mulai_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="alur_mulai_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="alur_mulai_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="alur_mulai_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="alur_mulai_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="alur_mulai_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="alur_mulai_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 3.</strong> Menerapkan teknik dasar algoritma umum</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      <tr>
                        <td rowspan="3">3.1 Algoritma untuk sorting dibuat.</td>
                      </tr>
                      <tr>
                        <td>3.1.1 Hasil asesi menjelaskan  algoritma untuk sorting</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="algoritma_sorting_menjelaskan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_menjelaskan_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="algoritma_sorting_menjelaskan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_menjelaskan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="algoritma_sorting_menjelaskan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_menjelaskan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_sorting_menjelaskan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_menjelaskan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_sorting_menjelaskan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_menjelaskan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_sorting_menjelaskan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_menjelaskan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_sorting_menjelaskan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_menjelaskan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="algoritma_sorting_menjelaskan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_menjelaskan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>3.1.2 Hasil asesi membuat algoritma untuk sorting</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="algoritma_sorting_membuat_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_membuat_tl" value="Tidak" required> Tidak
                        </td>
                        <td>                          
                          <input type="radio" name="algoritma_sorting_membuat_t" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_membuat_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="algoritma_sorting_membuat_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_membuat_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_sorting_membuat_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_membuat_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_sorting_membuat_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_membuat_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_sorting_membuat_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_membuat_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_sorting_membuat_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_membuat_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="algoritma_sorting_membuat_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_sorting_membuat_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr> 
                      <tr>
                        <td rowspan="3">3.2 Algoritma untuk searching dibuat</td>
                      </tr>
                      <tr>
                        <td>3.2.1 Hasil asesi menjelaskan algoritma untuk searching</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="algoritma_searching_menjelaskan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_menjelaskan_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="algoritma_searching_menjelaskan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_menjelaskan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="algoritma_searching_menjelaskan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_menjelaskan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_searching_menjelaskan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_menjelaskan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_searching_menjelaskan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_menjelaskan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_searching_menjelaskan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_menjelaskan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_searching_menjelaskan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_menjelaskan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="algoritma_searching_menjelaskan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_menjelaskan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>3.2.2 Hasil asesi membuat algoritma searching</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="algoritma_searching_membuat_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_membuat_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="algoritma_searching_membuat_t" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_membuat_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="algoritma_searching_membuat_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_membuat_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_searching_membuat_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_membuat_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_searching_membuat_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_membuat_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_searching_membuat_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_membuat_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="algoritma_searching_membuat_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_membuat_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="algoritma_searching_membuat_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="algoritma_searching_membuat_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 4.</strong> Menggunakan prosedur dan fungsi</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>
                      <tr>
                        <td>4.1 Konsep penggunaan kembali prosedur dan fungsi dapat diidentifikasi</td>
                        <td>4.1.1 Hasil asesi mengidentifikasi konsep penggunaan kembali prosedur dan fungsi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="konsep_penggunaan_prosedur_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_penggunaan_prosedur_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="konsep_penggunaan_prosedur_t" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_penggunaan_prosedur_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="konsep_penggunaan_prosedur_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_penggunaan_prosedur_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konsep_penggunaan_prosedur_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_penggunaan_prosedur_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konsep_penggunaan_prosedur_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_penggunaan_prosedur_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konsep_penggunaan_prosedur_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_penggunaan_prosedur_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="konsep_penggunaan_prosedur_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_penggunaan_prosedur_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="konsep_penggunaan_prosedur_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="konsep_penggunaan_prosedur_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>4.2 Prosedur dapat digunakan</td>
                        <td>4.2.1 Hasil asesi menggunakan prosedur</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="prosedur_dapat_digunakan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="prosedur_dapat_digunakan_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="prosedur_dapat_digunakan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="prosedur_dapat_digunakan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="prosedur_dapat_digunakan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="prosedur_dapat_digunakan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="prosedur_dapat_digunakan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="prosedur_dapat_digunakan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="prosedur_dapat_digunakan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="prosedur_dapat_digunakan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="prosedur_dapat_digunakan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="prosedur_dapat_digunakan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="prosedur_dapat_digunakan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="prosedur_dapat_digunakan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="prosedur_dapat_digunakan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="prosedur_dapat_digunakan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>4.3 Fungsi dapat digunakan</td>
                        <td>4.3.1 Hasil asesi menggunakan fungsi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="fungsi_dapat_digunakan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_dapat_digunakan_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="fungsi_dapat_digunakan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_dapat_digunakan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="fungsi_dapat_digunakan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_dapat_digunakan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fungsi_dapat_digunakan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_dapat_digunakan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fungsi_dapat_digunakan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_dapat_digunakan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fungsi_dapat_digunakan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_dapat_digunakan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fungsi_dapat_digunakan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_dapat_digunakan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="fungsi_dapat_digunakan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_dapat_digunakan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>4.4.1 Hasil asesi menulis perintah DML secara efisien</td>
                        <td></td>
                        <td>
                          <input type="radio" name="fungsi_perintah_dml_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_perintah_dml_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="fungsi_perintah_dml_t" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_perintah_dml_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="fungsi_perintah_dml_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_perintah_dml_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fungsi_perintah_dml_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_perintah_dml_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fungsi_perintah_dml_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_perintah_dml_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fungsi_perintah_dml_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_perintah_dml_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="fungsi_perintah_dml_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_perintah_dml_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="fungsi_perintah_dml_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="fungsi_perintah_dml_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 5.</strong> Mengidentifikasikan kompleksitas algoritma</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>
                      <tr>
                        <td>5.1 Kompleksitas waktu algoritma diidentifikasi</td>
                        <td>5.1.1 Hasil asesi mengidentifikasi kompleksitas waktu algoritma</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kompleksitas_waktu_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_waktu_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kompleksitas_waktu_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_waktu_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kompleksitas_waktu_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_waktu_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kompleksitas_waktu_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_waktu_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kompleksitas_waktu_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_waktu_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kompleksitas_waktu_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_waktu_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kompleksitas_waktu_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_waktu_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kompleksitas_waktu_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_waktu_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                        <td>5.2 Kompleksitas penggunaan memory algoritma diidentifikasi</td>
                        <td>5.2.1 Hasil asesi mengidentifikasi kompleksitas penggunaan memory algoritma</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kompleksitas_memory_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_memory_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kompleksitas_memory_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_memory_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kompleksitas_memory_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_memory_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kompleksitas_memory_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_memory_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kompleksitas_memory_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_memory_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kompleksitas_memory_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_memory_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kompleksitas_memory_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_memory_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kompleksitas_memory_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kompleksitas_memory_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: J.620100.023.02</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Membuat Dokumen Kode Program</td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 1.</strong> Melakukan identifikasi kode program</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>
                      <tr>
                        <td>1.1 Modul program diidentifikasi</td>
                        <td>1.1.1 Hasil asesi mengidentifikasi modul program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="modul_program_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="modul_program_tl" value="Tidak" required> Tidak
                        </td>
                        <td>                          
                          <input type="radio" name="modul_program_t" value="Ya" required> Ya <br>
                          <input type="radio" name="modul_program_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="modul_program_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="modul_program_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="modul_program_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="modul_program_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="modul_program_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="modul_program_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="modul_program_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="modul_program_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="modul_program_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="modul_program_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="modul_program_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="modul_program_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.2 Parameter yang dipergunakan diidentifikasi</td>
                        <td>1.2.1 Hasil asesi mengidentifikasi parameter yang dipergunakan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="paremeter_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="paremeter_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="paremeter_t" value="Ya" required> Ya <br>
                          <input type="radio" name="paremeter_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="paremeter_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="paremeter_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="paremeter_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="paremeter_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="paremeter_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="paremeter_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="paremeter_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="paremeter_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="paremeter_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="paremeter_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="paremeter_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="paremeter_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td rowspan="4">1.3 Algoritma dijelaskan cara kerjanya</td>
                      </tr>
                      <tr>
                        <td>1.3.1 Hasil asesi mengetahui definisi algoritma dan cara kerjanya</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="mengetahui_defisi_algoritma_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="mengetahui_defisi_algoritma_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="mengetahui_defisi_algoritma_t" value="Ya" required> Ya <br>
                          <input type="radio" name="mengetahui_defisi_algoritma_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="mengetahui_defisi_algoritma_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="mengetahui_defisi_algoritma_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengetahui_defisi_algoritma_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="mengetahui_defisi_algoritma_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengetahui_defisi_algoritma_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="mengetahui_defisi_algoritma_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengetahui_defisi_algoritma_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="mengetahui_defisi_algoritma_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="mengetahui_defisi_algoritma_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="mengetahui_defisi_algoritma_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="mengetahui_defisi_algoritma_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="mengetahui_defisi_algoritma_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.3.2 Hasil asesi memahami definisi algoritma dan cara kerjanya</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="memahami_defisi_algoritma_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="memahami_defisi_algoritma_tl" value="Tidak" required> Tidak
                        </td>
                        <td>                          
                          <input type="radio" name="memahami_defisi_algoritma_t" value="Ya" required> Ya <br>
                          <input type="radio" name="memahami_defisi_algoritma_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="memahami_defisi_algoritma_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="memahami_defisi_algoritma_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memahami_defisi_algoritma_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="memahami_defisi_algoritma_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memahami_defisi_algoritma_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="memahami_defisi_algoritma_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memahami_defisi_algoritma_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="memahami_defisi_algoritma_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="memahami_defisi_algoritma_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="memahami_defisi_algoritma_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="memahami_defisi_algoritma_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="memahami_defisi_algoritma_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.3.2 Hasil asesi menjelaskan algoritma dan cara kerjanya</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="menjelaskan_defisi_algoritma_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="menjelaskan_defisi_algoritma_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="menjelaskan_defisi_algoritma_t" value="Ya" required> Ya <br>
                          <input type="radio" name="menjelaskan_defisi_algoritma_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="menjelaskan_defisi_algoritma_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="menjelaskan_defisi_algoritma_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menjelaskan_defisi_algoritma_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="menjelaskan_defisi_algoritma_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menjelaskan_defisi_algoritma_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="menjelaskan_defisi_algoritma_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menjelaskan_defisi_algoritma_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="menjelaskan_defisi_algoritma_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="menjelaskan_defisi_algoritma_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="menjelaskan_defisi_algoritma_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="menjelaskan_defisi_algoritma_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="menjelaskan_defisi_algoritma_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.4 Komentar setiap baris kode termasuk data, eksepsi, fungsi, prosedur dan class (bila ada) diberikan</td>
                        <td>1.4.1 Hasil asesi memberikan komentar setiap baris kode termasuk data, eksepsi, fungsi, prosedur dan class (bila ada)</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="komentar_baris_kode_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="komentar_baris_kode_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="komentar_baris_kode_t" value="Ya" required> Ya <br>
                          <input type="radio" name="komentar_baris_kode_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="komentar_baris_kode_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="komentar_baris_kode_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komentar_baris_kode_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="komentar_baris_kode_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komentar_baris_kode_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="komentar_baris_kode_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komentar_baris_kode_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="komentar_baris_kode_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="komentar_baris_kode_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="komentar_baris_kode_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="komentar_baris_kode_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="komentar_baris_kode_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 2.</strong> Membuat dokumentasi modul program</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr> 
                      <tr>
                        <td>2.1 Dokumentasi modul dibuat sesuai dengan identitas untuk memudahkan pelacakan</td>
                        <td>2.1.1 Hasil asesi membuat dokumentasi modul sesuai dengan identitas untuk memudahkan pelacakan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="dokumentasi_buku_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_buku_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="dokumentasi_buku_t" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_buku_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="dokumentasi_buku_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_buku_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_buku_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_buku_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_buku_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_buku_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_buku_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_buku_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_buku_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_buku_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="dokumentasi_buku_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_buku_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>2.2 Identifikasi dokumentasi diterapkan</td>
                        <td>2.2.1 Hasil asesi menerapkan cara identifikasi dokumentasi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="dokumentasi_terap_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_terap_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="dokumentasi_terap_t" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_terap_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="dokumentasi_terap_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_terap_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_terap_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_terap_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_terap_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_terap_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_terap_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_terap_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_terap_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_terap_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="dokumentasi_terap_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_terap_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td rowspan="4">2.3 Kegunaan modul dijelaskan</td>
                      </tr>
                      <tr>
                        <td>2.3.1 Hasil asesi mengetahui definisi modul</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kegunaan_mengetauhi_modul_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_mengetauhi_modul_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kegunaan_mengetauhi_modul_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_mengetauhi_modul_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kegunaan_mengetauhi_modul_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_mengetauhi_modul_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_mengetauhi_modul_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_mengetauhi_modul_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_mengetauhi_modul_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_mengetauhi_modul_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_mengetauhi_modul_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_mengetauhi_modul_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_mengetauhi_modul_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_mengetauhi_modul_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kegunaan_mengetauhi_modul_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_mengetauhi_modul_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>2.3.1 Hasil asesi memahami definisi modul</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kegunaan_memahami_modul_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_memahami_modul_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kegunaan_memahami_modul_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_memahami_modul_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kegunaan_memahami_modul_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_memahami_modul_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_memahami_modul_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_memahami_modul_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_memahami_modul_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_memahami_modul_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_memahami_modul_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_memahami_modul_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_memahami_modul_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_memahami_modul_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kegunaan_memahami_modul_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_memahami_modul_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>2.3.2 Hasil asesi menjelaskan kegunaan modul</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kegunaan_menjelaskan_modul_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_menjelaskan_modul_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kegunaan_menjelaskan_modul_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_menjelaskan_modul_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kegunaan_menjelaskan_modul_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_menjelaskan_modul_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_menjelaskan_modul_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_menjelaskan_modul_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_menjelaskan_modul_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_menjelaskan_modul_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_menjelaskan_modul_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_menjelaskan_modul_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kegunaan_menjelaskan_modul_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_menjelaskan_modul_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kegunaan_menjelaskan_modul_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kegunaan_menjelaskan_modul_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>2.4 Dokumen direvisi sesuai perubahan kode program</td>
                        <td>2.4.1 Hasil asesi merevisi dokumen sesuai kebutuhan kode program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="dokumen_revisi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="dokumen_revisi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="dokumen_revisi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumen_revisi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumen_revisi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumen_revisi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumen_revisi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="dokumen_revisi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 3.</strong> Membuat dokumentasi fungsi, prosedur atau method program</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr> 
                      <tr>
                        <td>3.1 Dokumentasi fungsi, prosedur atau metod dibuat</td>
                        <td>3.1.1 Hasil asesi membuat dokumentasi fungsi, prosedur atau method</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="dokumentasi_fungsi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_fungsi_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="dokumentasi_fungsi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_fungsi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="dokumentasi_fungsi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_fungsi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_fungsi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_fungsi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_fungsi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_fungsi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_fungsi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_fungsi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumentasi_fungsi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_fungsi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="dokumentasi_fungsi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumentasi_fungsi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td rowspan="4">3.2 Kemungkinan eksepsi dijelaskan</td>
                      </tr>
                      <tr>
                        <td>3.2.1 Hasil asesi mengetahui definisi eksepsi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="eksepsi_mengetahui_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_mengetahui_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="eksepsi_mengetahui_t" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_mengetahui_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="eksepsi_mengetahui_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_mengetahui_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_mengetahui_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_mengetahui_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_mengetahui_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_mengetahui_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_mengetahui_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_mengetahui_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_mengetahui_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_mengetahui_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="eksepsi_mengetahui_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_mengetahui_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>3.2.2 Hasil asesi memahami definisi eksepsi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="eksepsi_memahami_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_memahami_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="eksepsi_memahami_t" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_memahami_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="eksepsi_memahami_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_memahami_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_memahami_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_memahami_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_memahami_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_memahami_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_memahami_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_memahami_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_memahami_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_memahami_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="eksepsi_memahami_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_memahami_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>3.2.3 Hasil asesi menjelaskan eksepsi dan kemungkinan eksepsi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="eksepsi_menjelaskan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_menjelaskan_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="eksepsi_menjelaskan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_menjelaskan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="eksepsi_menjelaskan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_menjelaskan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_menjelaskan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_menjelaskan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_menjelaskan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_menjelaskan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_menjelaskan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_menjelaskan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="eksepsi_menjelaskan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_menjelaskan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="eksepsi_menjelaskan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="eksepsi_menjelaskan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>3.3 Dokumen direvisi sesuai perubahan kode program</td>
                        <td>3.3.1 Hasil asesi merevisi dokumen sesuai pribahan kode program</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="dokumen_revisi_kode_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_kode_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="dokumen_revisi_kode_t" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_kode_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="dokumen_revisi_kode_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_kode_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumen_revisi_kode_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_kode_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumen_revisi_kode_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_kode_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumen_revisi_kode_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_kode_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="dokumen_revisi_kode_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_kode_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="dokumen_revisi_kode_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="dokumen_revisi_kode_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 4.</strong> Men-generate dokumentasi</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>
                      <tr>
                        <td>4.1 Tools untuk generate dokumentasi diidentifikasi</td>
                        <td>4.1.1 Hasil asesi mengidentifikasi tools untuk generate dokumentasi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="tools_generate_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_generate_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="tools_generate_t" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_generate_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="tools_generate_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_generate_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_generate_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_generate_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_generate_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_generate_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_generate_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_generate_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="tools_generate_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_generate_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="tools_generate_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="tools_generate_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>4.2 Generate dokumentasi dilakukan</td>
                        <td>4.2.1 Hasil asesi melakukan generate dokumentasi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="generate_dokumentasi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="generate_dokumentasi_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="generate_dokumentasi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="generate_dokumentasi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="generate_dokumentasi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="generate_dokumentasi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="generate_dokumentasi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="generate_dokumentasi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="generate_dokumentasi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="generate_dokumentasi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="generate_dokumentasi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="generate_dokumentasi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="generate_dokumentasi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="generate_dokumentasi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="generate_dokumentasi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="generate_dokumentasi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>                      
                      <tr>
                        <td>Kode Unit</td>
                        <td colspan="12">: J.620100.025.02</td>
                      </tr>
                      <tr>
                        <td>Judul Unit</td>
                        <td colspan="12">: Melakukan Debugging</td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 1.</strong> Mempersiapkan kode program</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>
                      <tr>
                        <td>1.1 Kode program sesuai spesifikasi disiapkan</td>
                        <td>1.1.1 Hasil asesi menyiapkan kode program sesuai spesifikasi</td>
                        <td>V</td>
                        <td class="col-md-1">
                          <input type="radio" name="kode_program_spesifikasi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_program_spesifikasi_tl" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">                          
                          <input type="radio" name="kode_program_spesifikasi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_program_spesifikasi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kode_program_spesifikasi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_program_spesifikasi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="kode_program_spesifikasi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_program_spesifikasi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td class="col-md-1">
                          <input type="radio" name="kode_program_spesifikasi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_program_spesifikasi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_program_spesifikasi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_program_spesifikasi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_program_spesifikasi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_program_spesifikasi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2" class="col-md-1">
                          <input type="radio" name="kode_program_spesifikasi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_program_spesifikasi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>1.2 Debugging tools untuk melihat proses suatu modul dipersiapkan.</td>
                        <td>1.2.1 Hasil asesi mempersiapkan debugging tools untuk melihat proses suatu modul</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="debugging_tools_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="debugging_tools_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="debugging_tools_t" value="Ya" required> Ya <br>
                          <input type="radio" name="debugging_tools_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="debugging_tools_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="debugging_tools_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="debugging_tools_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="debugging_tools_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="debugging_tools_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="debugging_tools_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="debugging_tools_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="debugging_tools_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="debugging_tools_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="debugging_tools_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="debugging_tools_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="debugging_tools_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 2.</strong> Melakukan debugging</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>
                      <tr>
                        <td>2.1 Kode program dikompilasi sesuai bahasa pemrograman yang digunakan</td>
                        <td>2.1.1 Hasil asesi mengkompiasi kode program sesuai bahasa pemrograman yang digunakan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kode_kompilasi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kompilasi_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kode_kompilasi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kompilasi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kode_kompilasi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kompilasi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_kompilasi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kompilasi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_kompilasi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kompilasi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_kompilasi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kompilasi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_kompilasi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kompilasi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kode_kompilasi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kompilasi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>2.2 Kriteria lulus build dianalisis</td>
                        <td>2.2.1 Hasil asesi menganalisis kriteria lulus build</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kriteria_lulus_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_lulus_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kriteria_lulus_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_lulus_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kriteria_lulus_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_lulus_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kriteria_lulus_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_lulus_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kriteria_lulus_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_lulus_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kriteria_lulus_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_lulus_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kriteria_lulus_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_lulus_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kriteria_lulus_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_lulus_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>2.3 Kriteria eksekusi aplikasi dianalisis</td>
                        <td>2.3.1 Hasil asesi menganalisis kriteria eksekusi aplikasi</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kriteria_eksekusi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_eksekusi_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kriteria_eksekusi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_eksekusi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kriteria_eksekusi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_eksekusi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kriteria_eksekusi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_eksekusi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kriteria_eksekusi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_eksekusi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kriteria_eksekusi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_eksekusi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kriteria_eksekusi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_eksekusi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kriteria_eksekusi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kriteria_eksekusi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>2.4 Kode kesalahan dicatat</td>
                        <td>2.4.1 Hasil asesi mencatat kode kesalahan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="kode_kesalahan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kesalahan_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="kode_kesalahan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kesalahan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="kode_kesalahan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kesalahan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_kesalahan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kesalahan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_kesalahan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kesalahan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_kesalahan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kesalahan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="kode_kesalahan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kesalahan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="kode_kesalahan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="kode_kesalahan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="13"><strong>ELEMEN 3.</strong> Memperbaiki program</td>
                      </tr>                   
                      <tr>
                        <td rowspan="6"><strong>KRITERIA UNJUK KERJA</strong></td>
                        <td rowspan="6"><strong>BUKTI-BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>JENIS BUKTI</strong></td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">PERANGKAT ASESMEN</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">CLO : Ceklis Observasi , CLP : Ceklis Portofolio, VPK: Verifikasi Pihak Ketiga, DPL: Daftar Pertanyaan Lisan, DPT *) : Daftar Pertanyaan Tertulis, SK : Studi Kasus, PW: Pertanyaan Wawancara</td>
                      </tr>
                      <tr>
                        <td colspan="13" class="text-center">METODE</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>TL</td>
                        <td>T</td>
                        <td>Observasi Demonstrasi</td>
                        <td>Verifikasi PortoFolio</td>
                        <td>Tes Lisan</td>
                        <td>Tes Tertulis</td>
                        <td>Wawancara</td>
                        <td>Verifikasi Pihak Ketiga</td>
                        <td colspan="2">Studi Kasus</td>
                      </tr>
                      <tr>
                        <td>3.1 Perbaikan terhadap kesalahan kompilasi maupun build dirumuskan</td>
                        <td>3.1.1 Hasil asesi merumuskan perbaikan terhadap kesalahan kompilasi maupun build</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="perbaikan_kompilasi_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_kompilasi_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="perbaikan_kompilasi_t" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_kompilasi_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="perbaikan_kompilasi_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_kompilasi_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="perbaikan_kompilasi_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_kompilasi_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="perbaikan_kompilasi_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_kompilasi_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="perbaikan_kompilasi_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_kompilasi_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="perbaikan_kompilasi_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_kompilasi_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="perbaikan_kompilasi_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_kompilasi_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                      <tr>
                        <td>3.2 Perbaikan dilakukan</td>
                        <td>3.2.1 Hasil asesi melakukan perbaikan</td>
                        <td>V</td>
                        <td>
                          <input type="radio" name="perbaikan_dilakukan_tl" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_dilakukan_tl" value="Tidak" required> tidak
                        </td>
                        <td>                          
                          <input type="radio" name="perbaikan_dilakukan_t" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_dilakukan_t" value="Tidak" required> tidak
                        </td>
                        <td>CLO TPD</td>
                        <td>
                          <input type="radio" name="perbaikan_dilakukan_verifikasi" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_dilakukan_verifikasi" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="perbaikan_dilakukan_tes_lisan" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_dilakukan_tes_lisan" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="perbaikan_dilakukan_tes_tulis" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_dilakukan_tes_tulis" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="perbaikan_dilakukan_wawancara" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_dilakukan_wawancara" value="Tidak" required> tidak
                        </td>
                        <td>
                          <input type="radio" name="perbaikan_dilakukan_pihak_tiga" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_dilakukan_pihak_tiga" value="Tidak" required> tidak
                        </td>
                        <td colspan="2">
                          <input type="radio" name="perbaikan_dilakukan_studi_kasus" value="Ya" required> Ya <br>
                          <input type="radio" name="perbaikan_dilakukan_studi_kasus" value="Tidak" required> tidak
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br>
                  <table class="table">
                    <tbody>
                      <tr>
                        <td><strong>Sumber Daya Phisik/Material :</strong>
                            <li>Observasi Demonstrasi : Ruangan TUK, laptop, mouse, pendingin,  ceklis observasi</li>
                            <li>Tes Tertulis : Ruangan TUK, daftar pertanyaan tertulis, lembar jawaban tertulis, kunci jawaban tertulis </li>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <p>Catatan : *) L = Buklti langsung,  TL = Bukti tidak langsung,  T = Bukti tambahan</p>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>
                      <tr>
                        <td rowspan="3" colspan="2">Pemenuhan terhadap seluruh bagian unit standar kompetensi : (bila tersedia)</td>
                      </tr>
                      <tr>
                        <td>Batasan Variabel</td>
                        <td>Panduan Asesmen</td>
                      </tr>
                      <tr>
                        <td>
                          <label>
                            <input type="radio" name="batasan_variable" required value="Ya">
                          </label>Ya
                          <br>
                          <label>
                            <input type="radio" name="batasan_variable" required value="Tidak">
                          </label>tidak
                        </td>
                        <td>
                          <label>
                            <input type="radio" name="panduan_asesmen" required value="Ya">
                          </label>Ya
                          <br>
                          <label>
                            <input type="radio" name="panduan_asesmen" required value="Tidak">
                          </label>tidak
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">Peran dan tanggung jawab Tim/Personil terkait: *) Khusus persetujuan Peserta dapat dilakukan pada saat Konsultasi Pra Uji dan ditanda tangani pada formulir khusus persetujuan asesmen.</td>
                      </tr>
                      <tr>
                        <td>Nama</td>
                        <td>Jabatan/pekerjaan</td>
                        <td>Peran dan tanggung jawab dalam asesmen</td>
                        <td>Paraf/tanggal</td>
                      </tr>
                      <tr>
                        <td>Juliana Mansur, S.Kom.</td>
                        <td>Asesor</td>
                        <td>Merencanakan dan mengembangkan perangkat asesmen dan Mengases kompetensi peserta sertifikasi</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Juliana Mansur, S.Kom.</td>
                        <td>Kepala TUK</td>
                        <td>Membantu kegiatan uji kompetensi</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Jangka dan periode waktu asesmen</td>
                        <td colspan="3">
                          <li>Tanggal Asesmen :
                            <li>Konsultasi pra asesmen: 30 menit</li>
                            <li>Asesmen (durasi per metode ):
                              <p>Tes Praktek/Observasi Demonstrasi : 180 menit</p>
                              <p>Tes Tertulis/Lisan : 30 menit</p>
                            </li>
                            <li>Keputusan dan umpan balik : 15 menit</li>
                            <li>Umpan balik dari peserta: 10 menit</li>
                            <li>Laporan asesmen: 10 menit</li>
                            <li>Meninjau proses sesmen: 10 menit</li>
                          </li>
                        </td>
                      </tr>
                      <tr>
                        <td>Lokasi asesmen</td>
                        <td colspan="3">TUK Sewaktu</td>
                      </tr>
                      <tr>
                        <td colspan="4" style="background-color: grey; color: white;">3. Kontekstualisasi dan meninjau rencana asesmen :</td>
                      </tr>
                      <tr>
                        <td colspan="2">3.1. Karakteristik Peserta :</td>
                        <td colspan="2">Penyesuaian kebutuhan spesifik asesi:</td>
                      </tr>
                      <tr>
                        <td colspan="2">Peserta sertifikasi (normal atau tidak)*</td>
                        <td colspan="2">(ada atau tidak ada )*penyesuaian</td>
                      </tr>
                      <tr>
                        <td rowspan="3" colspan="2">3.2. Kontekstualisasi standar kompetensi : (untuk mengakomodasi persyaratan spesifik industri, pada batasan variabel dan pedoman bukti)</td>
                      </tr>
                      <tr>
                        <td colspan="2">Pada batasan variabel : (peralatan yang digunakan atau dipakai)(ada atau tidak ada)* penyesuaian</td>
                      </tr>
                      <tr>
                        <td colspan="2">Pada panduan penilaian : (ada atau tidak ada )* penyesuaian</td>
                      </tr>
                      <tr>
                        <td colspan="4">3.3.  Memeriksa metoda dan perangkat asesmen yang dipilih (sesuai/tidak sesuai) dengan skema sertifikasi</td>
                      </tr>
                      <tr>
                        <td colspan="3">Bila diperlukan penyesuaian Metode dan perangkat asesmen dipertimbangkan terhadap :</td>
                        <td>Catatan (Tuliskan bila ada penyesuaian)</td>
                      </tr>
                      <tr>
                        <td colspan="3">1. Berbagai kontekstualisasi Standar Kompetensi</td>
                        <td>Tidak ada</td>
                      </tr>
                      <tr>
                        <td colspan="3">2. Penyesuaian yang beralasan</td>
                        <td>Tidak ada</td>
                      </tr>
                      <tr>
                        <td colspan="3">Bila diperlukan penyesuaian Metode dan perangkat asesmen dipertimbangkan terhadap :</td>
                        <td>Catatan (Tuliskan bila ada penyesuaian)</td>
                      </tr>
                      <tr>
                        <td colspan="3">3.  Kegiatan asesmen terintegrasi</td>
                        <td>Tidak ada</td>
                      </tr>
                      <tr>
                        <td colspan="3">4.  Kapasitas untuk mendukung RPL</td>
                        <td>Tidak ada</td>
                      </tr>
                      <tr>
                        <td colspan="3">3.4. Meninjau Perangkat asesmen yang disesuaikan terhadap spesifikasi standar kompetensi  (Ya/Tidak)</td>
                        <td>Catatan (Tuliskan bila ada)</td>
                      </tr>
                      <tr>
                        <td colspan="3"></td>
                        <td>Tidak ada</td>
                      </tr>
                      <tr>
                        <td colspan="4" style="background-color: grey; color: white;">4. Mengorganisasikan asesmen :</td>
                      </tr>
                      <tr>
                        <td>4.1. Pengaturan sumber daya asesmen</td>
                        <td colspan="3">Sumber daya Asesmen : <br> Ruangan TUK, perangkat asesmen, alat kerja (Laptop/Komputer), bahan (Kertas,Pensil dan Ballpoint), Instruksi kerja, teknisi TUK dipastikan minimal H-3 hari siap digunakan</td>
                      </tr>
                      <tr>
                        <td>4.2. Pengaturan dukungan spesialis</td>
                        <td colspan="3">Ada/ Tidak ada</td>
                      </tr>
                      <tr>
                        <td rowspan="5">4.3. Pengorganisasian personil yang terlibat</td>
                      </tr>
                      <tr>
                        <td colspan="3">Personil :</td>
                      </tr>
                      <tr>
                        <td>Asesor</td>
                        <td colspan="2">- Memeriksa Kesiapan dokumen/berkas Asesmen<br>
                        - Memeriksa kesiapan sumber daya asesmen yg dibutuhkan<br>
                        - Memberikan arahan kepada peserta asesmen<br>
                        - Melakukan & mengawasi proses asesmen<br>
                        - Mengumpulkan & memerikasa kelengkapan berkas/dokumen asesmen</td>
                      </tr>
                      <tr>
                        <td>Asesi</td>
                        <td colspan="3">- Asesi Berkumpul ditempat yg telah disediakan<br>
                            - Asesi diminta mengisi & menandatangani daftar hadir<br>
                            - Asesi menerima penjelasan & pengarahan mengenai pelaksanaan asesmen, termasuk tata tertib asesmen yg berlaku<br>
                            - Asesi mengikuti jadwal asesmen yg sudah ditetapkan 
                        </td>
                      </tr>
                      <tr>
                        <td>Panitia</td>
                        <td colspan="3">
                          - Menyiapkan ruangan ruangan/fasilitas asesmen<br>
                          - Menyiapkan berkas/form asesmen<br>
                          - Menyiapkan peralatan yulis yg dibutuhkan<br>
                          - Menyiapkan daftar hadir & memeriksa kehadiran peserta<br>
                          - Memeriksa, mengumpulkan & mendokumentasikan berkas asesmen<br>
                          - Menyiapkan konsumsi, akomodasi & transportasi Asesor & peserta
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">4.4. Strategi Komunikasi (pilih yang sesuai)</td>
                      </tr>
                      <tr>
                        <td>
                          <input type="radio" name="wawancara" value="Ya" required> Ya
                          <input type="radio" name="wawancara" value="Tidak" required style="margin-left: 10px;"> tidak
                        </td>
                        <td colspan="3">• Wawancara, baik secara berhadapan maupun melalui telepon</td>
                      </tr>
                      <tr>
                        <td>
                          <input type="radio" name="email" value="Ya" required> Ya
                          <input type="radio" name="email" value="Tidak" required style="margin-left: 10px;"> tidak
                        </td>
                        <td colspan="3">• Email, memo, korespondensi</td>
                      </tr>
                      <tr>
                        <td>
                          <input type="radio" name="rapat" value="Ya" required> Ya
                          <input type="radio" name="rapat" value="Tidak" required style="margin-left: 10px;"> tidak
                        </td>
                        <td colspan="3">• Rapat</td>
                      </tr>
                      <tr>
                        <td>
                          <input type="radio" name="video" value="Ya" required> Ya
                          <input type="radio" name="video" value="Tidak"  required style="margin-left: 10px;"> tidak
                        </td>
                        <td colspan="3">• Video Conference/Pembelajaran Berbasis Elektronik</td>
                      </tr>
                      <tr>
                        <td>
                          <input type="radio" name="fokus" value="Ya" required> Ya
                          <input type="radio" name="fokus" value="Tidak" required style="margin-left: 10px;"> tidak
                        </td>
                        <td colspan="3">• Fokus Group</td>
                      </tr>
                      <tr>
                        <td colspan="2">4.5. Penyimpanan Rekaman Asesmen dan Pelaporan</td>
                        <td colspan="2">Rekaman asesmen meliputi : form APL.01 sd MAK.07 dilaporkan kepada Bidang  sertifikasi LSP</td>
                      </tr>
                    </tbody>
                  </table>
                  <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <tbody>
                      <tr>
                        <td colspan="3">Konfirmasi dengan pihak yang relevan :</td>
                      </tr>
                      <tr>
                        <td class="text-center">Nama</td>
                        <td class="text-center">jabatan</td>
                        <td class="text-center">paraf/tanggal</td>
                      </tr>
                      <tr>
                        <td>Didik Suryabuana, M.Kom.</td>
                        <td><strong>Kepala Bagian Sertifikasi LSP</strong></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Ardi Ariansyah, S.Kom.</td>
                        <td><strong>Kepala Bagian Mutu LSP</strong></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Irma Rohima, S.Si.  Ketua LSP </td>
                        <td><strong>Ketua LSP</strong></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td rowspan="3">Penyusun Rencana dan Pengorganisasi Asesmen : Perangkat asesmen dapat/tidak dapat digunakan
                        </td>
                      </tr>
                      <tr>
                        <td><strong>Nama Asesor :</strong></td>
                        <td><strong>Juliana Mansur, S.Kom.</strong></td>
                      </tr>
                      <tr>
                        <td>No. Reg.</td>
                        <td>MET 000 008193 2016</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>Tanda tangan/Tanggal</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td rowspan="4"><strong>Diverifikasi oleh Kepala Bagian Sertifikasi LSP</strong></td>
                      </tr>
                      <tr>
                        <td><strong>Nama :</strong></td>
                        <td><strong>Didik Suryabuana, M.Kom.</strong></td>
                      </tr>
                      <tr>
                        <td>Jabatan :</td>
                        <td>Kepala Bagian Sertifikasi LSP</td>
                      </tr>
                      <tr>
                        <td>Tanda tangan/Tanggal</td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                  </div>
                </div>
              </div>
              <div class="form-group">                
              <button value="save" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>  
        </div> 
  	</div>
@endsection
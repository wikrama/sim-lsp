@extends('layouts.mimin')

@section('content')
	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
		<div class="container">						
			<div class="form-group">
				<h2 class="text-center" style="color: white;">Lembaga Sertifikasi Profesi</h2>
				<a href="{{URL('peserta-lsp')}}" class="btn ripple btn-outline btn-danger">
					<div>
						<span>Peserta LSP</span>
						<span class="ink animate"></span>
					</div>
				</a>
			</div>
              <div class="panel-body">
                <div class="col-md-12">
                    <h3 class="animated fadeInDown" style="color: white;">Alur Pelaksanaan Uji Kompetensi</h3>
                </div>
              </div>
			<div class="form-group">				
		        <div class="col-md-4">
		            <div class="panel box-v1">
		              <div class="panel-heading bg-white border-none">
		                <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
		                  <h4 class="text-left">01</h4>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
		                   <h4>
		                   <span class="fa fa-arrow-right text-right"></span>
		                   </h4>
		                </div>
		              </div>
		              <div class="panel-body text-center">
		                <h4>Mendaftar ke LSP</h4>
		                <p>Mengisi APL 01 & APL 02</p>
		                <hr/>
		              </div>
		            </div>
		        </div>
		        <div class="col-md-4">
		            <div class="panel box-v1">
		              <div class="panel-heading bg-white border-none">
		                <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
		                  <h4 class="text-left">02</h4>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
		                   <h4>
		                   <span class="fa fa-arrow-right text-right"></span>
		                   </h4>
		                </div>
		              </div>
		              <div class="panel-body text-center">
		                <h4>Konsultasi</h4>
		                <p>Pra-Asesmen</p>
		                <hr/>
		              </div>
		            </div>
		        </div>
		        <div class="col-md-4">
		            <div class="panel box-v1">
		              <div class="panel-heading bg-white border-none">
		                <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
		                  <h4 class="text-left">03</h4>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
		                   <h4>
		                   <span class="fa fa-arrow-right text-right"></span>
		                   </h4>
		                </div>
		              </div>
		              <div class="panel-body text-center">
		                <h4>Melaksanakan</h4>
		                <p>Asesmen</p>
		                <hr/>
		              </div>
		            </div>
		        </div>
		        <div class="col-md-4">
		            <div class="panel box-v1">
		              <div class="panel-heading bg-white border-none">
		                <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
		                  <h4 class="text-left">04</h4>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
		                   <h4>
		                   <span class="fa fa-arrow-right text-right"></span>
		                   </h4>
		                </div>
		              </div>
		              <div class="panel-body text-center">
		                <h4>Mencatat Bukti</h4>
		                <p>& Membuat Keputusan</p>
		                <hr/>
		              </div>
		            </div>
		        </div>
		        <div class="col-md-4">
		            <div class="panel box-v1">
		              <div class="panel-heading bg-white border-none">
		                <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
		                  <h4 class="text-left">05</h4>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
		                   <h4>
		                   <span class="fa fa-arrow-right text-right"></span>
		                   </h4>
		                </div>
		              </div>
		              <div class="panel-body text-center">
		                <h4>Memberikan</h4>
		                <p>keputusan pada peserta sertifikat</p>
		                <hr/>
		              </div>
		            </div>
		        </div>
		        <div class="col-md-4">
		            <div class="panel box-v1">
		              <div class="panel-heading bg-white border-none">
		                <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
		                  <h4 class="text-left">06</h4>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-6 text-right">		                	
		                   <h4>
		                   <span class="fa fa-arrow-right text-right"></span>
		                   </h4>
		                </div>
		              </div>
		              <div class="panel-body text-center">
		                <h4>Mengkaji Ulang</h4>
		                <p>Pelaksanaan asesmen</p>
		                <hr/>
		              </div>
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="panel box-v1">
		              <div class="panel-heading bg-white border-none">
		                <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
		                  <h4 class="text-left">07</h4>
		                </div>
		                <div class="col-md-6 col-sm-6 col-xs-6 text-right">		                	
		                   <h4>
		                   <span class="fa fa-arrow-right text-right"></span>
		                   </h4>
		                </div>
		              </div>
		              <div class="panel-body text-center">
		                <h4>Rapat Pleno</h4>
		                <p>Keputusan rapat sertifikat keluar</p>
		                <hr/>
		              </div>
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="panel box-v1">
		              <div class="panel-heading bg-white border-none">
		                <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
		                  <h4 class="text-left">08</h4>
		                </div>
		              </div>
		              <div class="panel-body text-center">
		                <h4>Penyerahan</h4>
		                <p>Sertifikat</p>
		                <hr/>
		              </div>
		            </div>
		        </div>
			</div>
		</div>	
	</div>
@endsection
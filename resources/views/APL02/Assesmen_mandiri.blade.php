@extends('layouts.mimin')

@section('content')
	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
	<div class="col-md-12" style="background-color: rgba(34, 49, 63, 0.8); height: 100%;">
		<div class="container">
        	<div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <form action="{{URL('/asesmen_mandiri/store')}}" method="post">
                        {{ csrf_field() }}
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-heading">
                            <h2>APL 02</h2>
                        </div>
                        <div class="panel-heading">
                            <h6>Pada bagian ini, anda diminta untuk menilai diri sendiri terhadap unit (unit-unit) kompetensi yang akan di-ases. <br> <br> <br>
                            1. Pelajari seluruh standar Kriteria Unjuk Kerja (KUK), batasan variabel, panduan penilaian dan aspek kritis serta yakinkan bahwa anda sudah benar-benar memahami seluruh isinya. <br> <br>
                            2. Laksanakan penilaian mandiri dengan mempelajari dan menilai kemampuan yang anda miliki secara obyektif terhadap seluruh daftar pertanyaan yang ada, serta tentukan apakah sudah kompeten (K) atau belum kompeten (BK) dengan mencantumkan tanda  dan tuliskan bukti-bukti pendukung yang anda anggap relevan terhadap setiap elemen/KUK unit kompetensi. <br> <br>
                            3. Asesor dan Peserta menandatangi form Asesmen Mandiri 
</h6>
                        </div>
                        <div class="panel-body">
                        	<div>
                        		<table width="100%">
                        				<tr>
                        					<td>
                        						Nama Peserta : <input required="" class="form-control border-bottom" type="text" name="nama_psrt">
                        					</td>
                        					<td>
                        						Tanggal/Waktu : <input required="" class="form-control border-bottom" type="datetime-local" name="tgl">
                        					</td>
                        				</tr>
                        				<tr>
                        					<td>
                        						Nama Asesor : <input required="" class="form-control border-bottom" type="text" name="nama_assesor">
                        					</td>
                        					<td>
                        						TUK : <input required="" class="form-control border-bottom" type="text" name="tuk">
                        					</td>
                        				</tr>

                        				<!-- 1 -->
                        		</table><br>
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Nomor Skema Sertifikasi</strong></td>
                        				<td><input type="text" name="no_skema" required="" class="form-control"></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Skema Sertifikasi</strong></td>
                        				<td><strong>Kualifikasi KKNI Level II Pada Kompetensi Keahlian Rekayasa Perangkat Lunak Klaster Pemrograman Dasar</strong></td>
                        			</tr>
                        		</table>
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>LOG.OO01.002.01</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Menerapkan prinsip-prinsip keselamatan dan kesehatan kerja di lingkungan kerja</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mengikuti praktek-praktek kerja yang aman</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Apakah anda dapat memperoleh, mengerti, dan menjelaskan Instruksi-instruksi tentang prosedur?</td>
                        				<td><input type="radio" name="1_1" value="K"></td>
                        				<td><input type="radio" name="1_1" value="BK"></td>
                        				<td><textarea class="form-control" name="1_1_alesan"></textarea></td>
                        				<td><input type="checkbox" name="1_1_asesi" value="V"></td>
                        				<td><input type="checkbox" name="1_1_asesi" value="A"></td>
                        				<td><input type="checkbox" name="1_1_asesi" value="T"></td>
                        				<td><input type="checkbox" name="1_1_asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Apakah anda dapat memperoleh, mengerti dan menjelaskan Spesifikasi yang relevan terhadap hasil-hasil tugas?</td>
                        				<td><input type="radio" name="1_2" value="K"></td>
                        				<td><input type="radio" name="1_2" value="BK"></td>
                        				<td><textarea class="form-control" name="1_2_alesan"></textarea></td>
                        				<td><input type="checkbox" name="1_2_asesi" value="V"></td>
                        				<td><input type="checkbox" name="1_2_asesi" value="A"></td>
                        				<td><input type="checkbox" name="1_2_asesi" value="T"></td>
                        				<td><input type="checkbox" name="1_2_asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.3</td>
                        				<td>Apakah anda dapat mengenali hasil-hasil tugas?</td>
                                                <td><input type="radio" name="1_3" value="K"></td>
                                                <td><input type="radio" name="1_3" value="BK"></td>
                                                <td><textarea class="form-control" name="1_3_alesan"></textarea></td>
                                                <td><input type="checkbox" name="1_3_asesi" value="V"></td>
                                                <td><input type="checkbox" name="1_3_asesi" value="A"></td>
                                                <td><input type="checkbox" name="1_3_asesi" value="T"></td>
                                                <td><input type="checkbox" name="1_3_asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.4</td>
                        				<td>Apakah anda dapat mengenali Syarat-syarat tugas seperti waktu penyelesaian dan ukuran kualitas?</td>
                                                <td><input type="radio" name="1_4" value="K"></td>
                                                <td><input type="radio" name="1_4" value="BK"></td>
                                                <td><textarea class="form-control" name="1_4_alesan"></textarea></td>
                                                <td><input type="checkbox" name="1_4_asesi" value="V"></td>
                                                <td><input type="checkbox" name="1_4_asesi" value="A"></td>
                                                <td><input type="checkbox" name="1_4_asesi" value="T"></td>
                                                <td><input type="checkbox" name="1_4_asesi" value="M"></td>
                        			</tr>
                                          <tr>
                                                <td align="center">1.5</td>
                                                <td>Apakah anda dapat menggunakan semua perlengkapan
                                                dan alat-alat keselamatan sesuai dengan Persyaratan perundang-undangan dan prosedur Perusahaan?</td>
                                                <td><input type="radio" name="1_5" value="K"></td>
                                                <td><input type="radio" name="1_5" value="BK"></td>
                                                <td><textarea class="form-control" name="1_5_alesan"></textarea></td>
                                                <td><input type="checkbox" name="1_5_asesi" value="V"></td>
                                                <td><input type="checkbox" name="1_5_asesi" value="A"></td>
                                                <td><input type="checkbox" name="1_5_asesi" value="T"></td>
                                                <td><input type="checkbox" name="1_5_asesi" value="M"></td>
                                          </tr>
                                          <tr>
                                                <td align="center">1.6</td>
                                                <td>Apakah anda dapat mengenal dan mengikuti Tanda-tanda/simbol sesuai instruksi?</td>
                                                <td><input type="radio" name="1_6" value="K"></td>
                                                <td><input type="radio" name="1_6" value="BK"></td>
                                                <td><textarea class="form-control" name="1_6_alesan"></textarea></td>
                                                <td><input type="checkbox" name="1_6_asesi" value="V"></td>
                                                <td><input type="checkbox" name="1_6_asesi" value="A"></td>
                                                <td><input type="checkbox" name="1_6_asesi" value="T"></td>
                                                <td><input type="checkbox" name="1_6_asesi" value="M"></td>
                                          </tr>
                                          <tr>
                                                <td align="center">1.7</td>
                                                <td>  Apakah anda dapat melaksanakan semua pedoman
                                                      penanganan sesuai dengan persyaratan, prosedur
                                                       perusahaan dan pedoman Komisi Kesehatan dan
                                                      Keselamatan Kerja Nasional yang sah.?</td>
                                                <td><input type="radio" name="1_7" value="K"></td>
                                                <td><input type="radio" name="1_7" value="BK"></td>
                                                <td><textarea class="form-control" name="1_7_alesan"></textarea></td>
                                                <td><input type="checkbox" name="1_7_asesi" value="V"></td>
                                                <td><input type="checkbox" name="1_7_asesi" value="A"></td>
                                                <td><input type="checkbox" name="1_7_asesi" value="T"></td>
                                                <td><input type="checkbox" name="1_7_asesi" value="M"></td>
                                          </tr>
                                          <tr>
                                                <td align="center">1.8</td>
                                                <td>Apakah anda dapat mengenal dan mendemonstrasikan
                                                perlengkapan darurat dengan tepat?</td>
                                                <td><input type="radio" name="1_8" value="K"></td>
                                                <td><input type="radio" name="1_8" value="BK"></td>
                                                <td><textarea class="form-control" name="1_8_alesan"></textarea></td>
                                                <td><input type="checkbox" name="1_8_asesi" value="V"></td>
                                                <td><input type="checkbox" name="1_8_asesi" value="A"></td>
                                                <td><input type="checkbox" name="1_8_asesi" value="T"></td>
                                                <td><input type="checkbox" name="1_8_asesi" value="M"></td>
                                          </tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Merencanakan langkah-langkah yang dibutuhkan untuk menyelesaikan tugas</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
                        				<td>Apakah anda dapat mengenali dan melaporkan bahaya-
											bahaya di tempat kerja selama waktu kerja kepada
											orang yang tepat sesuai dengan prosedur
											pengoperasian standar?
										</td>
                                                <td><input type="radio" name="2_1" value="K"></td>
                                                <td><input type="radio" name="2_1" value="BK"></td>
                                                <td><textarea class="form-control" name="2_1_alesan"></textarea></td>
                                                <td><input type="checkbox" name="2_1_asesi" value="V"></td>
                                                <td><input type="checkbox" name="2_1_asesi" value="A"></td>
                                                <td><input type="checkbox" name="2_1_asesi" value="T"></td>
                                                <td><input type="checkbox" name="2_1_asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>3. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Mengikuti prosedur-prosedur darurat</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.1</td>
                        				<td>Apakah anda dapat mendemonstrasikan cara-cara
											menghubungi personil yang tepat dan layanan darurat
											jika terjadi kecelakaan ?
										</td>
                                                <td><input type="radio" name="3_1" value="K"></td>
                                                <td><input type="radio" name="3_1" value="BK"></td>
                                                <td><textarea class="form-control" name="3_1_alesan"></textarea></td>
                                                <td><input type="checkbox" name="3_1_asesi" value="V"></td>
                                                <td><input type="checkbox" name="3_1_asesi" value="A"></td>
                                                <td><input type="checkbox" name="3_1_asesi" value="T"></td>
                                                <td><input type="checkbox" name="3_1_asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.2</td>
                        				<td>Apakah anda dapat mengerti dan melaksanakan bila
											diperlukan prosedur kondisi darurat dan evakuasi
											(pengungsian)?
										</td>
                                                <td><input type="radio" name="3_2" value="K"></td>
                                                <td><input type="radio" name="3_2" value="BK"></td>
                                                <td><textarea class="form-control" name="3_2_alesan"></textarea></td>
                                                <td><input type="checkbox" name="3_2_asesi" value="V"></td>
                                                <td><input type="checkbox" name="3_2_asesi" value="A"></td>
                                                <td><input type="checkbox" name="3_2_asesi" value="T"></td>
                                                <td><input type="checkbox" name="3_2_asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>
	</div>


	<!-- 2 -->
	<div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>LOG.OO01.004.01</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Merencanakan Tugas Rutin</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mengenali Persyaratan Tugas</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Apakah anda dapat memperoleh, mengerti, dan menjelaskan Instruksi-instruksi tentang prosedur?</td>
                                                <td><input type="radio" name="1-1" value="K"></td>
                                                <td><input type="radio" name="1-1" value="BK"></td>
                                                <td><textarea class="form-control" name="1-1-alesan"></textarea></td>
                                                <td><input type="checkbox" name="1-1-asesi" value="V"></td>
                                                <td><input type="checkbox" name="1-1-asesi" value="A"></td>
                                                <td><input type="checkbox" name="1-1-asesi" value="T"></td>
                                                <td><input type="checkbox" name="1-1-asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Apakah anda dapat memperoleh, mengerti dan
											menjelaskan Spesifikasi yang relevan terhadap hasil-
											hasil tugas?</td>
                                                <td><input type="radio" name="1-2" value="K"></td>
                                                <td><input type="radio" name="1-2" value="BK"></td>
                                                <td><textarea class="form-control" name="1-2-alesan"></textarea></td>
                                                <td><input type="checkbox" name="1-2-asesi" value="V"></td>
                                                <td><input type="checkbox" name="1-2-asesi" value="A"></td>
                                                <td><input type="checkbox" name="1-2-asesi" value="T"></td>
                                                <td><input type="checkbox" name="1-2-asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.3</td>
                        				<td>Apakah anda dapat mengenali hasil-hasil tugas?</td>
                                                <td><input type="radio" name="1-3" value="K"></td>
                                                <td><input type="radio" name="1-3" value="BK"></td>
                                                <td><textarea class="form-control" name="1-3-alesan"></textarea></td>
                                                <td><input type="checkbox" name="1-3-asesi" value="V"></td>
                                                <td><input type="checkbox" name="1-3-asesi" value="A"></td>
                                                <td><input type="checkbox" name="1-3-asesi" value="T"></td>
                                                <td><input type="checkbox" name="1-3-asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.4</td>
                        				<td>Apakah anda dapat mengenali Syarat-syarat tugas seperti waktu penyelesaian dan ukuran kualitas?</td>
                                                <td><input type="radio" name="1-4" value="K"></td>
                                                <td><input type="radio" name="1-4" value="BK"></td>
                                                <td><textarea class="form-control" name="1-4-alesan"></textarea></td>
                                                <td><input type="checkbox" name="1-4-asesi" value="V"></td>
                                                <td><input type="checkbox" name="1-4-asesi" value="A"></td>
                                                <td><input type="checkbox" name="1-4-asesi" value="T"></td>
                                                <td><input type="checkbox" name="1-4-asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Merencanakan langkah-langkah yang dibutuhkan untuk menyelesaikan tugas</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
                        				<td>Apakah anda dapat menjelaskan Berdasarkan instruksi-
											instruksi dan spesifikasi-spesifikasi yang ada, langkah-
											langkah atau kegiatan-kegiatan individu yang
											diperlukan untuk melaksanakan tugas?
										</td>
                                                <td><input type="radio" name="2-1" value="K"></td>
                                                <td><input type="radio" name="2-1" value="BK"></td>
                                                <td><textarea class="form-control" name="2-1-alesan"></textarea></td>
                                                <td><input type="checkbox" name="2-1-asesi" value="V"></td>
                                                <td><input type="checkbox" name="2-1-asesi" value="A"></td>
                                                <td><input type="checkbox" name="2-1-asesi" value="T"></td>
                                                <td><input type="checkbox" name="2-1-asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.2</td>
                        				<td>Apakah anda dapat mencantumkan Rangkaian kegiatan
											yang perlu diselesaikan dalam rencana?
										</td>
                                                <td><input type="radio" name="2-2" value="K"></td>
                                                <td><input type="radio" name="2-2" value="BK"></td>
                                                <td><textarea class="form-control" name="2-2-alesan"></textarea></td>
                                                <td><input type="checkbox" name="2-2-asesi" value="V"></td>
                                                <td><input type="checkbox" name="2-2-asesi" value="A"></td>
                                                <td><input type="checkbox" name="2-2-asesi" value="T"></td>
                                                <td><input type="checkbox" name="2-2-asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.3</td>
                        				<td>Apakah anda dapat memeriksa Langkah-langkah dan
											hasil yang direncanakan untuk menjamin kesesuaian
											dengan instruksi-instruksi dan spesifikasi-spesifikasi
											yang relevan?
										</td>
                                                <td><input type="radio" name="2-3" value="K"></td>
                                                <td><input type="radio" name="2-3" value="BK"></td>
                                                <td><textarea class="form-control" name="2-3-alesan"></textarea></td>
                                                <td><input type="checkbox" name="2-3-asesi" value="V"></td>
                                                <td><input type="checkbox" name="2-3-asesi" value="A"></td>
                                                <td><input type="checkbox" name="2-3-asesi" value="T"></td>
                                                <td><input type="checkbox" name="2-3-asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>3. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Mengulas rencana</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.1</td>
                        				<td>Apakah anda dapat mengenali dan membandingkan
											Hasil-hasil dengan sasaran-sasaran (yang direncanakan)
											instruksi-instruksi tugas, spesifikasi-spesifikasi dan
											syarat-syarat tugas?
										</td>
                                                <td><input type="radio" name="3-1" value="K"></td>
                                                <td><input type="radio" name="3-1" value="BK"></td>
                                                <td><textarea class="form-control" name="3-1-alesan"></textarea></td>
                                                <td><input type="checkbox" name="3-1-asesi" value="V"></td>
                                                <td><input type="checkbox" name="3-1-asesi" value="A"></td>
                                                <td><input type="checkbox" name="3-1-asesi" value="T"></td>
                                                <td><input type="checkbox" name="3-1-asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.2</td>
                        				<td>Apakah anda dapat memperbaiki Jika perlu, rencana
											untuk memenuhi sasaran-sasaran dan syarat-syarat
											tugas yang lebih baik?
										</td>
                                                <td><input type="radio" name="3-2" value="K"></td>
                                                <td><input type="radio" name="3-2" value="BK"></td>
                                                <td><textarea class="form-control" name="3-2-alesan"></textarea></td>
                                                <td><input type="checkbox" name="3-2-asesi" value="V"></td>
                                                <td><input type="checkbox" name="3-2-asesi" value="A"></td>
                                                <td><input type="checkbox" name="3-2-asesi" value="T"></td>
                                                <td><input type="checkbox" name="3-2-asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>

	<!-- 3 -->
	<div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>TIK.OP01.002.01</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mengidentifikasi aspek kode etik dan HAKI dibidang TIK</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mengidentifikasi kode etik yang berlaku di dunia TIK</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Norma yang berlaku di dunia TIK dapat diidentifikasi</td>
                                                <td><input type="radio" name="1+1" value="K"></td>
                                                <td><input type="radio" name="1+1" value="BK"></td>
                                                <td><textarea class="form-control" name="1+1+alesan"></textarea></td>
                                                <td><input type="checkbox" name="1+1+asesi" value="V"></td>
                                                <td><input type="checkbox" name="1+1+asesi" value="A"></td>
                                                <td><input type="checkbox" name="1+1+asesi" value="T"></td>
                                                <td><input type="checkbox" name="1+1+asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Aspek legal atas dokumen elektronik hasil karya orang
                        					lain dapat dipahami</td>
                                                <td><input type="radio" name="1+2" value="K"></td>
                                                <td><input type="radio" name="1+2" value="BK"></td>
                                                <td><textarea class="form-control" name="1+2+alesan"></textarea></td>
                                                <td><input type="checkbox" name="1+2+asesi" value="V"></td>
                                                <td><input type="checkbox" name="1+2+asesi" value="A"></td>
                                                <td><input type="checkbox" name="1+2+asesi" value="T"></td>
                                                <td><input type="checkbox" name="1+2+asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Mengidentifikasi hal-hal yang berkaitan dengan HAKI di dunia TIK</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
                        				<td>Apakah anda dapat memahami Copyright piranti lunak
											dan isu hukum berkaitan dengan menggandakan dan
											membagi file dapat?
										</td>
                                                <td><input type="radio" name="2+1" value="K"></td>
                                                <td><input type="radio" name="2+1" value="BK"></td>
                                                <td><textarea class="form-control" name="2+1+alesan"></textarea></td>
                                                <td><input type="checkbox" name="2+1+asesi" value="V"></td>
                                                <td><input type="checkbox" name="2+1+asesi" value="A"></td>
                                                <td><input type="checkbox" name="2+1+asesi" value="T"></td>
                                                <td><input type="checkbox" name="2+1+asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.2</td>
                        				<td>Apakah anda dapat memahami Akibat yang dapat
											terjadi jika bertukar file pada suatu jaringan internet ?
										</td>
                                                <td><input type="radio" name="2+2" value="K"></td>
                                                <td><input type="radio" name="2+2" value="BK"></td>
                                                <td><textarea class="form-control" name="2+2+alesan"></textarea></td>
                                                <td><input type="checkbox" name="2+2+asesi" value="V"></td>
                                                <td><input type="checkbox" name="2+2+asesi" value="A"></td>
                                                <td><input type="checkbox" name="2+2+asesi" value="T"></td>
                                                <td><input type="checkbox" name="2+2+asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.3</td>
                        				<td>Apakah anda dapat mengenal dan memahami Istilah-
											istilah shareware, freeware dan user license ?
										</td>
                                                <td><input type="radio" name="2+3" value="K"></td>
                                                <td><input type="radio" name="2+3" value="BK"></td>
                                                <td><textarea class="form-control" name="2+3+alesan"></textarea></td>
                                                <td><input type="checkbox" name="2+3+asesi" value="V"></td>
                                                <td><input type="checkbox" name="2+3+asesi" value="A"></td>
                                                <td><input type="checkbox" name="2+3+asesi" value="T"></td>
                                                <td><input type="checkbox" name="2+3+asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>

	<!-- 4 -->
	<div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>J.620100.004.02</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Menggunakan Struktur Data</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mengidentifikasi konsep data dan struktur data</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Apakah anda dapat mengidentifikasi konsep data dan
                        					struktur data sesuai dengan konteks permasalahan</td>
                                                <td><input type="radio" name="1=1" value="K"></td>
                                                <td><input type="radio" name="1=1" value="BK"></td>
                                                <td><textarea class="form-control" name="1=1=alesan"></textarea></td>
                                                <td><input type="checkbox" name="1=1=asesi" value="V"></td>
                                                <td><input type="checkbox" name="1=1=asesi" value="A"></td>
                                                <td><input type="checkbox" name="1=1=asesi" value="T"></td>
                                                <td><input type="checkbox" name="1=1=asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Apakah anda dapat membandingkan alternatif struktur data kelebihan dan kekurangannya untuk konteks permasalahan yang diselesaikan?</td>
                                                <td><input type="radio" name="1=2" value="K"></td>
                                                <td><input type="radio" name="1=2" value="BK"></td>
                                                <td><textarea class="form-control" name="1=2=alesan"></textarea></td>
                                                <td><input type="checkbox" name="1=2=asesi" value="V"></td>
                                                <td><input type="checkbox" name="1=2=asesi" value="A"></td>
                                                <td><input type="checkbox" name="1=2=asesi" value="T"></td>
                                                <td><input type="checkbox" name="1=2=asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Menerapkan struktur data dan akses terhadap struktur data tersebut</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
                        				<td>Apakah Anda dapat mengimplementasikan Struktur
											data sesuai dengan bahasa pemrograman yang akan
											dipergunakan
										</td>
                                                <td><input type="radio" name="2=1" value="K"></td>
                                                <td><input type="radio" name="2=1" value="BK"></td>
                                                <td><textarea class="form-control" name="2=1=alesan"></textarea></td>
                                                <td><input type="checkbox" name="2=1=asesi" value="V"></td>
                                                <td><input type="checkbox" name="2=1=asesi" value="A"></td>
                                                <td><input type="checkbox" name="2=1=asesi" value="T"></td>
                                                <td><input type="checkbox" name="2=1=asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.2</td>
                        				<td>Apakah anda dapat menyatakan akses terhadap data
											dalam algoritma yang efisiensi sesuai bahasa
											pemrograman yang akan dipakai ?
										</td>
                                                <td><input type="radio" name="2=2" value="K"></td>
                                                <td><input type="radio" name="2=2" value="BK"></td>
                                                <td><textarea class="form-control" name="2=2=alesan"></textarea></td>
                                                <td><input type="checkbox" name="2=2=asesi" value="V"></td>
                                                <td><input type="checkbox" name="2=2=asesi" value="A"></td>
                                                <td><input type="checkbox" name="2=2=asesi" value="T"></td>
                                                <td><input type="checkbox" name="2=2=asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>

	<!-- 5 -->
	<div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>J.620100.005.01</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mengimplementasikan User Interface</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mengidentifikasi rancangan user interface</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Apakah anda dapat mengidentifikasi rancangan user interface diidentifikasi sesuai kebutuhan</td>
                                                <td><input type="radio" name="1)1" value="K"></td>
                                                <td><input type="radio" name="1)1" value="BK"></td>
                                                <td><textarea class="form-control" name="1)1)alesan"></textarea></td>
                                                <td><input type="checkbox" name="1)1)asesi" value="V"></td>
                                                <td><input type="checkbox" name="1)1)asesi" value="A"></td>
                                                <td><input type="checkbox" name="1)1)asesi" value="T"></td>
                                                <td><input type="checkbox" name="1)1)asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Apakah anda dapat mengidentifikasi komponen user interface dialog sesuai konteks rancangan proses</td>
                                                <td><input type="radio" name="1)2" value="K"></td>
                                                <td><input type="radio" name="1)2" value="BK"></td>
                                                <td><textarea class="form-control" name="1)2)alesan"></textarea></td>
                                                <td><input type="checkbox" name="1)2)asesi" value="V"></td>
                                                <td><input type="checkbox" name="1)2)asesi" value="A"></td>
                                                <td><input type="checkbox" name="1)2)asesi" value="T"></td>
                                                <td><input type="checkbox" name="1)2)asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.3</td>
                        				<td>Apakah anda dapat menjelaskan urutan dari akses komponen user interface dialog?</td>
                                                <td><input type="radio" name="1)3" value="K"></td>
                                                <td><input type="radio" name="1)3" value="BK"></td>
                                                <td><textarea class="form-control" name="1)3)alesan"></textarea></td>
                                                <td><input type="checkbox" name="1)3)asesi" value="V"></td>
                                                <td><input type="checkbox" name="1)3)asesi" value="A"></td>
                                                <td><input type="checkbox" name="1)3)asesi" value="T"></td>
                                                <td><input type="checkbox" name="1)3)asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.4</td>
                        				<td>Apakah anda dapat membuat simulasi (mock-up) dari aplikasi yang akan dikembangkan</td>
                                                <td><input type="radio" name="1)4" value="K"></td>
                                                <td><input type="radio" name="1)4" value="BK"></td>
                                                <td><textarea class="form-control" name="1)4)alesan"></textarea></td>
                                                <td><input type="checkbox" name="1)4)asesi" value="V"></td>
                                                <td><input type="checkbox" name="1)4)asesi" value="A"></td>
                                                <td><input type="checkbox" name="1)4)asesi" value="T"></td>
                                                <td><input type="checkbox" name="1)4)asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Melakukan implementasi rancangan user interface</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
                        				<td>Apakah anda dapat menerapkan menu program sesuai
dengan rancangan program diterapkan
										</td>
                                                <td><input type="radio" name="2)1" value="K"></td>
                                                <td><input type="radio" name="2)1" value="BK"></td>
                                                <td><textarea class="form-control" name="2)1)alesan"></textarea></td>
                                                <td><input type="checkbox" name="2)1)asesi" value="V"></td>
                                                <td><input type="checkbox" name="2)1)asesi" value="A"></td>
                                                <td><input type="checkbox" name="2)1)asesi" value="T"></td>
                                                <td><input type="checkbox" name="2)1)asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.2</td>
                        				<td>Apakah anda dapat mengatur penempatan user
interface dialog secara sekuensial
										</td>
                                                <td><input type="radio" name="2)2" value="K"></td>
                                                <td><input type="radio" name="2)2" value="BK"></td>
                                                <td><textarea class="form-control" name="2)2)alesan"></textarea></td>
                                                <td><input type="checkbox" name="2)2)asesi" value="V"></td>
                                                <td><input type="checkbox" name="2)2)asesi" value="A"></td>
                                                <td><input type="checkbox" name="2)2)asesi" value="T"></td>
                                                <td><input type="checkbox" name="2)2)asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.3</td>
                        				<td>Apakah anda dapat menyesuaikan setting aktif-pasif
komponen user interface dialog dengan urutan alur
proses
										</td>
                                                <td><input type="radio" name="2)3" value="K"></td>
                                                <td><input type="radio" name="2)3" value="BK"></td>
                                                <td><textarea class="form-control" name="2)3)alesan"></textarea></td>
                                                <td><input type="checkbox" name="2)3)asesi" value="V"></td>
                                                <td><input type="checkbox" name="2)3)asesi" value="A"></td>
                                                <td><input type="checkbox" name="2)3)asesi" value="T"></td>
                                                <td><input type="checkbox" name="2)3)asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.4</td>
                        				<td>Apakah anda dapat menentukan bentuk style dari
komponen user interface?
										</td>
                                                <td><input type="radio" name="2)4" value="K"></td>
                                                <td><input type="radio" name="2)4" value="BK"></td>
                                                <td><textarea class="form-control" name="2)4)alesan"></textarea></td>
                                                <td><input type="checkbox" name="2)4)asesi" value="V"></td>
                                                <td><input type="checkbox" name="2)4)asesi" value="A"></td>
                                                <td><input type="checkbox" name="2)4)asesi" value="T"></td>
                                                <td><input type="checkbox" name="2)4)asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.5</td>
                        				<td>Apakah anda dapat menerapkan simulasi menjadi
suatu proses yang sesungguhnya?
										</td>
                                                <td><input type="radio" name="2)5" value="K"></td>
                                                <td><input type="radio" name="2)5" value="BK"></td>
                                                <td><textarea class="form-control" name="2)5)alesan"></textarea></td>
                                                <td><input type="checkbox" name="2)5)asesi" value="V"></td>
                                                <td><input type="checkbox" name="2)5)asesi" value="A"></td>
                                                <td><input type="checkbox" name="2)5)asesi" value="T"></td>
                                                <td><input type="checkbox" name="2)5)asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>

	<!-- 6 -->
	<div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>J.620100.011.01</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Melakukan Instalasi Software Tools Pemrograman</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Memilih tools pemrograman yang sesuai dengan kebutuhan</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Apakah anda dapat mengidentifikasi platform
(lingkungan) yang akan digunakan untuk menjalankan
tools pemrograman sesuai dengan kebutuhan ?</td>
                                                <td><input type="radio" name="1(1" value="K"></td>
                                                <td><input type="radio" name="1(1" value="BK"></td>
                                                <td><textarea class="form-control" name="1(1(alesan"></textarea></td>
                                                <td><input type="checkbox" name="1(1(asesi" value="V"></td>
                                                <td><input type="checkbox" name="1(1(asesi" value="A"></td>
                                                <td><input type="checkbox" name="1(1(asesi" value="T"></td>
                                                <td><input type="checkbox" name="1(1(asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Apakah anda dapat memilih tools bahasa pemrogram
sesuai dengan kebutuhaan dan lingkungan
pengembangan?</td>
                                                <td><input type="radio" name="1(2" value="K"></td>
                                                <td><input type="radio" name="1(2" value="BK"></td>
                                                <td><textarea class="form-control" name="1(2(alesan"></textarea></td>
                                                <td><input type="checkbox" name="1(2(asesi" value="V"></td>
                                                <td><input type="checkbox" name="1(2(asesi" value="A"></td>
                                                <td><input type="checkbox" name="1(2(asesi" value="T"></td>
                                                <td><input type="checkbox" name="1(2(asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Instalasi tool pemrograman</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
                        				<td>Apakah anda dapat menginstal tools pemrogaman
sesuai dengan prosedur?
										</td>
                                                <td><input type="radio" name="2(1" value="K"></td>
                                                <td><input type="radio" name="2(1" value="BK"></td>
                                                <td><textarea class="form-control" name="2(1(alesan"></textarea></td>
                                                <td><input type="checkbox" name="2(1(asesi" value="V"></td>
                                                <td><input type="checkbox" name="2(1(asesi" value="A"></td>
                                                <td><input type="checkbox" name="2(1(asesi" value="T"></td>
                                                <td><input type="checkbox" name="2(1(asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.2</td>
                        				<td>Apakah anda dapat menjalankan tools pemrograman
di lingkungan pengembangan yang telah ditetapkan?
										</td>
                                                <td><input type="radio" name="2(2" value="K"></td>
                                                <td><input type="radio" name="2(2" value="BK"></td>
                                                <td><textarea class="form-control" name="2(2(alesan"></textarea></td>
                                                <td><input type="checkbox" name="2(2(asesi" value="V"></td>
                                                <td><input type="checkbox" name="2(2(asesi" value="A"></td>
                                                <td><input type="checkbox" name="2(2(asesi" value="T"></td>
                                                <td><input type="checkbox" name="2(2(asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>3. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Menerapkan hasil pemodelan kedalam eksekusi script sederhana</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.1</td>
                        				<td>Apakah anda dapat membuat script (source code)
sederhana sesuai tools pemrogaman yang di-install?
										</td>
                                                <td><input type="radio" name="3(1" value="K"></td>
                                                <td><input type="radio" name="3(1" value="BK"></td>
                                                <td><textarea class="form-control" name="3(1(alesan"></textarea></td>
                                                <td><input type="checkbox" name="3(1(asesi" value="V"></td>
                                                <td><input type="checkbox" name="3(1(asesi" value="A"></td>
                                                <td><input type="checkbox" name="3(1(asesi" value="T"></td>
                                                <td><input type="checkbox" name="3(1(asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.2</td>
                        				<td>Apakah anda dapat menjalankan script dengan benar
dan menghasilkan keluaran sesuai scenario yang
diharapkan?
										</td>
                                                <td><input type="radio" name="3(2" value="K"></td>
                                                <td><input type="radio" name="3(2" value="BK"></td>
                                                <td><textarea class="form-control" name="3(2(alesan"></textarea></td>
                                                <td><input type="checkbox" name="3(2(asesi" value="V"></td>
                                                <td><input type="checkbox" name="3(2(asesi" value="A"></td>
                                                <td><input type="checkbox" name="3(2(asesi" value="T"></td>
                                                <td><input type="checkbox" name="3(2(asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>


	<!-- 7 -->
	<div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>J.620100.012.01</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Melakukan Pengaturan Software Tools Pemrograman</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Melakukan konfigurasi tools untuk pemrograman</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Apakah anda dapat menentukan target hasil dari
konfigurasi?</td>
                                                <td><input type="radio" name="1*1" value="K"></td>
                                                <td><input type="radio" name="1*1" value="BK"></td>
                                                <td><textarea class="form-control" name="1*1*alesan"></textarea></td>
                                                <td><input type="checkbox" name="1*1*asesi" value="V"></td>
                                                <td><input type="checkbox" name="1*1*asesi" value="A"></td>
                                                <td><input type="checkbox" name="1*1*asesi" value="T"></td>
                                                <td><input type="checkbox" name="1*1*asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Apakan anda dapat mengkonfigurasi tools
pemrograman, dengan tetap bisa digunakan
sebagaimana mestinya</td>
                                                <td><input type="radio" name="1*2" value="K"></td>
                                                <td><input type="radio" name="1*2" value="BK"></td>
                                                <td><textarea class="form-control" name="1*2*alesan"></textarea></td>
                                                <td><input type="checkbox" name="1*2*asesi" value="V"></td>
                                                <td><input type="checkbox" name="1*2*asesi" value="A"></td>
                                                <td><input type="checkbox" name="1*2*asesi" value="T"></td>
                                                <td><input type="checkbox" name="1*2*asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Menggunakan tools sesuai kebutuhan pembuatan program</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
                        				<td>Apakah anda dapat mengidentifikasi fitur-fitur dasar
yang dibutuhkan untuk mendukung pembuatan
program ?
										</td>
                                                <td><input type="radio" name="2*1" value="K"></td>
                                                <td><input type="radio" name="2*1" value="BK"></td>
                                                <td><textarea class="form-control" name="2*1*alesan"></textarea></td>
                                                <td><input type="checkbox" name="2*1*asesi" value="V"></td>
                                                <td><input type="checkbox" name="2*1*asesi" value="A"></td>
                                                <td><input type="checkbox" name="2*1*asesi" value="T"></td>
                                                <td><input type="checkbox" name="2*1*asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.2</td>
                        				<td>Apakah anda dapat menguasai fitur-fitur dasar tools
untuk pembuatan program dikuasai?
										</td>
                                                <td><input type="radio" name="2*2" value="K"></td>
                                                <td><input type="radio" name="2*2" value="BK"></td>
                                                <td><textarea class="form-control" name="2*2*alesan"></textarea></td>
                                                <td><input type="checkbox" name="2*2*asesi" value="V"></td>
                                                <td><input type="checkbox" name="2*2*asesi" value="A"></td>
                                                <td><input type="checkbox" name="2*2*asesi" value="T"></td>
                                                <td><input type="checkbox" name="2*2*asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>

	<!-- 8 -->
	<div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>J.620100.017.02</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mengimplementasikan Pemrograman Terstruktur</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Menggunakan tipe data dan control program</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Apakah anda dapat membuat source code dengan tipe
data sesuai kebutuhan?</td>
                                                <td><input type="radio" name="1&1" value="K"></td>
                                                <td><input type="radio" name="1&1" value="BK"></td>
                                                <td><textarea class="form-control" name="1&1&alesan"></textarea></td>
                                                <td><input type="checkbox" name="1&1&asesi" value="V"></td>
                                                <td><input type="checkbox" name="1&1&asesi" value="A"></td>
                                                <td><input type="checkbox" name="1&1&asesi" value="T"></td>
                                                <td><input type="checkbox" name="1&1&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Apakah anda dapat membuat source code dengan
sintax yang benar?</td>
                                                <td><input type="radio" name="1&2" value="K"></td>
                                                <td><input type="radio" name="1&2" value="BK"></td>
                                                <td><textarea class="form-control" name="1&2&alesan"></textarea></td>
                                                <td><input type="checkbox" name="1&2&asesi" value="V"></td>
                                                <td><input type="checkbox" name="1&2&asesi" value="A"></td>
                                                <td><input type="checkbox" name="1&2&asesi" value="T"></td>
                                                <td><input type="checkbox" name="1&2&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.3</td>
                        				<td>Apakah anda dapat membuat coding program
menggunakan struktur kontrol?</td>
                                                <td><input type="radio" name="1&3" value="K"></td>
                                                <td><input type="radio" name="1&3" value="BK"></td>
                                                <td><textarea class="form-control" name="1&3&alesan"></textarea></td>
                                                <td><input type="checkbox" name="1&3&asesi" value="V"></td>
                                                <td><input type="checkbox" name="1&3&asesi" value="A"></td>
                                                <td><input type="checkbox" name="1&3&asesi" value="T"></td>
                                                <td><input type="checkbox" name="1&3&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Membuat program sederhana</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
                        				<td>Apakah anda dapat membuat aplikasi sederhana
dengan input user dan output hasil pengolahan data?
										</td>
                                                <td><input type="radio" name="2&1" value="K"></td>
                                                <td><input type="radio" name="2&1" value="BK"></td>
                                                <td><textarea class="form-control" name="2&1&alesan"></textarea></td>
                                                <td><input type="checkbox" name="2&1&asesi" value="V"></td>
                                                <td><input type="checkbox" name="2&1&asesi" value="A"></td>
                                                <td><input type="checkbox" name="2&1&asesi" value="T"></td>
                                                <td><input type="checkbox" name="2&1&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.2</td>
                        				<td>Apakah anda dapat membuat code program
menggunakan struktur kontrol dan pengulangan?
										</td>
                                                <td><input type="radio" name="2&2" value="K"></td>
                                                <td><input type="radio" name="2&2" value="BK"></td>
                                                <td><textarea class="form-control" name="2&2&alesan"></textarea></td>
                                                <td><input type="checkbox" name="2&2%asesi" value="V"></td>
                                                <td><input type="checkbox" name="2&2%asesi" value="A"></td>
                                                <td><input type="checkbox" name="2&2%asesi" value="T"></td>
                                                <td><input type="checkbox" name="2&2%asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>3. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Membuat program menggunakan prosedur dan fungsi</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.1</td>
                        				<td>Apakah anda dapat membuat code program
menggunakan procedure?
										</td>
                                                <td><input type="radio" name="3&1" value="K"></td>
                                                <td><input type="radio" name="3&1" value="BK"></td>
                                                <td><textarea class="form-control" name="3&1&alesan"></textarea></td>
                                                <td><input type="checkbox" name="3&1&sesi" value="V"></td>
                                                <td><input type="checkbox" name="3&1&sesi" value="A"></td>
                                                <td><input type="checkbox" name="3&1&sesi" value="T"></td>
                                                <td><input type="checkbox" name="3&1&sesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.2</td>
                        				<td>Apakah anda dapat membuat code program
menggunakan function?
										</td>
                                                <td><input type="radio" name="3&2" value="K"></td>
                                                <td><input type="radio" name="3&2" value="BK"></td>
                                                <td><textarea class="form-control" name="3&2&alesan"></textarea></td>
                                                <td><input type="checkbox" name="3&2&asesi" value="V"></td>
                                                <td><input type="checkbox" name="3&2&asesi" value="A"></td>
                                                <td><input type="checkbox" name="3&2&asesi" value="T"></td>
                                                <td><input type="checkbox" name="3&2&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.3</td>
                        				<td>Apakah anda dapat membuat code program
menggunakan function dan procedure dalam sebuah
proses?
										</td>
                                                <td><input type="radio" name="3&3" value="K"></td>
                                                <td><input type="radio" name="3&3" value="BK"></td>
                                                <td><textarea class="form-control" name="3&3&alesan"></textarea></td>
                                                <td><input type="checkbox" name="3&3&asesi" value="V"></td>
                                                <td><input type="checkbox" name="3&3&asesi" value="A"></td>
                                                <td><input type="checkbox" name="3&3&asesi" value="T"></td>
                                                <td><input type="checkbox" name="3&3&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>4. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Membuat program menggunakan prosedur dan fungsi</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">4.1</td>
                        				<td>Apakah anda dapat membuat code program
menggunakan minimal array 1 dimensi?
										</td>
                                                <td><input type="radio" name="4&1" value="K"></td>
                                                <td><input type="radio" name="4&1" value="BK"></td>
                                                <td><textarea class="form-control" name="4&1&alesan"></textarea></td>
                                                <td><input type="checkbox" name="4&1&asesi" value="V"></td>
                                                <td><input type="checkbox" name="4&1&asesi" value="A"></td>
                                                <td><input type="checkbox" name="4&1&asesi" value="T"></td>
                                                <td><input type="checkbox" name="4&1&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">4.2</td>
                        				<td>Apakah anda dapat membuat code program
menggunakan salah satu tipe data?
										</td>
                                                <td><input type="radio" name="4&2" value="K"></td>
                                                <td><input type="radio" name="4&2" value="BK"></td>
                                                <td><textarea class="form-control" name="4&2&alesan"></textarea></td>
                                                <td><input type="checkbox" name="4&2&asesi" value="V"></td>
                                                <td><input type="checkbox" name="4&2&asesi" value="A"></td>
                                                <td><input type="checkbox" name="4&2&asesi" value="T"></td>
                                                <td><input type="checkbox" name="4&2&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">4.3</td>
                        				<td>AApakah anda dapat membuat array dengan panjang
data sesuai kebutuhan?
										</td>
                                                <td><input type="radio" name="4&3" value="K"></td>
                                                <td><input type="radio" name="4&3" value="BK"></td>
                                                <td><textarea class="form-control" name="4&3&alesan"></textarea></td>
                                                <td><input type="checkbox" name="4&3&asesi" value="V"></td>
                                                <td><input type="checkbox" name="4&3&asesi" value="A"></td>
                                                <td><input type="checkbox" name="4&3&asesi" value="T"></td>
                                                <td><input type="checkbox" name="4&3&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">4.4</td>
                        				<td>Apakah anda dapat membuat pengurutan data
menggunakan array?
										</td>
                                                <td><input type="radio" name="4&4" value="K"></td>
                                                <td><input type="radio" name="4&4" value="BK"></td>
                                                <td><textarea class="form-control" name="4&4&alesan"></textarea></td>
                                                <td><input type="checkbox" name="4&4&asesi" value="V"></td>
                                                <td><input type="checkbox" name="4&4&asesi" value="A"></td>
                                                <td><input type="checkbox" name="4&4&asesi" value="T"></td>
                                                <td><input type="checkbox" name="4&4&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>5. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Membuat program menggunakan prosedur dan fungsi</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">5.1</td>
                        				<td>Apakah anda dapat menyimpan data hasil aplikasi
kedalam file txt?
										</td>
                                                <td><input type="radio" name="5&1" value="K"></td>
                                                <td><input type="radio" name="5&1" value="BK"></td>
                                                <td><textarea class="form-control" name="5&1&alesan"></textarea></td>
                                                <td><input type="checkbox" name="5&1&asesi" value="V"></td>
                                                <td><input type="checkbox" name="5&1&asesi" value="A"></td>
                                                <td><input type="checkbox" name="5&1&asesi" value="T"></td>
                                                <td><input type="checkbox" name="5&1&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">5.2</td>
                        				<td>Apakah anda dapat membuka/mengambil data dari
file txt?
										</td>
                                                <td><input type="radio" name="5&2" value="K"></td>
                                                <td><input type="radio" name="5&2" value="BK"></td>
                                                <td><textarea class="form-control" name="5&2&alesan"></textarea></td>
                                                <td><input type="checkbox" name="5&2&asesi" value="V"></td>
                                                <td><input type="checkbox" name="5&2&asesi" value="A"></td>
                                                <td><input type="checkbox" name="5&2&asesi" value="T"></td>
                                                <td><input type="checkbox" name="5&2&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>6. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Membuat program menggunakan prosedur dan fungsi</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">6.1</td>
                        				<td>Apakah anda dapat membuat penanganan error pada
source code program?
										</td>
                                                <td><input type="radio" name="6&1" value="K"></td>
                                                <td><input type="radio" name="6&1" value="BK"></td>
                                                <td><textarea class="form-control" name="6&1&alesan"></textarea></td>
                                                <td><input type="checkbox" name="6&1&asesi" value="V"></td>
                                                <td><input type="checkbox" name="6&1&asesi" value="A"></td>
                                                <td><input type="checkbox" name="6&1&asesi" value="T"></td>
                                                <td><input type="checkbox" name="6&1&asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">6.2</td>
                        				<td>Apakah anda dapat membuat penanganan error untuk
kesalahan penulisan syntax?
										</td>
                                                <td><input type="radio" name="6&2" value="K"></td>
                                                <td><input type="radio" name="6&2" value="BK"></td>
                                                <td><textarea class="form-control" name="6&2&alesan"></textarea></td>
                                                <td><input type="checkbox" name="6&2&asesi" value="V"></td>
                                                <td><input type="checkbox" name="6&2&asesi" value="A"></td>
                                                <td><input type="checkbox" name="6&2&asesi" value="T"></td>
                                                <td><input type="checkbox" name="6&2&asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>

	<!-- 9 -->
	<div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>J.620100.022.02</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mengimplementasikan Algoritma Pemrograman</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Menjelaskan varian dan invarian</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Apakah anda dapat menjelaskan Tipe data telah sesuai
kaidah pemrograman?</td>
                                                <td><input type="radio" name="1^1" value="K"></td>
                                                <td><input type="radio" name="1^1" value="BK"></td>
                                                <td><textarea class="form-control" name="1^1^alesan"></textarea></td>
                                                <td><input type="checkbox" name="1^1^asesi" value="V"></td>
                                                <td><input type="checkbox" name="1^1^asesi" value="A"></td>
                                                <td><input type="checkbox" name="1^1^asesi" value="T"></td>
                                                <td><input type="checkbox" name="1^1^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Apakah anda dapat menjelaskan Variabel sesuai
kaidah pemrograman?</td>
                                                <td><input type="radio" name="1^2" value="K"></td>
                                                <td><input type="radio" name="1^2" value="BK"></td>
                                                <td><textarea class="form-control" name="1^2^alesan"></textarea></td>
                                                <td><input type="checkbox" name="1^2^asesi" value="V"></td>
                                                <td><input type="checkbox" name="1^2^asesi" value="A"></td>
                                                <td><input type="checkbox" name="1^2^asesi" value="T"></td>
                                                <td><input type="checkbox" name="1^2^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.3</td>
                        				<td>Apakah anda dapat menjelaskan Konstanta sesuai
kaidah pemrograman?</td>
                        				 <td><input type="radio" name="1^3" value="K"></td>
                                                <td><input type="radio" name="1^3" value="BK"></td>
                                                <td><textarea class="form-control" name="1^3^alesan"></textarea></td>
                                                <td><input type="checkbox" name="1^3^asesi" value="V"></td>
                                                <td><input type="checkbox" name="1^3^asesi" value="A"></td>
                                                <td><input type="checkbox" name="1^3^asesi" value="T"></td>
                                                <td><input type="checkbox" name="1^3^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Membuat alur logika pemrograman</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
<td>Apakah anda dapat menentukan Metode yang sesuai ?</td>
                        				 <td><input type="radio" name="2^1" value="K"></td>
                                                <td><input type="radio" name="2^1" value="BK"></td>
                                                <td><textarea class="form-control" name="2^1^alesan"></textarea></td>
                                                <td><input type="checkbox" name="2^1^asesi" value="V"></td>
                                                <td><input type="checkbox" name="2^1^asesi" value="A"></td>
                                                <td><input type="checkbox" name="2^1^asesi" value="T"></td>
                                                <td><input type="checkbox" name="2^1^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.2</td>
<td>Apakah anda dapat menentukan Komponen yang dibutuhkan?</td>
								 <td><input type="radio" name="2^2" value="K"></td>
                                                <td><input type="radio" name="2^2" value="BK"></td>
                                                <td><textarea class="form-control" name="2^2^alesan"></textarea></td>
                                                <td><input type="checkbox" name="2^2^asesi" value="V"></td>
                                                <td><input type="checkbox" name="2^2^asesi" value="A"></td>
                                                <td><input type="checkbox" name="2^2^asesi" value="T"></td>
                                                <td><input type="checkbox" name="2^2^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.3</td>
<td>Apakah anda dapat menetapkan Relasi antar komponen ?</td>
								 <td><input type="radio" name="2^3" value="K"></td>
                                                <td><input type="radio" name="2^3" value="BK"></td>
                                                <td><textarea class="form-control" name="2^3^alesan"></textarea></td>
                                                <td><input type="checkbox" name="2^3^asesi" value="V"></td>
                                                <td><input type="checkbox" name="2^3^asesi" value="A"></td>
                                                <td><input type="checkbox" name="2^3^asesi" value="T"></td>
                                                <td><input type="checkbox" name="2^3^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.4</td>
<td>Apakah anda dapat menetapkan Alur mulai dan selesai?</td>
								 <td><input type="radio" name="2^4" value="K"></td>
                                                <td><input type="radio" name="2^4" value="BK"></td>
                                                <td><textarea class="form-control" name="2^4^alesan"></textarea></td>
                                                <td><input type="checkbox" name="2^4^asesi" value="V"></td>
                                                <td><input type="checkbox" name="2^4^asesi" value="A"></td>
                                                <td><input type="checkbox" name="2^4^asesi" value="T"></td>
                                                <td><input type="checkbox" name="2^4^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>3. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Menerapkan teknik dasar algoritma umum</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.1</td>
<td>Apakah anda dapat membuat Algoritma untuk sorting?</td>
								<td><input type="radio" name="3^1" value="K"></td>
                                                <td><input type="radio" name="3^1" value="BK"></td>
                                                <td><textarea class="form-control" name="3^1^alesan"></textarea></td>
                                                <td><input type="checkbox" name="3^1^asesi" value="V"></td>
                                                <td><input type="checkbox" name="3^1^asesi" value="A"></td>
                                                <td><input type="checkbox" name="3^1^asesi" value="T"></td>
                                                <td><input type="checkbox" name="3^1^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.2</td>
<td>Apakah anda dapat membuat Algoritma untuk searching? </td>
								 <td><input type="radio" name="3^2" value="K"></td>
                                                <td><input type="radio" name="3^2" value="BK"></td>
                                                <td><textarea class="form-control" name="3^2^alesan"></textarea></td>
                                                <td><input type="checkbox" name="3^2^asesi" value="V"></td>
                                                <td><input type="checkbox" name="3^2^asesi" value="A"></td>
                                                <td><input type="checkbox" name="3^2^asesi" value="T"></td>
                                                <td><input type="checkbox" name="3^2^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>4. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Menggunakan prosedur dan fungsi</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">4.1</td>
<td>Apakah anda dapat mengidentifikasi Konsep penggunaan kembali prosedur dan fungsi?</td>
								 <td><input type="radio" name="4^1" value="K"></td>
                                                <td><input type="radio" name="4^1" value="BK"></td>
                                                <td><textarea class="form-control" name="4^1^alesan"></textarea></td>
                                                <td><input type="checkbox" name="4^1^asesi" value="V"></td>
                                                <td><input type="checkbox" name="4^1^asesi" value="A"></td>
                                                <td><input type="checkbox" name="4^1^asesi" value="T"></td>
                                                <td><input type="checkbox" name="4^1^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">4.2</td>
<td>Apakah anda dapat menggunakan Prosedur?
								<td><input type="radio" name="4^2" value="K"></td>
                                                <td><input type="radio" name="4^2" value="BK"></td>
                                                <td><textarea class="form-control" name="4^2^alesan"></textarea></td>
                                                <td><input type="checkbox" name="4^2^asesi" value="V"></td>
                                                <td><input type="checkbox" name="4^2^asesi" value="A"></td>
                                                <td><input type="checkbox" name="4^2^asesi" value="T"></td>
                                                <td><input type="checkbox" name="4^2^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">4.3</td>
<td>Apakah anda dapat menggunakan Fungsi?</td>
								 <td><input type="radio" name="4^3" value="K"></td>
                                                <td><input type="radio" name="4^3" value="BK"></td>
                                                <td><textarea class="form-control" name="4^3^alesan"></textarea></td>
                                                <td><input type="checkbox" name="4^3^asesi" value="V"></td>
                                                <td><input type="checkbox" name="4^3^asesi" value="A"></td>
                                                <td><input type="checkbox" name="4^3^asesi" value="T"></td>
                                                <td><input type="checkbox" name="4^3^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>5. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Mengidentifikasikan kompleksitas algoritma</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">5.1</td>
                        				<td>Apakah anda dapat mengidentifikasi Kompleksitas
waktu algoritma?
										</td>
 <td><input type="radio" name="5^1" value="K"></td>
                                                <td><input type="radio" name="5^1" value="BK"></td>
                                                <td><textarea class="form-control" name="5^1^alesan"></textarea></td>
                                                <td><input type="checkbox" name="5^1^asesi" value="V"></td>
                                                <td><input type="checkbox" name="5^1^asesi" value="A"></td>
                                                <td><input type="checkbox" name="5^1^asesi" value="T"></td>
                                                <td><input type="checkbox" name="5^1^asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">5.2</td>
                        				<td>Apakah anda dapat mengidentifikasi kompleksitas
penggunaan memory algoritma?
										</td>
 <td><input type="radio" name="5^2" value="K"></td>
                                                <td><input type="radio" name="5^2" value="BK"></td>
                                                <td><textarea class="form-control" name="5^2^alesan"></textarea></td>
                                                <td><input type="checkbox" name="5^2^asesi" value="V"></td>
                                                <td><input type="checkbox" name="5^2^asesi" value="A"></td>
                                                <td><input type="checkbox" name="5^2^asesi" value="T"></td>
                                                <td><input type="checkbox" name="5^2^asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>

	<!-- 10 -->
	<div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                        		<table width="100%">
                        			<tr>
                        				<td><strong>Kode Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>J.620100.025.02</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>Judul Unit Kompetensi</strong></td>
                        				<td colspan="8"><strong>Melakukan Debugging</strong></td>
                        			</tr>
                        			<tr>
                        				<td><strong>1. Elemen Kompetensi</strong></td>
                        				<td colspan="8"><strong>Mempersiapkan kode program</strong></td>
                        			</tr>
                        			<tr style="background-color: green;color: white">
                        				<td rowspan="2" ><strong>Nomor KUK</strong></td>
                        				<td rowspan="2" ><strong>Daftar Pertanyaan </strong></td>
                        				<td colspan="2"><strong>Penilaian</strong>
                        					<td rowspan="2"><strong>Bukti-bukti Pendukung</strong></td>
                        					<td colspan="4"><strong>Diisi Assesor</strong></td>
                        					<tr style="background-color: green;color: white">
                        						<td>K</td>
                        						<td>BK</td>
                        						<td>V</td>
                        						<td>A</td>
                        						<td>T</td>
                        						<td>M</td>
                        					</tr>
                        				</td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.1</td>
                        				<td>Apakah anda dapat menyiapkan Kode program sesuai
spesifikasi?</td>
                                                 <td><input type="radio" name="1!1" value="K"></td>
                                                <td><input type="radio" name="1!1" value="BK"></td>
                                                <td><textarea class="form-control" name="1!1!alesan"></textarea></td>
                                                <td><input type="checkbox" name="1!1!asesi" value="V"></td>
                                                <td><input type="checkbox" name="1!1!asesi" value="A"></td>
                                                <td><input type="checkbox" name="1!1!asesi" value="T"></td>
                                                <td><input type="checkbox" name="1!1!asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">1.2</td>
                        				<td>Apakah anda dapat mempersiapkan Debugging tools
untuk melihat proses suatu modul?</td>
                                                 <td><input type="radio" name="1!2" value="K"></td>
                                                <td><input type="radio" name="1!2" value="BK"></td>
                                                <td><textarea class="form-control" name="1!2!alesan"></textarea></td>
                                                <td><input type="checkbox" name="1!2!asesi" value="V"></td>
                                                <td><input type="checkbox" name="1!2!asesi" value="A"></td>
                                                <td><input type="checkbox" name="1!2!asesi" value="T"></td>
                                                <td><input type="checkbox" name="1!2!asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>2. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Melakukan debugging</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.1</td>
                        				<td>Apakah anda dapat menggunakan Kode program
dikompilasi sesuai bahasa pemrograman yang?
										</td>
                                                 <td><input type="radio" name="2!1" value="K"></td>
                                                <td><input type="radio" name="2!1" value="BK"></td>
                                                <td><textarea class="form-control" name="2!1!alesan"></textarea></td>
                                                <td><input type="checkbox" name="2!1!asesi" value="V"></td>
                                                <td><input type="checkbox" name="2!1!asesi" value="A"></td>
                                                <td><input type="checkbox" name="2!1!asesi" value="T"></td>
                                                <td><input type="checkbox" name="2!1!asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.2</td>
                        				<td>Apakah anda dapat menganalisis Kriteria lulus build?
										</td>
                                                 <td><input type="radio" name="2!2" value="K"></td>
                                                <td><input type="radio" name="2!2" value="BK"></td>
                                                <td><textarea class="form-control" name="2!2!alesan"></textarea></td>
                                                <td><input type="checkbox" name="2!2!asesi" value="V"></td>
                                                <td><input type="checkbox" name="2!2!asesi" value="A"></td>
                                                <td><input type="checkbox" name="2!2!asesi" value="T"></td>
                                                <td><input type="checkbox" name="2!2!asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.3</td>
                        				<td>Apakah anda dapat menganalisis Kriteria eksekusi
aplikasi?
										</td>
                                                 <td><input type="radio" name="2!3" value="K"></td>
                                                <td><input type="radio" name="2!3" value="BK"></td>
                                                <td><textarea class="form-control" name="2!3!alesan"></textarea></td>
                                                <td><input type="checkbox" name="2!3!asesi" value="V"></td>
                                                <td><input type="checkbox" name="2!3!asesi" value="A"></td>
                                                <td><input type="checkbox" name="2!3!asesi" value="T"></td>
                                                <td><input type="checkbox" name="2!3!asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">2.4</td>
                        				<td>Apakah andadapat mencatat Kode kesalahan?
										</td>
                                                 <td><input type="radio" name="2!4" value="K"></td>
                                                <td><input type="radio" name="2!4" value="BK"></td>
                                                <td><textarea class="form-control" name="2!4!alesan"></textarea></td>
                                                <td><input type="checkbox" name="2!4!asesi" value="V"></td>
                                                <td><input type="checkbox" name="2!4!asesi" value="A"></td>
                                                <td><input type="checkbox" name="2!4!asesi" value="T"></td>
                                                <td><input type="checkbox" name="2!4!asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td colspan="2"><strong>3. Elemen Kompetensi</strong></td>
                        				<td colspan="7"><strong>Memperbaiki program</strong></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.1</td>
                        				<td>Apakah anda dapat merumuskan Perbaikan terhadap
kesalahan kompilasi maupun build?
										</td>
                                                 <td><input type="radio" name="3!1" value="K"></td>
                                                <td><input type="radio" name="3!1" value="BK"></td>
                                                <td><textarea class="form-control" name="3!1!alesan"></textarea></td>
                                                <td><input type="checkbox" name="3!1!asesi" value="V"></td>
                                                <td><input type="checkbox" name="3!1!asesi" value="A"></td>
                                                <td><input type="checkbox" name="3!1!asesi" value="T"></td>
                                                <td><input type="checkbox" name="3!1!asesi" value="M"></td>
                        			</tr>
                        			<tr>
                        				<td align="center">3.2</td>
                        				<td>Apakah anda dapat melakukan Perbaikan?
										</td>
                                                 <td><input type="radio" name="3!2" value="K"></td>
                                                <td><input type="radio" name="3!2" value="BK"></td>
                                                <td><textarea class="form-control" name="3!2!alesan"></textarea></td>
                                                <td><input type="checkbox" name="3!2!asesi" value="V"></td>
                                                <td><input type="checkbox" name="3!2!asesi" value="A"></td>
                                                <td><input type="checkbox" name="3!2!asesi" value="T"></td>
                                                <td><input type="checkbox" name="3!2!asesi" value="M"></td>
                        			</tr>
                        		</table>
			</div>		
		</div>

		 <!-- 11 -->
                    <div class="panel" style="border-radius: 5px;">
                        <div class="panel-body">
                            <div>
                                <table align="center" width="100%">
                                <tr>
                                    <td rowspan="7"><strong><div style="margin-bottom: 600px">
                                    Rekomendasi Asesor :</div></strong>                   
                                    </td>
                                    <td colspan="2"><strong>Peserta:</strong>
                                <tr>
                                    <td>Nama</td>
                                    <td class="col-md-4"><input required="" type="text" class="form-control" name="nama"></td>
                                </tr>
                                    </td>
                            <td>Tanda tangan/Tanggal</td>
                            <td><div align="center">Tanda Tangan :</div><br><br><br><br>_________________________________________________ <br><br><input required="" type="date" class="form-control border-bottom" name="tgl"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><strong>Asesor:</strong></td>
                        </tr>
                        <tr>
                            <td>Nama :</td>
                            <td><input required="" type="text" class="form-control" name="ass_nama"></td>
                        </tr>
                        <tr>
                            <td>No Reg :</td>
                            <td><input required="" type="text" class="form-control" name="no_reg"></td>
                        </tr>
                        <tr>
                            <td>Tanda tangan/Tanggal</td>
                            <td><div align="center">Tanda Tangan :</div><br><br><br><br>_________________________________________________ <br><br><input required="" type="date" class="form-control border-bottom" name="tgl_ass"></td>
                        </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div align="right"> 
                        <input type="submit" class="btn btn-primary btn-round" name="" value="SAVE">
                    </div>
</form>
</div>
@endsection
@extends('layouts.mimin-login')

@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;">
    <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft" style="color: white">Data Peserta LSP</h3>
                        <p class="animated fadeInDown" style="color: white" >
                          Home <span class="fa-angle-right fa"></span> Data Peserta
                        </p>
                    </div>
                  </div>
              </div>
              <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Data Peserta</h3></div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Jenis Kelamin</th>
                          <th>TTL</th>
                          <th>Kebangsaan</th>
                          <th>Alamat</th>
                          <th>email</th>
                          <th>Pendidikan Terakhir</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      	@foreach ($get as $key => $get)
                      <tbody>
                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$get->nama}}</td>
                          <td>{{$get->jk}}</td>
                          <td>{{$get->tempat_lahir}}-{{$get->tanggal_lahir}}</td>
                          <td>{{$get->kebangsaan}}</td>
                          <td>{{$get->alamat_rmh}}</td>
                          <td>{{$get->email}}</td>
                          <td>{{$get->pendidikan_terakhir}}</td>
                          <td>
                            <div class="btn-group">
                              <a class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-eye"></i></i></a>
                              <!-- href="{{URL('/edit/'.$get->id)}}" -->
                              <a onclick="return confirm('Jika Kamu Klik Yes Semua Data MAK,MPA,MMA DLL akan Hilang Permeanen Apa Kamu Yakin? {{$get->nama}}??')" href="{{URL('/data-peserta/delete/'.$get->id)}}" class="btn btn-danger">Delete</a>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                      @endforeach
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  



                    <!-- Modal -->
                    <br>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Form</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                              <thead>
                                <tr>
                                  <th>MPA</th>
                                  <th>MAK</th>
                                  <th>MMA</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary">Ok, Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
@endsection
@extends('layouts.mimin-login')

@section('content')

	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;"></img>
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft" style="color: white">Data Asesor,TUK Dan Skema</h3>
                        <p class="animated fadeInDown" style="color: white">
                          Home <span class="fa-angle-right fa"></span> Data T.A.S
                        </p>
                    </div>
                  </div>
              </div>

          <div class="col-md-9 top-20" align="center" style="padding-left: 300px">
              <div class="panel">
              	<div class="panel-heading"><h3>Data T.A.S</h3></div>
              		<div class="panel-body">
              			<a href="{{URL('/data')}}" class="btn btn-primary btn-round">TUK</a>
              			<span class=" icon-arrow-right"></span>

              			<a href="{{URL('/dataAsesor')}}" class="btn btn-primary btn-round">Asesor</a>
              			<span class=" icon-arrow-right"></span>

              			<a href="{{URL('/dataSkema')}}" class="btn btn-primary btn-round">Skema</a>
              		</div>
              </div>
          </div>

              <div class="col-md-12">
                <div class="col-md-12">
                	<div align="right">
          				<a href="{{URL('/create')}}" class="btn btn-primary btn-round">Create TUK</a>
                	</div>
                	<br>
                  <div class="panel">
                    <div class="panel-heading"><h3>Data TUK</h3></div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode</th>
                          <th>Jenis TUK</th>
                          <th>Nama TUK</th>
                          <th>Alamat</th>
                          <TH>Action</TH>
                        </tr>
                      </thead>
                      <tbody>
                      	@foreach($tuk as $key=>$p)
                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$p->kode}}</td>
                          <td>{{$p->jnis}}</td>
                          <td>{{$p->nama}}</td>
                          <td>{{$p->alamat}}</td>
                          <td>
                            <div class="btn-group">
                              <a onclick="return confirm('Do You Want To Edit {{$p->nama}}??')" href="{{URL('/edit/'.$p->id)}}" class="btn btn-warning">Edit</a>
                              <a onclick="return confirm('Do You Want To Delete TUK {{$p->nama}}??')" href="{{URL('/data/delete/'.$p->id)}}" class="btn btn-danger"> Delete</a>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                      @endforeach
                        </table>
                      </div>
                  </div>
                </div>
              </div>  
              </div>
@endsection
@extends('layouts.mimin-login')

@section('content')

	<img src="asset/img/la.jpg" class="img-fluid" alt="Responsive image" style="position: fixed; width: 100%; height: 100%;"></img>
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft" style="color: white">Create Asesor</h3>
                        <p class="animated fadeInDown" style="color: white">
                          Home <span class="fa-angle-right fa"></span> Data T.A.S
                        </p>
                    </div>
                  </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Create Asesor</h3></div>
                    <div class="panel-body">
                      <form action="{{URL('/dataAsesor/store')}}" method="post">
                        {{ csrf_field() }}
                      <div class="form-group">
                  <label class="text blue"><b>Nama Asesor :</b></label>
                  <input class="input border form-control" name="nama" type="text" required="required"><br>
                  <label class="text blue"><b>No Registrasi :</b></label>
                  <input class="input border form-control" name="no_regis" type="text" required="required"><br>
                  <label class="text blue"><b>No Sertifikat :</b></label>
                  <input class="input border form-control" name="no_sertifikat" type="number" required="required"><br>
                  <label class="text blue"><b>No Blanko :</b></label>
                  <input class="input border form-control" name="no_blanko" type="number" required="required">
                  <br>
                  <label class="text blue"><b>Tgl. Exp. Sertifikat :</b></label>
                  <input class="input border form-control" name="tgl_exp" type="date" required="required">
                  <br>
                  <label class="text blue"><b>Alamat :</b></label>
                  <input class="input border form-control" name="alamat" type="text" required="required">
                  <div class="col-md-7">
                  </br>
                    <button value="Save" class="btn btn-primary">Save</button>
                    <a onclick="return confirm('Do You Want To Back?')" href="{{URL('/dataAsesor')}}" class="btn btn-danger">Cancel</a>
                  </div> 
<!--                       <input type="text" name="kode">
                      <input type="text" name="jnis">
                      <input type="text" name="jnis">
                      <input type="text" name="alamat"> -->
                      </form>
                  </div>
                </div>
              </div>  
              </div>
            </div>
@endsection